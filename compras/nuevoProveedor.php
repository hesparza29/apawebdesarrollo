<!DOCTYPE html>
<html>
<head>
	<title>Nuevo Proveedor</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloCompras.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="ajax/js/proveedores.js"></script>
	<script type="text/javascript" src="ajax/js/compras.js"></script>
	<script type="text/javascript" src="ajax/eventos/guardarProveedor.js"></script>
	
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Generar Nuevo Proveedor</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="proveedores" class="btn btn-primary" value="Regresar" />
				</div>
			</div>
		</div>
    </header>
    <br /><br />
	<section>
		<div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                </div>
				<div class="col-sm-12 col-md-4">
					<table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th colspan="2">Datos del proveedor</th>
                            </tr>
                        </thead>
                        <form enctype="multipart/form-data" id="formularioProveedor" method="post">
                        <tr>
                            <td>
                                <div class="form-group col-sm-12 col-md-12">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" id="nombre" name="nombre" value="" />
                                </div>
                            </td>
                        </tr>
						<tr>
                            <td>
                                <div class="form-group col-sm-12 col-md-12">
                                    <label for="contacto">Contacto</label>
                                    <input type="text" class="form-control" id="contacto" name="contacto" value="" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group col-sm-12 col-md-12">
                                    <label for="ciudad">Ciudad</label>
                                    <input type="text" class="form-control" id="ciudad" name="ciudad" value="" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center">
                                <div class="form-group col-sm-12 col-md-12">
                                    <input type="button" class="btn btn-success" id="guardar" value="Guardar" />
                                </div>
                            </td>
                        </tr>
                        </form>
                    </table>
                </div>
                <div class="col-sm-12 col-md-4">
                </div>
			</div>
		</div>
	</section>
</body>
</html>