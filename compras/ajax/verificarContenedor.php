<?php

    require_once("../../funciones.php");
    $contenedor = $_POST["contenedor"];
    $estatus = "";
    $datos = array();

    $base = conexion_local();

    //Consulta para saber si un número de contenedor ya fue capturado
    $consultaContenedor = "SELECT Folio FROM COMPRA WHERE Contenedor=?";
    $resultadoContenedor = $base->prepare($consultaContenedor);
    $resultadoContenedor->execute(array($contenedor));

    switch ($resultadoContenedor->rowCount()){
        case 1:
            $estatus = "No disponible";
            $registroContenedor = $resultadoContenedor->fetch(PDO::FETCH_ASSOC);
            $datos["folio"] = $registroContenedor["Folio"];
            break;
        
        case 0:
            $estatus = "Correcto";
            break;
    }

    $datos["estatus"] = $estatus;
    $datos["contenedor"] = $contenedor;

    echo json_encode($datos);

?>