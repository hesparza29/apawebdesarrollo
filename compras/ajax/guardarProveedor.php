<?php
    require_once("../../funciones.php");
    $nombre = $_POST["nombre"];
    $contacto = $_POST["contacto"];
    $ciudad = $_POST["ciudad"];
    $estatus = "";
    $datos = array();

    $base = conexion_local();

    //Verificar que el nombre de proveedor no exista en la base de datos
    $consultaProveedor = "SELECT idProveedor FROM PROVEEDOR WHERE Nombre=?";
    $resultadoProveedor = $base->prepare($consultaProveedor);
    $resultadoProveedor->execute(array($nombre));
    
    if($resultadoProveedor->rowCount()==1){
        $estatus = "Proveedor repetido";
    }
    else{
        //Insertar el nuevo proveedor en la base de datos
        $consultaInsertarProveedor = "INSERT INTO PROVEEDOR VALUES(?,?,?,?)";
        $resultadoInsertarProveedor = $base->prepare($consultaInsertarProveedor);
        $resultadoInsertarProveedor->execute(array(NULL, $nombre, $contacto, $ciudad));
        if($resultadoInsertarProveedor->rowCount()==1){
            $estatus = "Proveedor registrado";
        }
        else{
            $estatus = "Proveedor no registrado";
        }
        $resultadoInsertarProveedor->closeCursor();
    }
    $resultadoProveedor->closeCursor();

    $datos["estatus"] = $estatus;
    $datos["nombre"] = $nombre;
    $datos["contacto"] = $contacto;
    $datos["ciudad"] = $ciudad;

    $base = null;
    echo json_encode($datos);
?>