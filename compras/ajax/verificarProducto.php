<?php 

    require_once("../../funciones.php");

    $clave = $_POST["clave"];
    $id = $_POST["id"];
    $datos = array();
    $base = conexion_local();

    //Consulta para verificar si la clave del producto es válida
    $consultaClave = "SELECT Descripcion, Imagen FROM PRODUCTO WHERE NumeroAPA=? LIMIT 1";
    $resultadoClave = $base->prepare($consultaClave);
    $resultadoClave->execute(array($clave));

    if($resultadoClave->rowCount()==1){
        $registroClave = $resultadoClave->fetch(PDO::FETCH_ASSOC);
        $datos["id"] = $id;
        $datos["clave"] = $clave;
        $datos["descripcion"] = $registroClave["Descripcion"];
        $datos["imagen"] = $registroClave["Imagen"];
    }
    else{
        $datos["id"] = $id;
        $datos["clave"] = $clave;
        $datos["descripcion"] = "";
    }

    $resultadoClave->closeCursor();

    $base = null;
    

    echo json_encode($datos);
?>