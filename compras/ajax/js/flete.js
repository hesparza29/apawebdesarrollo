$(document).ready(function(){
    //Verificar si el valor de inicio es 0.00 para limpiarlo en cuanto recibe el foco
    $("body").on('focusin', '.flete', function(){
        if($(this).val()==0.00){
            $(this).val("");
        }
    });

    //Verificar que los importes introducidos sean mayor a 0.00
    $("body").on('change', '.flete', function(){
        
        if($("#flete").val()<=0){
            $("#flete").val(0.00);
        }
        //Obtener el valor del flete
        let flete = $("#flete").val();

        //Obtener el contador
        let contador = $("#contador").val();

        for (let i = 1; i <= contador; i++) {
            if ($("#precioFlete-"+i+"").val()!=undefined) {
                //Obtener el porcentaje 
                let porcentaje = flete/100;
                porcentaje = porcentaje*$("#precio-"+i+"").val();
                //Obtener el precio con flete
                let precioFlete = parseFloat($("#precio-"+i+"").val())+parseFloat($("#precio-"+i+"").val()*(flete/100));
                precioFlete = Math.round(precioFlete*100)/100;
                //Calcular el valor del precio con flete
                $("#precioFlete-"+i+"").val(precioFlete);
            }
        }

        //Realizar la suma del total de la compra
        totalCompra();
        
    });

});