$(document).ready(function(){
    $("#agregarProducto").click(function(){
        //Obtener el siguiente id que se va a asignar
        let id = parseInt($("#contador").val())+1;
        //Construir el contenido que se va a agregar
        let contenido = '<tr id="fila-'+id+'">'+
                            '<td><input type="text" class="form-control form-control-sm clave total" placeholder="Número APA" id="clave-'+id+'"  value=""></td>'+
                            '<td id="imagen-'+id+'"></td>'+
                            '<td colspan="2"><input type="text" class="form-control form-control-sm" id="descripcion-'+id+'" placeholder="Descripción" value="" readonly></td>'+
                            '<td><input type="text" class="form-control form-control-sm" id="numeroProveedor-'+id+'" placeholder="Número Proveedor" value=""></td>'+
                            '<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-'+id+'" value=0 step=1></td>'+
                            '<td><input type="number" class="form-control form-control-sm importe total" id="precio-'+id+'" value=0.00 step=0.01></td>'+
                            '<td><input type="number" class="form-control form-control-sm importe total" id="precioFlete-'+id+'" value=0.00 step=0.01 readonly></td>'+
                            '<td class="text-center"><button class="btn btn-sm eliminarProducto" id="borrar-'+id+'"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto"></button></td>'+
                        '</tr>';
        //Agregar el contenido
        $("#productos").append(contenido);
        //Reasignar el contador de productos con el nuevo valor
        $("#contador").val(id);
    });
});