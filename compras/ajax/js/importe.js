$(document).ready(function(){
    //Verificar si el valor de inicio es 0.00 para limpiarlo en cuanto recibe el foco
    $("body").on('focusin', '.importe', function(){
        if($(this).val()==0.00){
            $(this).val("");
        }
    });

    //Verificar que los importes introducidos sean mayor a 0.00
    $("body").on('change', '.importe', function(){
        //Obtener el valor del flete
        let flete = $("#flete").val();
        //Obtener el identificador del producto
        let id = $(this).attr("id");
            id = id.split("-");
            id = id[1];
        //Obtener el porcentaje 
        let porcentaje = flete/100;
            porcentaje = porcentaje*$(this).val();
        //Obtener el precio con flete
        let precioFlete = parseFloat($(this).val())+parseFloat($(this).val()*(flete/100));
            precioFlete = Math.round(precioFlete*100)/100;
        
        if($(this).val()<=0){
            alert("El importe introducido debe de ser mayor a 0, verificalo por favor");
            $(this).addClass("importeIncorrecto");
            //Calcular el valor del precio con flete
            $("#precioFlete-"+id+"").val(precioFlete);
            //Realizar la suma del total de la compra
            totalCompra();
        }
        else{
            $(this).removeClass("importeIncorrecto");
            //Calcular el valor del precio con flete
            $("#precioFlete-"+id+"").val(precioFlete);
            //Realizar la suma del total de la compra
            totalCompra();
        }
    });

});