$(document).ready(function(){
    
    $("#productos").on('click', '.eliminarProducto', function(){
        //Obtener el id del producto que se va a eliminar
        let id = $(this).attr("id");
            id = id.split("-");
            id = id[1];
        //Eliminar el producto
        $("#fila-" + id + "").remove();
        //Realizar la suma del total de la compra
        totalCompra();
        //Realizar la suma del total de las piezas
        totalPiezas();
    });
});