function totalCompra() {
  //Obtener el número de productos
  let contador = $("#contador").val();
  let total = 0.0;
  //Realizar la suma solamente de aquellos precios que existan
  for (let i = 1; i <= contador; i++) {
    //Verificar que la clave del producto sea diferente de undefined y además sea diferente de vacio
    if ($("#clave-" + i + "").val() != undefined) {
        if($("#precio-" + i + "").val()!=""){
            total += parseFloat($("#precioFlete-" + i + "").val()*$("#cantidad-" + i + "").val());
        }
    }
  }
  //Redondear el resultado a 2 cifras
  total = Math.round(total * 100) / 100;
  //Darle formato al total
  total = formatNumber.new(total, "$");
  //Actualizar el valor del total
  $("#total").val(total);
}
