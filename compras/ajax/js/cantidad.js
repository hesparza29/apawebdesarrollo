$(document).ready(function(){
    //Verificar si el valor de inicio es 0 para limpiarlo en cuanto recibe el foco
    $("body").on('focusin', '.cantidad', function(){
        if($(this).val()==0){
            $(this).val("");
        }
    });

    //Verificar que las cantidades introducidas sean mayor a 0
    $("body").on('change', '.cantidad', function(){
        if($(this).val()<=0){
            alert("La cantidad introducida debe de ser mayor a 0, verificala por favor");
            $(this).addClass("importeIncorrecto");
            //Truncar valores decimales
            $(this).val(Math.trunc($(this).val()/1));
            //Realizar la suma del total de las piezas
            totalPiezas();
            //Realizar la suma del total de la compra
            totalCompra();
        }
        else{
            $(this).removeClass("importeIncorrecto");
            //Truncar valores decimales
            $(this).val(Math.trunc($(this).val()/1));
            //Realizar la suma del total de las piezas
            totalPiezas();
            //Realizar la suma del total de la compra
            totalCompra();
        }
    });

});