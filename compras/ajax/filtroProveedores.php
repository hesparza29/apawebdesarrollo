<?php
    require_once("../../funciones.php");
    $nombre = $_POST['nombre'];
    $ciudad = $_POST['ciudad'];
    $nombres = array();
    $ciudades = array();
    $datos = array();
    $contador = 0;

    $base = conexion_local();

    if($nombre!="" && $ciudad==""){
        $consulta = "SELECT * FROM PROVEEDOR WHERE Nombre=?";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($nombre));
    }
    elseif($ciudad!="" && $nombre==""){
        $consulta = "SELECT * FROM PROVEEDOR WHERE Ciudad=?";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($ciudad));
    }
    elseif($nombre!="" && $ciudad!=""){
        $consulta = "SELECT * FROM PROVEEDOR WHERE Nombre=? AND Ciudad=?";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($nombre, $ciudad));
    }

    while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)){
        $nombres[$contador] = $registro["Nombre"];
        $ciudades[$contador] = $registro["Ciudad"];
        $contador++;
    }

    $datos["nombre"] = $nombres;
    $datos["ciudad"] = $ciudades;

    echo json_encode($datos);

?>