$(document).ready(function(){
    $("#guardar").click(function(){

        if($("#nombre").val()!="" && $("#contacto").val()!="" && $("#ciudad").val()!=""){
            enviar();
        }
        else{
            alert("Captura todos los campos, por favor");
        }
    });

    function enviar() {

        var formData = new FormData(document.getElementById("formularioProveedor"));

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: false, //Especificaremos el tipo de contenido
            url: "ajax/guardarProveedor.php", //Sera el archivo que va a procesar la petición AJAX
            data: formData,
            cache: false,
            processData: false,
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 10000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando el guardado del proveedor...");
    }

    function llegada(datos) {
        console.log(datos);
        switch (datos.estatus) {
            case "Proveedor repetido":
                alert("El proveedor "+datos.nombre+" ya fue registrado anteriormente");
                location.reload();
                break;
            
            case "Proveedor registrado":
                alert("El proveedor "+datos.nombre+" se registro de manera correcta");
                location.reload();
                break;
            
            case "Proveedor no registrado":
                alert("El proveedor "+datos.nombre+" no se pudo registrar intentalo más tarde, por favor");
                location.reload();
                break;
            default:
                break;
        }
        
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});