$(document).ready(function(){
    $("#buscar").click(function(){
        if($("#folio").val()!="" || $("#nombre").val()!="" || 
                $("#fechaInicio").val()!="" || $("#fechaFin").val()){
            enviar();
        }
        else{
            alert("Captura al menos un campo, por favor");
        }
    });

    function enviar(){
      
        var parametros =
        {
          folio: $("#folio").val(),
          proveedor: $("#nombre").val(),
          fechaInicio: $("#fechaInicio").val(),
          fechaFin: $("#fechaFin").val()
        }
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/filtroCompras.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Cargando Filtro Compras...");
    }
    
    function llegada(datos){
        console.log(datos);
        var contador = Object.keys(datos.folio).length;
        var contenido = "";

        if (contador > 0) {
            for (var i = 0; i < contador; i++) {
                contenido += '<tr>' +
                                '<td>' + datos.folio[i] + '</td>' +
                                '<td>' + datos.fecha[i] + '</td>' +
                                '<td>' + formatNumber.new(datos.total[i], '$') + '</td>' +
                                '<td>' + formatNumber.new(datos.contenedor[i]) + '</td>' +
                                '<td>' + formatNumber.new(datos.totalDePiezas[i]) + '</td>' +
                                '<td>' + formatNumber.new(datos.tipoDeCambio[i], '$') + '</td>' +
                                '<td>' + formatNumber.new(datos.flete[i]) + '%</td>' +
                                '<td>' + datos.proveedor[i] + '</td>' +
                                '<td>' + datos.usuario[i] + '</td>' +
                                '<td><input type="button" class="btn btn-info btn-sm verImpresion" id="compras-'+datos.folio[i]+'" value="Ver" /></td>' +
                            '</tr>';
            }
            $("#paginador").hide();
            $("#table").empty();
            $("#table").append(contenido);

        }
        else {
            alert("No se encontraron datos en la consulta");
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});