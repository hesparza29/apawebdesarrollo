$(document).ready(function(){

    //Obteniendo el folio de la cobranza
    var url = window.location.search;
    url = url.split("?");
    url = url[1];
    url = url.split("=");
    folio = url[1];
    enviar(folio);

    function enviar(folio){
        
        var parametros =
        {
            folio: folio,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/cargarPartidas.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 6000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
      }
      function inicioEnvio(){
          console.log("Cargando Partidas...");
      }
    
      function llegada(datos){
        console.log(datos);
        $("#cobranzaDelDia").text(datos.cobranzaDelDia);
        var contador = Object.keys(datos.clave).length;
        var contenido = "";
        for (var i = 0; i < contador; i++){

            contenido += '<tr>'+
                            '<td colspan="2">'+datos.clave[i]+'</td>'+
                            '<td colspan="2">'+datos.descripcion[i]+'</td>'+
                            '<td colspan="2">'+datos.numeroProveedor[i]+'</td>'+
                            '<td colspan="2">'+formatNumber.new(datos.cantidad[i])+'</td>'+
                            '<td colspan="2">'+formatNumber.new(datos.precio[i], "$")+'</td>'+
                            '<td colspan="2">'+formatNumber.new(datos.precioFlete[i], "$")+'</td>'+
                        '</tr>';
            
        }
        $("#table").append(contenido);
        $("#folio").text(datos.folio);
        $("#fecha").text(datos.fecha);
        $("#contenedor").text(datos.contenedor);
        $("#proveedor").text(datos.proveedor);
        $("#tipoDeCambio").text(formatNumber.new(datos.tipoDeCambio, "$"));
        $("#flete").text(formatNumber.new(datos.flete)+"%");
        $("#total").text(formatNumber.new(datos.total, "$"));
        $("#totalDePiezas").text(formatNumber.new(datos.totalDePiezas));
        
      }
    
      function problemas(textError, textStatus) {
            //var error = JSON.parse(textError);
            alert("Problemas en el Servlet: " + JSON.stringify(textError));
            alert("Problemas en el servlet: " + JSON.stringify(textStatus));
      }
});