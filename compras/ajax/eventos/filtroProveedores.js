$(document).ready(function(){
    $("#buscar").click(function(){
        if($("#nombre").val()!="" || $("#ciudad").val()!=""){
            enviar();
        }
        else{
            alert("Captura al menos un campo, por favor");
        }
    });

    function enviar(){
      
        var parametros =
        {
          nombre: $("#nombre").val(),
          ciudad: $("#ciudad").val(),
        }
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/filtroProveedores.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
      }
      function inicioEnvio(){
          console.log("Cargando Filtro Proveedores...");
      }
    
      function llegada(datos){
        console.log(datos);
        var contador = Object.keys(datos.nombre).length;
        var contenido = "";

        if (contador > 0) {
            for (var i = 0; i < contador; i++) {
                contenido += '<tr>' +
                    '<td>' + datos.nombre[i] + '</td>' +
                    '<td>' + datos.ciudad[i] + '</td>' +
                  '</tr>';
            }
            $("#paginador").hide();
            $("#table").empty();
            $("#table").append(contenido);
            $("#scriptParaCargas").empty();

        }
        else {
            alert("No se encontraron datos en la consulta");
        }
        
      }
    
      function problemas(textError, textStatus) {
            //var error = JSON.parse(textError);
            alert("Problemas en el Servlet: " + JSON.stringify(textError));
            alert("Problemas en el servlet: " + JSON.stringify(textStatus));
      }
});