$(document).ready(function(){
    $("#guardarCompra").on('click', function(){
        let clave = [];
        let numeroProveedor = [];
        let cantidad = [];
        let precio = [];
        let contador = $("#contador").val();
        let estatus = true;

        for (let i = 1; i <= contador; i++) {
            //Verificar que se haya capturado una fecha de compra
            if($("#fecha").val()==""){
                alert("Captura una fecha de compra, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un proveedor
            else if($("#nombre").val()==""){
                alert("Captura un proveedor, verificalo por favor");
                estatus = false;
                break;
            }
            //Veificar que el importe introducido para el tipo de cambio sea mayor a 0
            else if(parseFloat($("#tipoDeCambio").val())<=0){
                alert("El importe del tipo de cambio debe de ser mayor a 0, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que el porcentaje introducido para el flete sea mayor o igual a 0
            else if($("#flete").val()<0){
                alert("El importe del flete debe de ser mayor o igual a 0, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que el número de contenedor introducido sea mayor a 0
            else if($("#contenedor").val()<=0){
                alert("El valor del contenedor debe de ser mayor a 0, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que la clave del producto sea diferente de undefined y además sea diferente de vacio
            else if($("#clave-"+i+"").val()!=undefined && $("#clave-"+i+"").val()!=""){
                //Verificar que el producto no se encuentre capturado más de una vez
                if(clave.includes($("#clave-"+i+"").val())==true){
                    alert("El producto "+$("#clave-"+i+"").val()+" se capturo más de una vez, verificalo por favor");
                    estatus = false;
                    break;
                }
                //Verificar que el número del proveedor se encuentre capturado
                else if($("#numeroProveedor-"+i+"").val()==""){
                    alert("El producto "+$("#clave-"+i+"").val()+" no tiene un número proveedor asignado, verificalo por favor");
                    estatus = false;
                    break;
                }
                //Verificar que la cantidad ingresada para cada producto sea mayor a 0
                else if($("#cantidad-"+i+"").val()<=0){
                    alert("La cantidad ingresada para el producto "+$("#clave-"+i+"").val()+" debe de ser mayor a 0, verificalo por favor");
                    estatus = false;
                    break;
                }
                //Verificar que el precio ingresado para cada producto sea mayor a 0
                else if(parseFloat($("#precio-"+i+"").val())<=0 || $("#precio-"+i+"").val()==""){
                    alert("El precio ingresado para el producto "+$("#clave-"+i+"").val()+" debe de ser mayor a 0, verificalo por favor");
                    estatus = false;
                    break;
                }
                //Guardar los datos validos en arreglos
                else{
                    clave.push($("#clave-"+i+"").val());
                    numeroProveedor.push($("#numeroProveedor-"+i+"").val());
                    cantidad.push($("#cantidad-"+i+"").val());
                    precio.push($("#precio-"+i+"").val());
                }
            }
        }

        //Verificar que se haya capturado por lo menos un producto y su respectivo precio
        if(clave.length==0 && numeroProveedor.length==0 && cantidad.length==0 && precio.length==0){
            alert("Captura al menos un producto, por favor");
            estatus = false;
        }

        if(estatus){
            enviar(clave, numeroProveedor, cantidad, precio);
        }
    });

    function enviar(clave, numeroProveedor, cantidad, precio) {
        var parametros = {
          fecha: $("#fecha").val(),
          proveedor: $("#nombre").val(),
          tipoDeCambio: $("#tipoDeCambio").val(),
          flete: $("#flete").val(),
          contenedor: $("#contenedor").val(),
          clave: clave,
          numeroProveedor: numeroProveedor,
          cantidad: cantidad,
          precio: precio,
        };
        $.ajax({
          async: true, //Activar la transferencia asincronica
          type: "POST", //El tipo de transaccion para los datos
          dataType: "json", //Especificaremos que datos vamos a enviar
          contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
          url: "ajax/guardarCompra.php", //Sera el archivo que va a procesar la petición AJAX
          data: parametros, //Datos que le vamos a enviar
          // data: "total="+total+"&penalizacion="+penalizacion,
          beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
          success: llegada, //Función que se ejecuta en caso de tener exito
          timeout: 4000,
          error: problemas, //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando el guardado de la compra...");
    }
    
    function llegada(datos) {
        console.log(datos);
        
        switch (datos.estatus){
            case "sin proveedor":
                alert("El proveedor "+datos.proveedor+" no existe en la base de datos, verificalo por favor");
                break;
            
            case "correcto":
                alert("Se guardo de manera correcta la compra con folio "+datos.folio);
                //Desarrollo
                var url = window.location.host+"/apawebdesarrollo/compras/impresion.php?folio="+datos.folio;
                //Produccion
                //var url = "apawebdesarrollo.com/compras/impresion.php?folio="+datos.folio;
                window.open('http://'+url+'', '_blank');
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            default:
                break;
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});