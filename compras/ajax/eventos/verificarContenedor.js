$(document).ready(function(){
    $("#contenedor").change(function(){
        if($("#contenedor").val()>0){
            enviar();
        }
    });

    function enviar(){
      
        var parametros =
        {
          contenedor: $("#contenedor").val(),
        }
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/verificarContenedor.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Verificando número de contenedor...");
    }
    
    function llegada(datos){
        console.log(datos);

        switch (datos.estatus) {
            case "No disponible":
                alert("El número de contenedor "+datos.contenedor+" ya se "+
                        "encuentra capturado en la compra con folio "+datos.folio+", verificalo por favor");
                $("#contenedor").val("");
                break;
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});