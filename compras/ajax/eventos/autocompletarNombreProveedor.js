$(document).ready(function(){
    $('#nombre').autocomplete({
        source: function(request, response){
                $.ajax({
                        url:"ajax/autocompletarNombreProveedor.php",
                        dataType:"json",
                        data:{q:request.term},
                        success: function(data){
                                response(data);
                        }
                });
        },
        minLength:1,
        select: function(event, ui){
                //alert("Selecciono: "+ui.item.label);
        }
    });
});