$(document).ready(function () {
  
  $('#productos').on('change', '.clave', function(){
    let id = $(this).attr("id");
        id = id.split("-");
        id = id[1];
    enviar($(this).val(), id);
  });

  function enviar(clave, id) {
    var parametros = {
      clave: clave,
      id: id,
    };
    $.ajax({
      async: true, //Activar la transferencia asincronica
      type: "POST", //El tipo de transaccion para los datos
      dataType: "json", //Especificaremos que datos vamos a enviar
      contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
      url: "ajax/verificarProducto.php", //Sera el archivo que va a procesar la petición AJAX
      data: parametros, //Datos que le vamos a enviar
      // data: "total="+total+"&penalizacion="+penalizacion,
      beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
      success: llegada, //Función que se ejecuta en caso de tener exito
      timeout: 4000,
      error: problemas, //Función que se ejecuta si se tiene problemas al superar el timeout
    });
    return false;
  }
  function inicioEnvio() {
    console.log("Cargando la verificación del producto...");
  }

  function llegada(datos) {
    console.log(datos);

    if (datos.descripcion!=""){
        $("#imagen-"+datos.id+"").empty();
        $("#descripcion-"+ datos.id +"").val(datos.descripcion);
        $("#imagen-"+datos.id+"").append('<img src="../imagenes/productos/'+datos.imagen+'"'+ 
                                          'class="img-fluid rounded" width="300" height="250" alt="'+datos.clave+'">');
    }
    else{
        alert("El producto con clave " + datos.clave + " no existe en la base de datos, verificalo por favor");
        $("#clave-"+ datos.id +"").val("");
        $("#descripcion-"+ datos.id +"").val("");
        $("#precio-"+ datos.id +"").val(0.00);
        $("#imagen-"+datos.id+"").empty();
        //Realizar la suma del total de la compra
        totalCompra();
    }
    
  }

  function problemas(textError, textStatus) {
    //var error = JSON.parse(textError);
    alert("Problemas en el Servlet: " + JSON.stringify(textError));
    alert("Problemas en el servlet: " + JSON.stringify(textStatus));
  }
});
