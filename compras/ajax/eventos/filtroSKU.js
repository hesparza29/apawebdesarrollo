//Importando función para crear la gráfica
import {construirGrafica} from "../../../js/graficaTimeData.js";

$(document).ready(function(){
    
    $("#buscar").click(function(){
        if($("#clave").val()!=""){
            enviar();
        }
        else{
            alert("Captura la clave del producto, por favor");
        }
    });

    $("body").keypress(function (e) {
        let code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13){
            if($("#clave").val()!=""){
                enviar();
            }
            else{
                alert("Captura la clave del producto, por favor");
            }
        }
    });

    function enviar(){
      
        let parametros =
        {
          clave: $("#clave").val(),
        }
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/filtroSKU.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Cargando Filtro Compras...");
    }
    
    function llegada(datos){
        console.log(datos);
        let contador = Object.keys(datos.folio).length;
        let contenido = "";

        if (contador > 0) {
            const puntosDeLaGraficaA = [];
            let punto = [];
            for(let i = 0; i < contador; i++){
                contenido += `
                    <tr>
                        <td>${datos.folio[i]}</td>
                        <td>${formatNumber.new(datos.precio[i], '$')}</td>
                        <td>${formatNumber.new(datos.precioFlete[i], '$')}</td>
                        <td class="importeIncorrecto">${formatNumber.new(datos.precioFletePesos[i], '$')}</td>
                        <td>${datos.tipoDeCambio[i]}</td>
                        <td>${datos.cantidad[i]}</td>
                        <td>${datos.fechaDeCompra[i]}</td>
                        <td>${datos.flete[i]}</td>
                        <td>${datos.proveedor[i]}</td>
                        <td><input type='button' class='btn btn-info btn-sm verImpresion' id='compras-${datos.folio[i]}' value='Ver' /></td>
                    </tr>
                `;
                if(i<10){
                    //Valores de la primera gráfica considerando precio con flete y tipo de cambio
                    punto.push(Date.UTC(datos.anio[i], datos.mes[i], datos.dia[i]));
                    punto.push(parseFloat(datos.precioFleteUltimos10[i]));
                    puntosDeLaGraficaA.push(punto);
                    punto = [];
                }
            }
            $("#table").empty();
            $("#table").append(contenido);
            //Creando la gráfica y agregando la imagen del producto
            const textosDeLaGrafica = {
                titulo: `${datos.clave} ${datos.descripcion}`,
                subtitulo: `El producto ${datos.clave} cuenta con una existencia de ${datos.existencia} piezas`,
                nombreDeLaGraficaA: `Costo en Dólares $`,
                tituloEjeX: `Fecha de Compra`,
                tituloEjeY: `Precio en Dólares ($)`,
                tituloInformativoA: `Producto`,
                tituloInformativoB: `Fecha`,
                tituloInformativoC: `Precio Dólares`,

            };
            construirGrafica(textosDeLaGrafica, puntosDeLaGraficaA);
            $("#imagenProducto").empty();
            let imagenProducto = `<h2>${datos.clave}</h2>
                                <p>
                                    <img src="./../imagenes/productos/${datos.imagen}" class="img-fluid rounded" 
                                    width="300" height="250" alt="${datos.clave}">
                                </p>
                                <h2>${datos.existencia} piezas en existencia</h2>`;
            $("#imagenProducto").append(imagenProducto);

        }
        else {
            alert(`No se encontraron datos del producto ${datos.clave} en la consulta`);
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});