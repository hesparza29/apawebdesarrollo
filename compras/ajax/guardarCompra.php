<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $fechaDeCompra = fechaConsulta($_POST["fecha"]);
    $proveedor = $_POST["proveedor"];
    $clave = $_POST["clave"];
    $numeroProveedor = $_POST["numeroProveedor"];
    $cantidad = $_POST["cantidad"];
    $precio = $_POST["precio"];
    $tipoDeCambio = $_POST["tipoDeCambio"];
    $flete = $_POST["flete"];
    $contenedor = $_POST["contenedor"];
    $totalDePiezas = array_sum($cantidad);
    $porcentaje = $flete/100;
    $idCompra = 0;
    $folio = "";
    $fechaDeCaptura = fecha();
    $total = importeTotalCompra($cantidad, $precio, $flete);
    $idUsuario = 0;
    $idProveedor = 0;
    $estatus = "correcto";
    $contador = count($clave);
    $datos = array();
    
    


    $base = conexion_local();
    //Consulta para verificar si el proveedor capturado existe
    $consultaProveedor = "SELECT idProveedor FROM PROVEEDOR WHERE Nombre=?";
    $resultadoProveedor = $base->prepare($consultaProveedor);
    $resultadoProveedor->execute(array($proveedor));
    $registroProveedor = $resultadoProveedor->fetch(PDO::FETCH_ASSOC);
    $idProveedor = $registroProveedor["idProveedor"];
    $resultadoProveedor->closeCursor();
    //Consulta para obtener el idProducto de cada clave
    $consultaIdProducto = "SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?";
    $resultadoIdProducto = $base->prepare($consultaIdProducto);
    //Consulta para insertar las partidas de la compra
    $consultaGuardaPartidas = "INSERT INTO COMPRA_PRODUCTO VALUES(?,?,?,?,?,?,?)";
    $resultadoGuardaPartidas = $base->prepare($consultaGuardaPartidas);

    //Si el proveedor existe se prosigue a capturar la compra
    if($idProveedor>0){
        //Obtener el folio consecutivo
        $consultaFolio = "SELECT Folio FROM COMPRA ORDER BY idCompra DESC LIMIT 1";
        $resultadoFolio = $base->prepare($consultaFolio);
        $resultadoFolio->execute(array());
        $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
        $resultadoFolio->closeCursor();
        if ($registroFolio!=""){
            $registroFolio = explode("A", $registroFolio["Folio"]);
            $folio = "CA" . (intval($registroFolio[1])+1);
        }
        else{
            $folio = "CA1";
        }
        //Obtener el idUsuario
        $consultaIdUsuario = "SELECT idUsuario FROM USUARIO WHERE Usuario=?";
        $resultadoIdUsuario = $base->prepare($consultaIdUsuario);
        $resultadoIdUsuario->execute(array($usuario));
        $registroIdUsuario = $resultadoIdUsuario->fetch(PDO::FETCH_ASSOC);
        $resultadoIdUsuario->closeCursor();
        $idUsuario = $registroIdUsuario["idUsuario"];
        //Guardar la compra
        $consultaGuardarCompra = "INSERT INTO COMPRA VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        $resultadoGuardarCompra = $base->prepare($consultaGuardarCompra);
        $resultadoGuardarCompra->execute(array(NULL, $folio, $fechaDeCompra, $fechaDeCaptura, $total, 
                                                $tipoDeCambio, $flete, $contenedor, $totalDePiezas, $idUsuario, $idProveedor));
        $resultadoGuardarCompra->closeCursor();
        $idCompra = $base->lastInsertId();

        //Si el idCompra es diferente de 0 se procede a insertar las partidas de la compra
        if($idCompra!=0){
            for ($i=0; $i < $contador; $i++) {
                //Se obtiene el idProducto
                $resultadoIdProducto->execute(array($clave[$i]));
                $registroIdProducto = $resultadoIdProducto->fetch(PDO::FETCH_ASSOC);
                //Calculando el precio con el flete
                $precioFlete = $precio[$i]+($precio[$i]*$porcentaje);
                $precioFlete = round($precioFlete*100)/100;
                //Se inserta cada partida
                $resultadoGuardaPartidas->execute(array(NULL, $idCompra, $registroIdProducto["idProducto"], 
                                                    $numeroProveedor[$i], $cantidad[$i], $precio[$i], $precioFlete));
                
            }
            $resultadoIdProducto->closeCursor();
            $resultadoGuardaPartidas->closeCursor();
        }
        else{
            $estatus = "no se inserto la compra";
        }


    }
    else{
        $estatus = "sin proveedor";
    }

    $datos["estatus"] = $estatus;
    $datos["proveedor"] = $proveedor;
    $datos["total"]  = $total;
    $datos["folio"] = $folio;

    $base = null;
    echo json_encode($datos);
?>