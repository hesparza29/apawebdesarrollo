<?php
    require_once("../../funciones.php");
    $folio = $_POST["folio"];
    $fecha = "";
    $proveedor = "";
    $tipoDeCambio = "";
    $flete = "";
    $clave = array();
    $numeroProveedor = array();
    $cantidad = array();
    $precioFlete = array();
    $descripcion = array();
    $precio = array();
    $total = "";
    $totalDePiezas = "";
    $contenedor = "";
    
    $datos = array();
    $contador = 0;

    $base = conexion_local();
    $consulta = "SELECT FechaDeCompra, PROVEEDOR.Nombre, TipoDeCambio, Flete, NumeroAPA, Descripcion, COMPRA_PRODUCTO.Precio, Total, 
                    TotalDePiezas, Contenedor, COMPRA_PRODUCTO.NumeroProveedor, COMPRA_PRODUCTO.Cantidad, COMPRA_PRODUCTO.PrecioFlete 
                    FROM COMPRA INNER JOIN PROVEEDOR ON COMPRA.idProveedor=PROVEEDOR.idProveedor
                    INNER JOIN COMPRA_PRODUCTO ON COMPRA.idCompra=COMPRA_PRODUCTO.idCompra
                    INNER JOIN PRODUCTO ON COMPRA_PRODUCTO.idProducto=PRODUCTO.idProducto WHERE COMPRA.Folio=?
                    ORDER BY idCompraProducto ASC";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array($folio));
    
    while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)){
        $fecha = fechaStandar($registro["FechaDeCompra"]);
        $proveedor = $registro["Nombre"];
        $tipoDeCambio = $registro["TipoDeCambio"];
        $flete = $registro["Flete"];
        $clave[$contador] = $registro["NumeroAPA"];
        $descripcion[$contador] = $registro["Descripcion"];
        $numeroProveedor[$contador] = $registro["NumeroProveedor"];
        $cantidad[$contador] = $registro["Cantidad"];
        $precioFlete[$contador] = $registro["PrecioFlete"];
        $precio[$contador] = $registro["Precio"];
        $total = $registro["Total"];
        $totalDePiezas = $registro["TotalDePiezas"];
        $contenedor = $registro["Contenedor"];
        $contador++;
    }

    $resultado->closeCursor();
    
    $base = null;

    $datos["folio"] = $folio;
    $datos["fecha"] = $fecha;
    $datos["proveedor"] = $proveedor;
    $datos["tipoDeCambio"] = $tipoDeCambio;
    $datos["flete"] = $flete;
    $datos["clave"] = $clave;
    $datos["descripcion"] = $descripcion;
    $datos["numeroProveedor"] = $numeroProveedor;
    $datos["cantidad"] = $cantidad;
    $datos["precioFlete"] = $precioFlete;
    $datos["precio"] = $precio;
    $datos["total"] = $total;
    $datos["totalDePiezas"] = $totalDePiezas;
    $datos["contenedor"] = $contenedor;


    echo json_encode($datos);
?>