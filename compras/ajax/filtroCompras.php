<?php
    require_once("../../funciones.php");
    $folio = $_POST['folio'];
    $proveedor = $_POST['proveedor'];
    $fechaInicio = $_POST['fechaInicio'];
    $fechaFin = $_POST['fechaFin'];
    $datos = array();
    $folios = array();
    $fechas = array();
    $totales = array();
    $contenedores = array();
    $totalesDePiezas = array();
    $tiposDeCambio = array();
    $fletes = array();
    $proveedores = array();
    $usuarios = array();
    $contador = 0;

    if($fechaInicio==""){
        $fechaInicio = '00/00/0000';
    }
    if($fechaFin==""){
        $fechaFin = fechaStandar(fecha());
    }

    $base = conexion_local();

    if($folio!="" && $proveedor!=""){
        $consulta = "SELECT Folio, FechaDeCompra, Total, Contenedor, TotalDePiezas, TipoDeCambio, Flete, 
                        PROVEEDOR.Nombre AS Proveedor, USUARIO.Nombre, USUARIO.Apellido FROM COMPRA 
                        INNER JOIN PROVEEDOR ON COMPRA.idProveedor=PROVEEDOR.idProveedor 
                        INNER JOIN USUARIO ON COMPRA.idUsuario=USUARIO.idUsuario 
                        WHERE Folio=? AND PROVEEDOR.Nombre=? AND FechaDeCompra BETWEEN ? AND ?
                        ORDER BY FechaDeCompra DESC";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($folio, $proveedor, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
    }
    elseif($folio!=""){
        $consulta = "SELECT Folio, FechaDeCompra, Total, Contenedor, TotalDePiezas, TipoDeCambio, Flete, 
                        PROVEEDOR.Nombre AS Proveedor, USUARIO.Nombre, USUARIO.Apellido FROM COMPRA 
                        INNER JOIN PROVEEDOR ON COMPRA.idProveedor=PROVEEDOR.idProveedor 
                        INNER JOIN USUARIO ON COMPRA.idUsuario=USUARIO.idUsuario 
                        WHERE Folio=? AND FechaDeCompra BETWEEN ? AND ?
                        ORDER BY FechaDeCompra DESC";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($folio, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
    }
    elseif($proveedor!=""){
        $consulta = "SELECT Folio, FechaDeCompra, Total, Contenedor, TotalDePiezas, TipoDeCambio, Flete, 
                        PROVEEDOR.Nombre AS Proveedor, USUARIO.Nombre, USUARIO.Apellido FROM COMPRA 
                        INNER JOIN PROVEEDOR ON COMPRA.idProveedor=PROVEEDOR.idProveedor 
                        INNER JOIN USUARIO ON COMPRA.idUsuario=USUARIO.idUsuario 
                        WHERE PROVEEDOR.Nombre=? AND FechaDeCompra BETWEEN ? AND ?
                        ORDER BY FechaDeCompra DESC";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($proveedor, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
    }
    else{
        $consulta = "SELECT Folio, FechaDeCompra, Total, Contenedor, TotalDePiezas, TipoDeCambio, Flete, 
                        PROVEEDOR.Nombre AS Proveedor, USUARIO.Nombre, USUARIO.Apellido FROM COMPRA 
                        INNER JOIN PROVEEDOR ON COMPRA.idProveedor=PROVEEDOR.idProveedor 
                        INNER JOIN USUARIO ON COMPRA.idUsuario=USUARIO.idUsuario 
                        WHERE FechaDeCompra BETWEEN ? AND ?
                        ORDER BY FechaDeCompra DESC";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array(fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
    }

    while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)){
        $folios[$contador] = $registro["Folio"];
        $fechas[$contador] = fechaStandar($registro["FechaDeCompra"]);
        $totales[$contador] = $registro["Total"];
        $contenedores[$contador] = $registro["Contenedor"];
        $totalesDePiezas[$contador] = $registro["TotalDePiezas"];
        $tiposDeCambio[$contador] = $registro["TipoDeCambio"];
        $fletes[$contador] = $registro["Flete"];
        $proveedores[$contador] = $registro["Proveedor"];
        $usuarios[$contador] = $registro["Nombre"] . " " . $registro["Apellido"];
        $contador++;
    }

    $resultado->closeCursor();

    $base = null;

    $datos["folio"] = $folios;
    $datos["fecha"] = $fechas;
    $datos["total"] = $totales;
    $datos["contenedor"] = $contenedores;
    $datos["totalDePiezas"] = $totalesDePiezas;
    $datos["tipoDeCambio"] = $tiposDeCambio;
    $datos["flete"] = $fletes;
    $datos["proveedor"] = $proveedores;
    $datos["usuario"]  = $usuarios;
    

    echo json_encode($datos);

?>