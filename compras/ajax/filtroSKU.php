<?php
    require_once("../../funciones.php");
    $clave = $_POST["clave"];
    $estatus = "Correcto";
    $descripcion = "";
    $existencia = "";
    $imagen = "";
    $fechaAux = "";
    $contador = 0;
    $folio = array();
    $precio = array();
    $precioFlete = array();
    $precioFletePesos = array();
    $tipoDeCambio = array();
    $cantidad = array();
    $fechaDeCompra = array();
    $flete = array();
    $usuario = array();
    $proveedor = array();
    $precioUltimos10 = array();
    $precioFleteUltmios10 = array();
    $dia = array();
    $mes = array();
    $anio = array();
    $datos = array();

    $base = conexion_local();
    //Consulta para obtener el tipo de cambio dada una fecha
    $consultaTipoDeCambio = "SELECT Importe FROM TIPO_DE_CAMBIO WHERE Fecha BETWEEN ? AND CURDATE() LIMIT 1";
    $resultadoTipoDeCambio = $base->prepare($consultaTipoDeCambio);
    //Consulta para obtener la información del sku
    $consultaInformacion = "SELECT PRODUCTO.Descripcion, PRODUCTO.Existencia, PRODUCTO.Imagen,
                            COMPRA.Folio, COMPRA_PRODUCTO.Precio, COMPRA_PRODUCTO.PrecioFlete, 
                            ROUND(COMPRA_PRODUCTO.PrecioFlete*COMPRA.TipoDeCambio, 2) AS PrecioFletePesos,
                            COMPRA.TipoDeCambio, COMPRA_PRODUCTO.Cantidad, 
                            COMPRA.FechaDeCompra, COMPRA.Flete, USUARIO.Nombre, USUARIO.Apellido, PROVEEDOR.Nombre AS Proveedor  
                            FROM COMPRA INNER JOIN COMPRA_PRODUCTO ON COMPRA.idCompra=COMPRA_PRODUCTO.idCompra 
                            INNER JOIN PRODUCTO ON COMPRA_PRODUCTO.idProducto=PRODUCTO.idProducto 
                            INNER JOIN USUARIO ON COMPRA.idUsuario=USUARIO.idUsuario 
                            INNER JOIN PROVEEDOR ON PROVEEDOR.idProveedor=COMPRA.idProveedor 
                            WHERE PRODUCTO.NumeroAPA=?  
                            ORDER BY COMPRA.FechaDeCompra DESC";
    $resultadoInformacion = $base->prepare($consultaInformacion);
    $resultadoInformacion->execute(array($clave));

    if($resultadoInformacion->rowCount()>0){
        while($registroInformacion = $resultadoInformacion->fetch(PDO::FETCH_ASSOC)){
            $descripcion = $registroInformacion["Descripcion"];
            $existencia = $registroInformacion["Existencia"];
            $imagen = $registroInformacion["Imagen"];
            $folio[$contador] = $registroInformacion["Folio"];
            $precio[$contador] = $registroInformacion["Precio"];
            $precioFlete[$contador] = $registroInformacion["PrecioFlete"];
            $precioFletePesos[$contador] = $registroInformacion["PrecioFletePesos"];
            $tipoDeCambio[$contador] = $registroInformacion["TipoDeCambio"];
            $cantidad[$contador] = $registroInformacion["Cantidad"];
            $fechaDeCompra[$contador] = fechaStandar($registroInformacion["FechaDeCompra"]);
            $flete[$contador] = $registroInformacion["Flete"];
            $usuario[$contador] = $registroInformacion["Nombre"] . " " . $registroInformacion["Apellido"];
            $proveedor[$contador] = $registroInformacion["Proveedor"];
            //Construyendo las últimas 10 compras
            if($contador<10){
                $precioUltimos10[$contador] = $registroInformacion["Precio"];
                if($tipoDeCambio[$contador]==1.00){
                    
                    $resultadoTipoDeCambio->execute(array($registroInformacion["FechaDeCompra"]));
                    switch ($resultadoTipoDeCambio->rowCount()){
                        case 1:
                            $registroTipoDeCambio = $resultadoTipoDeCambio->fetch(PDO::FETCH_ASSOC);
                            $precioFleteUltmios10[$contador] = round((
                                $registroInformacion["Precio"]/$registroTipoDeCambio["Importe"])*100)/100;
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                }
                else{
                    $precioFleteUltmios10[$contador] = $registroInformacion["Precio"];
                }
                $fechaAux = explode("-", $registroInformacion["FechaDeCompra"]);
                $anio[$contador] = $fechaAux[0];
                $mes[$contador] = ($fechaAux[1]-1);
                $dia[$contador] = $fechaAux[2];
            }
            $contador++;

        }
        $datos["descripcion"] = $descripcion;
        $datos["existencia"] = $existencia;
        $datos["imagen"] = $imagen;
        $datos["precio"] = $precio;
        $datos["precioFlete"] = $precioFlete;
        $datos["precioFletePesos"] = $precioFletePesos;
        $datos["tipoDeCambio"] = $tipoDeCambio;
        $datos["cantidad"] = $cantidad;
        $datos["fechaDeCompra"] = $fechaDeCompra;
        $datos["flete"] = $flete;
        $datos["usuario"] = $usuario;
        $datos["proveedor"] = $proveedor;
        $datos["precioUltimos10"] = $precioUltimos10;
        $datos["precioFleteUltimos10"] = $precioFleteUltmios10;
        $datos["anio"] = $anio;
        $datos["mes"] = $mes;
        $datos["dia"] = $dia;
    }
    else{
        $estatus = "Sin resultados";
    }

    $resultadoInformacion->closeCursor();

    $datos["estatus"] = $estatus;
    $datos["clave"] = $clave;
    $datos["folio"] = $folio;

    $base = null;
    
    echo json_encode($datos);
?>