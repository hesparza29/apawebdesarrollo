<!DOCTYPE html>
<html>
<head>
	<title>Análisis de Compras</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloCompras.css" rel="stylesheet" />
	<link href="../css/estiloGraficaTimeData.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/paginator.min.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/recargarPagina.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/verImpresion.js"></script>
	<script type="text/javascript" src="../js/visualizacion.js"></script>
	<script type="module" src="ajax/eventos/filtroSKU.js"></script>
	<!--Gráfica-->
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/series-label.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Análisis de Compras</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type="button" class="btn btn-success" id="visualizacion" value="Regresar" />
				</div>
				<div class="col-sm-12 col-md-4">
						<h3 class="text-left">Filtro de Búsqueda</h3>
				</div>
				<div class="col-sm-12 col-md-2">
					
				</div>
				<div class="col-sm-12 col-md-2">
					
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					
				</div>
				<div class="col-sm-12 col-md-4">
					<span class="analisisDeCompras"></span>
				</div>
				<div class="col-sm-12 col-md-4">
				
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="container">
				<div class="row form-group">
					<div class="col-sm-12 col-md-4">
						
					</div>
					<div class="col-sm-12 col-md-4">
						<input type="text" id="clave" class="clave form-control" placeholder="SKU"/>
					</div>
					<div class="col-sm-12 col-md-4">
						<input type="button" class="btn btn-primary" id="buscar" value="Buscar" />
					</div>
				</div>
				<br />
				<div class="row">
						<div class="col-sm-12 col-md-8">
							<div id="grafica">
								<figure class="highcharts-figure">
									<div id="container"></div>
								</figure>
							</div>
						</div>
						<div class="col-sm-12 col-md-4">
							<div id="imagenProducto"></div>
						</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-12 col-md-4">
						
					</div>
					<div class="col-sm-12 col-md-4">
						
					</div>
					<div class="col-sm-12 col-md-4">
						
					</div>
				</div>
				<br />		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead class="thead-dark">
									<th>Folio</th>
									<th>Precio Neto</th>
									<th>Precio Con Flete Dólares</th>
									<th>Precio Con Flete En Pesos</th>
									<th>Tipo de Cambio</th>
									<th>Cantidad</th>
									<th>Fecha de Compra</th>
									<th>Flete</th>
									<th>Proveedor</th>
									<th>Info</th>
								</thead>
								<tbody id="table">

								</tbody>
							</table>
						</div>
					</div>
				</div>			
		</div>
	</section>
</body>
</html>
