<!DOCTYPE html>
<html>

<head>
	<title>Nueva Compra</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="./../css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloCompras.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="ajax/js/proveedores.js"></script>
	<script type="text/javascript" src="ajax/js/compras.js"></script>
	<script type="text/javascript" src="ajax/js/importe.js"></script>
	<script type="text/javascript" src="ajax/js/agregarProducto.js"></script>
	<script type="text/javascript" src="ajax/js/eliminarProducto.js"></script>
	<script type="text/javascript" src="ajax/js/totalCompra.js"></script>
	<script type="text/javascript" src="ajax/js/cantidad.js"></script>
	<script type="text/javascript" src="ajax/js/totalPiezas.js"></script>
	<script type="text/javascript" src="ajax/js/flete.js"></script>
	<script type="text/javascript" src="ajax/eventos/verificarProducto.js"></script>
	<script type="text/javascript" src="ajax/eventos/autocompletarNombreProveedor.js"></script>
	<script type="text/javascript" src="ajax/eventos/guardarCompra.js"></script>
	<script type="text/javascript" src="ajax/eventos/verificarContenedor.js"></script>

</head>

<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block img-fluid" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Generar Nueva Compra</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="compras" class="btn btn-primary" value="Regresar" />
				</div>
			</div>
		</div>
	</header>
	<br />
	<section>
		<div class="container">
			<div class="row">
				<table class="table table-success table-hover table-sm table-bordered">
					<thead>
						<tr>
							<th colspan="3">Fecha de Compra</th>
							<th colspan="2"><input type="text" id="fecha" class="fecha form-control" placeholder="Fecha de compra"/></th>
							<th colspan="2">Proveedor</th>
							<th colspan="2"><input type="text" class="form-control form-control-sm" id="nombre" placeholder="Nombre del proveedor" value=""></th>
						</tr>
						<tr>
							<th colspan="3">Tipo de Cambio $</th>
							<th colspan="2"><input type="number" class="form-control form-control-sm importe" id="tipoDeCambio" value=0.00 step=0.01></th>
							<th colspan="2">Flete %</th>
							<th colspan="2"><input type="number" class="form-control form-control-sm flete" id="flete" value=0.00 step=0.01></th>
						</tr>
						<tr>
							<th colspan="5">Número de Contenedor</th>
							<th colspan="4"><input type="number" class="form-control form-control-sm cantidad" id="contenedor" value=0 step=1></th>
						</tr>
						<tr>
							<th colspan="9"></th>
						</tr>
						<tr>
							<th>Clave</th>
							<th>Imagen</th>
							<th colspan="2">Descripción</th>
							<th>Número Proveedor</th>
							<th>Cantidad</th>
							<th>Precio $</th>
							<th colspan="2">Precio con Flete $</th>
						</tr>
					</thead>
					<tbody id="productos">
						<tr id="fila-1">
							<td><input type="text" class="form-control form-control-sm clave total" id="clave-1"  placeholder="Número APA" value=""></td>
							<td id="imagen-1"></td>
							<td colspan="2"><input type="text" class="form-control form-control-sm" id="descripcion-1"  placeholder="Descripción" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="numeroProveedor-1" placeholder="Número Proveedor" value=""></td>
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-1" value=0 step=1></td>
							<td><input type="number" class="form-control form-control-sm importe total" id="precio-1" value=0.00 step=0.01></td>
							<td><input type="number" class="form-control form-control-sm" id="precioFlete-1" value=0.00 step=0.01 readonly></td>
							<td class="text-center"><button class="btn btn-sm eliminarProducto" id="borrar-1"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto"></button></td>
						</tr>
						<tr id="fila-2">
							<td><input type="text" class="form-control form-control-sm clave total" id="clave-2"  placeholder="Número APA" value=""></td>
							<td id="imagen-2"></td>
							<td colspan="2"><input type="text" class="form-control form-control-sm" id="descripcion-2" placeholder="Descripción" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="numeroProveedor-2" placeholder="Número Proveedor" value=""></td>
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-2" value=0 step=1></td>
							<td><input type="number" class="form-control form-control-sm importe total" id="precio-2" value=0.00 step=0.01></td>
							<td><input type="number" class="form-control form-control-sm" id="precioFlete-2" value=0.00 step=0.01 readonly></td>
							<td class="text-center"><button class="btn btn-sm eliminarProducto" id="borrar-2"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto"></button></td>
						</tr>
						<tr id="fila-3">
							<td><input type="text" class="form-control form-control-sm clave total" id="clave-3" placeholder="Número APA" value=""></td>
							<td id="imagen-3"></td>
							<td colspan="2"><input type="text" class="form-control form-control-sm" id="descripcion-3" placeholder="Descripción" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="numeroProveedor-3" placeholder="Número Proveedor" value=""></td>
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-3" value=0 step=1></td>
							<td><input type="number" class="form-control form-control-sm importe total" id="precio-3" value=0.00 step=0.01></td>
							<td><input type="number" class="form-control form-control-sm" id="precioFlete-3" value=0.00 step=0.01 readonly></td>
							<td class="text-center"><button class="btn btn-sm eliminarProducto" id="borrar-3"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto"></button></td>
						</tr>
						<tr id="fila-4">
							<td><input type="text" class="form-control form-control-sm clave total" id="clave-4" placeholder="Número APA" value=""></td>
							<td id="imagen-4"></td>
							<td colspan="2"><input type="text" class="form-control form-control-sm" id="descripcion-4" placeholder="Descripción" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="numeroProveedor-4" placeholder="Número Proveedor" value=""></td>
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-4" value=0 step=1></td>
							<td><input type="number" class="form-control form-control-sm importe total" id="precio-4" value=0.00 step=0.01></td>
							<td><input type="number" class="form-control form-control-sm" id="precioFlete-4" value=0.00 step=0.01 readonly></td>
							<td class="text-center"><button class="btn btn-sm eliminarProducto" id="borrar-4"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto"></button></td>
						</tr>
						<td></td>
						<tr id="fila-5">
							<td><input type="text" class="form-control form-control-sm clave total" id="clave-5" placeholder="Número APA" value=""></td>
							<td id="imagen-5"></td>
							<td colspan="2"><input type="text" class="form-control form-control-sm" id="descripcion-5" placeholder="Descripción" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="numeroProveedor-5" placeholder="Número Proveedor" value=""></td>
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-5" value=0 step=1></td>
							<td><input type="number" class="form-control form-control-sm importe total" id="precio-5" value=0.00 step=0.01></td>
							<td><input type="number" class="form-control form-control-sm" id="precioFlete-5" value=0.00 step=0.01 readonly></td>
							<td class="text-center"><button class="btn btn-sm eliminarProducto" id="borrar-5"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto"></button></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="5"></th>
							<th class="text-left">Total de Piezas</th>
							<th colspan="2" class="text-right">Total de la Compra</th>
							<th></th>
						</tr>
						<tr>
							<td colspan="2" class="text-left"><input type="button" class="btn btn-sm btn-info" id="agregarProducto" value="Agregar Producto"></td>
							<td colspan="3" class="text-center"><input type="button" class="btn btn-sm btn-success" id='guardarCompra' value="Guardar"></td>
							<td class="text-right"><input type="text" class="form-control form-control-sm" id="totalPiezas" value="0.00" readonly></td>
							<td></td>
							<td class="text-right"><input type="text" class="form-control form-control-sm" id="total" value="0.00" readonly></td>
							<input type="hidden" id="contador" value=5 />
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</section>
</body>

</html>