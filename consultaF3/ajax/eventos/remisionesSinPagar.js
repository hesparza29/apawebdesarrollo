$(document).ready(function(){
    $("#sinPagar").click(function(){
        enviar();
    });

    function enviar() {

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/remisionesSinPagar.php", //Sera el archivo que va a procesar la petición AJAX
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 8000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando remisiones sin pagar...");
    }

    function llegada(datos) {
        console.log(datos);

        switch(datos.estatus){
            case 'Correcto':
                let contador = Object.keys(datos.clave).length;
                let contenido = "";
                for (let i = 0; i < contador; i++){
                    contenido += `<tr>
                                    <td id="remision-${i}">${datos.clave[i]}</td>
                                    <td>${datos.fecha[i]}</td>
                                    <td id="importe-${i}">${formatNumber.new(datos.importe[i], "$")}</td>
                                    <td id="saldo-${i}">${formatNumber.new(datos.saldo[i], "$")}</td>
                                    <td>
                                        <input type="button" class="btn btn-warning btn-sm consultar" id="consultar-${i}" value="?"
                                        data-toggle="modal" data-target="#historial" />
                                    </td>
                                </tr>`;
                }
                $("#paginador").hide();
                $("#table").empty();
                $("#table").append(contenido);
                break;
            case 'Sin resultados':
                alert("No se encontraron datos en la consulta");
                break;
            
            default:
                break;
        }
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});