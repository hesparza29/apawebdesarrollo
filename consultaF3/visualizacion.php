<!DOCTYPE html>
<html>

<head>
	<title>Visualización Remisiones</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloConsultaF3.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/paginator.min.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/recargarPagina.js"></script>
	<script type="text/javascript" src="ajax/js/main.js"></script>
	<script type="text/javascript" src="ajax/eventos/filtroRemisiones.js"></script>
	<script type="text/javascript" src="ajax/eventos/remisionesSinPagar.js"></script>

</head>

<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Visualización Remisiones</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="home" class="btn btn-primary" style='background:url("../imagenes/home3.jpg"); width: 50px; height: 50px;' />
				</div>
				<div class="col-sm-12 col-md-8">
					<h4 style="margin: 2em 4em 0em 0em;" id="infoCliente"></h4>
					<h4 style="margin: 2em 4em 0em 0em;" id="saldoTotal"></h4>
				</div>
			</div>
		</div>
	</header>
	<br /><br />
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-4"></div>
				<div class="col-sm-12 col-md-4">
					<h3 class="text-left">Filtro de Búsqueda</h3>
				</div>
				<div class="col-sm-12 col-md-4"></div>
			</div>
			<div class="row form-group">
				<div class="col-sm-12 col-md-6">
					<input type="text" id="folio" class="folio form-control" placeholder="Folio de Remisión" />
				</div>
				<div class="col-sm-12 col-md-6">
					<input type="text" id="fecha" class="fecha form-control" placeholder="Fecha de elaboración" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-2">

				</div>
				<div class="col-sm-12 col-md-6" id="botonesBusqueda">
					<input type="button" class="btn btn-primary" id="buscar" value="Buscar" />
					<input type="button" class="btn btn-primary" id="recargar" value='Tabla Completa' />
					<input type="button" class="btn btn-primary" id="sinPagar" value='Remisiones Pendientes' />
				</div>
				<div class="col-sm-12 col-md-2">

				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-3"></div>
				<div class="col-sm-12 col-md-5">
					<ul class="pagination" id="paginador"></ul>
				</div>
				<div class="col-sm-12 col-md-4"></div>
			</div>
			<br /><br />
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped text-center">
							<thead class="thead-dark">
								<th>Folio</th>
								<th>Fecha de Elaboración</th>
								<th>Importe</th>
								<th>Saldo</th>
								<th>Info</th>
							</thead>
							<tbody id="table">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Ventanas de jquery UI para cargar historial de abonos-->
	<div id="contenidoHistorial" title="Historial de abonos" hidden>
		
	</div>
	<div id="scriptParaCargas"></div>
	<script>

	</script>
</body>

</html>