$(document).ready(function () {
  $("#buscar").click(function () {
    if ($("#numeroCliente").val() != "" || $("#nombreCliente").val() != "" || $("#fecha").val() != "" ||
      $("#folio").val() != "" || $("#documentador").val() != "") {
      enviar();
    } 
    else {
      alert("Captura al menos un campo, por favor");
    }
  });

  function enviar() {
    var parametros = {
      numeroCliente: $("#numeroCliente").val(),
      nombreCliente: $("#nombreCliente").val(),
      fecha: $("#fecha").val(),
      folio: $("#folio").val(),
      documentador:  $("#documentador").val(),
    };

    $.ajax({
      async: true, //Activar la transferencia asincronica
      type: "POST", //El tipo de transaccion para los datos
      dataType: "json", //Especificaremos que datos vamos a enviar
      contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
      url: "ajax/filtro.php", //Sera el archivo que va a procesar la petición AJAX
      data: parametros, //Datos que le vamos a enviar
      // data: "total="+total+"&penalizacion="+penalizacion,
      beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
      success: llegada, //Función que se ejecuta en caso de tener exito
      timeout: 6000,
      error: problemas, //Función que se ejecuta si se tiene problemas al superar el timeout
    });
    return false;
  }
  function inicioEnvio() {
    console.log("Cargando Filtro...");
  }

  function llegada(datos) {
    console.log(datos);
    var contador = Object.keys(datos.folio).length;
    var contenido = "";

    if (contador > 0) {
        for (var i = 0; i < contador; i++) {
            if(datos.notasae!=""){
              contenido += '<tr class="'+datos.clase[i]+'">' +
                '<td id="folio-' + i + '">' + datos.folio[i] + '</td>' +
                '<td>' + datos.nocliente[i] + '</td>' +
                '<td>' + datos.cliente[i] + '</td>' +
                '<td>' + datos.foliopedido[i] + '</td>' +
                '<td>' + datos.notasae[i] + '</td>' +
                '<td>' + formatNumber.new(datos.total[i], "$") + '</td>' +
                '<td>' + datos.fecha[i] + '</td>' +
                '<td>' + datos.estatus[i]+'</td>'+
                '<td>' + datos.usuario[i] + '</td>' +
                '<td>'+
                         '<input type="button" class="btn btn-info btn-sm verImpresion" id="pedido-'+datos.folio[i]+'" value="Ver" />'+
                '</td>'+
              '</tr>';
            }
            else{
              contenido += '<tr class="'+datos.clase[i]+'">' +
                '<td id="folio-' + i + '">' + datos.folio[i] + '</td>' +
                '<td>' + datos.nocliente[i] + '</td>' +
                '<td>' + datos.cliente[i] + '</td>' +
                '<td>' + datos.foliopedido[i] + '</td>' +
                '<td>' + datos.notasae[i] + '</td>' +
                '<td>' + formatNumber.new(datos.total[i], "$") + '</td>' +
                '<td>' + datos.fecha[i] + '</td>' +
                '<td>' + datos.estatus[i]+'</td>'+
                '<td>' + datos.usuario[i] + '</td>' +
                '<td>'+
                         '<input type="button" class="btn btn-warning btn-sm nuevaCartaFactura" id="cartaFactura-' + datos.folio[i] + '" value="!"/>' +
                         '&nbsp;'+
                         '<input type="button" class="btn btn-info btn-sm verImpresion" id="pedido-'+datos.folio[i]+'" value="Ver" />'+
                '</td>'+
              '</tr>';
            }
        }
        $("#paginador").hide();
        $("#table").empty();
        $("#table").append(contenido);
        $("#scriptParaCargas").empty();
        $("#scriptParaCargas").append('<script type="text/javascript" src="../js/verImpresion.js"></script>');
    }
    else {
        alert("No se encontraron datos en la consulta");
    }
    
  }

  function problemas(textError, textStatus) {
    //var error = JSON.parse(textError);
    alert("Problemas en el Servlet: " + JSON.stringify(textError));
    alert("Problemas en el servlet: " + JSON.stringify(textStatus));
  }
});
