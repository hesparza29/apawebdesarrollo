<?php
    require_once("../../funciones.php");
    $numeroCliente = $_POST["numeroCliente"];
    $nombreCliente = $_POST["nombreCliente"];
    $fecha = $_POST["fecha"];
    $folio = $_POST["folio"];
    $documentador = $_POST["documentador"];
    $folios = array();
    $numerosCliente = array();
    $nombresCliente = array();
    $foliosPedidos = array();
    $numerosSAE = array();
    $totales = array();
    $fechas = array();
    $estatus = array();
    $documentadores = array();
    $clases = array();
    $datos = array();
    $contador = 0;
    


    $base = conexion_local();

    if($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folio!=""&&$documentador!=""){
    // echo "Consulta AND de los 5";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND CLIENTE LIKE ? AND FECHA=? AND FOLIOINTERNO=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array($numeroCliente, '%' . $nombreCliente . '%', fechaConsulta($fecha), $folio,  $documentador));
    }
    elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folio!=""){
    // echo "Consulta AND de los 4; nocliente, cliente, fecha, folio";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND CLIENTE LIKE ? AND FECHA=? AND FOLIOINTERNO=? ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array($numeroCliente, '%' . $nombreCliente . '%', fechaConsulta($fecha), $folio));
    }
    elseif($numeroCliente!=""&&$fecha!=""&&$folio!=""&&$documentador!=""){
    // echo "Consulta AND de los 4; nocliente, cliente, fecha, folioUSUARIO";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND FECHA=? AND FOLIOINTERNO=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folio, $documentador));
    }
    elseif ($nombreCliente!=""&&$fecha!=""&&$folio!=""&&$documentador!="") {
    // echo "Consulta AND de los 4; cliente, fecha, folio, folioUSUARIO";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE CLIENTE LIKE ? AND FECHA=? AND FOLIOINTERNO=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array('%' . $nombreCliente . '%', fechaConsulta($fecha), $folio,  $documentador));
    }
    elseif ($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$documentador!="") {
        // echo "Consulta AND de los 4; nocliente, cliente, fecha, folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND CLIENTE LIKE ? AND FECHA=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($numeroCliente, '%' . $nombreCliente . '%', fechaConsulta($fecha), $documentador));
    }
    elseif ($numeroCliente!=""&&$nombreCliente!=""&&$fecha!="") {
        // echo "Consulta AND de los 3; nocliente, cliente, fecha";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND CLIENTE LIKE ? AND FECHA=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($numeroCliente, '%' . $nombreCliente . '%', fechaConsulta($fecha)));
    }
    elseif ($numeroCliente!=""&&$fecha!=""&&$folio!="") {
        // echo "Consulta AND de los 3; nocliente, fecha, folio";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND FECHA=? AND FOLIOINTERNO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folio));
    }
    elseif ($numeroCliente!=""&&$folio!=""&&$documentador!="") {
        // echo "Consulta AND de los 3; nocliente, folio, folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND FOLIOINTERNO=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($numeroCliente, $folio, $documentador));
    }
    elseif ($nombreCliente!=""&&$fecha!=""&&$folio!="") {
        // echo "Consulta AND de los 3; cliente, fecha, folio";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND FECHA=? AND FOLIOINTERNO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folio));
    }
    elseif ($nombreCliente!=""&&$folio!=""&&$documentador!="") {
        // echo "Consulta AND de los 3; cliente, folio, folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE CLIENTE LIKE ? AND FOLIOINTERNO=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array('%' . $nombreCliente . '%', $folio, $documentador));
    }
    elseif ($fecha!=""&&$folio!=""&&$documentador!="") {
        // echo "Consulta AND de los 3; fecha, folio, folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE FECHA=? AND FOLIOINTERNO=? AND USUARIO=?ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array(fechaConsulta($fecha), $folio, $documentador));
    }
    elseif ($folio!=""&&$numeroCliente!=""&&$nombreCliente!="") {
        // echo "Consulta AND de los 3; folio, nocliente, cliente";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE FOLIOINTERNO=? AND NOCLIENTE=? AND CLIENTE=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($folio, $numeroCliente, '%' . $nombreCliente . '%'));
    }
    elseif ($numeroCliente!=""&&$nombreCliente!=""&&$documentador!="") {
        // echo "Consulta AND de los 3; nocliente, cliente, folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND CLIENTE=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($numeroCliente, '%' . $nombreCliente . '%', $documentador));
    }
    elseif ($nombreCliente!=""&&$fecha!=""&&$documentador!="") {
        // echo "Consulta AND de los 3; cliente, fecha, folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE CLIENTE=? AND FECHA=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($folio, $numeroCliente, $documentador));
    }
    elseif ($numeroCliente!=""&&$nombreCliente!="") {
        // echo "Consulta AND de los 2; nocliente y cliente";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE CLIENTE LIKE ? AND NOCLIENTE=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array('%' . $nombreCliente . '%', $numeroCliente));
    }
    elseif ($numeroCliente!=""&&$fecha!="") {
        // echo "Consulta AND de los 2; nocliente y fecha";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND FECHA=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($numeroCliente, $fecha));
    }
    elseif ($numeroCliente!=""&&$folio!="") {
        // echo "Consulta AND de los 2; nocliente y folio";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE FOLIOINTERNO=? AND NOCLIENTE=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($folio, $numeroCliente));
    }
    elseif ($numeroCliente!=""&&$documentador!="") {
        // echo "Consulta AND de los 2; nocliente y folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($numeroCliente, $documentador));
    }
    elseif ($nombreCliente!=""&&$fecha!="") {
        // echo "Consulta AND de los 2; cliente y fecha";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE CLIENTE LIKE ? AND FECHA=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array('%' . $nombreCliente . '%', fechaConsulta($fecha)));
    }
    elseif ($nombreCliente!=""&&$folio!="") {
        // echo "Consulta AND de los 2; cliente y folio";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE CLIENTE LIKE ? AND FOLIOINTERNO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array('%' . $nombreCliente . '%', $folio));
    }
    elseif ($nombreCliente!=""&&$documentador!="") {
        // echo "Consulta AND de los 2; cliente y folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE CLIENTE LIKE ? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array('%' . $nombreCliente . '%', $documentador));
    }
    elseif ($fecha!=""&&$folio!="") {
        // echo "Consulta AND de los 2; fecha y folio";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE FECHA=? AND FOLIOINTERNO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array(fechaConsulta($fecha), $folio));
    }
    elseif ($fecha!=""&&$documentador!="") {
        // echo "Consulta AND de los 2; fecha y folioUSUARIO";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE FECHA=? AND USUARIO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array(fechaConsulta($fecha), $documentador));
    }
    elseif ($documentador!=""&&$folio!="") {
        // echo "Consulta AND de los 2; folioUSUARIO y folio";
        $consulta = "SELECT * FROM CARTAS_VIS WHERE USUARIO=? AND FOLIOINTERNO=? ORDER BY REGISTRO DESC LIMIT 20";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($documentador, $folio));
    }
    elseif ($numeroCliente!="") {
    // echo "Consulta individual nocliente";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE NOCLIENTE= ?  ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array($numeroCliente));
    }
    elseif ($nombreCliente!="") {
    // echo "Consulta individual cliente";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE CLIENTE LIKE ?  ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array('%' . $nombreCliente . '%'));
    }
    elseif ($fecha!="") {
    // echo "Consulta individual fecha";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE FECHA= ?  ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array(fechaConsulta($fecha)));
    }
    elseif ($folio!="") {
    // echo "Consulta individual folio";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE FOLIOINTERNO= ?  ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array($folio));
    }
    elseif ($documentador!="") {
    // echo "Consulta individual folioUSUARIO";
    $consulta = "SELECT * FROM CARTAS_VIS WHERE USUARIO= ?  ORDER BY REGISTRO DESC LIMIT 20";
    $resultado = $base->prepare($consulta);
    $resultado->execute(array($documentador));
    }

    while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)) {
        
        $folios[$contador] = $registro["FOLIOINTERNO"];
        $numerosCliente[$contador] = $registro["NOCLIENTE"];
        $nombresCliente[$contador] = $registro["CLIENTE"];
        $foliosPedidos[$contador] = $registro["FOLIOPEDIDO"];
        $numerosSAE[$contador] = $registro["NOTASAE"];
        $totales[$contador] = $registro["TOTAL"];
        $fechas[$contador] = fechaStandar($registro["FECHA"]);
        $estatus[$contador] = $registro["STATUS"];
        $documentadores[$contador] = $registro["USUARIO"];

        switch ($numerosSAE[$contador]) {
            case '':
                $clases[$contador] = "noAsociado";
                break;
            case 'CANCELADA':
                $clases[$contador] = "cancelado";
                break;	
            default:
                $clases[$contador] = "asociado";
                break;
        }
    
        $contador++;
    }

    $resultado->closeCursor();

    $datos["folio"] = $folios;
    $datos["nocliente"] = $numerosCliente;
    $datos["cliente"] = $nombresCliente;
    $datos["foliopedido"] = $foliosPedidos;
    $datos["notasae"] = $numerosSAE;
    $datos["total"] = $totales;
    $datos["fecha"] = $fechas;
    $datos["estatus"] = $estatus;
    $datos["usuario"] = $documentadores;
    $datos["clase"] = $clases;
    $base = null;

    echo json_encode($datos);

?>