// url para llamar la peticion por ajax
var url_listar_usuario = "cartasFactura.php";
var i = 0;

$( document ).ready(function() {
   // se genera el paginador
   paginador = $(".pagination");
	// cantidad de items por pagina
	var items = 10, numeros =4;
	// inicia el paginador
	init_paginator(paginador,items,numeros);
	// se envia la peticion ajax que se realizara como callback
	set_callback(get_data_callback);
	cargaPagina(0);
});

// peticion ajax enviada como callback
function get_data_callback(){
	$.ajax({
		data:{
		limit: itemsPorPagina,
		offset: desde,
		},
		type:"POST",
		url:url_listar_usuario
	}).done(function(data,textStatus,jqXHR){
		// obtiene la clave lista del json data
		var lista = data.lista;
		$("#table").html("");

		// si es necesario actualiza la cantidad de paginas del paginador
		if(pagina==0){
			creaPaginador(data.cantidad);
		}
		// genera el cuerpo de la tabla
		$.each(lista, function(ind, elem){
			if(elem.notasae!=""){
				$('<tr class="'+elem.clase+'">'+
				'<td id="folio-'+i+'">'+elem.folio+'</td>'+
				'<td>'+elem.nocliente+'</td>'+
				'<td>'+elem.cliente+'</td>'+
				'<td>'+elem.foliopedido+'</td>'+
				'<td>'+elem.notasae+'</td>'+
				'<td>'+formatNumber.new(elem.total, "$")+'</td>'+
				'<td>'+elem.fecha+'</td>'+
				'<td>'+elem.estatus+'</td>'+
				'<td>'+elem.usuario+'</td>'+
				'<td>'+
					'<input type="button" class="btn btn-info btn-sm verImpresion" id="cartaFactura-'+elem.folio+'" value="Ver" />'+
				'</td>'+
				'</tr>').appendTo($("#table"));

			}
			else{
				$('<tr class="'+elem.clase+'">'+
				'<td id="folio-'+i+'">'+elem.folio+'</td>'+
				'<td>'+elem.nocliente+'</td>'+
				'<td>'+elem.cliente+'</td>'+
				'<td>'+elem.foliopedido+'</td>'+
				'<td>'+elem.notasae+'</td>'+
				'<td>'+formatNumber.new(elem.total, "$")+'</td>'+
				'<td>'+elem.fecha+'</td>'+
				'<td>'+elem.estatus+'</td>'+
				'<td>'+elem.usuario+'</td>'+
				'<td>'+
					'<input type="button" class="btn btn-warning btn-sm capturaSae" id="cartaFactura-' + elem.folio + '" value="!"/>' +
					'&nbsp;'+
					'<input type="button" class="btn btn-info btn-sm verImpresion" id="cartaFactura-'+elem.folio+'" value="Ver" />'+
				'</td>'+
				'</tr>').appendTo($("#table"));
			}
		
			i++;

    	});
    //Agregando el srcipt para poder hacer el redireccionamiento hacia la vista de la cobranza con las facturas pagadas en efectivo
	$("#scriptParaCargas").append('<script type="text/javascript" src="../js/verImpresion.js"></script>'+
									'<script type="text/javascript" src="ajax/js/capturaSae.js"></script>');
	}).fail(function(jqXHR,textStatus,textError){
		alert("Error al realizar la peticion dame".textError);
    console.log("Hubo un error: "+textError);
	});
}
