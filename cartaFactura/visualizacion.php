<!DOCTYPE html>
<html>

<head>
	<title>Visualización Cartas Factura</title>
	<link rel="shortcut icon" href="imagenes/favicon.ico" type="image/x-icon" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloCartasFactura.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/paginator.min.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/recargarPagina.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="ajax/js/main.js"></script>
	<script type="text/javascript" src="ajax/eventos/filtro.js"></script>
</head>

<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Visualización de Cartas Factura</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="home" class="btn btn-primary" style='background:url("../imagenes/home3.jpg"); width: 50px; height: 50px;' />
				</div>
				<div class="col-sm-12 col-md-8">
					<h3 class="text-left">Filtro de Búsqueda</h3>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="container">
			<div class="row form-group">
				<div class="col-sm-12 col-md-4">
					<input type="number" id="numeroCliente" class="numeroCliente form-control" placeholder="Número Cliente" />
				</div>
				<div class="col-sm-12 col-md-4">
					<input type="text" id="nombreCliente" class="nombreCliente form-control" placeholder="Nombre Cliente" />
				</div>
				<div class="col-sm-12 col-md-4">
					<input type="text" id="fecha" class="fecha form-control" placeholder="Fecha" />
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-12 col-md-6">
					<input type="text" id="folio" class="folio form-control" placeholder="Folio Interno" />
				</div>
				<div class="col-sm-12 col-md-6">
					<input type="text" id="documentador" class="documentador form-control" placeholder="Documentador" />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">

				</div>
				<div class="col-sm-12 col-md-4" id="botonesBusqueda">
					<input type="button" class="btn btn-primary" id="buscar" value="Buscar" />
					<input type="button" class="btn btn-primary" id="recargar" value='Tabla Completa' />
				</div>
				<div class="col-sm-12 col-md-4">

				</div>
			</div>
			<br />
			<div class="row">
					<div class="col-sm-12 col-md-12 text-center">
						<ul class="pagination" id="paginador"></ul>
					</div>
				</div>		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead class="thead-dark">
									<th>Folio</th>
									<th>Número Cliente</th>
									<th>Nombre Cliente</th>
									<th>Pedido</th>
									<th>NO SAE</th>
									<th>Total</th>
									<th>Fecha</th>
									<th>Estatus</th>
									<th>Documentador</th>
									<th colspan="2">Info</th>
								</thead>
								<tbody id="table">

								</tbody>
							</table>
						</div>
					</div>
				</div>			
		</div>
		<div id="scriptParaCargas"></div>
		<script type="text/javascript">
			$(document).ready(function() {

				$('#tag').autocomplete({
					source: function(request, response) {
						$.ajax({
							url: "colores.php",
							dataType: "json",
							data: {
								q: request.term
							},
							success: function(data) {
								response(data);
							}
						});
					},
					minLength: 3,
					select: function(event, ui) {
						//alert("Selecciono: "+ui.item.label);
					}
				});
			});
		</script>
</body>

</html>
