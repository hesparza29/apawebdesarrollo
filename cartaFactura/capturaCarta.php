<?php
    session_start();
    $usuario = $_SESSION['user'];
    echo $usuario . "<br />";

    require_once("../funciones.php");

    $cantidad = array();
    $clave = array();
    $lista = array();
    $costo = array();
    $importe = array();
    $descripcion = array();
    $subtotal1 = 0; //Este es la suma de todos los importes
    $subtotal2 = 0; //Resta del subtotal1 y el descuento
    $ivaCarta = 0; //IVA aplicado al subtotal2
    $total = 0; //Suma del subtotal2 y el iva
    $letra = ""; //La representación en formato letra del total
    $nombre = ""; //Nombre del cliente
    $descuentodeCarta = 0;
    $fecha = fecha();
    $folioPedido = $_POST['folioPedido'];
    $descuentodeCliente = $_POST['descuentoConsulta'];
    $cliente = $_POST['cliente']; //Clave del cliente
    $cont = $_POST['contador']; //Contador para saber cuantas partidas fueron capturadas
    $tipo = $_POST['tipo'];
    $flete = $_POST['flete'];
    $aux = 1;

    //echo "El tipo de devolución es: " . $tipo . " el descuento es de " . $descuentodeCliente . " el pedido es " . $folioPedido . "<br />";
    for ($i = 1; $i <= 24; $i++) {
      if (isset($_POST['cantidad' . $i])) {
        $cantidad[$aux] = $_POST['cantidad' . $i];
        $clave[$aux] = $_POST['clave' . $i];
        $lista[$aux] = $_POST['lista' . $i];
        $costo[$aux] = $_POST['cost' . $i];
        $descripcion[$aux] = $_POST['description' . $i];
        $importe[$aux] = imp($cantidad[$aux], $costo[$aux]);
        // echo "Cantidad: " . $cantidad[$aux] . " Clave: " . $clave[$aux] . " Lista: " . $lista[$aux] . 
        //         ' Descripcion: ' . $descripcion[$aux] . " Costo: " . $costo[$aux] . " Importe: " . $importe[$aux] . "<br />";
        $aux++;
      }
    }

    //echo "Partidas " . count($cantidad) . "<br />";

    // $importe[$i] = imp($cantidad[$i],$costo[$i]);

    // echo "Flete: <h1>" . $flete . "</h1>";

    // echo 'Hola Amigos!!!<br />';
    //
    // for ($i=1; $i <=count($cantidad); $i++) {
    //   echo 'Cantidad: ' . $cantidad[$i] . ' Clave: ' . $clave[$i] . ' Costo: ' . $costo[$i] . ' Descripcion: ' . $descripcion[$i] . ' Importe: ' . $importe[$i] . '<br />';
    // }
    $subtotal1 = subtotal($importe);
    echo $subtotal1 . " " . $descuentodeCliente . "<br />";
    $descuentodeCarta = round((($descuentodeCliente / 100) * $subtotal1) * 100) / 100;
    echo $descuentodeCarta . "<br />";
    $subtotal2 = $subtotal1 - $descuentodeCarta;
    if ($flete > 0) {
      $cantidad[] = 1;
      $clave[] = "FLETE";
      $costo[] = $flete;
      $lista[] = "PRODUCTOS1";
      $subtotal2 = $subtotal2 + $flete;
    }
    if ($tipo == "Carta Factura") {
      $ivaCarta = iva($subtotal2);
      $total = $subtotal2 + $ivaCarta;
    } else {
      $ivaCarta = 0;
      $total = $subtotal2 + $ivaCarta;
    }

    echo $total . "<br />";

    
    
    try {
      $base = conexion_local();
      $consulta = "SELECT NOMBRE FROM CLIENTE WHERE idCliente=?";
      $resultado = $base->prepare($consulta);
      $resultado->execute(array($cliente));
      $registro = $resultado->fetch(PDO::FETCH_ASSOC);
      $nombre = $registro["NOMBRE"];
      $resultado->closeCursor();

      $verificaFolio = folioCartas($tipo);

      echo $nombre . " " . $verificaFolio . "<br />";

      //Obteniendo al usuario que captura el pedido
        $consultaUsuario = "SELECT Nombre, Apellido FROM USUARIO WHERE Usuario=?";
        $resultadoUsuario = $base->prepare($consultaUsuario);
        $resultadoUsuario->execute(array($usuario));
        $registroUsuario = $resultadoUsuario->fetch(PDO::FETCH_ASSOC);
        $resultadoUsuario->closeCursor();
        $usuario = $registroUsuario["Nombre"] . " " . $registroUsuario["Apellido"];

        echo $usuario . "<br />";

      for ($i = 1; $i <= count($cantidad); $i++) {
        $consulta = "INSERT INTO CARTAS(TIPO, FECHA, FOLIOINTERNO, NOCLIENTE, NOMBRE, SKU, UNIDADESxSKU, MONTO,
                                  USUARIO, STATUS, DESCUENTO)
                VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($tipo, $fecha, $verificaFolio, $cliente, $nombre, $clave[$i], $cantidad[$i], $costo[$i], $usuario, "ACTIVA", $descuentodeCliente));
      }
      $resultado->closeCursor();
      $consulta = "INSERT INTO CARTAS_VIS(FOLIOINTERNO, CLIENTE, TOTAL, FECHA, STATUS, NOCLIENTE, FOLIOPEDIDO, USUARIO)
                          VALUES(?,?,?,?,?,?,?,?)";
      $resultado = $base->prepare($consulta);
      $resultado->execute(array($verificaFolio, $nombre, $total, $fecha, "ACTIVA", $cliente, $folioPedido, $usuario));
      $resultado->closeCursor();

      $consulta = "UPDATE PEDIDOS SET CARTAFACTURA=?, STATUS=? WHERE FOLIOINTERNO=?";
      $resultado = $base->prepare($consulta);
      $resultado->execute(array($verificaFolio, "ASOCIADO", $folioPedido));
      $resultado->closeCursor();

      $consulta = "UPDATE PEDIDOS_VIS SET CARTAFACTURA=?, STATUS=? WHERE FOLIOINTERNO=?";
      $resultado = $base->prepare($consulta);
      $resultado->execute(array($verificaFolio, "ASOCIADO", $folioPedido));
      $resultado->closeCursor();

      header("location:impresion.php?folio=" . $verificaFolio);
    } catch (Exception $e) {
      $mensaje = $e->GetMessage();
      $linea = $e->getline();
      echo "<h1>Error: " . $mensaje . "</h1><br />";
      echo "<h1>Linea del Error: " . $linea . "</h1><br />";

      // die($e->GetMessage());
      // die($e->getline());
      // die("<h1>ERROR: " . $e->GetMessage());
      // echo "<br /><h3>" . $e->getline() . "</h3>";
    } finally {
      $base = null;
    }
