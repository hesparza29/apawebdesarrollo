<?php
require_once("../funciones.php");
session_start();
$usuario = $_SESSION["user"];
$modulo = "Recepción Dinero";
$permiso = "";
$fechaConHora = "";
// obtiene los valores para realizar la paginacion
$limit = isset($_POST["limit"]) && intval($_POST["limit"]) > 0 ? intval($_POST["limit"])	: 10;
$offset = isset($_POST["offset"]) && intval($_POST["offset"])>=0	? intval($_POST["offset"])	: 0;

$base = conexion_local();
// realiza la conexion
//$con = new mysqli("50.62.209.84","hesparza","b29194303","aplicacion");
$con = new mysqli("localhost","root","","aplicacion");
$con->set_charset("utf8");
//$base = new PDO('mysql:host=localhost; dbname=aplicacion', 'root', '');
//$base = new PDO("mysql:host=50.62.209.117;dbname=aplicacion","hesparza","b29194303");
//$base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
//$base->exec("SET CHARACTER SET utf8");

// array para devolver la informacion
$json = array();
$data = array();
//consulta que deseamos realizar a la db
//$query = $con->prepare("select id_usuario,nombres,apellidos from  usuarios limit ? offset ?");

//Consulta para obtener el usuario que aplico el dinero a las remisiones
$consultaUsuarioQueAplico = "SELECT USUARIO.Nombre, USUARIO.Apellido FROM 
								RECEPCION_DINERO INNER JOIN SALDO_RECEPCION_DINERO 
								ON RECEPCION_DINERO.idRecepcionDinero=SALDO_RECEPCION_DINERO.idRecepcionDinero 
								INNER JOIN SALDO ON SALDO.idSaldo=SALDO_RECEPCION_DINERO.idSaldo 
								INNER JOIN USUARIO ON USUARIO.idUsuario=SALDO.idUsuario 
								WHERE RECEPCION_DINERO.Folio=?
								ORDER BY SALDO.idSaldo DESC LIMIT 1";
$resultadoUsuarioQueAplico = $base->prepare($consultaUsuarioQueAplico);
//Consula para obtener el importe total por aplicar
$consultaImporteDisponible = "SELECT (SELECT Total FROM RECEPCION_DINERO WHERE Folio=?)-
								(SELECT IFNULL(SUM(Abono), 0) FROM RECEPCION_DINERO 
								INNER JOIN SALDO_RECEPCION_DINERO ON 
								RECEPCION_DINERO.idRecepcionDinero=SALDO_RECEPCION_DINERO.idRecepcionDinero 
								INNER JOIN SALDO ON SALDO_RECEPCION_DINERO.idSaldo=SALDO.idSaldo 
								WHERE RECEPCION_DINERO.Folio=?) AS Disponible";
$resultadoImporteDisponible = $base->prepare($consultaImporteDisponible);
//Obtener las notas que puede ver el usuario
$consultaPermiso = "SELECT Identificador FROM USUARIO 
					INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
					INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
					WHERE Usuario=? AND MODULO.Nombre=?";
$resultadoPermiso = $base->prepare($consultaPermiso);
$resultadoPermiso->execute(array($usuario, $modulo));
$registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
$resultadoPermiso->closeCursor();
$permiso = $registroPermiso["Identificador"];

//El limite empieza con 10 y el Offset con 0
switch ($permiso){
	case 'administrador':
		$query = $con->prepare("SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
								CLIENTE.idCliente, CLIENTE.Nombre, 
								USUARIO.Nombre, USUARIO.Apellido 
								FROM RECEPCION_DINERO INNER JOIN USUARIO
								ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
								INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
								ORDER BY idRecepcionDinero DESC LIMIT ? OFFSET ?");
		$query->bind_param("ii",$limit,$offset);
		$query->execute();
		break;
	
	default:
		$query = $con->prepare("SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
								CLIENTE.idCliente, CLIENTE.Nombre, 
								USUARIO.Nombre, USUARIO.Apellido 
								FROM RECEPCION_DINERO INNER JOIN USUARIO
								ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
								INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
								WHERE Lugar=? ORDER BY idRecepcionDinero DESC LIMIT ? OFFSET ?");
		$query->bind_param("sii",$permiso,$limit,$offset);
		$query->execute();
		break;
}



// vincular variables a la sentencia preparada                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
$query->bind_result($folio, $fecha, $total, $estatus, $idCliente, $nombreCliente, $nombre, $apellido);

// obtener valores
while ($query->fetch()) {
	$data_json = array();

	$data_json["folio"] = $folio;
	$fechaConHora = explode(" ", $fecha);
	$data_json["fecha"] =fechaStandar($fechaConHora[0]);
	$data_json["hora"] = $fechaConHora[1];
	$data_json["total"] = $total;
	$data_json["estatus"] = $estatus;
	$data_json["cliente"] = $idCliente . " " . $nombreCliente;
	$data_json["estatus"] = $estatus;
	switch ($estatus) {
		case 'Revisando':
			$data_json["clase"] = "revisando";
			break;
		case 'Cancelada':
			$data_json["clase"] = "cancelada";
			break;
		case 'Aplicado':
			$data_json["clase"] = "correcta";
			break;
		
	}
	$data_json["usuario"] = $nombre . " " . $apellido;
	//Obteniendo el importe disponible por aplicar
	$resultadoImporteDisponible->execute(array($folio, $folio));
	$registroImporteDisponible = $resultadoImporteDisponible->fetch(PDO::FETCH_ASSOC);
	$data_json["importeDisponible"] = $registroImporteDisponible["Disponible"];
	//Obteniendo el usuario que aplico el dinero a las remisiones
	$resultadoUsuarioQueAplico->execute(array($folio));
	$registroUsuarioQueAplico = $resultadoUsuarioQueAplico->fetch(PDO::FETCH_ASSOC);
	$data_json["usuarioQueAplico"] = $registroUsuarioQueAplico["Nombre"] . " " . $registroUsuarioQueAplico["Apellido"];
	$data[]=$data_json;
}

$base = null;

switch ($permiso) {
	case 'administrador':
		// obtiene la cantidad de registros
		$cantidad_consulta = $con->query("select count(*) as total from RECEPCION_DINERO");
		$row = $cantidad_consulta->fetch_assoc();
		$cantidad['cantidad']=$row['total'];
		break;
	
	default:
		// obtiene la cantidad de registros
		$cantidad_consulta = $con->query("select count(*) as total FROM RECEPCION_DINERO WHERE Lugar='$permiso'");
		$row = $cantidad_consulta->fetch_assoc();
		$cantidad['cantidad']=$row['total'];
		break;
}


$json["lista"] = array_values($data);
$json["cantidad"] = array_values($cantidad);

// envia la respuesta en formato json
header("Content-type:application/json; charset = utf-8");
echo json_encode($json);
exit();
?>
