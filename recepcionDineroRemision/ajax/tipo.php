<?php
    require_once("../../funciones.php");

    $lugar = $_POST["lugar"];
    $folio = "";
    $datos = array();

    $base = conexion_local();

    //Consulta para saber el último folio capturado del tipo de nota que se quiere capturar
    $consultaNota = "SELECT Folio FROM RECEPCION_DINERO WHERE Lugar=? ORDER BY idRecepcionDinero DESC LIMIT 1";
    $resultadoNota = $base->prepare($consultaNota);
    $resultadoNota->execute(array($lugar));
    //Construir el folio consecutivo
    switch ($lugar){
        case 'azcapotzalco':
            if($resultadoNota->rowCount()==0){
                $folio = "REPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "REPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac':
            if($resultadoNota->rowCount()==0){
                $folio = "RETC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "RETC" . (intval($registroFolio[1])+1);
            }
            break;
    }
    
    $datos["folio"] = $folio;

    $base = null;
    echo json_encode($datos);
?>