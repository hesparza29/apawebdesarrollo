<?php
    session_start();
    $usuario = $_SESSION["user"];
    require_once("../../funciones.php");

    $lugar = $_POST["lugar"];
    $cliente = $_POST["cliente"];
    $importe = $_POST["importe"];
    $observacion = $_POST["observacion"];
    $fecha = fechaConHora();
    $estatus = "Correcto";
    $folio = "";
    $estatusRecepcion = "Revisando";
    $datos = array();

    $base = conexion_local();
    //Consulta para saber el último folio capturado del lugar de la recepción de dinero
    $consultaRecepcion = "SELECT Folio FROM RECEPCION_DINERO WHERE Lugar=? ORDER BY idRecepcionDinero DESC LIMIT 1";
    $resultadoRecepcion = $base->prepare($consultaRecepcion);
    $resultadoRecepcion->execute(array($lugar));
    //Construir el folio consecutivo
    switch ($lugar){
        case 'azcapotzalco':
            if($resultadoRecepcion->rowCount()==0){
                $folio = "REPB1";
            }
            else{
                $registroRecepcion = $resultadoRecepcion->fetch(PDO::FETCH_ASSOC);
                $registroRecepcion = explode("B", $registroRecepcion["Folio"]);
                $folio = "REPB" . (intval($registroRecepcion[1])+1);
            }
            break;
        case 'tecamac':
            if($resultadoRecepcion->rowCount()==0){
                $folio = "RETC1";
            }
            else{
                $registroRecepcion = $resultadoRecepcion->fetch(PDO::FETCH_ASSOC);
                $registroRecepcion = explode("C", $registroRecepcion["Folio"]);
                $folio = "RETC" . (intval($registroRecepcion[1])+1);
            }
            break;
    }
    $resultadoRecepcion->closeCursor();
    //Insertar la nueva recepcion de dinero
    $consultaInsertarRecepcion = "INSERT INTO RECEPCION_DINERO VALUES(?,?,?,?,?,?,?,?,
                                                    (SELECT idUsuario FROM USUARIO WHERE Usuario=?))";
    $resultadoInsertarRecepcion = $base->prepare($consultaInsertarRecepcion);
    $resultadoInsertarRecepcion->execute(array(NULL, $folio, $fecha, $lugar, $importe, 
                                                $estatusRecepcion, $observacion, $cliente, $usuario));
    if ($resultadoInsertarRecepcion->rowCount()==1){
        $estatus = "Correcto";
    }
    else {
        $estatus = "Error";
    }
    $resultadoInsertarRecepcion->closeCursor();
    
    $datos["estatus"] = $estatus;
    $datos["folio"] = $folio;
    
    $base = null;
    echo json_encode($datos);
?>