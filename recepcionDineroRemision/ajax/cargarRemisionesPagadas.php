<?php
    require_once("../../funciones.php");
    $folio = $_POST["folio"];
    $contador = 0;
    $importeRecepcion = 0;
    $estatus = "Sin Resultados";
    $cliente = "";
    $folios = array();
    $fechaElaboracion = array();
    $importe = array();
    $abono = array();
    $saldo = array();
    $fechaAplicacion = array();
    $aplicadoPor = array();
    $datos = array();

    $base = conexion_local();

    //Consulta para obtener el saldo de cada remision
    $consultaSaldoRemision = "SELECT (SELECT IMPORTE FROM CARGAS WHERE CLAVE=?)-
                                    (SELECT IFNULL(SUM(Abono),0) AS total FROM SALDO INNER JOIN CARGAS 
                                    ON SALDO.idFacturaRemision=CARGAS.idFacturaRemision WHERE CARGAS.CLAVE=?) AS Saldo";
    $resultadoSaldoRemision = $base->prepare($consultaSaldoRemision);
    //Consulta para obtener las remisiones que se pagaron con la recepción consultada
    $consultaRemisionesPagadas = "SELECT RECEPCION_DINERO.Total, CARGAS.CLAVE, CARGAS.FECHA, 
                                    CARGAS.IMPORTE, CARGAS.CLIENTE, CARGAS.NOMBRE, SALDO.Abono, 
                                    SALDO.Fecha, USUARIO.Nombre, USUARIO.Apellido
                                    FROM RECEPCION_DINERO INNER JOIN SALDO_RECEPCION_DINERO 
                                    ON RECEPCION_DINERO.idRecepcionDinero=SALDO_RECEPCION_DINERO.idRecepcionDinero 
                                    INNER JOIN SALDO ON SALDO_RECEPCION_DINERO.idSaldo=SALDO.idSaldo 
                                    INNER JOIN CARGAS ON SALDO.idFacturaRemision=CARGAS.idFacturaRemision 
                                    INNER JOIN USUARIO ON SALDO.idUsuario=USUARIO.idUsuario
                                    WHERE RECEPCION_DINERO.Folio=?";
    $resultadoRemisionesPagadas = $base->prepare($consultaRemisionesPagadas);
    $resultadoRemisionesPagadas->execute(array($folio));

    while($registroRemisionesPagadas = $resultadoRemisionesPagadas->fetch(PDO::FETCH_ASSOC)){

        $cliente = $registroRemisionesPagadas["CLIENTE"] . " " . $registroRemisionesPagadas["NOMBRE"];
        $importeRecepcion = $registroRemisionesPagadas["Total"];
        $folios[$contador] = $registroRemisionesPagadas["CLAVE"];
        $fechaElaboracion[$contador] = fechaStandar($registroRemisionesPagadas["FECHA"]);
        $importe[$contador] = $registroRemisionesPagadas["IMPORTE"];
        $abono[$contador] = $registroRemisionesPagadas["Abono"];
        //Obteniendo el Saldo total de la remisión
        $resultadoSaldoRemision->execute(array($registroRemisionesPagadas["CLAVE"], $registroRemisionesPagadas["CLAVE"]));
        $registroSaldoRemision = $resultadoSaldoRemision->fetch(PDO::FETCH_ASSOC);
        $resultadoSaldoRemision->closeCursor();
        $saldo[$contador] = $registroSaldoRemision["Saldo"];
        $fechaAplicacion[$contador] = fechaStandar($registroRemisionesPagadas["Fecha"]);
        $aplicadoPor[$contador] = $registroRemisionesPagadas["Nombre"] . " " . $registroRemisionesPagadas["Apellido"];
        $estatus = "Correcto";
        $contador++;
    }

    $resultadoRemisionesPagadas->closeCursor();

    $datos["folio"] = $folio;
    $datos["cliente"] = $cliente;
    $datos["importeRecepcion"] = $importeRecepcion;
    $datos["estatus"] = $estatus;
    $datos["clave"] = $folios;
    $datos["fechaElaboracion"] = $fechaElaboracion;
    $datos["importe"] = $importe;
    $datos["abono"] = $abono;
    $datos["saldo"] = $saldo;
    $datos["fechaAplicacion"] = $fechaAplicacion;
    $datos["aplicadoPor"] = $aplicadoPor;

    $base = null;

    echo json_encode($datos);
?>