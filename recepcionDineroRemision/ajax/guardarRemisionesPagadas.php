<?php

    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $remision = $_POST["remision"];
    $remisiones = $_POST["remisiones"];
    $contador = count($remisiones);
    $estatus = "Correcto";
    $saldo = 0;
    $datos = array();

    $base = conexion_local();
    //Consulta para obtener el saldo de una remisión en especifico
    $consultaSaldoRemision = "SELECT (SELECT IMPORTE FROM CARGAS WHERE CLAVE=?)-
                                    (SELECT IFNULL(SUM(Abono),0) AS total FROM SALDO INNER JOIN CARGAS 
                                    ON SALDO.idFacturaRemision=CARGAS.idFacturaRemision WHERE CARGAS.CLAVE=?) AS Saldo";
    $resultadoSaldoRemision = $base->prepare($consultaSaldoRemision);
    //Consulta para actualizar el estatus de la recepción de dinero
    $consultaActualizarRecepcion = "UPDATE RECEPCION_DINERO SET Estatus=? WHERE Folio=?";
    $resultadoActualizarRecepcion = $base->prepare($consultaActualizarRecepcion);
    //Consulta para guardar el abono de una remisión en especifico
    $consultaInsertarAbonoRemision = "INSERT INTO SALDO VALUES(?,
                                        (SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=?),
                                        ?,CURDATE(), ?,
                                        (SELECT idUsuario FROM USUARIO WHERE Usuario=?))";
    $resultadoInsertarAbonoRemision = $base->prepare($consultaInsertarAbonoRemision);
    //Consulta para guardar la relación ente saldos y recepción de dinero
    $consultaInsertarSaldosRecepcion = "INSERT INTO SALDO_RECEPCION_DINERO VALUES(?,?,
                                        (SELECT idRecepcionDinero FROM RECEPCION_DINERO WHERE Folio=?))";
    $resultadoInsertarSaldosRecepcion = $base->prepare($consultaInsertarSaldosRecepcion);
    for($i=0; $i < $contador; $i++){
        if($i!=0){
            //Obtenemos el saldo de la remisión en especifico
            $resultadoSaldoRemision->execute(array($remisiones[$i], $remisiones[$i]));
            $registroSaldoRemision = $resultadoSaldoRemision->fetch(PDO::FETCH_ASSOC);
            $saldo = $registroSaldoRemision["Saldo"];
            //Guardar el abono generado de la remisión
            $resultadoInsertarAbonoRemision->execute(array(NULL, $remisiones[$i], $saldo, "", $usuario));
            
            switch($resultadoInsertarAbonoRemision->rowCount()){
                case 1:
                    $resultadoInsertarSaldosRecepcion->execute(array(NULL, $base->lastInsertId(), $folio));
                    break;
                
                default:
                    $estatus = "Error";
                    break;
            }
        }
    }
    $resultadoInsertarSaldosRecepcion->closeCursor();
    $resultadoInsertarAbonoRemision->closeCursor();
    //Consula para obtener el importe total por aplicar
    $consultaImporteDisponible = "SELECT (SELECT Total FROM RECEPCION_DINERO WHERE Folio=?)-
                                    (SELECT IFNULL(SUM(Abono), 0) FROM RECEPCION_DINERO 
                                    INNER JOIN SALDO_RECEPCION_DINERO ON 
                                    RECEPCION_DINERO.idRecepcionDinero=SALDO_RECEPCION_DINERO.idRecepcionDinero 
                                    INNER JOIN SALDO ON SALDO_RECEPCION_DINERO.idSaldo=SALDO.idSaldo 
                                    WHERE RECEPCION_DINERO.Folio=?) AS Disponible";
    $resultadoImporteDisponible = $base->prepare($consultaImporteDisponible);
    $resultadoImporteDisponible->execute(array($folio, $folio));
    $registroImporteDisponible = $resultadoImporteDisponible->fetch(PDO::FETCH_ASSOC);
    $resultadoImporteDisponible->closeCursor();
    $importeDisponible = $registroImporteDisponible["Disponible"];
    //Obtenemos el saldo de la última remisión
    $resultadoSaldoRemision->execute(array($remision, $remision));
    $registroSaldoUltimaRemision = $resultadoSaldoRemision->fetch(PDO::FETCH_ASSOC);
    $saldoUltimaRemision = $registroSaldoUltimaRemision["Saldo"];
    $resultadoSaldoRemision->closeCursor();
    /**
     * Sí al restar el importe disponible por aplicar
     * menos el saldo de la última remisión nos da un valor 
     * negativo, entonces el Abono aplicado será igual a $importeDisponible 
     * en caso contrario el Abono aplicado será igual a $saldoUltimaRemision
     */
    if(($importeDisponible-$saldoUltimaRemision)<=0){
        $resultadoInsertarAbonoRemision->execute(array(NULL, $remision, $importeDisponible, "", $usuario));
        if($resultadoInsertarAbonoRemision->rowCount()==1){
            $resultadoInsertarSaldosRecepcion->execute(array(NULL, $base->lastInsertId(), $folio));
            //Actualizamos el estatus de la recepción de dinero a Aplicado
            $resultadoActualizarRecepcion->execute(array("Aplicado", $folio));
            $resultadoActualizarRecepcion->closeCursor();
        }
        else{
            $estatus = "Error";
        }
    }
    else{
        $resultadoInsertarAbonoRemision->execute(array(NULL, $remision, $saldoUltimaRemision, "", $usuario));
        if($resultadoInsertarAbonoRemision->rowCount()==1){
            $resultadoInsertarSaldosRecepcion->execute(array(NULL, $base->lastInsertId(), $folio));
        }
        else{
            $estatus = "Error";
        }
    }
    $resultadoInsertarAbonoRemision->closeCursor();
    $resultadoInsertarSaldosRecepcion->closeCursor();

    $datos["folio"] = $folio;
    $datos["estatus"] = $estatus;

    $base = null;

    echo json_encode($datos);
?>