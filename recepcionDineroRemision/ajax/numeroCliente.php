<?php
    require_once("../../funciones.php");
    $cliente = $_POST["cliente"];
    $estatus = "";
    $datos = array();

    $base = conexion_local();

    //Consulta para obtener la información del cliente
    $consultaCliente = "SELECT Nombre, Porcentaje FROM CLIENTE 
                        INNER JOIN DESCUENTO ON CLIENTE.idDescuento=DESCUENTO.idDescuento 
                        WHERE idCliente=?";
    $resultadoCliente = $base->prepare($consultaCliente);
    $resultadoCliente->execute(array($cliente));
    if ($resultadoCliente->rowCount()==1){
        $registroCliente = $resultadoCliente->fetch(PDO::FETCH_ASSOC);
        $estatus = "Correcto";
        $datos["nombre"] = $registroCliente["Nombre"];
        $datos["descuento"] = $registroCliente["Porcentaje"];
    }
    else{
        $estatus = "Sin información";
    }
    $resultadoCliente->closeCursor();

    $datos["estatus"] = $estatus;
    $datos["cliente"] = $cliente;

    $base = null;
    echo json_encode($datos);
    
?>