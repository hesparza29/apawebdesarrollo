<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $estatus = "Correcto";
    $modulo = "Recepción Dinero";
    $permiso = "";
    $fechaConHora = "";
    $saldoTotal = 0;
    $contador = 0;
    $clave = array();
    $fecha = array();
    $importe = array();
    $saldo = array();
    $datos = array();

    $base = conexion_local();

    //Consulta para obtener las remisiones pendientes del cliente
    $consultaRemisiones = "SELECT idFacturaRemision, CLAVE, FECHA, IMPORTE, ENTRADA FROM CARGAS 
                            WHERE CLIENTE=?  AND (CLAVE LIKE ? OR CLAVE LIKE ?) AND ESTATUS=? 
                            AND FECHA BETWEEN  DATE_SUB(NOW(),INTERVAL 6 MONTH) AND  NOW() ORDER BY FECHA DESC";
    $resultadoRemisiones = $base->prepare($consultaRemisiones);
    //Obtener las recepciones que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
    INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
    INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
    WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    //Obtener la información de la recepción de dinero
    switch ($permiso){
        case 'administrador':
            $consultaRecepcion = "SELECT Fecha, Total,  
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente WHERE Folio=?";
            $resultadoRecepcion = $base->prepare($consultaRecepcion);
            $resultadoRecepcion->execute(array($folio));
            break;
        
        default:
            $consultaRecepcion = "SELECT Fecha, Total,  
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente WHERE Folio=? AND Lugar=?";
            $resultadoRecepcion = $base->prepare($consultaRecepcion);
            $resultadoRecepcion->execute(array($folio, $permiso));
            break;
    }
    //Verificar si existe la recepcion con el folio solicitado
    switch ($resultadoRecepcion->rowCount()) {
        case 1:
            $registroRecepcion = $resultadoRecepcion->fetch(PDO::FETCH_ASSOC);
            $fechaConHora = explode(" ", $registroRecepcion["Fecha"]);
            $datos["fecha"] = fechaStandar($fechaConHora[0]);
            $datos["hora"] = $fechaConHora[1] . " hrs.";
            $datos["total"] = $registroRecepcion["Total"];
            $datos["cliente"]  = $registroRecepcion["idCliente"] . " " . $registroRecepcion["Cliente"];
            $datos["usuario"] = $registroRecepcion["Nombre"] . " " . $registroRecepcion["Apellido"];
            //Buscar las remisiones del cliente y además que estén con un estatus "Emitida"
            $consultaRemisiones = "SELECT idFacturaRemision, CLAVE, FECHA, IMPORTE, ENTRADA FROM CARGAS 
                                    WHERE CLIENTE=?  AND (CLAVE LIKE ? OR CLAVE LIKE ?) AND ESTATUS=? 
                                    AND FECHA BETWEEN  DATE_SUB(NOW(),INTERVAL 6 MONTH) AND  NOW() ORDER BY FECHA DESC";
            $resultadoRemisiones = $base->prepare($consultaRemisiones);
            $resultadoRemisiones->execute(array($registroRecepcion["idCliente"], 'RR%', 'RTC%', 'Emitida'));
            while ($registroRemisiones = $resultadoRemisiones->fetch(PDO::FETCH_ASSOC)) {
                //Verificar que el saldo sea mayor a 0.00
                $consultaSaldo = "SELECT SUM(Abono) AS total FROM SALDO INNER JOIN CARGAS 
                                ON SALDO.idFacturaRemision=CARGAS.idFacturaRemision WHERE CARGAS.CLAVE=?";
                $resultadoSaldo = $base->prepare($consultaSaldo);
                $resultadoSaldo->execute(array($registroRemisiones["CLAVE"]));
                $registroSaldo = $resultadoSaldo->fetch(PDO::FETCH_ASSOC);
                if($registroSaldo["total"]==null){
                    $saldoTotal = $registroRemisiones["IMPORTE"];
                }
                else{
                    $saldoTotal = round(($registroRemisiones["IMPORTE"]-$registroSaldo["total"])*100)/100;
                }
                $resultadoSaldo->closeCursor();
                //Si el saldo es mayor a 0.00 recuperamos esas remisiones
                if($saldoTotal>0){
                    $clave[$contador] = $registroRemisiones["CLAVE"];
                    $fecha[$contador] = fechaStandar($registroRemisiones["FECHA"]);
                    $importe[$contador] = $registroRemisiones["IMPORTE"];
                    $saldo[$contador] = $saldoTotal;
                    $folioCaja[$contador] = $registroRemisiones["ENTRADA"];
                    $contador++;
                }
            }
            $resultadoRemisiones->closeCursor();

            //Consula para obtener el importe total por aplicar
            $consultaImporteDisponible = "SELECT (SELECT Total FROM RECEPCION_DINERO WHERE Folio=?)-
                                            (SELECT IFNULL(SUM(Abono), 0) FROM RECEPCION_DINERO 
                                            INNER JOIN SALDO_RECEPCION_DINERO ON 
                                            RECEPCION_DINERO.idRecepcionDinero=SALDO_RECEPCION_DINERO.idRecepcionDinero 
                                            INNER JOIN SALDO ON SALDO_RECEPCION_DINERO.idSaldo=SALDO.idSaldo 
                                            WHERE RECEPCION_DINERO.Folio=?) AS Disponible";
            $resultadoImporteDisponible = $base->prepare($consultaImporteDisponible);
            $resultadoImporteDisponible->execute(array($folio, $folio));
            $registroImporteDisponible = $resultadoImporteDisponible->fetch(PDO::FETCH_ASSOC);
            $resultadoImporteDisponible->closeCursor();
            $importeDisponible = $registroImporteDisponible["Disponible"];

            $datos["clave"] = $clave;
            $datos["fecha"] = $fecha;
            $datos["importe"] = $importe;
            $datos["saldo"] = $saldo;
            $datos["importeDisponible"] = $importeDisponible;
            break;
        
        case 0:
            $estatus = "Sin resultados";
            break;
    }
    
    $resultadoRecepcion->closeCursor();
    $base = null;

    $datos["folio"] = $folio;
    $datos["estatus"] = $estatus;

    echo json_encode($datos);
?>