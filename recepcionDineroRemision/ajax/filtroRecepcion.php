<?php

    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $numeroCliente = $_POST["numeroCliente"];
    $nombreCliente = $_POST["nombreCliente"];
    $fecha = $_POST["fecha"];
    $folio = $_POST["folio"];
    $modulo = "Recepción Dinero";
    $estatus = "Correcto";
    $fechaConHora = "";
    $folios = array();
    $clientes = array();
    $totales = array();
    $fechas = array();
    $usuarios = array();
    $estatuses = array();
    $clases = array();
    $datos = array();
    $contador = 0;
    $datos = array();
    $base = conexion_local();

    //Obtener las cobranzas que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
    INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
    INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
    WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    switch ($permiso){
        case 'administrador':
            if($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.idCliente=? AND CLIENTE.Nombre=? AND Fecha BETWEEN ? AND ? AND Folio=?
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $folioRecepcion, $clave, $folio));
            }
            //Grupos de 3
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.idCliente=? AND CLIENTE.Nombre=? AND Fecha BETWEEN ? AND ? 
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" )));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.idCliente=? AND CLIENTE.Nombre=? AND Folio=?
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folio));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.idCliente=? AND Fecha BETWEEN ? AND ? AND Folio=?
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $folio));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Fecha BETWEEN ? AND ? AND Folio=?
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $folio));
            }
            //Grupos 2
            elseif($numeroCliente!=""&&$nombreCliente!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.idCliente=? AND CLIENTE.Nombre=? 
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente));
            }
            elseif($numeroCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.idCliente=? AND Fecha BETWEEN ? AND ? 
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" )));
            }
            elseif($numeroCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.idCliente=? AND Folio=?
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folio));
            }
            elseif($nombreCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Fecha BETWEEN ? AND ? 
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" )));
            }
            elseif($nombreCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Folio=?
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folio));
            }
            elseif($fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? AND Folio=?
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array((fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $folio));
            }
            //Grupos de 1
            elseif ($numeroCliente!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.idCliente=? 
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente));
            }
            elseif ($nombreCliente!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? 
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente));
            }
            elseif ($fecha!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE Fecha BETWEEN ? AND ? 
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array((fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" )));
            }
            elseif ($folio!=""){
                $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                                CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                                USUARIO.Nombre, USUARIO.Apellido 
                                FROM RECEPCION_DINERO INNER JOIN USUARIO
                                ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                                WHERE Folio=?
                                ORDER BY idRecepcionDinero DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folio));
            }
          break;
        //Para los usuarios que no son administradores
        default:
        if($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folio!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.idCliente=? AND CLIENTE.Nombre=? AND Fecha BETWEEN ? AND ? AND Folio=? AND Lugar=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($numeroCliente, $nombreCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $folioRecepcion, $clave, $folio, $permiso));
        }
        //Grupos de 3
        elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.idCliente=? AND CLIENTE.Nombre=? AND Fecha BETWEEN ? AND ? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($numeroCliente, $nombreCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $permiso));
        }
        elseif($numeroCliente!=""&&$nombreCliente!=""&&$folio!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.idCliente=? AND CLIENTE.Nombre=? AND Folio=? AND Lugar=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($numeroCliente, $nombreCliente, $folio, $permiso));
        }
        elseif($numeroCliente!=""&&$fecha!=""&&$folio!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.idCliente=? AND Fecha BETWEEN ? AND ? AND Folio=? AND Lugar=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($numeroCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $folio, $permiso));
        }
        elseif($nombreCliente!=""&&$fecha!=""&&$folio!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.Nombre=? AND Fecha BETWEEN ? AND ? AND Folio=? AND Lugar=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($nombreCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $folio, $permiso));
        }
        //Grupos 2
        elseif($numeroCliente!=""&&$nombreCliente!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.idCliente=? AND CLIENTE.Nombre=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($numeroCliente, $nombreCliente, $permiso));
        }
        elseif($numeroCliente!=""&&$fecha!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.idCliente=? AND Fecha BETWEEN ? AND ? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($numeroCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $permiso));
        }
        elseif($numeroCliente!=""&&$folio!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.idCliente=? AND Folio=? AND Lugar=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($numeroCliente, $folio, $permiso));
        }
        elseif($nombreCliente!=""&&$fecha!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.Nombre=? AND Fecha BETWEEN ? AND ? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($nombreCliente, (fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $permiso));
        }
        elseif($nombreCliente!=""&&$folio!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.Nombre=? AND Folio=? AND Lugar=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($nombreCliente, $folio, $permiso));
        }
        elseif($fecha!=""&&$folio!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE Fecha=? AND Folio=? AND Lugar=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array((fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $folio, $permiso));
        }
        //Grupos de 1
        elseif ($numeroCliente!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.idCliente=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($numeroCliente, $permiso));
        }
        elseif ($nombreCliente!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE CLIENTE.Nombre=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($nombreCliente, $permiso));
        }
        elseif ($fecha!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE Fecha=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array((fechaConsulta($fecha) . " 00:00:00" ), (fechaConsulta($fecha) . " 23:59:59" ), $permiso));
        }
        elseif ($folio!=""){
            $consulta = "SELECT Folio, Fecha, Total, RECEPCION_DINERO.Estatus, 
                            CLIENTE.idCliente, CLIENTE.Nombre AS Cliente, 
                            USUARIO.Nombre, USUARIO.Apellido 
                            FROM RECEPCION_DINERO INNER JOIN USUARIO
                            ON RECEPCION_DINERO.idUsuario=USUARIO.idUsuario 
                            INNER JOIN CLIENTE ON RECEPCION_DINERO.idCliente=CLIENTE.idCliente 
                            WHERE Folio=? AND Lugar=? 
                            ORDER BY idRecepcionDinero DESC";
            $resultado = $base->prepare($consulta);
            $resultado->execute(array($folio, $permiso));
        }
            break;
    }

    if($resultado->rowCount()>0){
        while($registro = $resultado->fetch(PDO::FETCH_ASSOC)){
            $folios[$contador] = $registro["Folio"];
            $clientes[$contador] = $registro["idCliente"] . " " . $registro["Cliente"];
            $totales[$contador] = $registro["Total"];
            $fechaConHora = explode(" ", $registro["Fecha"]);
            $fechas[$contador] = fechaStandar($fechaConHora[0]);
            $estatuses[$contador] = $registro["Estatus"];
            switch($registro["Estatus"]){
                case 'Revisando':
                    $clases[$contador] = "revisando";
                    break;
                case 'Cancelada':
                    $clases[$contador] = "cancelada";
                    break;
                case 'Aplicado':
                    $clases[$contador] = "correcta";
                    break;
                
            }
            $usuarios[$contador] = $registro["Nombre"] . " " . $registro["Apellido"];
            $contador++;
        }
    }
    else{
        $estatus = "Sin resultados";
    }

    $datos["estatus"] = $estatus;
    $datos["folio"] = $folios;
	$datos["cliente"] = $clientes;
    $datos["total"] = $totales;
	$datos["fecha"] = $fechas;
    $datos["usuario"] = $usuarios;
    $datos["estatuses"] = $estatuses;
    $datos["clase"] = $clases;
	
    

    $base = null;

    echo json_encode($datos);

?>