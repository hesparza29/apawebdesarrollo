$(document).ready(function(){

    $("#lugar").change(function(){
        if($("#lugar").val()!=""){
            enviar($("#lugar").val());
        }
        //Si se cambia el lugar setear a vacio el tipo y el folio
        $("#folio").val("");
    });

    function enviar(lugar){
    
        var parametros = {
            lugar: lugar
        };
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/lugar.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando permiso...");
    }

    function llegada(datos){
        console.log(datos);

        if (datos.estatus=="Sin permiso"){
            alert("No tiene permiso de capturar recepción de dinero de "+datos.lugar);
            $("#lugar").val("");
        }
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});