$(document).ready(function(){
    $("body").on('click', "#guardar", function(){

        let ultimaRemisionGuardar = $("#ultimaRemision").val();
        let contadorGuardar = $("#contador").val();
        let folioGuardar = $("#folio").val();
        let alMenosUnaRemision = false;
        const remisionesGuardar = [""];

        for(let i = 0; i < contadorGuardar; i++){
            //Verificamos que al menos se haya seleccionado 1 remisión
            if($(`#pagar-${i}`).prop("checked")){
                alMenosUnaRemision = true;
            }
            /**
             * Guardamos en un arreglo los folios 
             * de las remisiones que se hayan
             * seleccionado excepto la última remisión
             * seleccionada 
             */
            if($(`#remision-${i}`).text()!=ultimaRemisionGuardar && $(`#pagar-${i}`).prop("checked")){
                remisionesGuardar.push($(`#remision-${i}`).text());
            }
        }
        console.log(remisionesGuardar.length, remisionesGuardar[1]);
        if(alMenosUnaRemision){
            enviar(folioGuardar, ultimaRemisionGuardar, remisionesGuardar);
        }
        else{
            alert("Selecciona al menos 1 remisión, por favor");
        }

    });

    function enviar(folioGuardar, ultimaRemisionGuardar, remisionesGuardar){

        let parametros =
        {
            folio: folioGuardar,
            remision: ultimaRemisionGuardar,
            remisiones: remisionesGuardar,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/guardarRemisionesPagadas.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros,
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando guardado del abono...");
    }

    function llegada(datos) {
        console.log(datos);

        switch(datos.estatus){
            case "Correcto":
                alert(`Se pudo aplicar el importe disponible de la recepción ${datos.folio} a las remisiones seleccionadas de manera correcta`);
                setTimeout("location.href='visualizacion.php'", 500);
                break;

            case "Error":
                alert(`Ocurrio un error intenta más tarde, por favor`);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
        
            default:
                break;
        }
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});