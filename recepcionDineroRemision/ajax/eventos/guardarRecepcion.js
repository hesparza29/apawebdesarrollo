$(document).ready(function(){

    $("#guardarRecepcion").on('click', function(){
        let estatus = true;
        
        //Verificar que se haya capturado un lugar de nota de crédito
        if($("#lugar").val()==""){
            alert("Captura un lugar para la nota de crédito, verificalo por favor");
            estatus = false;
        }
        //Verificar que se haya capturado un cliente de nota de crédito
        else if($("#cliente").val()==""){
            alert("Captura un cliente para la nota de crédito, verificalo por favor");
            estatus = false;
        }
        //Verificar que se haya capturado un descuento de nota de crédito
        else if(parseFloat($("#importe").val())<=0 || $("#importe").val()==""){
            alert("El importe ingresado debe de ser mayor a 0, verificalo por favor");
            estatus = false;
        }

        if(estatus){
            enviar();
        }
    });

    function enviar() {
        var parametros = {
          lugar: $("#lugar").val(),
          cliente: $("#cliente").val(),
          importe: $("#importe").val(),
          observacion: $("#observacion").val(),
        };
        
        $.ajax({
          async: true, //Activar la transferencia asincronica
          type: "POST", //El tipo de transaccion para los datos
          dataType: "json", //Especificaremos que datos vamos a enviar
          contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
          url: "ajax/guardarRecepcion.php", //Sera el archivo que va a procesar la petición AJAX
          data: parametros, //Datos que le vamos a enviar
          // data: "total="+total+"&penalizacion="+penalizacion,
          beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
          success: llegada, //Función que se ejecuta en caso de tener exito
          timeout: 4000,
          error: problemas, //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando el guardado de la recepción...");
    }
    
    function llegada(datos){
        console.log(datos);

        switch (datos.estatus){
            case "Correcto":
                alert("Se pudo capturar de manera exitosa la recepción de dinero "+datos.folio);
                //Desarrollo
                var url = window.location.host+"/apawebdesarrollo/recepcionDineroRemision/impresion.php?folio="+datos.folio;
                //Produccion
                //var url = "apawebdesarrollo.com/recepcionDineroRemision/impresion.php?folio="+datos.folio;
                window.open('http://'+url+'', '_blank');
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            case "Error":
                alert("Ocurrio un error al capturar la recepcion de dinero "+datos.folio+" intentalo más tarde, por favor");
                break;
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});