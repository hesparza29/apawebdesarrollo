$(document).ready(function(){
    $("table").on('click', ".pagar", function(){
        let id = $(this).attr("id");
            id = id.split("-");
            id = id[1];
        let remision = "";
        let ultimaRemision = $("#ultimaRemision").val();
        if($(this).prop("checked")){
            remision = $(`#remision-${id}`).text();
        }
        
        let contador = $("#contador").val();
        let folio = $("#folio").val();
        const remisiones = [""];

        for(let i = 0; i < contador; i++){
            if(i!=id && $(`#pagar-${i}`).prop("checked")){
                remisiones.push($(`#remision-${i}`).text());
            }
        }

        enviar(folio, id, remision, remisiones, ultimaRemision);
    });

    function enviar(folio, id, remision, remisiones, ultimaRemision){

        let parametros =
        {
            folio: folio,
            id: id,
            remision: remision,
            remisiones: remisiones,
            ultimaRemision: ultimaRemision,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/pagarRemision.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros,
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando guardado del abono...");
    }

    function llegada(datos) {
        console.log(datos);
        let contador = $("#contador").val();
        //Si el importe disponible es 0 bloquear las remisiones que aún no se han seleccionado
        switch(datos.importeDisponible){
            case 0:
                alert(`A la última remisión seleccionada ${datos.remision} 
solamente se le podrá abonar un importe de ${formatNumber.new(datos.saldoCubierto, "$")} pesos 
quedando un saldo pendiente de ${formatNumber.new(datos.saldoPendiente, "$")} pesos.`);
                for (let i = 0; i < contador; i++) {
                    if(!($(`#pagar-${i}`).prop("checked"))){
                        $(`#pagar-${i}`).attr("disabled", true);
                    }
                }
                break;
        
            default:
                for (let i = 0; i < contador; i++) {
                    if(!($(`#pagar-${i}`).prop("checked"))){
                        $(`#pagar-${i}`).attr("disabled", false);
                    }
                }
                break;
        }
        //Asignando la última remisión que se selecciono
        if(datos.remision!=""){
            $("#ultimaRemision").val(datos.remision);
        }
        //Asignando el importe total por asignar que tiene disponible el usuario
        $("#importeDisponible").text(formatNumber.new(datos.importeDisponible, "$"));
        //Asignando el importe del saldo por pagar para la remisión que se selecciono
        $(`#saldo-${datos.id}`).text(formatNumber.new(datos.saldoPendiente, "$"));
        
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});