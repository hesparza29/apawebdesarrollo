$(document).ready(function(){
    
    //Obteniendo el folio de la recepción de egfectivo
    let url = window.location.search;
    url = url.split("?");
    url = url[1];
    url = url.split("=");
    folio = url[1];
    $("body").ready(function(){
        enviar(folio)
    });

    function enviar(folio){
        
        var parametros =
        {
            folio: folio,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/cargarRemisionesPagadas.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 10000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Cargando remisiones pagadas...");
    }
    
    function llegada(datos){
        console.log(datos);
        
        switch (datos.estatus){
            case "Correcto":
                let contador = Object.keys(datos.clave).length;
                let contenido = "";

                for (let i = 0; i < contador; i++){
                    contenido += `<tr>
                                    <td>${datos.clave[i]}</td>
                                    <td>${datos.fechaElaboracion[i]}</td>
                                    <td>${formatNumber.new(datos.importe[i], "$")}</td>
                                    <td>${formatNumber.new(datos.abono[i], "$")}</td>
                                    <td id="saldo-${i}">${formatNumber.new(datos.saldo[i], "$")}</td>
                                    <td>${datos.fechaAplicacion[i]}</td>
                                    <td>${datos.aplicadoPor[i]}</td>
                                </tr>`;
                }
                $("#table").append(contenido);
                //Agregar la información a las remisiones pagadas
                $("#informacion").text(`Recepción de dinero ${datos.folio} 
                                        con un importe total de ${formatNumber.new(datos.importeRecepcion, "$")} 
                                        para el cliente ${datos.cliente}`);
                break;
        
            case "Sin Resultados":
                alert("No se han pagado remisiones con la recepción de dinero "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
        }
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});