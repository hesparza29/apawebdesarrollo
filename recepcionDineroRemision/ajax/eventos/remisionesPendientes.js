$(document).ready(function(){
    
    //Obteniendo el folio de la recepción de egfectivo
    let url = window.location.search;
    url = url.split("?");
    url = url[1];
    url = url.split("=");
    folio = url[1];
    $("body").ready(function(){
        enviar(folio)
    });

    function enviar(folio){
        
        var parametros =
        {
            folio: folio,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/remisionesPendientes.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 20000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Cargando remisiones...");
    }
    
    function llegada(datos){
        console.log(datos);
        
        switch (datos.estatus){
            case "Correcto":
                let contador = Object.keys(datos.clave).length;
                let contenido = "";

                for (let i = 0; i < contador; i++){
                    contenido += `<tr>
                                    <td id="remision-${i}">${datos.clave[i]}</td>
                                    <td>${datos.fecha[i]}</td>
                                    <td>${formatNumber.new(datos.importe[i], "$")}</td>
                                    <td id="saldo-${i}">${formatNumber.new(datos.saldo[i], "$")}</td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input pagar" id="pagar-${i}" />
                                        </div>
                                    </td>
                                </tr>`;
                }
                $("#table").append(contenido);
                //Asignando el contador para saber cuantas remisiones se mostraron
                $("#contador").val(contador);
                //Asignando el folio de la recepción de dinero dentro del documento
                $("#folio").val(datos.folio);
                /**
                 * Si el importe disponible por 
                 * aplicar es igual a 0 se reedireciona a
                 * la página de la visualización de recepción de efectivos
                 */
                if(parseFloat(datos.importeDisponible)===0){
                    alert(`El importe disponible de la recepción de dinero ${datos.folio} 
es ${formatNumber.new(datos.importeDisponible, "$")}, por lo tanto ya no puede pagar más remisiones.`);
                    setTimeout("location.href='visualizacion.php'", 500);
                }
                else{
                    //Asignando el importe disponible por aplicar
                    $("#importeDisponible").text(formatNumber.new(datos.importeDisponible, "$"));
                }
                break;
        
            case "Sin resultados":
                alert("No se econtraron resultados de la recepción de dinero "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
        }
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});