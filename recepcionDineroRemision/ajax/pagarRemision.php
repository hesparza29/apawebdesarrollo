<?php
    require_once("../../funciones.php");

    $folio = $_POST["folio"];
    $id = $_POST["id"];
    $remision = $_POST["remision"];
    $remisiones = $_POST["remisiones"];
    $ultimaRemision = $_POST["ultimaRemision"];
    $contador = count($remisiones);
    $importeRemisiones = 0;
    $importeUltimaRemision = 0;
    $estatus  = "";
    $prueba = "";
    $saldoCubierto = 0;
    $saldoPendiente = 0;
    $datos = array();

    $base = conexion_local();

    //Consula para obtener el importe total por aplicar
    $consultaImporteDisponible = "SELECT (SELECT Total FROM RECEPCION_DINERO WHERE Folio=?)-
                                    (SELECT IFNULL(SUM(Abono), 0) FROM RECEPCION_DINERO 
                                    INNER JOIN SALDO_RECEPCION_DINERO ON 
                                    RECEPCION_DINERO.idRecepcionDinero=SALDO_RECEPCION_DINERO.idRecepcionDinero 
                                    INNER JOIN SALDO ON SALDO_RECEPCION_DINERO.idSaldo=SALDO.idSaldo 
                                    WHERE RECEPCION_DINERO.Folio=?) AS Disponible";
    $resultadoImporteDisponible = $base->prepare($consultaImporteDisponible);
    $resultadoImporteDisponible->execute(array($folio, $folio));
    $registroImporteDisponible = $resultadoImporteDisponible->fetch(PDO::FETCH_ASSOC);
    $resultadoImporteDisponible->closeCursor();
    $importeDisponible = $registroImporteDisponible["Disponible"];

    //Consulta para obtener el importe total de las remisiones que se quieren pagar
    $consultaSaldoRemision = "SELECT (SELECT IMPORTE FROM CARGAS WHERE CLAVE=?)-
                                (SELECT IFNULL(SUM(Abono),0) AS total FROM SALDO INNER JOIN CARGAS 
                                ON SALDO.idFacturaRemision=CARGAS.idFacturaRemision WHERE CARGAS.CLAVE=?) AS Saldo";
    $resultadoSaldoRemision = $base->prepare($consultaSaldoRemision);
    for ($i=0; $i < $contador; $i++){
        if($i!=0){
            $resultadoSaldoRemision->execute(array($remisiones[$i], $remisiones[$i]));
            $registroSaldoRemision = $resultadoSaldoRemision->fetch(PDO::FETCH_ASSOC);
            $importeRemisiones += $registroSaldoRemision["Saldo"];
        }
    }
    $resultadoSaldoRemision->closeCursor();

    //Restar el importe total por aplicar menos el importe total de las remisiones
    $importeDisponible -= $importeRemisiones;

    //Si el importe disponible es menor o igual a 0 ya no se puede pagar otra remisión
    if($importeDisponible>0){
        switch ($remision){
            case '':
                //Obtener el saldo de la última remisión que se selecciono
                $resultadoSaldoRemision->execute(array($ultimaRemision, $ultimaRemision));
                $registroSaldoRemision = $resultadoSaldoRemision->fetch(PDO::FETCH_ASSOC);
                $importeUltimaRemision = $registroSaldoRemision["Saldo"];
                $resultadoSaldoRemision->closeCursor();
                $estatus = "Ya no se selecciono la remisión";
                $saldoCubierto = 0;
                $saldoPendiente = $importeUltimaRemision;
                $datos["saldoCubierto"] = round($saldoCubierto*100)/100;
                $datos["saldoPendiente"] = round($saldoPendiente*100)/100;
                break;
            
            default:
                //Obtener el saldo de la última remisión que se selecciono
                $resultadoSaldoRemision->execute(array($remision, $remision));
                $registroSaldoRemision = $resultadoSaldoRemision->fetch(PDO::FETCH_ASSOC);
                $importeUltimaRemision = $registroSaldoRemision["Saldo"];
                $resultadoSaldoRemision->closeCursor();
                if(($importeDisponible-$importeUltimaRemision)<0){
                    $estatus = "No se alcanzo a cubrir el importe";
                    $saldoCubierto = $importeDisponible;
                    $saldoPendiente = $importeUltimaRemision-$importeDisponible;
                    $importeDisponible = 0;
                    $datos["saldoCubierto"] = round($saldoCubierto*100)/100;
                    $datos["saldoPendiente"] = round($saldoPendiente*100)/100;
                }
                else{
                    $estatus = "Se alcanzo a cubrir el importe";
                    $saldoCubierto = $importeUltimaRemision;
                    $saldoPendiente = 0;
                    $importeDisponible -= $importeUltimaRemision;
                    $datos["saldoCubierto"] = round($saldoCubierto*100)/100;
                    $datos["saldoPendiente"] = round($saldoPendiente*100)/100;
                }
                
                break;
        }
    }
    else{
        //Ya no tiene importe suficiente para pagar otra remisión
        $estatus = "Ya no tienes importe para pagar";
    }

    
    

    $datos["folio"] = $folio;
    $datos["id"] = $id;
    $datos["remision"] = $remision;
    $datos["remisiones"] = $remisiones;
    $datos["importeDisponible"] = round($importeDisponible*100)/100;
    $datos["importeRemisiones"] = round($importeRemisiones*100)/100;
    $datos["importeUltimaRemision"] = $importeUltimaRemision;
    $datos["estatus"] = $estatus;
    $datos["prueba"] = $prueba;

    $base = null;

    echo json_encode($datos);
?>