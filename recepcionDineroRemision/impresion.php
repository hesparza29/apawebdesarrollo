<!DOCTYPE html>
<html>
  <head>
  	<title>Impresión Recepción de Dinero</title>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estiloRecepcionDineroRemision.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/verificarSesion.js"></script>
    <script type="text/javascript" src="../js/cierreSesion.js"></script>
    <script type="text/javascript" src="../js/cierreInactividad.js"></script>
    <script type="text/javascript" src="../js/formatoNumero.js"></script>
    <script type="text/javascript" src="../js/visualizacion.js"></script>
    <script type="text/javascript" src="../js/imprimir.js"></script>
    <script type="text/javascript" src="ajax/js/aplicarDinero.js"></script>
    <script type="text/javascript" src="ajax/js/remisionesPagadas.js"></script>
    <script type="text/javascript" src="ajax/eventos/cargarPartidas.js"></script>
    <script type="text/javascript" src="ajax/eventos/acciones.js"></script>
  </head>
  <body>
  <header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
          <input type="button" class="btn btn-success" id="visualizacion" value="Regresar" />
				</div>
				<div class="col-sm-12 col-md-6">
          
				</div>
				<div class="col-sm-12 col-md-2">
          <input class="btn btn-info" type="button" id="imprimir" value="Imprimir" />
        </div>
        <div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-3">
          
				</div>
        <div class="col-sm-12 col-md-3">
          <span class="aplicarDinero"></span>
				</div>
        <div class="col-sm-12 col-md-3">
          <span class="cancelar"></span>
				</div>
				<div class="col-sm-12 col-md-3">
          <span class="remisionesPagadas"></span>
				</div>
			</div>
		</div>
  </header>
  <br />
  <section>
		<div class="container">		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-sm table-bordered table-striped text-center">
								<thead>
                  <tr>
                    <th colspan="6"><img src="./../imagenes/apa.jpg" class="img-fluid" alt="apa"></th>
                  </tr>
                  <tr>
                    <th colspan="6">REPORTE RECEPCION DE DINERO</th>
                  </tr>
                  <tr>
                    <th colspan="3">FECHA</th>
                    <th colspan="3" class="mes"></th>
                  </tr>
                  <tr>
                    <th colspan="3">HORA</th>
                    <th colspan="3" class="hora"></th>
                  </tr>
                  <tr>
                    <th colspan="3">FOLIO</th>
                    <th colspan="3" class="folio"></th>
                  </tr>
                  <tr>
                    <th colspan="3">IMPORTE</th>
                    <th colspan="3" class="importe"></th>
                  </tr>
                  <tr>
                    <th colspan="3">CLIENTE</th>
                    <th colspan="3" class="cliente"></th>
                  </tr>
                  <tr>
                    <th colspan="3">OBSERVACIONES</th>
                    <th colspan="3" class="observacion"></th>
                  </tr>
                </thead>        
								<tbody id="table">

                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6">
                      <strong>ELABORO</strong>
                      <br />
                      <strong class="usuario"></strong>
                      <br />
                      <strong>ENCARGADA DE CAJA</strong>
                    </td>
                  </tr>
                </tfoot>
							</table>
						</div>
					</div>
				</div>			
		</div>
    <br /><br />
    <div class="container">		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-sm table-bordered table-striped text-center">
								<thead>
                  <tr>
                    <th colspan="6"><img src="./../imagenes/apa.jpg" class="img-fluid" alt="apa"></th>
                  </tr>
                  <tr>
                    <th colspan="6">REPORTE RECEPCION DE DINERO</th>
                  </tr>
                  <tr>
                    <th colspan="3">FECHA</th>
                    <th colspan="3" class="mes"></th>
                  </tr>
                  <tr>
                    <th colspan="3">HORA</th>
                    <th colspan="3" class="hora"></th>
                  </tr>
                  <tr>
                    <th colspan="3">FOLIO</th>
                    <th colspan="3" class="folio"></th>
                  </tr>
                  <tr>
                    <th colspan="3">IMPORTE</th>
                    <th colspan="3" class="importe"></th>
                  </tr>
                  <tr>
                    <th colspan="3">CLIENTE</th>
                    <th colspan="3" class="cliente"></th>
                  </tr>
                  <tr>
                    <th colspan="3">OBSERVACIONES</th>
                    <th colspan="3" class="observacion"></th>
                  </tr>
                </thead>        
								<tbody id="table">

                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6">
                      <strong>ELABORO</strong>
                      <br />
                      <strong class="usuario"></strong>
                      <br />
                      <strong>ENCARGADA DE CAJA</strong>
                    </td>
                  </tr>
                </tfoot>
							</table>
						</div>
					</div>
				</div>			
		</div>
	</section>	
  </body>
</html>