<!DOCTYPE html>
<html>
<head>
	<title>Nueva Recepcion de Dinero</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="./../css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloRecepcionDineroRemision.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/visualizacion.js"></script>
	<script type="text/javascript" src="ajax/eventos/numeroCliente.js"></script>
	<script type="text/javascript" src="ajax/eventos/autocompletarNombreCliente.js"></script>
	<script type="text/javascript" src="ajax/eventos/nombreCliente.js"></script>
	<script type="text/javascript" src="ajax/eventos/lugar.js"></script>
	<script type="text/javascript" src="ajax/eventos/tipo.js"></script>
	<script type="text/javascript" src="ajax/eventos/guardarRecepcion.js"></script>
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block img-fluid" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Generar Nueva Recepción de Dinero</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="visualizacion" class="btn btn-primary" value="Regresar" />
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
			</div>
		</div>
	</header>
	<br />
	<section>
		<div class="container">
			<div class="row">
				<table class="table table-success table-hover table-sm table-bordered text-center">
					<thead>
						<tr>
							<th colspan="2">Lugar</th>
							<th colspan="2">
								<select class="form-control" id="lugar">
									<option selected value=""></option>
									<option value="azcapotzalco">Pozo Brasil</option>
									<option value="tecamac">Tecámac</option>
								</select>
							</th>
							<th colspan="2">Folio</th>
							<th colspan="2"><input type="text" class="form-control" id="folio" value="" readonly></th>
						</tr>
						<tr>
							<th colspan="2">Cliente</th>
							<th colspan="2"><input type="text" class="form-control" id="cliente" value="" ></th>
							<th colspan="2">Importe $</th>
							<th colspan="2"><input type="number" class="form-control" id="importe" value="" ></th>
							
						</tr>
						<tr>
							<th colspan="8">Nombre</th>
						</tr>
					</thead>
					<tbody id="recepcion">
						<tr>
							<td colspan="8"><input type="text" class="form-control text-center" id="nombreCliente" value=""></td>
						</tr>
						<tr>
							<td class="text-center" colspan="8"><strong>Obervaciones</strong></td>
						</tr>
						<tr>
							<td colspan="8"><textarea class="form-control text-center" id="observacion" placeholder="Opcional"></textarea></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td class="text-center" colspan="8">
								<input type="button" class="btn btn-sm btn-success" id='guardarRecepcion' value="Guardar">
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</section>
</body>
</html>
