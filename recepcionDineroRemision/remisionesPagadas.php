<!DOCTYPE html>
<html>

<head>
	<title>Remisiones Pagadas</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloAplicarDinero.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/visualizacion.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/imprimir.js"></script>
    <script type="text/javascript" src="ajax/eventos/cargarRemisionesPagadas.js"></script>
</head>

<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					
				</div>
				<div class="col-sm-12 col-md-8">
					
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
                    <input type="button" class="btn btn-success" id="visualizacion" value="Regresar" />
				</div>
				<div class="col-sm-12 col-md-6">
                    
				</div>
				<div class="col-sm-12 col-md-2">
                    <input class="btn btn-info" type="button" id="imprimir" value="Imprimir" />
				</div>
			</div>
		</div>
	</header>
    <br />
	<section>
		<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead class="thead-dark">
                                    <tr>
                                        <th colspan="7" id="informacion"></th>
                                    </tr>
                                    <tr>
                                        <th>Folio</th>
                                        <th>Fecha de Elaboración</th>
                                        <th>Importe</th>
                                        <th>Abono</th>
                                        <th>Saldo</th>
										<th>Fecha de Aplicación</th>
                                        <th>Aplicado Por</th>
                                    </tr>
								</thead>
								<tbody id="table">

								</tbody>
							</table>
						</div>
					</div>
				</div>			
		</div>
	</section>
</body>

</html>