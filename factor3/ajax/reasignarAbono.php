<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];  
    $remisionActual = $_POST["remisionActual"];
    $abono = $_POST["abono"];
    $observaciones = $_POST["observaciones"];
    $remisionPasada = $_POST["remisionPasada"];
    $saldo = $_POST["saldo"];
    $estatus = 0;
    $auxiliar = 0;
    $fecha = "";
    $datos = array();

    $base = conexion_local();

    //Verificar si la remisión actual es del mismo cliente que la remisión pasada
        //remisionPasada
        $consultaClienteRemision = "SELECT idFacturaRemision, CLIENTE FROM CARGAS WHERE CLAVE=?";
        $resultadoClienteRemision = $base->prepare($consultaClienteRemision);
        $resultadoClienteRemision->execute(array($remisionPasada));
        $registroRemisionPasada = $resultadoClienteRemision->fetch(PDO::FETCH_ASSOC);
        $resultadoClienteRemision->closeCursor();
        //remisionActual
        $consultaClienteRemision = "SELECT idFacturaRemision, CLIENTE, IMPORTE FROM CARGAS WHERE CLAVE=?";
        $resultadoClienteRemision = $base->prepare($consultaClienteRemision);
        $resultadoClienteRemision->execute(array($remisionActual));
        $registroRemisionActual = $resultadoClienteRemision->fetch(PDO::FETCH_ASSOC);
        $resultadoClienteRemision->closeCursor();

        if($registroRemisionPasada["CLIENTE"]!=$registroRemisionActual["CLIENTE"]){
            $estatus = 1;
        }
    // //Verificar que el usuario que capturo el saldo pasado sea el mismo que va a reasignar el saldo
    //     $consultaUsuarioRemision = "SELECT Usuario FROM SALDO INNER JOIN USUARIO ON SALDO.idUsuario=USUARIO.idUsuario 
    //                                 INNER JOIN CARGAS ON SALDO.idFacturaRemision=CARGAS.idFacturaRemision 
    //                                 WHERE CLAVE=?";
    //     $resultadoUsuarioRemision = $base->prepare($consultaUsuarioRemision);
    //     $resultadoUsuarioRemision->execute(array($remisionPasada));
    //     $registroUsuarioRemision = $resultadoUsuarioRemision->fetch(PDO::FETCH_ASSOC);
    //     $resultadoUsuarioRemision->closeCursor();

    //     if($registroUsuarioRemision["Usuario"]!=$usuario){
    //         $estatus = 2;
    //     }

    //Verificar si se le aplica el abono a la remisión actual no quede un saldo negativo
        $consultaSaldoRemision = "SELECT SUM(Abono) AS total FROM SALDO INNER JOIN CARGAS 
        ON SALDO.idFacturaRemision=CARGAS.idFacturaRemision WHERE CARGAS.CLAVE=?";
        $resultadoSaldoRemision = $base->prepare($consultaSaldoRemision);
        $resultadoSaldoRemision->execute(array($remisionActual));
        $registroSaldoRemision = $resultadoSaldoRemision->fetch(PDO::FETCH_ASSOC);
        $resultadoSaldoRemision->closeCursor();
        if($registroSaldoRemision["total"]!=null){
            //Dicha remisión ya cuenta con un historial de abonos
            //Ahora se tiene que verificar que al sumar los abonos más el abono que se quiere reasignar menos el importe no sea negativo
            if(($registroRemisionActual["IMPORTE"]-($registroSaldoRemision["total"]+$abono)<0)){
                $estatus = 2;
            }
        }
        else{
            //Dicha remisión no cuenta con un historial de abonos
            //Ahora se tiene que verificar que el importe menos el abono que se quiere reasignar no sea negativo
            if(($registroRemisionActual["IMPORTE"]-$abono<0)){
                $estatus = 3;
            }
        }
    
    //Si el estatus es igual a 0 proseguimos a reasignar el saldo
    if($estatus==0){
        //Restar el abono del saldo
        $auxiliar = $saldo-$abono;
        //Eliminar los registros anteriores en la tabla saldo de la remision pasada
        $consultaEliminarAbonos = "DELETE FROM SALDO WHERE idFacturaRemision=?";
        $resultadoEliminarAbonos = $base->prepare($consultaEliminarAbonos);
        $resultadoEliminarAbonos->execute(array($registroRemisionPasada["idFacturaRemision"]));
        $resultadoEliminarAbonos->closeCursor();
        //Si auxiliar es mayor a 0 crear un nuevo registro
        if($auxiliar>0){
            //Obtener la fecha de hoy
            $fecha = fecha();
            //Obtener el id del usuario
            $consultaIdUsuario = "SELECT idUsuario FROM USUARIO WHERE Usuario=?";
            $resultadoIdUsuario = $base->prepare($consultaIdUsuario);
            $resultadoIdUsuario->execute(array($usuario));
            $registroIdUsuario = $resultadoIdUsuario->fetch(PDO::FETCH_ASSOC);
            $idUsuario = $registroIdUsuario["idUsuario"];
            $resultadoIdUsuario->closeCursor();
            //Insertar el abono para general el historial
            $consultaInsertarAbono = "INSERT INTO SALDO VALUES(?,?,?,?,?,?)";
            $resultadoInsertarAbono = $base->prepare($consultaInsertarAbono);
            $resultadoInsertarAbono->execute(array(NULL, $registroRemisionPasada["idFacturaRemision"], $auxiliar, $fecha, 'Reasignación de saldo', $idUsuario));
            $resultadoInsertarAbono->closeCursor();
        }
        //Insertar el nuevo abono
        //Obtener la fecha de hoy
        $fecha = fecha();
        //Obtener el id del usuario
        $consultaIdUsuario = "SELECT idUsuario FROM USUARIO WHERE Usuario=?";
        $resultadoIdUsuario = $base->prepare($consultaIdUsuario);
        $resultadoIdUsuario->execute(array($usuario));
        $registroIdUsuario = $resultadoIdUsuario->fetch(PDO::FETCH_ASSOC);
        $idUsuario = $registroIdUsuario["idUsuario"];
        $resultadoIdUsuario->closeCursor();
        //Insertar el abono para general el historial
        $consultaInsertarAbono = "INSERT INTO SALDO VALUES(?,?,?,?,?,?)";
        $resultadoInsertarAbono = $base->prepare($consultaInsertarAbono);
        $resultadoInsertarAbono->execute(array(NULL, $registroRemisionActual["idFacturaRemision"], $abono, $fecha, $observaciones, $idUsuario));
        $resultadoInsertarAbono->closeCursor();

    }
    

    $datos["remisionActual"] = $remisionActual;
    $datos["importeRemisionActual"] = $registroRemisionActual["IMPORTE"];
    $datos["abono"] = $abono;
    $datos["observaciones"] = $observaciones;
    $datos["remisionPasada"] = $remisionPasada;
    $datos["saldo"] = $saldo;
    $datos["usuario"] = $usuario;
    $datos["estatus"] = $estatus;
    $datos["clientePasada"] = $registroRemisionPasada["CLIENTE"];
    $datos["clienteActual"] = $registroRemisionActual["CLIENTE"];
    $datos["resultado"] = $auxiliar;
    
    $base = null;
    echo json_encode($datos);
?>