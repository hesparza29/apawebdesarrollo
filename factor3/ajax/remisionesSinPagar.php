<?php
    require_once("../../funciones.php");
    $cliente = $_POST["cliente"];
    $clave = array();
    $fecha = array();
    $importe = array();
    $saldo = array();
    $folioCaja = array();
    $datos = array();
    $saldoTotal = 0;
    $contador = 0;
    $consultaSaldo = "(SELECT A.IMPORTE-(SELECT IFNULL((SELECT SUM(Abono) 
                        FROM SALDO WHERE idFacturaRemision=A.idFacturaRemision), 0)))";
    $estatus = "";
    $base = conexion_local();

    //Buscar las remisiones del cliente con saldo mayor a 0 y además que estén con un estatus "Emitida"
    $consultaRemisiones = "SELECT A.CLAVE, A.FECHA, A.IMPORTE, A.ENTRADA, $consultaSaldo AS Saldo
                            FROM CARGAS AS A WHERE A.CLIENTE=?  AND (A.CLAVE LIKE ? OR A.CLAVE LIKE ?) 
                            AND A.ESTATUS=? AND $consultaSaldo>0 ORDER BY A.FECHA DESC";
    $resultadoRemisiones = $base->prepare($consultaRemisiones);
    $resultadoRemisiones->execute(array($cliente, 'RR%', 'RTC%', 'Emitida'));

    if($resultadoRemisiones->rowCount()>0){
        while ($registroRemisiones = $resultadoRemisiones->fetch(PDO::FETCH_ASSOC)){
            $clave[$contador] = $registroRemisiones["CLAVE"];
            $fecha[$contador] = fechaStandar($registroRemisiones["FECHA"]);
            $importe[$contador] = $registroRemisiones["IMPORTE"];
            $saldo[$contador] = $registroRemisiones["Saldo"];
            $folioCaja[$contador] = $registroRemisiones["ENTRADA"];
            $contador++;
        }
        $resultadoRemisiones->closeCursor();

        $estatus = "Correcto";
        $datos["clave"] = $clave;
        $datos["fecha"] = $fecha;
        $datos["importe"] = $importe;
        $datos["saldo"] = $saldo;
        $datos["folioCaja"] = $folioCaja;
    }
    else{
        $estatus = "Sin resultados";
    }
    
    $base = null;

    $datos["estatus"] = $estatus;
    echo json_encode($datos);
?>