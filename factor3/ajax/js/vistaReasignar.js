$(document).ready(function(){
    $(".reasignar").click(function(){

        var id = $(this).attr("id");
        id = id.split("-");
        id = id[1];

        //Cargando los datos en el modal
        var importe = $("#importe-"+id).text();
        importe = importe.split("$");
        importe = importe[1];
        importe = parseFloat(importe.replace(",", ""));
        var saldo = $("#saldo-"+id).text();
        saldo = saldo.split("$");
        saldo = saldo[1];
        saldo = parseFloat(saldo.replace(",", ""));
       
        $("#remisionAbono").val($("#remision-"+id).text());
        $("#importeAbono").val($("#importe-"+id).text());
        $("#saldoDisponible").val(formatNumber.new(importe-saldo, "$"));
        $("#remisionCancelada").val($("#remision-"+id).text());
        $("#saldo").val(importe-saldo);
        saldo = parseFloat($("#saldo").val());

        $( "#reasignarVista" ).dialog({
            height: 550,
            width: 375,
            dialogClass: "no-close",
            buttons: [
              {
                class: "btn btn-danger",
                text: "Cancelar",
                click: function() {
                  $( this ).dialog( "close" );
                }
              },
              {
                class: "btn btn-success",
                text: "Reasignar",
                click: function() {
                    if($("#folioRemision").val()!="" && $("#abono").val()!=""){
                        if(parseFloat($("#abono").val())>0 && (parseFloat($("#abono").val())<=saldo)){
                            enviar();
                        }
                        else{
                            alert("Captura un importe para el abono mayor a 0 y menor o igual al saldo disponible, por favor");
                        }
                    }
                    else{
                        alert("Captura todos los campor, por favor");
                    }
                }
              }
            ]
        });
    });

    function enviar() {

        var parametros =
        {
            remisionActual : $("#folioRemision").val(),
            abono: $("#abono").val(),
            observaciones: $("#observaciones").val(),
            remisionPasada : $("#remisionCancelada").val(),
            saldo : $("#saldo").val(),

        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/reasignarAbono.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros,
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 6000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando guardado del abono...");
    }

    function llegada(datos) {
       console.log(datos);
       if(datos.estatus==0){
           alert("Se pudo reasignar saldo de la remisión "+datos.remisionPasada+" a la remisión "
                    +datos.remisionActual+" por un total de "+formatNumber.new(datos.abono, "$"));
                    
            location.reload();
       }
       else if(datos.estatus==1){
            alert("El cliente de la remisión "+datos.remisionPasada+
                    " no es el mismo de la remisión "+datos.remisionActual+" no se pudo reasignar el saldo");
       }
       else if(datos.estatus==2){
        alert("No se puede reasignar el saldo por un total de "+
             formatNumber.new(datos.abono, "$")+" a la remisión "+datos.remisionActual+ 
             " porque dicha remisión ya se pago o el saldo quedaría negativo");
       }
       else if(datos.estatus==3){
           alert("No se puedo reasignar el saldo por un total de "+
                formatNumber.new(datos.abono, "$")+" a la remisión "+datos.remisionActual+" ya que el importe total de dicha "+
                "remisión es "+formatNumber.new(datos.importeRemisionActual, "$")+" y el saldo quedaría negativo");
       }
        
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});