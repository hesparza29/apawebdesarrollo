<?php
  session_start();
  require_once("../funciones.php");
  $datos = array();
  $modulos = array();
  $identificador = array();
  $departamento = array();
  $contador = 0;
  $usuario = $_SESSION["user"];
  $home = 1;
  
  $base = conexion_local();
  $consultaObtenerModulos = "SELECT A.Nombre, A.Identificador, D.Nombre AS Departamento FROM MODULO AS A
                            INNER JOIN USUARIO_MODULO AS B ON A.idModulo=B.idModulo
                            INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                            INNER JOIN DEPARTAMENTO AS D ON A.idDepartamento=D.idDepartamento 
                            WHERE C.Usuario=?
                            AND A.Home=?";
  $resultadoObtenerModulos = $base->prepare($consultaObtenerModulos);
  $resultadoObtenerModulos->execute(array($usuario, $home));

  while ($registro = $resultadoObtenerModulos->fetch(PDO::FETCH_ASSOC)){
      $modulos[$contador] = $registro["Nombre"];
      $identificador[$contador] = $registro["Identificador"];
      $departamento[$contador] = strtolower(str_replace(" ", "", $registro["Departamento"]));
      $contador++;
  }

  $resultadoObtenerModulos->closeCursor();
  $base = null;

  $datos["modulos"] = $modulos;
  $datos["identificador"] = $identificador;
  $datos["departamento"] = $departamento;
  
  echo json_encode($datos);

?>