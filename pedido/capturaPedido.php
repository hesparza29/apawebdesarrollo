<?php 
      session_start();
      $usuario = $_SESSION['user'];
      echo $usuario . "<br />";

      require_once("../funciones.php");
      
      $cantidad = array();
      $clave = array();
      $lista = array();
      $costo = array();
      $importe = array();
      $subtotal1 = 0; //Este es la suma de todos los importes
      $subtotal2 = 0; //Resta del subtotal1 y el descuento
      $ivaCarta = 0; //IVA aplicado al subtotal2
      $total = 0; //Suma del subtotal2 y el iva
      $letra = ""; //La representación en formato letra del total
      $nombre = ""; //Nombre del cliente
      $descuentodeCarta = 0;
      $fecha = fecha();
      $folio = $_POST['consecutivo'];
      $descuentodeCliente = $_POST['descuentoConsulta'];
      $cliente = $_POST['cliente']; //Clave del cliente
      $cont = $_POST['contador']; //Contador para saber cuantas partidas fueron capturadas

      // echo "El tipo de devolución es: " . $tipo_dev;
      // echo "<h1>Contador: " . $cont . "</h1>";
      for ($i=1; $i <=$cont ; $i++)
      {

        $cantidad[$i] = $_POST['cantidad' . $i];
        $clave[$i] = $_POST['clave' . $i];
        $lista[$i] = $_POST['lista' . $i];
        $costo[$i] = $_POST['cost' . $i];
        $importe[$i] = imp($cantidad[$i],$costo[$i]);
        echo "Cantidad: " . $cantidad[$i] . " Clave: " . $clave[$i] . " Lista: " . $lista[$i] . 
              " Costo: " . $costo[$i] . " Importe: " . $importe[$i] . "<br />";

      }

      $subtotal1 = subtotal($importe);
      // $descuentodeCarta = sub($descuentodeCliente, $subtotal1);
      // $subtotal2 = $subtotal1-$descuentodeCarta;
      // $ivaCarta = iva($subtotal2);
      $total = sub($descuentodeCliente, $subtotal1);
      $iva = iva($total);
      $total = $total+$iva;

      echo "El Total es: " . $total . " fecha " . $fecha . " folio " . $folio . 
            " descuento cliente " . $descuentodeCliente . " cliente " . $cliente . "<br />";
      try{
        $base = conexion_local();
        $consulta = "SELECT Nombre FROM CLIENTE WHERE idCliente=?";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($cliente));
        $registro = $resultado->fetch(PDO::FETCH_ASSOC);
        $nombre = $registro["Nombre"];
        $resultado->closeCursor();
        echo "nombre " . $nombre . "<br />";
        $consulta = "SELECT REGISTRO FROM PEDIDOS_VIS ORDER BY REGISTRO DESC LIMIT 1";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array());
        $registro = $resultado->fetch(PDO::FETCH_ASSOC);

        if($registro["REGISTRO"]==""){
          $registro = 1;
        }
        else{
          $registro = $registro["REGISTRO"]+1;
        }
        $resultado->closeCursor();
        $verificaFolio = folioPedidos();

        //Obteniendo al usuario que captura el pedido
        $consultaUsuario = "SELECT Nombre, Apellido FROM USUARIO WHERE Usuario=?";
        $resultadoUsuario = $base->prepare($consultaUsuario);
        $resultadoUsuario->execute(array($usuario));
        $registroUsuario = $resultadoUsuario->fetch(PDO::FETCH_ASSOC);
        $resultadoUsuario->closeCursor();
        $usuario = $registroUsuario["Nombre"] . " " . $registroUsuario["Apellido"];

        echo "folio " . $verificaFolio . " registro " . $registro . " " . $usuario . "<br />";

        for ($i=1; $i <= $cont ; $i++) {
          $consulta = "INSERT INTO PEDIDOS(FECHA, FOLIOINTERNO, NOCLIENTE, NOMBRE, SKU, UNIDADESxSKU, MONTO,
                               LISTAPRECIOS, USUARIO, STATUS, DESCUENTO)
             VALUES(?,?,?,?,?,?,?,?,?,?,?)";
          $resultado = $base->prepare($consulta);
          $resultado->execute(array($fecha, $verificaFolio, $cliente, $nombre, $clave[$i], $cantidad[$i], $costo[$i], $lista[$i], $usuario, "NO ASOCIADO", $descuentodeCliente));
        }
        $resultado->closeCursor();
        $consulta = "INSERT INTO PEDIDOS_VIS(FOLIOINTERNO, CLIENTE, CARTAFACTURA, TOTAL, FECHA, STATUS, NOCLIENTE, REGISTRO, USUARIO)
                      VALUES(?,?,?,?,?,?,?,?,?)";
        $resultado = $base->prepare($consulta);
        $resultado->execute(array($verificaFolio, $nombre, "NO ASOCIADO", $total, $fecha, "NO ASOCIADO", $cliente, $registro, $usuario));
        $resultado->closeCursor();

        echo "<h1>Contador: " . $cont . "</h1>";
        
        header("location:impresion.php?folio=" . $verificaFolio);

      }
      catch (Exception $e){
        $mensaje = $e->GetMessage();
        $linea = $e->getline();
        echo "<h1>Error: " . $mensaje . "</h1><br />";
        echo "<h1>Linea del Error: " . $linea . "</h1><br />";

        // die($e->GetMessage());
        // die($e->getline());
        // die("<h1>ERROR: " . $e->GetMessage());
        // echo "<br /><h3>" . $e->getline() . "</h3>";
      }
      finally{
        $base = null;
      }
