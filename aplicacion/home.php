<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="" content="" />
  <meta name="" content="" />
  <link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link href="../css/estiloHome.css" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="../js/verificarSesion.js"></script>
  <script type="text/javascript" src="../js/cierreSesion.js"></script>
  <script type="text/javascript" src="../js/cierreInactividad.js"></script>
  <script type="text/javascript" src="../js/cargarModulos.js"></script>
  <script type="text/javascript" src="../js/popper.min.js"></script>
  <script type="text/javascript" src="../js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="../js/redireccionamientoModulos.js"></script>
  <title>Home</title>
</head>

<body>
  <header>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-2">
          <img class="img-responsive rounded mx-auto d-block" id="imagen" src='../imagenes/apa.png' />
        </div>
        <div class="col-sm-12 col-md-8">
          <h1 class="text-center">Bienvenidos!</h1>
        </div>
        <div class="col-sm-12 col-md-2">
          <input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
        </div>
      </div>
    </div>
  </header>
  <br /><br />
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-3">
          <div class="dropdown" id="head-ventas">
            <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownVentas" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Ventas
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownVentas" id="ventas">
              
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-3">
          <div class="dropdown" id="head-creditoycobranza">
            <button class="btn btn-info dropdown-toggle" type="button" id="dropdownCreditoyCobranza" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Crédito Y Cobranza
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownCreditoyCobranza" id="creditoycobranza">
              
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-3">
          <div class="dropdown" id="head-recursoshumanos">
            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownRecursosHumanos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Recursos Humanos
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownRecursosHumanos" id="recursoshumanos">
              
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-3">
          <div class="dropdown" id="head-administracion">
            <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownAdministracion" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Administración
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownAdministracion" id="administracion">
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>

</html>