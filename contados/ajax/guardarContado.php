<?php
    session_start();
    $usuario = $_SESSION["user"];
    require_once("../../funciones.php");

    $fecha = fechaConsulta($_POST["fecha"]);
    $lugar = $_POST["lugar"];
    $tipo = $_POST["tipo"];
    $clave = $_POST["clave"];
    $cajas = $_POST["cajas"];
    $peso = $_POST["peso"];
    $recibe = $_POST["recibe"];
    $observaciones = $_POST["observaciones"];
    $marcadores = str_repeat('?,', count($clave) - 1) . '?';
    $modulo = "Contado";
    $estatus = "Correcto";
    $permiso = "";
    $folio = "";
    $contador = count($clave);
    $datos = array();

    $base = conexion_local();

    //Consulta para saber si la fecha se encuentra disponible
    $consultaContado = "SELECT Folio FROM CONTADO WHERE Fecha=? AND Tipo=? AND Lugar=?";
    $resultadoContado = $base->prepare($consultaContado);
    $resultadoContado->execute(array($fecha, $tipo, $lugar));

    switch ($resultadoContado->rowCount()) {
        case 0:
            //Consulta para obtener el permiso del usuario del Contado
            $consultaPermiso = "SELECT Identificador FROM USUARIO 
                                INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
                                INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
                                WHERE Usuario=? AND MODULO.Nombre=?";
            $resultadoPermiso = $base->prepare($consultaPermiso);
            $resultadoPermiso->execute(array($usuario, $modulo));
            $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
            $resultadoPermiso->closeCursor();
            $permiso = $registroPermiso["Identificador"];
            //Sí el usuario es un administrador verificar que las facturas/remisiones pertenezcan al lugar
            if($permiso=="administrador"){
                for ($i=0; $i < $contador; $i++){ 
                    //Verificar que se capture dicha Factura este capturada en el lugar que le corresponde
                    if(($tipo=="facturas") && (($lugar=="azcapotzalco" && ($clave[$i][0] . $clave[$i][1]!="FD")) || 
                            ($lugar=="tecamac" && ($clave[$i][0] . $clave[$i][1] . $clave[$i][2]!="FTC")))){
                        $estatus = "No corresponde";
                        $datos["clave"] = $clave[$i];  
                        break;   
                    }//Verificar que se capture dicha Factura este capturada en el lugar que le corresponde
                    else if(($tipo=="remisiones") && (($lugar=="azcapotzalco" && ($clave[$i][0] . $clave[$i][1]!="RR")) || 
                            ($lugar=="tecamac" && ($clave[$i][0] . $clave[$i][1] . $clave[$i][2]!="RTC")))){
                        $estatus = "No corresponde";  
                        $datos["clave"] = $clave[$i];
                        break;    
                    }
                } 
            }
            //Si el estatus es correcto comenzar a capturar el nuevo contado
            if($estatus=="Correcto"){
                //Obtener el folio del último contado
                $consultaFolio = "SELECT Folio FROM CONTADO WHERE Lugar=? AND Tipo=? ORDER BY idContado DESC LIMIT 1";
                $resultadoFolio = $base->prepare($consultaFolio);
                $resultadoFolio->execute(array($lugar, $tipo));
                //Construir el folio consecutivo
                switch ($lugar." ".$tipo){
                    case 'azcapotzalco facturas':
                        if($resultadoFolio->rowCount()==0){
                            $folio = "CFPB1";
                        }
                        else{
                            $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
                            $registroFolio = explode("B", $registroFolio["Folio"]);
                            $folio = "CFPB" . (intval($registroFolio[1])+1);
                        }
                        break;
                    case 'azcapotzalco remisiones':
                        if($resultadoFolio->rowCount()==0){
                            $folio = "CRPB1";
                        }
                        else{
                            $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
                            $registroFolio = explode("B", $registroFolio["Folio"]);
                            $folio = "CRPB" . (intval($registroFolio[1])+1);
                        }
                        break;
                    case 'tecamac facturas':
                        if($resultadoFolio->rowCount()==0){
                            $folio = "CFTC1";
                        }
                        else{
                            $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
                            $registroFolio = explode("C", $registroFolio["Folio"]);
                            $registroFolio = $registroFolio[2];
                            $folio = "CFTC" . (intval($registroFolio)+1);
                        }
                        break;
                    case 'tecamac remisiones':
                        if($resultadoFolio->rowCount()==0){
                            $folio = "CRTC1";
                        }
                        else{
                            $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
                            $registroFolio = explode("C", $registroFolio["Folio"]);
                            $registroFolio = $registroFolio[2];
                            $folio = "CRTC" . (intval($registroFolio)+1);
                        }
                        break;
                }
                $resultadoFolio->closeCursor();
                $datos["nuevoFolio"] = $folio;
                //Consulta para actualizar la entrada de las facturas/remisiones
                $consultaActualizarEntrada = "UPDATE CARGAS SET idContado=?, Cajas_Contado=?, Peso_Contado=?, 
                                                Recibe_Contado=?, Observaciones_Contado=?, 
                                                Entrada_Contado=? WHERE CLAVE=?";
                $resultadoActualizarEntrada = $base->prepare($consultaActualizarEntrada);
                //Obtener el importe del contado
                $consultaTotal = "SELECT SUM(IMPORTE) AS Total FROM CARGAS WHERE CLAVE IN ($marcadores)";
                $resultadoTotal = $base->prepare($consultaTotal);
                $resultadoTotal->execute($clave);
                $registroTotal = $resultadoTotal->fetch(PDO::FETCH_ASSOC);
                $total = $registroTotal["Total"];
                $resultadoTotal->closeCursor();
                //Capturar el nuevo contado
                $consultaInsertarContado = "INSERT INTO CONTADO VALUES(?,?,?,?,?,?,
                                            (SELECT idUsuario FROM USUARIO WHERE Usuario=?))";
                $resultadoInsertarContado = $base->prepare($consultaInsertarContado);
                $resultadoInsertarContado->execute(array(NULL, $folio, $fecha, $tipo, $lugar, $total, $usuario));
                $datos["consulta"] = $resultadoInsertarContado->rowCount();
                $resultadoInsertarContado->closeCursor();
                $idContado = $base->lastInsertId();
                //Actualizando la entrada a caja de las facturas/remisiones
                for($i=0; $i < $contador ; $i++){
                    $resultadoActualizarEntrada->execute(array($idContado, $cajas[$i], $peso[$i], $recibe[$i], 
                                                                $observaciones[$i], (intval($i)+1), $clave[$i]));
                }
                $resultadoActualizarEntrada->closeCursor();
            }
            break;

        case 1:
            $registroContado = $resultadoContado->fetch(PDO::FETCH_ASSOC);
            $datos["folio"] = $registroContado["Folio"];
            $estatus = "Fecha no disponible";
            break;
        
        default:
            # code...
            break;
    }

    $resultadoContado->closeCursor();

    
    $datos["fecha"] = $fecha;
    if ($lugar=="azcapotzalco") {
        $datos["lugar"] = "Pozo Brasil";
    }
    else if($lugar=="tecamac"){
        $datos["lugar"] = "Tecámac";
    }
    $datos["estatus"] = $estatus;
    

    $base = null;
    echo json_encode($datos);
?>