<?php
  require_once("../../funciones.php");
  session_start();
  $usuario = $_SESSION["user"];
  $folio = $_POST['folio'];
  $fechaInicio = $_POST['fechaInicio'];
  $fechaFin = $_POST['fechaFin'];
  $modulo = "Contado";
  $estatus = "Correcto";
  $folios = array();
  $fecha = array();
  $total = array();
  $usuarios = array();
  $datos = array();
  $contador = 0;
  $base = conexion_local();

  //Si las fechas vienen vacias setearlas a un valor por defecto
  if($fechaInicio==""){
    $fechaInicio = '00/00/0000';
  }
  if($fechaFin==""){
    $fechaFin = fechaStandar(fecha());
  }
  //Obtener los contados que puede ver el usuario
  $consultaPermiso = "SELECT Identificador FROM USUARIO 
            INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
            INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
            WHERE Usuario=? AND MODULO.Nombre=?";
  $resultadoPermiso = $base->prepare($consultaPermiso);
  $resultadoPermiso->execute(array($usuario, $modulo));
  $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
  $resultadoPermiso->closeCursor();
  $permiso = $registroPermiso["Identificador"];

  switch ($permiso){
    case 'administrador':
      if($folio!=""){
        $consultaContado = "SELECT Folio, Fecha, Total, Nombre, Apellido
                            FROM CONTADO INNER JOIN USUARIO
                            ON CONTADO.idUsuario=USUARIO.idUsuario
                            WHERE Folio=? AND Fecha BETWEEN ? AND ? 
                            ORDER BY idContado DESC";
        $resultadoContado = $base->prepare($consultaContado);
        $resultadoContado->execute(array($folio, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
      }
      else{
        $consultaContado = "SELECT Folio, Fecha, Total, Nombre, Apellido
                            FROM CONTADO INNER JOIN USUARIO
                            ON CONTADO.idUsuario=USUARIO.idUsuario
                            WHERE Fecha BETWEEN ? AND ? 
                            ORDER BY idContado DESC";
        $resultadoContado = $base->prepare($consultaContado);
        $resultadoContado->execute(array(fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
      }
      break;
    
    default:
      if($folio!=""){
        $consultaContado = "SELECT Folio, Fecha, Total, Nombre, Apellido
                            FROM CONTADO INNER JOIN USUARIO
                            ON CONTADO.idUsuario=USUARIO.idUsuario
                            WHERE Folio=? AND Fecha BETWEEN ? AND ? 
                            AND Lugar=? ORDER BY idContado DESC";
        $resultadoContado = $base->prepare($consultaContado);
        $resultadoContado->execute(array($folio, fechaConsulta($fechaInicio), fechaConsulta($fechaFin), $permiso));
      }
      else{
        $consultaContado = "SELECT Folio, Fecha, Total, Nombre, Apellido
                            FROM CONTADO INNER JOIN USUARIO
                            ON CONTADO.idUsuario=USUARIO.idUsuario
                            WHERE Fecha BETWEEN ? AND ? 
                            AND Lugar=? ORDER BY idContado DESC";
        $resultadoContado = $base->prepare($consultaContado);
        $resultadoContado->execute(array(fechaConsulta($fechaInicio), fechaConsulta($fechaFin), $permiso));
      }
      break;
  }

  if($resultadoContado->rowCount()>0){
    while($registroContado = $resultadoContado->fetch(PDO::FETCH_ASSOC)){
      $folios[$contador] = $registroContado["Folio"];
      $fecha[$contador] = fechaStandar($registroContado["Fecha"]);
      $total[$contador] = $registroContado["Total"];
      $usuarios[$contador] = $registroContado["Nombre"] . " " . $registroContado["Apellido"];
      $contador++;
    }
  }
  else{
    $estatus = "Sin resultados";
  }


  $resultadoContado->closeCursor();
  $base = null;

  $datos["estatus"] = $estatus;
  $datos["folio"] = $folios;
  $datos["fecha"] = $fecha;
  $datos["total"] = $total;
  $datos["usuario"] = $usuarios;

  echo json_encode($datos);
?>
