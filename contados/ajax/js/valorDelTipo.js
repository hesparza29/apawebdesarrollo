$(document).ready(function(){
    $("#tipo").change(function(){
        let estatus = true;
        //Obtener el número de facturas/remisiones
        let contador = $("#contador").val();
        //Recuperar el tipo de cobranza
        let tipo = $("#tipo").val();

        for (let i = 1; i <= contador; i++) {
            //Verificar que la clave del producto sea diferente de undefined y además sea diferente de vacio
            if($("#clave-" + i + "").val()!= undefined && $("#clave-" + i + "").val()!=""){
                let clave = $("#clave-" + i + "").val();
                let primerCaracter = clave[0];

                if((tipo=="facturas" && primerCaracter!="F") || (tipo=="remisiones" && primerCaracter!="R")){
                    estatus = false;
                    break;
                }
            }
        }
        if(estatus==true){
            //Pasar a mayuscula la primera letra
            tipo = tipo.charAt(0).toUpperCase() + tipo.slice(1);
            //Asignar el valor para saber si se ingresaran Facturas o Remisiones
            $("#valorDelTipo").text(tipo);
            //Cambiar el valor del boton para agregar otra fila
            $("#agregarFacturaRemision").attr('value', 'Agregar '+tipo);
        }
        else{
            tipoActual = $("#valorDelTipo").text();
            //Pasar a minuscula la primera letra
            tipoActual = tipoActual.charAt(0).toLowerCase() + tipoActual.slice(1);
            alert("No puede cambiar a "+ tipo +", porque ya tiene capturadas algunas "+tipoActual);
            $("#tipo").val(tipoActual);
        }
        
    });
});