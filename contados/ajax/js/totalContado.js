function totalContado() {
    //Obtener el número de facturas/remisiones
    let contador = $("#contador").val();
    let total = 0.0;
    let aux = 0.0;
    //Realizar la suma solamente de aquellos importes que existan
    for (let i = 1; i <= contador; i++) {
      //Verificar que la clave del producto sea diferente de undefined y además sea diferente de vacio
      if ($("#clave-" + i + "").val() != undefined) {
          if($("#importe-" + i + "").val()!=""){
              console.log("Entramos");
              aux = $("#importe-" + i + "").val();
              aux = aux.replace("$", "");
              aux = aux.replace(",", "");
              total += parseFloat(aux);
          }
      }
    }
    //Redondear el resultado a 2 cifras
    total = Math.round(total * 100) / 100;
    //Darle formato al total
    total = formatNumber.new(total, "$");
    //Actualizar el valor del total
    $("#total").val(total);
  }