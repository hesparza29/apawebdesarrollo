// url para llamar la peticion por ajax
//var url_listar_usuario = "php/listar.php";
var url_listar_usuario = "contados.php";


$( document ).ready(function() {
   // se genera el paginador
   paginador = $(".pagination");
	// cantidad de items por pagina
	var items = 10, numeros =4;
	// inicia el paginador
	init_paginator(paginador,items,numeros);
	// se envia la peticion ajax que se realizara como callback
	set_callback(get_data_callback);
	cargaPagina(0);
});

// peticion ajax enviada como callback
function get_data_callback(){
	$.ajax({
		data:{
		limit: itemsPorPagina,
		offset: desde,
		},
		type:"POST",
		url:url_listar_usuario
	}).done(function(data,textStatus,jqXHR){
		// obtiene la clave lista del json data
		var lista = data.lista;
		$("#table").html("");

		// si es necesario actualiza la cantidad de paginas del paginador
		if(pagina==0){
			creaPaginador(data.cantidad);
		}
		// genera el cuerpo de la tabla
		$.each(lista, function(ind, elem){
     
			$('<tr>'+
                '<td>'+elem.folio+'</td>'+
                '<td>'+elem.fecha+'</td>'+
                '<td>'+formatNumber.new(elem.total, "$")+'</td>'+
                '<td>'+elem.usuario+'</td>'+
                '<td><input type="button" class="btn btn-info btn-sm verImpresion" id="contados-'+elem.folio+'" value="Ver" /></td>'+
			'</tr>').appendTo($("#table"));

		});

	}).fail(function(jqXHR,textStatus,textError){
    	console.log(jqXHR.responseText);
	});

}
