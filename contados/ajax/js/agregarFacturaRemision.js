$(document).ready(function(){
    $("#agregarFacturaRemision").click(function(){
        //Obtener el siguiente id que se va a asignar
        let id = parseInt($("#contador").val())+1;
        //Construir el contenido que se va a agregar
        let contenido = '<tr id="fila-'+id+'">'+
							'<td><input type="text" class="form-control form-control-sm clave" id="clave-'+id+'"  value=""></td>'+
							'<td><input type="text" class="form-control form-control-sm" id="cliente-'+id+'" value="" readonly></td>'+
							'<td><input type="text" class="form-control form-control-sm" id="importe-'+id+'" value="" readonly></td>'+
							'<td><input type="text" class="form-control form-control-sm" id="cajas-'+id+'" value="" ></td>'+
							'<td><input type="number" class="form-control form-control-sm" id="peso-'+id+'" value=0.00 ></td>'+
                            '<td><input type="text" class="form-control form-control-sm" id="recibe-'+id+'" value="" ></td>'+
                            '<td><input type="text" class="form-control form-control-sm" id="observaciones-'+id+'" value="" ></td>'+
							'<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-'+id+'"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>'+
						'</tr>';
        //Agregar el contenido
        $("#factura_remision").append(contenido);
        //Reasignar el contador de factura_remision con el nuevo valor
        $("#contador").val(id);
    });
});