<?php
    session_start();
    $usuario = $_SESSION["user"];
    require_once("../../funciones.php");

    $lugar = $_POST["lugar"];
    $modulo = "Contado";
    $estatus = "";
    $datos = array();

    $base = conexion_local();

    //Consulta para verificar que el usuario tenga permiso para capturar el contado de dicho lugar
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
					INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
					INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
					WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    switch ($permiso) {
        case 'administrador':
            $estatus = "Correcto";
            break;
        case 'azcapotzalco':
            if($lugar=='azcapotzalco'){
                $estatus = "Correcto";
            }
            else{
                $estatus = "Sin permiso";
            }
            break;
        case 'tecamac':
            if($lugar=='tecamac'){
                $estatus = "Correcto";
            }
            else{
                $estatus = "Sin permiso";
            }
            break;
        default:
            $estatus = "Sin permiso";
            break;
    }

    $datos["estatus"] = $estatus;
    if ($lugar=="azcapotzalco") {
        $datos["lugar"] = "Pozo Brasil";
    }
    else if($lugar=="tecamac"){
        $datos["lugar"] = "Tecámac";
    }

    
    $base = null;
    echo json_encode($datos);
?>