<?php
    session_start();
    $usuario = $_SESSION["user"];
    require_once("../../funciones.php");

    $clave = $_POST["clave"];
    $id = $_POST["id"];
    $tipo = $_POST["tipo"];
    $lugar = $_POST["lugar"];
    $modulo = "Contado";
    $permiso = "";
    $estatus = "Correcto";
    $datos = array();

    $base = conexion_local();

    //Consulta para obtener la información de la Factura/Remisión
    $consultaInformacion = "SELECT CLIENTE, NOMBRE, IMPORTE, ESTATUS, CONTADO.Folio FROM
                            CARGAS LEFT JOIN CONTADO ON CARGAS.idContado=CONTADO.idContado
                            WHERE CLAVE=?";
    $resultadoInformacion = $base->prepare($consultaInformacion);
    //Consulta para obtener el permiso del usuario en el contado
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
					INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
					INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
					WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    $datos["permiso"] = $permiso;

    switch ($permiso) {
        case 'administrador':
            //Ejecutando la consulta de la información
            $resultadoInformacion->execute(array($clave));
            $registroInformacion = $resultadoInformacion->fetch(PDO::FETCH_ASSOC);
            $resultadoInformacion->closeCursor();
            //Verificar que dicha Factura o Remisión exista en la base de datos
            if($registroInformacion["CLIENTE"]=="" && $registroInformacion["NOMBRE"]==""){
                $estatus = "Sin información";
            }
            //Verificar que se capture dicha Factura este capturada en el lugar que le corresponde
            else if(($tipo=="Facturas") && (($lugar=="azcapotzalco" && ($clave[0] . $clave[1]!="FD")) || 
                    ($lugar=="tecamac" && ($clave[0] . $clave[1] . $clave[2]!="FTC")))){
                $estatus = "No corresponde";      
            }//Verificar que se capture dicha Factura este capturada en el lugar que le corresponde
            else if(($tipo=="Remisiones") && (($lugar=="azcapotzalco" && ($clave[0] . $clave[1]!="RR")) || 
                    ($lugar=="tecamac" && ($clave[0] . $clave[1] . $clave[2]!="RTC")))){
                $estatus = "No corresponde";      
            }
            //Verificar que dicha Factura o Remisión no se encuentre cancelada
            else if($registroInformacion["ESTATUS"]=="Cancelada"){
                $estatus = "Cancelada";
            }
            //Verificar que dicha Factura o Remisión se encuentre capturada en un contado anterior
            else if($registroInformacion["Folio"]!=""){
                $estatus = "Ya capturada";
                $datos["folio"] = $registroInformacion["Folio"];
            }
            //Recuperar la información de dicha Factura o Remisión
            else{
                $datos["cliente"] = $registroInformacion["CLIENTE"] . " " . $registroInformacion["NOMBRE"];
                $datos["importe"] = $registroInformacion["IMPORTE"];
            }
            break;
        case 'azcapotzalco':
            //Ejecutando la consulta de la información
            $resultadoInformacion->execute(array($clave));
            $registroInformacion = $resultadoInformacion->fetch(PDO::FETCH_ASSOC);
            $resultadoInformacion->closeCursor();
            //Verificar que dicha Factura o Remisión exista en la base de datos
            if($registroInformacion["CLIENTE"]=="" && $registroInformacion["NOMBRE"]==""){
                $estatus = "Sin información";
            }
            else{
                //Obtener el prefijo de dicha Factura o Remisión('FD' y 'RR' para Pozo Brasil)
                $folio = $clave[0] . $clave[1];
                //Verificar que se tiene permiso de capturar dicha Factura o Remisión
                if($folio=="FD" || $folio=="RR"){
                    //Verificar que dicha Factura o Remisión no se encuentre cancelada
                    if($registroInformacion["ESTATUS"]=="Cancelada"){
                        $estatus = "Cancelada";
                    }
                    //Verificar que dicha Factura o Remisión se encuentre capturada en un contado anterior
                    else if($registroInformacion["Folio"]!=""){
                        $estatus = "Ya capturada";
                        $datos["folio"] = $registroInformacion["Folio"];
                    }
                    //Recuperar la información de dicha Factura o Remisión
                    else{
                        $datos["cliente"] = $registroInformacion["CLIENTE"] . " " . $registroInformacion["NOMBRE"];
                        $datos["importe"] = $registroInformacion["IMPORTE"];
                    }

                }
                else{
                    $estatus = "Sin permisos"; 
                }
            }
            
            break;
        case 'tecamac':
            //Ejecutando la consulta de la información
            $resultadoInformacion->execute(array($clave));
            $registroInformacion = $resultadoInformacion->fetch(PDO::FETCH_ASSOC);
            $resultadoInformacion->closeCursor();
            //Verificar que dicha Factura o Remisión exista en la base de datos
            if($registroInformacion["CLIENTE"]=="" && $registroInformacion["NOMBRE"]==""){
                $estatus = "Sin información";
            }
            else{
                //Obtener el prefijo de dicha Factura o Remisión('FTC' y 'RTC' para Tecámac)
                $folio = $clave[0] . $clave[1] . $clave[2];
                //Verificar que se tiene permiso de capturar dicha Factura o Remisión
                if($folio=="FTC" || $folio=="RTC"){
                    //Verificar que dicha Factura o Remisión no se encuentre cancelada
                    if($registroInformacion["ESTATUS"]=="Cancelada"){
                        $estatus = "Cancelada";
                    }
                    //Verificar que dicha Factura o Remisión se encuentre capturada en un contado anterior
                    else if($registroInformacion["Folio"]!=""){
                        $estatus = "Ya capturada";
                        $datos["folio"] = $registroInformacion["Folio"];
                    }
                    //Recuperar la información de dicha Factura o Remisión
                    else{
                        $datos["cliente"] = $registroInformacion["CLIENTE"] . " " . $registroInformacion["NOMBRE"];
                        $datos["importe"] = $registroInformacion["IMPORTE"];
                    }

                }
                else{
                    $estatus = "Sin permisos"; 
                }
            }
            
            break;
        
        default:
            $estatus = "Sin permisos";
            break;
    }

    $datos["id"] = $id;
    $datos["clave"] = $clave;
    $datos["estatus"] = $estatus;
    if($tipo=="Facturas"){
        $datos["documento"] = "Factura";
    }
    else if($tipo="Remisiones"){
        $datos["documento"] = "Remisión";
    }
    if ($lugar=="azcapotzalco") {
        $datos["lugar"] = "Pozo Brasil";
    }
    else if($lugar=="tecamac"){
        $datos["lugar"] = "Tecámac";
    }
    $base = null;
    echo json_encode($datos);
?>