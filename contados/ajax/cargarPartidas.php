<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $clave = array();
    $cliente = array();
    $importe = array();
    $cajas = array();
    $peso = array();
    $recibe = array();
    $observaciones = array();
    $contador = 0;
    $estatus = "Correcto";
    $modulo = "Contado";
    $permiso = "";
    
    
    $datos = array();

    $base = conexion_local();
    //Obtener los contados que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
    INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
    INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
    WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    //Obtener la información del contado
    switch ($permiso){
        case 'administrador':
            $consultaContado = "SELECT Fecha, Tipo, Total FROM CONTADO WHERE Folio=?";
            $resultadoContado = $base->prepare($consultaContado);
            $resultadoContado->execute(array($folio));
            break;
        
        default:
            $consultaContado = "SELECT Fecha, Tipo, Total FROM CONTADO WHERE Folio=? AND Lugar=?";
            $resultadoContado = $base->prepare($consultaContado);
            $resultadoContado->execute(array($folio, $permiso));
            break;
    }
    //Verificar si existe el contado con el folio solicitado
    switch ($resultadoContado->rowCount()) {
        case 1:
            $registroContado = $resultadoContado->fetch(PDO::FETCH_ASSOC);
            $datos["fecha"] = fechaStandar($registroContado["Fecha"]);
            $datos["tipo"] = strtoupper($registroContado["Tipo"]);
            $datos["total"] = $registroContado["Total"];
            $datos["fechaDeContado"] = saber_dia_contado($datos["fecha"]);
            switch ($datos["tipo"]){
                case 'FACTURAS':
                    $datos["valorDelTipo"] = "FACTURA";
                    break;
                    
                case 'REMISIONES':
                    $datos["valorDelTipo"] = "REMISIÓN";
                    break;
            }
            //Obtener todas las facturas/remisiones que se capturaron en el contado
            $consultaPartidas = "SELECT CLAVE, CLIENTE, NOMBRE, IMPORTE, Cajas_Contado, 
                                Peso_Contado, Recibe_Contado, Observaciones_Contado 
                                FROM CARGAS INNER JOIN CONTADO ON CARGAS.idContado=CONTADO.idContado
                                WHERE Folio=? ORDER BY Entrada_Contado ASC";
            $resultadoPartidas = $base->prepare($consultaPartidas);
            $resultadoPartidas->execute(array($folio));
            while ($registroPartidas = $resultadoPartidas->fetch(PDO::FETCH_ASSOC)){
                $clave[$contador] = $registroPartidas["CLAVE"];
                $cliente[$contador] = $registroPartidas["CLIENTE"] . " " . $registroPartidas["NOMBRE"];
                $importe[$contador] = $registroPartidas["IMPORTE"];
                $cajas[$contador] = $registroPartidas["Cajas_Contado"];
                $peso[$contador] = $registroPartidas["Peso_Contado"];
                $recibe[$contador] = $registroPartidas["Recibe_Contado"];
                $observaciones[$contador] = $registroPartidas["Observaciones_Contado"];
                $contador++;
            }
            $resultadoPartidas->closeCursor();
            $datos["clave"] = $clave;
            $datos["cliente"] = $cliente;
            $datos["importe"] = $importe;
            $datos["cajas"] = $cajas;
            $datos["peso"] = $peso;
            $datos["recibe"] = $recibe;
            $datos["observaciones"] = $observaciones;
            break;
        
        case 0:
            $estatus = "Sin resultados";
            break;
    }
    
    $resultadoContado->closeCursor();
    $base = null;

    $datos["folio"] = $folio;
    $datos["estatus"] = $estatus;
    $datos["permiso"] = $permiso;



    echo json_encode($datos);
?>