$(document).ready(function(){

    $("#guardarContado").on('click', function(){
        let clave = [];
        let cajas = [];
        let peso = [];
        let recibe = [];
        let observaciones = [];
        let contador = $("#contador").val();
        let estatus = true;
        for (let i = 1; i <= contador; i++) {
            //Verificar que se haya capturado la fecha de contado
            if($("#fecha").val()==""){
                alert("Captura una fecha para el contado, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un lugar de contado
            else if($("#lugar").val()==""){
                alert("Captura un lugar para el contado, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un tipo de contado
            else if($("#tipo").val()==""){
                alert("Captura un tipo para el contado, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que la clave de la factura/remision sea diferente de undefined y además sea diferente de vacio
            else if($("#clave-"+i+"").val()!=undefined && $("#clave-"+i+"").val()!=""){
                //Verificar que la factura/remision no se encuentre capturada más de una vez
                if(clave.includes($("#clave-"+i+"").val())==true){
                    alert("La factura/remisión "+$("#clave-"+i+"").val()+" se capturo más de una vez, verificalo por favor");
                    estatus = false;
                    break;
                }
                //Verificar que las cajas/bolsas ingresadas para cada factura/remisión sea mayor de 0 y debe de ser entero
                else if($("#cajas-"+i+"").val()==0 || $("#cajas-"+i+"").val()%1!=0){
                    alert("Los valores para cajas/bolsas deben de ser mayores a 0, "+
                            "además los números para cajas/bolsas deben"+
                            "de ser enteros para la factura/remisión "+$("#clave-"+i+"").val()+" , verificalo por favor");
                    estatus = false;
                    break;
                }
                //Verificar que el peso ingresado para cada factura/remisión sea mayor de 0 
                else if($("#peso-"+i+"").val()==0){
                    alert("Los valores del peso deben de ser mayores a 0, "+
                            " para la factura/remisión "+$("#clave-"+i+"").val()+" , verificalo por favor");
                    estatus = false;
                    break;
                }
                //Guardar los datos validos en arreglos
                else{
                    clave.push($("#clave-"+i+"").val());
                    cajas.push($("#cajas-"+i+"").val());
                    peso.push($("#peso-"+i+"").val());
                    recibe.push($("#recibe-"+i+"").val());
                    observaciones.push($("#observaciones-"+i+"").val());
                
                }
            }
            
        }

        //Verificar que se haya capturado por lo menos una factura/remision y su respectivo cajas
        if(clave.length==0 && cajas.length==0 && peso.length==0){
            alert("Captura al menos una factura/remisión, por favor");
            estatus = false;
        }

        if(estatus){
            enviar(clave, cajas, peso, recibe, observaciones);
        }
    });

    function enviar(clave, cajas, peso, recibe, observaciones) {
        var parametros = {
          fecha: $("#fecha").val(),
          lugar: $("#lugar").val(),
          tipo: $("#tipo").val(),
          clave: clave,
          cajas: cajas,
          peso: peso,
          recibe: recibe,
          observaciones: observaciones,
        };
        console.log(parametros);
        
        $.ajax({
          async: true, //Activar la transferencia asincronica
          type: "POST", //El tipo de transaccion para los datos
          dataType: "json", //Especificaremos que datos vamos a enviar
          contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
          url: "ajax/guardarContado.php", //Sera el archivo que va a procesar la petición AJAX
          data: parametros, //Datos que le vamos a enviar
          // data: "total="+total+"&penalizacion="+penalizacion,
          beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
          success: llegada, //Función que se ejecuta en caso de tener exito
          timeout: 4000,
          error: problemas, //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando el guardado del contado...");
    }
    
    function llegada(datos){
        console.log(datos);

        switch (datos.estatus){
            case "Correcto":
                alert("Se pudo capturar de manera exitosa el contado "+datos.nuevoFolio);
                //Desarrollo
                var url = window.location.host+"/apawebdesarrollo/contados/impresion.php?folio="+datos.nuevoFolio;
                //Produccion
                //var url = "apawebdesarrollo.com/contados/impresion.php?folio="+datos.nuevoFolio;
                window.open('http://'+url+'', '_blank');
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            case "Fecha no disponible":
                alert("La fecha ya fue seleccionada en el reporte de contado "+ datos.folio +", selecciona otra fecha por favor");
                break;

            case "No corresponde":
                alert("La factura/remisión "+ datos.clave +" no pertenece a "+datos.lugar+", verificalo por favor");
                break;
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});