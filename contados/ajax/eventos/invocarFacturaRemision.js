$(document).ready(function(){
    $('#factura_remision').on('change', '.clave', function(){
        if($("#tipo").val()=="" || $("#lugar").val()==""){
            alert("Selecciona un tipo y un lugar, por favor");
        }
        else{
            let lugar = $("#lugar").val();
            let tipo = $("#valorDelTipo").text();
            let clave = ($(this).val()).toUpperCase();
            let primerCaracter = clave[0];
            let id = $(this).attr("id");
                id = id.split("-");
                id = id[1];
        
            if((tipo=="Facturas" && primerCaracter!="F") || (tipo=="Remisiones" && primerCaracter!="R")){
                alert("Solamente se permiten capturar "+ tipo +", verificalo por favor");
                $("#clave-"+ id +"").val("");
                $("#cliente-"+ id +"").val("");
                $("#importe-"+ id +"").val("");
                $("#observaciones-"+ id +"").val("");
                $("#notaDeCredito-"+ id +"").val(0.00);
                //Realizar la suma del total de el contado
                totalContado();
            }
            else{
                enviar(clave, id, tipo, lugar);
            }
            
        }
        
    });

    function enviar(clave, id, tipo, lugar){
    
        var parametros = {
            clave: clave,
            id: id,
            tipo: tipo,
            lugar: lugar
        };
        console.log(parametros);
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/invocarFacturaRemision.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando Factura/Remisión...");
    }

    function llegada(datos){
        console.log(datos);

        switch (datos.estatus){
            case "Sin información":
                alert("La "+datos.documento+" "+ datos.clave + " no existe en la base de datos");
                $("#clave-"+ datos.id +"").val("");
                $("#cliente-"+ datos.id +"").val("");
                $("#importe-"+ datos.id +"").val("");
                $("#observaciones-"+ datos.id +"").val("");
                $("#notaDeCredito-"+ datos.id +"").val(0.00);
                break;
            case "No corresponde":
                alert("La "+datos.documento+" "+ datos.clave + " no pertenece a "+datos.lugar);
                $("#clave-"+ datos.id +"").val("");
                $("#cliente-"+ datos.id +"").val("");
                $("#importe-"+ datos.id +"").val("");
                $("#observaciones-"+ datos.id +"").val("");
                $("#notaDeCredito-"+ datos.id +"").val(0.00);
                break;
            case "Ya capturada":
                alert("La "+datos.documento+" "+ datos.clave + " ya se encuentra capturada en el contado "+datos.folio);
                $("#clave-"+ datos.id +"").val("");
                $("#cliente-"+ datos.id +"").val("");
                $("#importe-"+ datos.id +"").val("");
                $("#observaciones-"+ datos.id +"").val("");
                $("#notaDeCredito-"+ datos.id +"").val(0.00);
                break;
            case "Cancelada":
                alert("La "+datos.documento+" "+ datos.clave + " ya se encuentra cancelada");
                $("#clave-"+ datos.id +"").val("");
                $("#cliente-"+ datos.id +"").val("");
                $("#importe-"+ datos.id +"").val("");
                $("#observaciones-"+ datos.id +"").val("");
                $("#notaDeCredito-"+ datos.id +"").val(0.00);
                break;
            case "Sin permisos":
                    alert("No tiene permiso de capturar la "+datos.documento+" "+ datos.clave);
                    $("#clave-"+ datos.id +"").val("");
                    $("#cliente-"+ datos.id +"").val("");
                    $("#importe-"+ datos.id +"").val("");
                    $("#observaciones-"+ datos.id +"").val("");
                    $("#notaDeCredito-"+ datos.id +"").val(0.00);
                    break;
            case "Correcto":
                $("#clave-"+ datos.id +"").val(datos.clave);
                $("#cliente-"+ datos.id +"").val(datos.cliente);
                $("#importe-"+ datos.id +"").val(formatNumber.new(datos.importe, "$"));
                break;
        
            default:
                break;
        }

        //Realizar la suma del total de el contado
        totalContado();
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});