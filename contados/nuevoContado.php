<!DOCTYPE html>
<html>
<head>
	<title>Nuevo Contado</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="./../css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloContado.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/visualizacion.js"></script>
    <script type="text/javascript" src="ajax/js/valorDelTipo.js"></script>
	<script type="text/javascript" src="ajax/js/agregarFacturaRemision.js"></script>
	<script type="text/javascript" src="ajax/js/eliminarFacturaRemision.js"></script>
	<script type="text/javascript" src="ajax/js/fechaDeContado.js"></script>
	<script type="text/javascript" src="ajax/js/totalContado.js"></script>
	<script type="text/javascript" src="ajax/eventos/lugar.js"></script>
	<script type="text/javascript" src="ajax/eventos/invocarFacturaRemision.js"></script>
	<script type="text/javascript" src="ajax/eventos/guardarContado.js"></script>
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block img-fluid" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Generar Nuevo Contado</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="visualizacion" class="btn btn-primary" value="Regresar" />
				</div>
			</div>
		</div>
	</header>
	<br />
	<section>
		<div class="container">
			<div class="row">
				<table class="table table-primary table-hover table-sm table-bordered text-center">
					<thead>
						<tr>
							<th colspan="4">Fecha</th>
							<th colspan="4"><input type="text" id="fecha" class="fecha form-control" placeholder="dd/mm/aaaa"/></th>
						</tr>
						<tr>
							<th colspan="4">Lugar</th>
							<th colspan="4">
								<select class="form-control" id="lugar">
									<option selected value=""></option>
									<option value="azcapotzalco">Pozo Brasil</option>
									<option value="tecamac">Tecámac</option>
								</select>
							</th>
						</tr>
						<tr>
							<th colspan="4">Tipo</th>
							<th colspan="4">
								<select class="form-control" id="tipo">
									<option selected value=""></option>
									<option value="facturas">Facturas</option>
									<option value="remisiones">Remisiones</option>
								</select>
							</th>
						</tr>
						<tr>
							<th colspan="8" id="fechaDeContado"></th>
						</tr>
						<tr>
							<th id="valorDelTipo"></th>
							<th>Cliente</th>
							<th>Importe $</th>
                            <th>Caja/Bolsas</th>
                            <th>Peso(Kg)</th>
                            <th>Recibe</th>
							<th>Observaciones</th>
							<th></th>
						</tr>
					</thead>
					<tbody id="factura_remision">
						<tr id="fila-1">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-1"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-1" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-1" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-1" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-1" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-1" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-1" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-1"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-2">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-2"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-2" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-2" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-2" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-2" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-2" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-2" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-2"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-3">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-3"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-3" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-3" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-3" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-3" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-3" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-3" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-3"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-4">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-4"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-4" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-4" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-4" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-4" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-4" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-4" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-4"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-5">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-5"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-5" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-5" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-5" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-5" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-5" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-5" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-5"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-6">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-6"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-6" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-6" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-6" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-6" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-6" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-6" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-6"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-7">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-7"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-7" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-7" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-7" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-7" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-7" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-7" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-7"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-8">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-8"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-8" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-8" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-8" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-8" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-8" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-8" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-8"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-9">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-9"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-9" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-9" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-9" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-9" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-9" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-9" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-9"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-10">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-10"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-10" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-10" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-10" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-10" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-10" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-10" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-10"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-11">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-11"  value=""></td>
							
							<td><input type="text" class="form-control form-control-sm" id="cliente-11" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-11" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-11" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-11" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-11" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-11" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-11"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-12">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-12"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-12" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-12" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-12" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-12" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-12" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-12" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-12"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-13">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-13"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-13" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-13" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-13" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-13" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-13" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-13" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-13"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-14">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-14"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-14" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-14" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-14" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-14" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-14" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-14" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-14"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
						<tr id="fila-15">
							<td><input type="text" class="form-control form-control-sm clave" id="clave-15"  value=""></td>
							<td><input type="text" class="form-control form-control-sm" id="cliente-15" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="importe-15" value="" readonly></td>
							<td><input type="number" class="form-control form-control-sm" id="cajas-15" value="0" ></td>
							<td><input type="number" class="form-control form-control-sm" id="peso-15" value=0.00 ></td>
                            <td><input type="text" class="form-control form-control-sm" id="recibe-15" value="" ></td>
                            <td><input type="text" class="form-control form-control-sm" id="observaciones-15" value="" ></td>
							<td class="text-center"><button class="btn btn-sm eliminarFacturaRemision" id="borrar-15"><img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar factura remision"></button></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="2"></th>
							<th colspan="6" class="text-left">Total</th>
						</tr>
						<tr>
							<td class="text-left"><input type="button" class="btn btn-sm btn-info" id="agregarFacturaRemision" value="Agregar"></td>
							<td class="text-center"><input type="button" class="btn btn-sm btn-success" id='guardarContado' value="Guardar"></td>
							<td class="text-right"><input type="text" class="form-control form-control-sm" id="total" value="$0.00" readonly></td>
							<td colspan="5"></td>
							<input type="hidden" id="contador" value=15 />
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</section>
  
</body>
</html>