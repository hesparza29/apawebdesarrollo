<!DOCTYPE html>
<html>
<head>
	<title>Nueva Nota de Crédito</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="./../css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloNotas.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/visualizacion.js"></script>
	<script type="text/javascript" src="ajax/js/eliminarProducto.js"></script>
	<script type="text/javascript" src="ajax/js/agregarProducto.js"></script>
	<script type="text/javascript" src="ajax/js/totalNota.js"></script>
	<script type="text/javascript" src="ajax/js/adp.js"></script>
	<script type="text/javascript" src="ajax/js/calcularPenalizacion.js"></script>
	<script type="text/javascript" src="ajax/eventos/lugar.js"></script>
	<script type="text/javascript" src="ajax/eventos/tipo.js"></script>
	<script type="text/javascript" src="ajax/eventos/numeroCliente.js"></script>
	<script type="text/javascript" src="ajax/eventos/autocompletarNombreCliente.js"></script>
	<script type="text/javascript" src="ajax/eventos/nombreCliente.js"></script>
	<script type="text/javascript" src="ajax/eventos/invocarProductos.js"></script>
	<script type="text/javascript" src="ajax/eventos/cantidadProductos.js"></script>
	<script type="text/javascript" src="ajax/eventos/guardarNota.js"></script>
	<script type="text/javascript" src="ajax/eventos/listasDePrecio.js"></script>
	<script type="text/javascript" src="ajax/eventos/seleccionarListaDePrecio.js"></script>
	<script type="text/javascript" src="ajax/eventos/permisoListasDePrecio.js"></script>
	<script type="text/javascript" src="ajax/eventos/seleccionarPenalizacion.js"></script>
	<script type="text/javascript" src="ajax/eventos/cancelarPenalizacion.js"></script>
	<script type="text/javascript" src="ajax/eventos/permisoPenalizacion.js"></script>
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block img-fluid" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Generar Nueva Nota de Crédito</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="visualizacion" class="btn btn-primary" value="Regresar" />
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
				<div class="col-sm-12 col-md-4">
					<input type='button' id="seleccionarPenalizacion" class="btn btn-secondary" value="Penalización" 
						data-toggle='modal' data-target='#penalizacion'/>
				</div>
			</div>
		</div>
	</header>
	<br />
	<section>
		<div class="container">
			<div class="row">
				<table class="table table-dark table-hover table-sm table-bordered text-center">
					<thead>
						<tr>
							<th colspan="2">Lugar</th>
							<th colspan="2">
								<select class="form-control" id="lugar">
									<option selected value=""></option>
									<option value="azcapotzalco">Pozo Brasil</option>
									<option value="tecamac">Tecámac</option>
								</select>
							</th>
							<th colspan="2">Motivo</th>
							<th colspan="2">
								<select class="form-control" id="motivo">
									<option value=""></option>
									<option value="ERROR AL SOLICITAR">ERROR AL SOLICITAR</option>
									<option value="ERROR DE VENTAS">ERROR DE VENTAS</option>
									<option value="DEFECTO DE FÁBRICA">DEFECTO DE FÁBRICA</option>
									<option value="MUESTRAS">MUESTRAS</option>
									<option value="RECUPERACIÓN DE MERCANCÍA">RECUPERACIÓN DE MERCANCÍA</option>
									<option value="REFACTURACIÓN">REFACTURACIÓN</option>
									<option value="POR FALTA DE CANCELACIÓN EN EL MES">POR FALTA DE CANCELACIÓN EN EL MES</option>
									<option value="PRECIO MÁS CARO">PRECIO MÁS CARO</option>
									<option value="CAMBIO RAZÓN SOCIAL">CAMBIO RAZÓN SOCIAL</option>
									<option value="PRECIO ESPECIAL">PRECIO ESPECIAL</option>
									<option value="NO CORRESPONDE LA ESPECIFICACIÓN">NO CORRESPONDE LA ESPECIFICACIÓN</option>
									<option value="ERROR DE ALMACÉN">ERROR DE ALMACÉN</option>
									<option value="AFECTACIÓN ADMINISTRATIVA">AFECTACIÓN ADMINISTRATIVA</option>
								</select>
							</th>
						</tr>
						<tr>
							<th colspan="2">Tipo</th>
							<th colspan="2">
								<select class="form-control" id="tipo">
									<option value=''></option>
									<option value='Devolución Parcial'>1.Devolución Parcial</option>
									<option value='Factura Completa'>2.Factura Completa</option>
									<option value='Entrada Caja'>3.Entrada Caja</option>
									<option value='Factor 3'>4.Factor 3</option>
									<option value='Cambio Físico'>5.Cambio Físico</option>
									<option value='Entrada Caja Factor 3'>6.Entrada Caja Factor 3</option>
									<option value='Muestra'>7.Muestra</option>
									<option value='Condonación'>8.Condonación</option>
								</select>
							</th>
							<th colspan="2">Folio</th>
							<th colspan="2"><input type="text" class="form-control" id="folio" value="" readonly></th>
						</tr>
						<tr>
							<th colspan="2">Folio de Recepción</th>
							<th colspan="2"><input type="number" class="form-control" id="folioRecepcion" value="" ></th>
							<th colspan="2">Factura</th>
							<th colspan="2"><input type="text" class="form-control" id="factura" value="" ></th>
						</tr>
						<tr>
							<th colspan="2">Cliente</th>
							<th colspan="2"><input type="text" class="form-control" id="cliente" value="" ></th>
							<th colspan="2">Descuento</th>
							<th colspan="2"><input type="text" class="form-control" id="descuento" value="" readonly></th>
						</tr>
						<tr>
							<th colspan="4">Nombre</th>
							<th colspan="4"><input type="text" class="form-control" id="nombreCliente" value=""></th>
						</tr>
						<tr>
							<th colspan="8">Productos</th>
						</tr>
						<tr>
							<th>Cantidad</th>
							<th>Clave</th>
							<th colspan="2">Devolución</th>
							<th>Costo $</th>
							<th>Importe $</th>
							<th>Subtotal $</th>
							<th></th>
						</tr>
					</thead>
					<tbody id="productos">
						<tr id="fila-1">
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-1"  value=""></td>
							<td><input type="text" class="form-control form-control-sm clave" id="clave-1"  value=""></td>
							<td colspan="2">
								<select class="form-control form-control-sm devolucion" id="devolucion-1">
									<option value=""></option>
									<option value="Merma">Merma</option>
									<option value="Almacen">Almacen</option>
								</select>
							</td>
							<td>
								<div class="input-group mb-3">
									<input type="text" class="form-control form-control-sm adp" id="costo-1" value="" readonly>
									<div class="input-group-append">
										<input type="button" class="btn btn-warning btn-sm listas" 
											data-toggle='modal' data-target='#listas' id="precio-1" value="?">
									</div>
								</div>
							</td>
							<td><input type="text" class="form-control form-control-sm" id="importe-1" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="subtotal-1" value="" readonly></td>
							<td class="text-center">
								<button class="btn btn-sm eliminarProducto" id="borrar-1">
									<img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto">
								</button>
							</td>
							<input type="hidden" id="lista-1"  value="" />
						</tr>
						<tr id="fila-2">
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-2"  value=""></td>
							<td><input type="text" class="form-control form-control-sm clave" id="clave-2"  value=""></td>
							<td colspan="2">
								<select class="form-control form-control-sm devolucion" id="devolucion-2">
									<option value=""></option>
									<option value="Merma">Merma</option>
									<option value="Almacen">Almacen</option>
								</select>
							</td>
							<td>
								<div class="input-group mb-3">
									<input type="text" class="form-control form-control-sm adp" id="costo-2" value="" readonly>
									<div class="input-group-append">
										<input type="button" class="btn btn-warning btn-sm listas" 
											data-toggle='modal' data-target='#listas' id="precio-2" value="?">
									</div>
								</div>
							</td>
							<td><input type="text" class="form-control form-control-sm" id="importe-2" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="subtotal-2" value="" readonly></td>
							<td class="text-center">
								<button class="btn btn-sm eliminarProducto" id="borrar-2">
									<img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto">
								</button>
							</td>
							<input type="hidden" id="lista-2"  value="" />
						</tr>
						<tr id="fila-3">
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-3"  value=""></td>
							<td><input type="text" class="form-control form-control-sm clave" id="clave-3"  value=""></td>
							<td colspan="2">
								<select class="form-control form-control-sm devolucion" id="devolucion-3">
									<option value=""></option>
									<option value="Merma">Merma</option>
									<option value="Almacen">Almacen</option>
								</select>
							</td>
							<td>
								<div class="input-group mb-3">
									<input type="text" class="form-control form-control-sm adp" id="costo-3" value="" readonly>
									<div class="input-group-append">
										<input type="button" class="btn btn-warning btn-sm listas" 
											data-toggle='modal' data-target='#listas' id="precio-3" value="?">
									</div>
								</div>
							</td>
							<td><input type="text" class="form-control form-control-sm" id="importe-3" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="subtotal-3" value="" readonly></td>
							<td class="text-center">
								<button class="btn btn-sm eliminarProducto" id="borrar-3">
									<img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto">
								</button>
							</td>
							<input type="hidden" id="lista-3"  value="" />
						</tr>
						<tr id="fila-4">
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-4"  value=""></td>
							<td><input type="text" class="form-control form-control-sm clave" id="clave-4"  value=""></td>
							<td colspan="2">
								<select class="form-control form-control-sm devolucion" id="devolucion-4">
									<option value=""></option>
									<option value="Merma">Merma</option>
									<option value="Almacen">Almacen</option>
								</select>
							</td>
							<td>
								<div class="input-group mb-3">
									<input type="text" class="form-control form-control-sm adp" id="costo-4" value="" readonly>
									<div class="input-group-append">
										<input type="button" class="btn btn-warning btn-sm listas" 
											data-toggle='modal' data-target='#listas' id="precio-4" value="?">
									</div>
								</div>
							</td>
							<td><input type="text" class="form-control form-control-sm" id="importe-4" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="subtotal-4" value="" readonly></td>
							<td class="text-center">
								<button class="btn btn-sm eliminarProducto" id="borrar-4">
									<img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto">
								</button>
							</td>
							<input type="hidden" id="lista-4"  value="" />
						</tr>
						<tr id="fila-5">
							<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-5"  value=""></td>
							<td><input type="text" class="form-control form-control-sm clave" id="clave-5"  value=""></td>
							<td colspan="2">
								<select class="form-control form-control-sm devolucion" id="devolucion-5">
									<option value=""></option>
									<option value="Merma">Merma</option>
									<option value="Almacen">Almacen</option>
								</select>
							</td>
							<td>
								<div class="input-group mb-3">
									<input type="text" class="form-control form-control-sm adp" id="costo-5" value="" readonly>
									<div class="input-group-append">
										<input type="button" class="btn btn-warning btn-sm listas" 
											data-toggle='modal' data-target='#listas' id="precio-5" value="?">
									</div>
								</div>
							</td>
							<td><input type="text" class="form-control form-control-sm" id="importe-5" value="" readonly></td>
							<td><input type="text" class="form-control form-control-sm" id="subtotal-5" value="" readonly></td>
							<td class="text-center">
								<button class="btn btn-sm eliminarProducto" id="borrar-5">
									<img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto">
								</button>
							</td>
							<input type="hidden" id="lista-5"  value="" />
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5" id="informacionPenalizacion"></td>
							<td class="text-left">Subtotal</td>
							<td class="text-right"><input type="text" class="form-control form-control-sm" id="subtotal"  value="$0.00" readonly></td>
							<td></td>
						</tr>
						<tr>
							<td colspan="5" rowspan="2">
								<label for="observaciones" class="form-label">Observaciones</label>
								<textarea class="form-control" id="observaciones" rows="3" placeholder="Breve descripción..."></textarea>
							</td>
							<td class="text-left">Iva</td>
							<td class="text-right"><input type="text" class="form-control form-control-sm" id="iva"  value="$0.00" readonly></td>
							<td></td>
						</tr>
						<tr>
							<td class="text-left">Total</td>
							<td class="text-right"><input type="text" class="form-control form-control-sm" id="total"  value="$0.00" readonly></td>
							<td></td>
						</tr>
						<tr>
							<td colspan="5"></td>
							<td colspan="2" class="text-center"><input type="button" class="btn btn-sm btn-info" id="agregarProducto" value="Agregar Producto"></td>
							<td class="text-rigth"><input type="button" class="btn btn-sm btn-success" id='guardarNota' value="Guardar"></td>
							<input type="hidden" id="contador" value=5 />
							<input type="hidden" id="valorDePenalizacion" value=0 />
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</section>

	<!--Ventana modal para consultar las listas de precio-->
	<div class="modal fade" id="listas" tabindex="-1" role="dialog" aria-labelledby="listasDePrecio" aria-hidden="true">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="listasDePrecio">Listas de precio</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div id="contenidoModalDeListas">
			</div>
		</div>
		</div>
	</div>
	<!--Ventana modal para seleccionar una penalización-->
	<div class="modal fade" id="penalizacion" tabindex="-1" role="dialog" aria-labelledby="penalizaciones" aria-hidden="true">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="penalizaciones">Penalización</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div id="contenidoModalPenalizacion">
			</div>
		</div>
		</div>
	</div>
  
</body>
</html>
