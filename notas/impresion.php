<!DOCTYPE html>
<html>
  <head>
  	<title>Impresión Nota de Crédito</title>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estiloNotas.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/verificarSesion.js"></script>
    <script type="text/javascript" src="../js/cierreSesion.js"></script>
    <script type="text/javascript" src="../js/cierreInactividad.js"></script>
    <script type="text/javascript" src="../js/formatoNumero.js"></script>
    <script type="text/javascript" src="../js/visualizacion.js"></script>
    <script type="text/javascript" src="../js/imprimir.js"></script>
    <script type="text/javascript" src="ajax/js/agregarModalCancelar.js"></script>
    <script type="text/javascript" src="ajax/js/cerrarCancelarNota.js"></script>
    <script type="text/javascript" src="ajax/eventos/acciones.js"></script>
    <script type="text/javascript" src="ajax/eventos/guardarSAE.js"></script>
    <script type="text/javascript" src="ajax/eventos/cancelarNota.js"></script>
    <script type="text/javascript" src="ajax/eventos/guardarSAECancelada.js"></script>
    <script type="text/javascript" src="ajax/eventos/cargarPartidas.js"></script>
  </head>
  <body>
  <header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
          <input type="button" class="btn btn-success" id="visualizacion" value="Regresar" />
				</div>
				<div class="col-sm-12 col-md-6">
          
				</div>
				<div class="col-sm-12 col-md-2">
          <input class="btn btn-info" type="button" id="imprimir" value="Imprimir" />
        </div>
        <div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-3">
          
				</div>
        <div class="col-sm-12 col-md-3">
          <span class="sae"></span>
				</div>
        <div class="col-sm-12 col-md-3">
          <span class="cancelar"></span>
				</div>
				<div class="col-sm-12 col-md-3">
          
				</div>
			</div>
		</div>
  </header>
  <br />
  <section>
		<div class="container">		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-sm table-bordered table-striped text-center">
								<thead>
                  <tr>
                    <th colspan="6"><img src="./../imagenes/apa.jpg" class="img-fluid" alt="apa"></th>
                    <th>MES</th>
                    <th id="mes"></th>
                    <th>FECHA</th>
                    <th id="fecha"></th>
                  </tr>
                  <tr>
                    <th colspan="6">REPORTE DE ENTRADA DE MERCANCIA</th>
                    <th>TIPO</th>
                    <th id="tipo"></th>
                    <th>FOLIO</th>
                    <th id="folio"></th>
                  </tr>
                  <tr>
                    <th colspan="4" id="nombreCliente"></th>
                    <th colspan="2" id="vendedor"></th>
                    <th>FOLIO DE RECEPCIÓN</th>
                    <th id="folioRecepcion"></th>
                    <th>CLIENTE</th>
                    <th id="cliente"></th>
                  </tr>
                  <tr>
                    <th>CANTIDAD</th>
                    <th>CLAVE</th>
                    <th>DEVOLUCIÓN</th>
                    <th>FACTURA</th>
                    <th colspan="2">MOTIVO</th>
                    <th>COSTO</th>
                    <th>IMPORTE</th>
                    <th>DESCUENTO</th>
                    <th>SUBTOTAL</th>
                  </tr>
                </thead>        
								<tbody id="table">

                </tbody>
                <tfoot>
                  <tr>
                    <td colspan=7 rowspan=3><strong id="totalConLetra"></strong></td>
                    <td colspan=2><strong>SUBTOTAL</strong></td>
                    <td id="subtotal"></td>
                  </tr>
                  <tr>
                    <td colspan=2><strong>IVA</strong></td>
                    <td id="iva"></td>
                  </tr>
                  <tr>
                    <td colspan=2><strong>TOTAL</strong></td>
                    <td id="total"></td>
                  </tr>
                  <tr>
                    <td colspan="5">
                      <strong>ELABORO</strong>
                      <br />
                      <strong id="usuario"></strong>
                      <br />
                      <strong>DEPARTAMENTO DE VENTAS</strong>
                    </td>
                    <td colspan="5">
                      <strong>RECIBE</strong>
                      <br />
                      <strong>FERNANDA CARBALLEDA</strong>
                      <br />
                      <strong>DEPARTAMENTO DE CRÉDITO</strong>
                    </td>
                  </tr>
                  <input type="hidden" id="notaSAE" value="" />
                  <input type="hidden" id="notaEstatus" value="" />
                </tfoot>
							</table>
						</div>
					</div>
				</div>			
		</div>
	</section>	
  <!--Ventana modal para ingresar la captura de SAE-->
  <div class="modal fade" id="SAE" tabindex="-1" role="dialog" aria-labelledby="capturaSAE" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="capturaSAE">Captura Número SAE</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form enctype="multipart/form-data" id="formularioSAE" method="post">
            <div class="form-row">
              <div class="form-group col-sm-12 col-md-12">
                <label for="notaSAEForm">Nota SAE</label>
                <input type="text" class="form-control" id="notaSAEForm" name="notaSAEForm" placeholder="DD...." />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-sm-12 col-md-12">
                <label for="pdf">PDF</label>
                <input class="form-control" type="file" id="pdf" name="pdf"/>
              </div>
            </div>
            <input type="hidden" id="folioNota" name="folioNota" value="" />
          </form>
        </div>
        <div class="modal-footer">
          <input type="button" class="btn btn-success" id="guardarSAE" value="Guardar">
        </div>
      </div>
    </div>
  </div>
  <!--Ventana modal para cancelar notas de crédito-->
  <div class="modal fade" id="Cancelar" tabindex="-1" role="dialog" aria-labelledby="cancelaNota" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cancelaNota">Cancelar nota de crédito</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="contenidoModalCancelar">
        </div>
      </div>
    </div>
  </div>
  </body>
</html>