<?php
require_once("../funciones.php");
session_start();
$usuario = $_SESSION["user"];
$modulo = "Nota";
$permiso = "";
// obtiene los valores para realizar la paginacion
$limit = isset($_POST["limit"]) && intval($_POST["limit"]) > 0 ? intval($_POST["limit"])	: 10;
$offset = isset($_POST["offset"]) && intval($_POST["offset"])>=0	? intval($_POST["offset"])	: 0;

$base = conexion_local();
// realiza la conexion
//$con = new mysqli("50.62.209.84","hesparza","b29194303","aplicacion");
$con = new mysqli("localhost","root","","aplicacion");
$con->set_charset("utf8");
//$base = new PDO('mysql:host=localhost; dbname=aplicacion', 'root', '');
//$base = new PDO("mysql:host=50.62.209.117;dbname=aplicacion","hesparza","b29194303");
//$base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
//$base->exec("SET CHARACTER SET utf8");

// array para devolver la informacion
$json = array();
$data = array();
//consulta que deseamos realizar a la db
//$query = $con->prepare("select id_usuario,nombres,apellidos from  usuarios limit ? offset ?");

//Obtener las notas que puede ver el usuario
$consultaPermiso = "SELECT Identificador FROM USUARIO 
					INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
					INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
					WHERE Usuario=? AND MODULO.Nombre=?";
$resultadoPermiso = $base->prepare($consultaPermiso);
$resultadoPermiso->execute(array($usuario, $modulo));
$registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
$resultadoPermiso->closeCursor();
$permiso = $registroPermiso["Identificador"];

//El limite empieza con 10 y el Offset con 0
switch ($permiso){
	case 'administrador':
		$query = $con->prepare("SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre, 
								NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
								Pdf, PdfCancelada 
								FROM NOTA INNER JOIN USUARIO
								ON NOTA.idUsuario=USUARIO.idUsuario 
								INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
								ORDER BY idNota DESC LIMIT ? OFFSET ?");
		$query->bind_param("ii",$limit,$offset);
		$query->execute();
		break;
	
	default:
		$query = $con->prepare("SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre, 
								NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
								Pdf, PdfCancelada 
								FROM NOTA INNER JOIN USUARIO
								ON NOTA.idUsuario=USUARIO.idUsuario 
								INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
								WHERE Lugar=? ORDER BY idNota DESC LIMIT ? OFFSET ?");
		$query->bind_param("sii",$permiso,$limit,$offset);
		$query->execute();
		break;
}



// vincular variables a la sentencia preparada                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
$query->bind_result($folio, $folioRecepcion, $idCliente, $nombreCliente, $notaSAE, $total, 
						$fecha, $estatus, $nombre, $apellido, $pdf, $pdfCancelada);

// obtener valores
while ($query->fetch()) {
	$data_json = array();

	$data_json["folio"] = $folio;
	$data_json["folioRecepcion"] = $folioRecepcion;
	$data_json["cliente"] = $idCliente . " " . $nombreCliente;
	$data_json["notaSAE"] = $notaSAE;
	$data_json["total"] = $total;
	$data_json["fecha"] = fechaStandar($fecha);
	$data_json["estatus"] = $estatus;
	switch ($estatus) {
		case 'Revisando':
			$data_json["clase"] = "revisando";
			break;
		case 'Cancelada':
			$data_json["clase"] = "cancelada";
			break;
		case 'Correcta':
			$data_json["clase"] = "correcta";
			break;
		
	}
	$data_json["usuario"] = $nombre . " " . $apellido;
	$data_json["pdf"] = $pdf;
	$data_json["pdfCancelada"] = $pdfCancelada;
	$data[]=$data_json;
}

$base = null;

switch ($permiso) {
	case 'administrador':
		// obtiene la cantidad de registros
		$cantidad_consulta = $con->query("select count(*) as total from NOTA");
		$row = $cantidad_consulta->fetch_assoc();
		$cantidad['cantidad']=$row['total'];
		break;
	
	default:
		// obtiene la cantidad de registros
		$cantidad_consulta = $con->query("select count(*) as total FROM NOTA WHERE Lugar='$permiso'");
		$row = $cantidad_consulta->fetch_assoc();
		$cantidad['cantidad']=$row['total'];
		break;
}


$json["lista"] = array_values($data);
$json["cantidad"] = array_values($cantidad);

// envia la respuesta en formato json
header("Content-type:application/json; charset = utf-8");
echo json_encode($json);
exit();
?>
