<!DOCTYPE html>
<html>

<head>
	<title>Visualización Notas de Crédito</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloNotas.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/paginator.min.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/recargarPagina.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/verImpresion.js"></script>
	<script type="text/javascript" src="ajax/js/nuevaNota.js"></script>
	<script type="text/javascript" src="ajax/js/mainNotas.js"></script>
	<script type="text/javascript" src="ajax/js/vistaPdf.js"></script>
	<script type="text/javascript" src="ajax/js/vistaPdfCancelada.js"></script>
	<script type="text/javascript" src="ajax/eventos/acciones.js"></script>
	<script type="text/javascript" src="ajax/eventos/filtroNotas.js"></script>
	<script type="text/javascript" src="ajax/eventos/notasSinSae.js"></script>
</head>

<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Visualización Notas de Crédito</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="home" class="btn btn-primary" style='background:url("../imagenes/home3.jpg"); width: 50px; height: 50px;' />
				</div>
				<div class="col-sm-12 col-md-6">
						<h3 class="text-left">Filtro de Búsqueda</h3>
				</div>
				<div class="col-sm-12 col-md-2">
					<span class="nuevaNota"></span>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="container">
				<div class="row form-group">
					<div class="col-sm-12 col-md-4">
						<input type="text" id="numeroCliente" class="numeroCliente form-control" placeholder="Número de Cliente"/>
					</div>
					<div class="col-sm-12 col-md-4">
						<input type="text" id="nombreCliente" class="nombreCliente form-control" placeholder="Nombre de Cliente"/>
					</div>
					<div class="col-sm-12 col-md-4">
						<input type="text" id="fecha" class="fecha form-control" placeholder="Fecha"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-12 col-md-4">
						<input type="text" id="folio" class="folio form-control" placeholder="Folio"/>
					</div>
					<div class="col-sm-12 col-md-4">
						<input type="text" id="folioRecepcion" class="folioRecepcion form-control" placeholder="Folio de Recepción"/>
					</div>
					<div class="col-sm-12 col-md-4">
						<input type="text" id="clave" class="clave form-control" placeholder="Clave de Producto"/>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-12 col-md-4">
						
					</div>
					<div class="col-sm-12 col-md-4" id="botonesBusqueda">
						<input type="button" class="btn btn-primary" id="buscar" value="Buscar" />
						<input type="button" class="btn btn-primary" id="recargar" value='Tabla Completa' />
						<span class="notasSinSae"></span>
					</div>
					<div class="col-sm-12 col-md-4">
						
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-12 col-md-12 text-center">
						<ul class="pagination" id="paginador"></ul>
					</div>
				</div>		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead class="thead-dark">
									<th>Folio</th>
									<th>Folio Recepción</th>
									<th>Cliente</th>
									<th>Número SAE</th>
									<th>Total</th>
									<th>Fecha</th>
									<th>Estatus</th>
									<th>Usuario</th>
									<th>Info</th>
								</thead>
								<tbody id="table">

								</tbody>
							</table>
						</div>
					</div>
				</div>			
		</div>
	</section>
</body>

</html>