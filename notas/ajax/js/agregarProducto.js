$(document).ready(function(){
    $("#agregarProducto").click(function(){
        //Obtener el siguiente id que se va a asignar
        let id = parseInt($("#contador").val())+1;
        //Construir el contenido que se va a agregar
        let contenido = '<tr id="fila-'+id+'">'+
							'<td><input type="number" class="form-control form-control-sm cantidad" id="cantidad-'+id+'"  value=""></td>'+
							'<td><input type="text" class="form-control form-control-sm clave" id="clave-'+id+'"  value=""></td>'+
							'<td colspan="2">'+
								'<select class="form-control form-control-sm devolucion" id="devolucion-'+id+'">'+
									'<option value=""></option>'+
									'<option value="Merma">Merma</option>'+
									'<option value="Almacen">Almacen</option>'+
								'</select>'+
							'</td>'+
							'<td>'+
								'<div class="input-group mb-3">'+
									'<input type="text" class="form-control form-control-sm adp" id="costo-'+id+'" value="" readonly>'+
									'<div class="input-group-append">'+
										'<input type="button" class="btn btn-warning btn-sm listas"'+ 
											'data-toggle="modal" data-target="#listas" id="precio-'+id+'" value="?">'+
									'</div>'+
								'</div>'+
							'</td>'+
							'<td><input type="text" class="form-control form-control-sm" id="importe-'+id+'" value="" readonly></td>'+
							'<td><input type="text" class="form-control form-control-sm" id="subtotal-'+id+'" value="" readonly></td>'+
							'<td class="text-center">'+
								'<button class="btn btn-sm eliminarProducto" id="borrar-'+id+'">'+
									'<img src="./../imagenes/borrar.PNG" class="img-fluid" alt="eliminar producto">'+
								'</button>'+
							'</td>'+
							'<input type="hidden" id="lista-'+id+'"  value="" />'+
						'</tr>';
        //Agregar el contenido
        $("#productos").append(contenido);
        //Reasignar el contador de productos con el nuevo valor
        $("#contador").val(id);
    });
});