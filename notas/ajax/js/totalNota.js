function totalNota() {
    //Obtener el número de facturas/remisiones
    let contador = $("#contador").val();
    let subtotal = 0.0;
    let iva = 0.0;
    let total = 0.0;
    let aux = 0.0;
    //Realizar la suma solamente de aquellos importes que existan
    for (let i = 1; i <= contador; i++) {
      //Verificar que la clave del producto sea diferente de undefined y además sea diferente de vacio
      if ($("#clave-" + i + "").val() != undefined) {
          if($("#subtotal-" + i + "").val()!=""){
              aux = $("#subtotal-" + i + "").val();
              aux = aux.replace("$", "");
              aux = aux.replace(",", "");
              subtotal += parseFloat(aux);
          }
      }
    }
    switch($("#tipo").val()){
      case "Factor 3":
        //Redondear el resultado a 2 cifras
        subtotal = Math.round(subtotal * 100) / 100;
        //Obtener el iva
        iva = 0;
        //Obtener el total
        total = Math.round((subtotal+iva) * 100) / 100;
        //Darle formato a las cantidades obtenidas
        subtotal = formatNumber.new(subtotal, "$");
        iva = formatNumber.new(iva, "$");
        total = formatNumber.new(total, "$");
        break;
      case "Entrada Caja Factor 3":
        //Redondear el resultado a 2 cifras
        subtotal = Math.round(subtotal * 100) / 100;
        //Obtener el iva
        iva = 0;
        //Obtener el total
        total = Math.round((subtotal+iva) * 100) / 100;
        //Darle formato a las cantidades obtenidas
        subtotal = formatNumber.new(subtotal, "$");
        iva = formatNumber.new(iva, "$");
        total = formatNumber.new(total, "$");
        break;
      case "Muestra":
        //Redondear el resultado a 2 cifras
        subtotal = 0;
        //Obtener el iva
        iva = 0;
        //Obtener el total
        total = Math.round((subtotal+iva) * 100) / 100;
        //Darle formato a las cantidades obtenidas
        subtotal = formatNumber.new(subtotal, "$");
        iva = formatNumber.new(iva, "$");
        total = formatNumber.new(total, "$");
        break;
      default:
        //Redondear el resultado a 2 cifras
        subtotal = Math.round(subtotal * 100) / 100;
        //Obtener el iva
        iva = Math.round((subtotal*.16) * 100) / 100;
        //Obtener el total
        total = Math.round((subtotal+iva) * 100) / 100;
        //Darle formato a las cantidades obtenidas
        subtotal = formatNumber.new(subtotal, "$");
        iva = formatNumber.new(iva, "$");
        total = formatNumber.new(total, "$");
        break;
    }
    //Actualizar los valores de las cantidades
    $("#subtotal").val(subtotal);
    $("#iva").val(iva);
    $("#total").val(total);
  }