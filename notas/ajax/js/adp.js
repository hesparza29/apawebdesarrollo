$(document).ready(function(){
    $('#productos').on('change', '.adp', function(){

        let costo = $(this).val();
        let importe = 0;
        let subtotal = 0;
        let id = $(this).attr("id");
            id = id.split("-");
            id = id[1];
        let descuento = $("#descuento").val();
            descuento = descuento.split("%");
            descuento = parseFloat(descuento[0])/100;
        let clave = $("#clave-"+id).val();
        let cantidad = $("#cantidad-"+id).val();
        if(cantidad<1){
            cantidad = 1;  
        }

        if(clave=="ADP" && !isNaN(costo)){
            importe = Math.round((costo*cantidad)*100)/100;
            subtotal = Math.round((importe-(importe*descuento))*100)/100;
            $("#costo-"+id).val(formatNumber.new(costo, "$"));
            $("#importe-"+id).val(formatNumber.new(importe, "$"));
            $("#subtotal-"+id).val(formatNumber.new(subtotal, "$"));
            $("#lista-"+id).val(costo);
            //Realizar la suma del total de la nota
            totalNota();
        }
        else{
            alert("Introduce un número sin es pacios ni signos, por favor");
            $(this).val("");
        }
    });
});