$(document).ready(function(){
    $("#table").on('click', '.pdf', function(){
        //Obteniendo el id de la solicitud
        let id = $(this).attr("id");
        id = id.split("-");
        id = id[1];
        //Obteniendo el folio de la solicitud
        let folio = $("#folio-"+id+"").text();
        //Obteniendo el nombre del archivo
        let archivo = $("#archivoPdf-"+id+"").attr('value');
        
        switch (archivo) {
            case "":
                alert("La nota de crédito con folio "+folio+" aún no cuenta con un pdf asignado");
                break;
        
            default:
                //Desarrollo
                let url = window.location.host+"/apawebdesarrollo/notas/cargas/"+archivo;
                //Produccion
                //let url = "apawebdesarrollo.com/cobros/cargas/"+archivo;
                window.open('http://'+url+'', '_blank');
                break;
        }
    });
});