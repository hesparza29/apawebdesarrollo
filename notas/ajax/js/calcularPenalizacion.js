$(document).ready(function(){
    $("body").on('click', "#calcularPenlaizacion", function(){
        let contador = $("#contador").val();
        let producto = false;
        let penalizacion = $("#opcionPenalizacion").val()/100;
        let total = 0;

        if($("#opcionPenalizacion").val()=="" && $("#cliente").val()=="" && $("#descuento").val()==""){
            alert("Selecciona una opción de penalización y un cliente, por favor");
        }
        else{
            for (let i = 1; i <= contador; i++) {
                if($("#clave-"+i+"").val()!=undefined && $("#clave-"+i+"").val()!="" && $("#costo-"+i+"").val()!=""){
                    producto = true;
                    let costo = $("#costo-"+i+"").val();
                        costo = costo.replace(',', "");
                        costo = costo.split("$");
                        costo = parseFloat(costo[1]);
                    let importe = 0;
                    let subtotal = 0;
                    let cantidad = 1;
                    let descuento = $("#descuento").val();
                        descuento = descuento.split("%");
                        descuento = descuento[0]/100;
                    let contenido = "";
                    if($("#cantidad-"+i+"").val()>0){
                        cantidad = $("#cantidad-"+i+"").val();
                    }
                    
                    //Obteniendo el total del cargo
                    total += Math.round((costo*penalizacion)*100)/100;
                    //Obteniendo el costo con penalizacion
                    costo = Math.round((costo-(costo*penalizacion))*100)/100;
                    //Obteniendo el importe con penalizacion
                    importe = Math.round((costo*cantidad)*100)/100;
                    //Obteniendo el subtotal con penalizacion
                    subtotal = Math.round((importe-(importe*descuento))*100)/100;
                    //Asignación de los nuevos valores
                    $("#costo-"+i+"").val(formatNumber.new(costo, "$"));
                    $("#importe-"+i+"").val(formatNumber.new(importe, "$"));
                    $("#subtotal-"+i+"").val(formatNumber.new(subtotal, "$"));
                    $("#valorDePenalizacion").val(penalizacion*100);
                    //Agregando la informacion de la penalizacion
                    contenido += '<table class="table table-dark table-hover table-sm table-bordered text-center">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Cantidad</th>'+
                                            '<th>Clave</th>'+
                                            '<th>Cargo Total</th>'+
                                            '<th>&nbsp;</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '<tr>'+
                                            '<td>1</td>'+
                                            '<td>CARG'+$("#valorDePenalizacion").val()+'</td>'+
                                            '<td>'+formatNumber.new(total, "$")+'</td>'+
                                            '<td><input type="button" class="btn btn-danger btn-sm" '+
                                                'id="cancelarPenalizacion" value="Cancelar" /></td>'+
                                        '</tr>'+
                                    '</tbody>'+
                                '</table>';
                    $("#informacionPenalizacion").empty();
                    $("#informacionPenalizacion").append(contenido);
                }
            }
            if(producto==false){
                alert("Introduce al menos 1 producto, por favor");
            }
            //Realizar la suma del total de la nota
            totalNota();
        }
        //Ocultar el modal de la penalizacion
        $("#penalizacion").modal('hide');
    });
});