$(document).ready(function(){
    $(".cancelar").on('click', '#cancelar', function(){
        let contenido = "";

        if($("#notaSAE").val()!=""){
            contenido += '<div class="modal-body">'+
                            '<form enctype="multipart/form-data" id="formularioCancelaSAE" method="post">'+
                            '<div class="form-row">'+
                                '<div class="form-group col-sm-12 col-md-12">'+
                                '<label for="pdfCancelada">PDF de Cancelación</label>'+
                                '<input class="form-control" type="file" id="pdfCancelada" name="pdfCancelada"/>'+
                                '</div>'+
                            '</div>'+
                            '<input type="hidden" id="folioNotaCancelada" name="folioNotaCancelada" value="" />'+
                            '</form>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                            '<input type="button" class="btn btn-success" id="guardarSAECancelada" value="Guardar">'+
                        '</div>';
        }
        else{
            contenido += '<div class="modal-body">'+
                            '<form>'+
                                '<div class="form-row">'+
                                        '<div class="form-group col-sm-12 col-md-6">'+
                                            '<input type="button" class="btn btn-success" id="cancelarNota" value="Sí">'+
                                        '</div>'+
                                        '<div class="form-group col-sm-12 col-md-6 text-right">'+
                                            '<input type="button" class="btn btn-danger" id="cerrarCancelarNota" value="No">'+
                                        '</div>'+
                                '</div>'+
                            '</form>'+
                        '</div>';
        }
        
        $("#contenidoModalCancelar").empty();
        $("#contenidoModalCancelar").append(contenido);
    });
});