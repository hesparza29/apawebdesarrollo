<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $estatus = "Correcto";
    $modulo = "Nota Nueva";
    $identificador = "penalizacion";
    $datos = array();
    

    $base = conexion_local();
    
    //Consulta para obtener el permiso del usuario para ver listas de precios
    $consultaPermiso = "SELECT MODULO.idModulo FROM MODULO 
                            INNER JOIN USUARIO_MODULO ON MODULO.idModulo=USUARIO_MODULO.idModulo 
                            INNER JOIN USUARIO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario 
                            WHERE Usuario=? AND MODULO.Nombre=? AND MODULO.Identificador=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo, $identificador));
    $datos["permiso"] = $resultadoPermiso->rowCount();
    
    switch ($resultadoPermiso->rowCount()){
        case "1":
            $estatus = "Correcto";
            break;
        
        default:
            $estatus = "Sin permiso";
            break;
    }

    $base = null;
    
    $datos["estatus"] = $estatus;

    echo json_encode($datos);

?>