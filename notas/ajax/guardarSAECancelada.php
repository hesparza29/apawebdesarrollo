<?php

    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folioNotaCancelada"];
    $archivo = $_FILES['pdfCancelada']['name'];
    $ruta = $_FILES['pdfCancelada']['tmp_name'];
    $destino = "..\\cargas\\".$archivo;
    $info = new SplFileInfo($archivo);
    $tipo =  $info->getExtension();
    $estatus = "Correcto";
    $estatusNota = "Cancelada";
    $datos = array();
    $base = conexion_local();

    if($tipo=="pdf"){
        //Consulta para guardar la nota sae
        $consultaGuardarSAE = "UPDATE NOTA SET Estatus=?, NotaSAE=?, PdfCancelada=? WHERE Folio=?";
        $resultadoGuardarSAE = $base->prepare($consultaGuardarSAE);
        $resultadoGuardarSAE->execute(array($estatusNota, $estatusNota, $archivo, $folio));

        switch ($resultadoGuardarSAE->rowCount()){
            case 1:
                move_uploaded_file($ruta, $destino);
                break;
            
            case 0:
                $estatus = "Error";
                break;
        }
        $resultadoGuardarSAE->closeCursor();
    }
    else{
        $estatus = "Archivo invalido";
        $datos["tipo"] = $tipo;
    }
    $base = null;

    $datos["estatus"] = $estatus;
    $datos["folio"] = $folio;

    echo json_encode($datos);
?>