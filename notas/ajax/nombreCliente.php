<?php
    require_once("../../funciones.php");
    $nombreCliente = $_POST["nombreCliente"];
    $estatus = "";
    $datos = array();

    $base = conexion_local();

    //Consulta para obtener la información del cliente
    $consultaCliente = "SELECT idCliente, Porcentaje FROM CLIENTE 
                        INNER JOIN DESCUENTO ON CLIENTE.idDescuento=DESCUENTO.idDescuento 
                        WHERE Nombre=? LIMIT 1";
    $resultadoCliente = $base->prepare($consultaCliente);
    $resultadoCliente->execute(array($nombreCliente));
    if ($resultadoCliente->rowCount()==1){
        $registroCliente = $resultadoCliente->fetch(PDO::FETCH_ASSOC);
        $estatus = "Correcto";
        $datos["cliente"] = $registroCliente["idCliente"];
        $datos["descuento"] = $registroCliente["Porcentaje"];
    }
    else{
        $estatus = "Sin información";
    }
    $resultadoCliente->closeCursor();

    $datos["estatus"] = $estatus;
    $datos["nombreCliente"] = $nombreCliente;

    $base = null;
    echo json_encode($datos);
    
?>