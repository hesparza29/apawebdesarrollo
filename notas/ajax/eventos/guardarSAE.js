$(document).ready(function(){
    
    $("#guardarSAE").click(function(){

        if($("#notaSAEForm").val()!="" && $("#pdf").val()!=""){
            enviar();
        }
        else{
            alert("Captura todos los campos, por favor");
        }
    });

    function enviar() {
        //Asignar el folio de la nota al formulario de la captura SAE
        $("#folioNota").val($("#folio").text());
        var formData = new FormData(document.getElementById("formularioSAE"));

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: false, //Especificaremos el tipo de contenido
            url: "ajax/guardarSAE.php", //Sera el archivo que va a procesar la petición AJAX
            data: formData,
            cache: false,
            processData: false,
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 10000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando guardado del número SAE...");
    }

    function llegada(datos) {
        console.log(datos);
        switch (datos.estatus){
            case "Correcto":
                alert("Se pudo capturar de manera exitosa la nota de SAE "+datos.notaSAE+" para la nota de crédito "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            case "Error":
                alert("Ocurrio un error al capturar la nota de SAE "+datos.notaSAE+" para la nota de crédito "+datos.folio+" intentalo más tarde, por favor");
                break;
            case "Archivo invalido":
                alert("El archivo introducido es de tipo "+datos.tipo+" solamente se permiten introducir archivos pdf, verificalo por favor");
                break;
        }
        
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});