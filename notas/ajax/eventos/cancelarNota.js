$(document).ready(function(){
    $("#contenidoModalCancelar").on('click', '#cancelarNota', function(){
        enviar();
        
    });

    function enviar() {
        var parametros = {
            folio: $("#folio").text(),
        };

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/cancelarNota.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando cancelación de Nota...");
    }

    function llegada(datos) {
        console.log(datos);

        switch(datos.estatus){
            case "Correcto":
                alert("Se pudo cancelar de menera correcta la nota de crédito con folio "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            case "Error":
                alert("Ocurrio un error al tratar de cancelar la nota de crédito con folio "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
        }
        
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});