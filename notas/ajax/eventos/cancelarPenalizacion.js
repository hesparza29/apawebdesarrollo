$(document).ready(function(){
    $("body").on('click', '#cancelarPenalizacion', function(){

        let cantidad = [];
        let lista = [];
        let contador = $("#contador").val();

        for (let i = 1; i <= contador; i++) {
            //Verificar que la clave del producto sea diferente de undefined y además sea diferente de vacio
            if($("#clave-"+i+"").val()!=undefined && $("#clave-"+i+"").val()!=""){
                
                cantidad.push($("#cantidad-"+i+"").val());
                lista.push($("#lista-"+i+"").val());  
            }
        }
        //Verificar que se haya capturado por lo menos un producto con su cantidad y su respectiva devolución
        if(cantidad.length==0 && lista.length==0){
            alert("Captura al menos un producto, por favor");
        }
        else if($("#cliente").val()=="" && $("#descuento").val()=="") {
            alert("Selecciona un cliente, por favor");
        }
        else{
            let descuento = $("#descuento").val();
                descuento = descuento.split("%");
                descuento = descuento[0];
            enviar(cantidad, lista, descuento);
        }
    });

    function enviar(cantidad, lista, descuento){
    
        var parametros = {
            cantidad: cantidad,
            lista: lista,
            descuento: descuento,
        };
    
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/cancelarPenalizacion.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando cancelacion de la penalizacion...");
    }

    function llegada(datos){
        console.log(datos);

        let contador = Object.keys(datos.costo).length;
        for (let i = 0; i < contador; i++) {
            $("#costo-"+(parseInt(i)+1)+"").val(formatNumber.new(datos.costo[i], "$"));
            $("#importe-"+(parseInt(i)+1)+"").val(formatNumber.new(datos.importe[i], "$"));
            $("#subtotal-"+(parseInt(i)+1)+"").val(formatNumber.new(datos.subtotal[i], "$"));
        }

        $("#informacionPenalizacion").empty();
        $("#valorDePenalizacion").val(0);

        //Realizar la suma del total de la nota
        totalNota();
        
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});