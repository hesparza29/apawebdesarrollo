$(document).ready(function(){
    $("#productos").on('click', '.listas', function(){
        let id = $(this).attr("id");
                id = id.split("-");
                id = id[1];
        let clave = $("#clave-"+id).val();
        if(clave!="ADP"){
            enviar(id, clave);
        }
    });

    function enviar(id, clave){
    
        var parametros = {
            id: id,
            clave: clave,
        };
    
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/listasDePrecio.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando Producto...");
    }

    function llegada(datos){
        console.log(datos);
        let contenido = "";

        switch(datos.estatus){
            case "Correcto":
                let contador = Object.keys(datos.precio).length;
                for (let i = 0; i < contador; i++) {
                    contenido += '<div class="form-row">'+
                                    '<div class="form-group col-sm-5 col-md-5">'+
                                        '<input type="text" class="form-control form-control-sm"  '+
                                            'value="'+datos.descripcion[i]+'" readonly />'+
                                    '</div>'+
                                    '<div class="form-group col-sm-4 col-md-4">'+
                                        '<input type="text" class="form-control form-control-sm"  '+
                                            'value="'+formatNumber.new(datos.precio[i], "$")+'" readonly />'+
                                    '</div>'+
                                    '<div class="form-group col-sm-3 col-md-3">'+
                                        '<input type="button" class="btn btn-sm btn-secondary seleccionarListaDePrecio" '+
                                            ' id="boton-'+i+'-'+datos.id+'" value="Seleccionar" />'+
                                    '</div>'+
                                    '<input type="hidden" id="idProductoListaDePrecio-'+i+'" value="'+datos.listas[i]+'" />'+
                                '</div>'; 
                }
                $("#contenidoModalDeListas").empty();
                $("#contenidoModalDeListas").append(contenido);
                break;
            case "Sin permiso":
                contenido +='<p><strong>Sin permiso, introduzca las credenciales</strong></p>'+ 
                            '<div class="form-row">'+
                                '<div class="form-group col-sm-12 col-md-12">'+
                                    '<label for="usuario">Usuario</label>'+
                                    '<input type="text" class="form-control" id="usuario" name="usuario" value="" />'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-row">'+
                                '<div class="form-group col-sm-12 col-md-12">'+
                                    '<label for="password">Contraseña</label>'+
                                    '<input class="form-control" type="password" id="password" name="password" value="" />'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-row">'+
                                '<div class="form-group col-sm-12 col-md-12">'+
                                    '<input type="button" class="btn btn-success" id="permisoListasDePrecio" value="Continuar">'+
                                '</div>'+
                            '</div>'+
                            '<input type="hidden" id="id" value="'+datos.id+'" />'+
                            '<input type="hidden" id="clave" value="'+datos.clave+'" />';
                $("#contenidoModalDeListas").empty();
                $("#contenidoModalDeListas").append(contenido);
                break;
        
            case "Sin información":
                contenido += '<p><strong>No se encontro información para el producto '+datos.clave+'</strong></p>';
                $("#contenidoModalDeListas").empty();
                $("#contenidoModalDeListas").append(contenido);
                break;

        }
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});