$(document).ready(function(){
    $('#productos').on('change', '.cantidad', function(){
        let id = $(this).attr("id");
                id = id.split("-");
                id = id[1];
        let clave = $("#clave-"+id).val();
        if(clave!=""){
            if($("#cliente").val()=="" && $("#descuento").val()=="" ){
                alert("Selecciona un cliente, por favor");
            }
            else{
                let descuento = $("#descuento").val();
                    descuento = descuento.split("%");
                    descuento = descuento[0];
                let cantidad = $(this).val();
                let lista = $("#lista-"+id).val();
            
                enviar(clave, id, descuento, cantidad, lista);   
            }
        }
        
    });

    function enviar(clave, id, descuento, cantidad, lista){
    
        var parametros = {
            clave: clave,
            id: id,
            descuento: descuento,
            cantidad: cantidad,
            lista: lista,
        };
    
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/cantidadProductos.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando Producto...");
    }

    function llegada(datos){
        console.log(datos);

        switch (datos.estatus){
            case "Correcto":
                $("#cantidad-"+datos.id).val(datos.cantidad);
                $("#costo-"+datos.id).val(formatNumber.new(datos.costo, "$"));
                $("#importe-"+datos.id).val(formatNumber.new(datos.importe, "$"));
                $("#subtotal-"+datos.id).val(formatNumber.new(datos.subtotal, "$"));
                $("#lista-"+datos.id).val(datos.lista);
                break;
            
            case "Sin información":
                $("#cantidad-"+datos.id).val("");
                $("#costo-"+datos.id).val("");
                $("#importe-"+datos.id).val("");
                $("#subtotal-"+datos.id).val("");
                $("#lista-"+datos.id).val("");
                alert("El producto "+datos.clave+" no existe en la base de datos, verificalo por favor");
                break;
            
        }

        //Realizar la suma del total de la nota
        totalNota();
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});