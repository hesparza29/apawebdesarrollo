$(document).ready(function(){
    $("body").on('click', '#seleccionarPenalizacion', function(){
        enviar();
    });

    function enviar(){
    
        var parametros = {
            
        };
    
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/seleccionarPenalizacion.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando Producto...");
    }

    function llegada(datos){
        console.log(datos);
        let contenido = "";

        switch(datos.estatus){
            case "Correcto":
                
                contenido += '<div class="modal-body">'+
                                '<div class="form-row">'+
                                    '<div class="form-group col-sm-12 col-md-12">'+
                                        '<label for="opcionPenalizacion">Selecciona una opción</label>'+
                                        '<select class="form-control" name="opcionPenalizacion" id="opcionPenalizacion">'+
                                                '<option value=""></option>'+
                                                '<option value="10">10%</option>'+
                                                '<option value="20">20%</option>'+
                                                '<option value="30">30%</option>'+
                                                '<option value="40">40%</option>'+
                                                '<option value="50">50%</option>'+
                                                '<option value="60">60%</option>'+
                                                '<option value="70">70%</option>'+
                                                '<option value="80">80%</option>'+
                                                '<option value="90">90%</option>'+
                                                '<option value="100">100%</option>'+
                                        '</select>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="form-row">'+
                                    '<div class="form-group col-sm-12 col-md-12">'+
                                        '<input type="button" class="btn btn-success" '+
                                            'id="calcularPenlaizacion" value="Continuar" />'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
                
                $("#contenidoModalPenalizacion").empty();
                $("#contenidoModalPenalizacion").append(contenido);
                break;
            case "Sin permiso":
                contenido +='<p><strong>Sin permiso, introduzca las credenciales</strong></p>'+ 
                            '<div class="form-row">'+
                                '<div class="form-group col-sm-12 col-md-12">'+
                                    '<label for="usuario">Usuario</label>'+
                                    '<input type="text" class="form-control" id="usuario" name="usuario" value="" />'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-row">'+
                                '<div class="form-group col-sm-12 col-md-12">'+
                                    '<label for="password">Contraseña</label>'+
                                    '<input class="form-control" type="password" id="password" name="password" value="" />'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-row">'+
                                '<div class="form-group col-sm-12 col-md-12">'+
                                    '<input type="button" class="btn btn-success" id="permisoPenalizacion" value="Continuar">'+
                                '</div>'+
                            '</div>';
                $("#contenidoModalPenalizacion").empty();
                $("#contenidoModalPenalizacion").append(contenido);
                break;
        }
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});