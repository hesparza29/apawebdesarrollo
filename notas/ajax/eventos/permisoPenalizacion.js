$(document).ready(function(){
    $("#contenidoModalPenalizacion").on('click', '#permisoPenalizacion', function(){
        if($("#usuario").val()!="" && $("#password").val()!=""){
            enviar($("#usuario").val(), $("#password").val());
        }
        else{
            alert("Captura todos los campos, por favor");
        }
    });

    function enviar(usuario, password){
    
        var parametros = {
            usuario: usuario,
            password: password,
        };
    
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/permisoPenalizacion.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando Permiso para penalizacion...");
    }

    function llegada(datos){
        console.log(datos);
        let contenido = "";

        switch(datos.estatus){
            case "Correcto":
                contenido += '<div class="modal-body">'+
                                '<div class="form-row">'+
                                    '<div class="form-group col-sm-12 col-md-12">'+
                                        '<label for="opcionPenalizacion">Selecciona una opción</label>'+
                                        '<select class="form-control" name="opcionPenalizacion" id="opcionPenalizacion">'+
                                                '<option value=""></option>'+
                                                '<option value="10">10%</option>'+
                                                '<option value="20">20%</option>'+
                                                '<option value="30">30%</option>'+
                                                '<option value="40">40%</option>'+
                                                '<option value="50">50%</option>'+
                                                '<option value="60">60%</option>'+
                                                '<option value="70">70%</option>'+
                                                '<option value="80">80%</option>'+
                                                '<option value="90">90%</option>'+
                                                '<option value="100">100%</option>'+
                                        '</select>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="form-row">'+
                                    '<div class="form-group col-sm-12 col-md-12">'+
                                        '<input type="button" class="btn btn-success" '+
                                            'id="calcularPenlaizacion" value="Continuar" />'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                $("#contenidoModalPenalizacion").empty();
                $("#contenidoModalPenalizacion").append(contenido);
                break;
            case "Sin permiso":
                alert("El usuario introducido no tiene permiso para ver la penalizacion, verificalo por favor");
                break;

        }
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});