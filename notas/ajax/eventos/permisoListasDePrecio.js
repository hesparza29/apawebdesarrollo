$(document).ready(function(){
    $("#contenidoModalDeListas").on('click', '#permisoListasDePrecio', function(){
        if($("#usuario").val()!="" && $("#password").val()!="" && $("#clave").val()!="ADP"){
            enviar($("#usuario").val(), $("#password").val(), $("#id").val(), $("#clave").val());
        }
        else{
            alert("Captura todos los campos, por favor");
        }
    });

    function enviar(usuario, password, id, clave){
    
        var parametros = {
            usuario: usuario,
            password: password,
            id: id,
            clave: clave,
        };
    
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/permisoListasDePrecio.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando Precio de lista seleccionado...");
    }

    function llegada(datos){
        console.log(datos);
        let contenido = "";

        switch(datos.estatus){
            case "Correcto":
                let contador = Object.keys(datos.precio).length;
                for (let i = 0; i < contador; i++) {
                    contenido += '<div class="form-row">'+
                                    '<div class="form-group col-sm-5 col-md-5">'+
                                        '<input type="text" class="form-control form-control-sm"  '+
                                            'value="'+datos.descripcion[i]+'" readonly />'+
                                    '</div>'+
                                    '<div class="form-group col-sm-4 col-md-4">'+
                                        '<input type="text" class="form-control form-control-sm"  '+
                                            'value="'+formatNumber.new(datos.precio[i], "$")+'" readonly />'+
                                    '</div>'+
                                    '<div class="form-group col-sm-3 col-md-3">'+
                                        '<input type="button" class="btn btn-sm btn-secondary seleccionarListaDePrecio" '+
                                            ' id="boton-'+i+'-'+datos.id+'" value="Seleccionar" />'+
                                    '</div>'+
                                    '<input type="hidden" id="idProductoListaDePrecio-'+i+'" value="'+datos.listas[i]+'" />'+
                                '</div>'; 
                }
                $("#contenidoModalDeListas").empty();
                $("#contenidoModalDeListas").append(contenido);
                break;
            case "Sin permiso":
                alert("El usuario introducido no tiene permiso para ver las listas de precio, verificalo por favor");
                break;
        
            case "Sin información":
                alert("No se encontro información para el producto "+datos.clave+"");
                $("#listas").modal('hide');
                break;

        }
        
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});