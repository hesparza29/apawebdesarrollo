$(document).ready(function(){
    
    $("#contenidoModalCancelar").on('click', '#guardarSAECancelada', function(){

        if($("#pdfCancelada").val()!=""){
            enviar();
        }
        else{
            alert("Captura todos los campos, por favor");
        }
    });

    function enviar() {
        //Asignar el folio de la nota al formulario de la captura SAE
        $("#folioNotaCancelada").val($("#folio").text());
        var formData = new FormData(document.getElementById("formularioCancelaSAE"));

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: false, //Especificaremos el tipo de contenido
            url: "ajax/guardarSAECancelada.php", //Sera el archivo que va a procesar la petición AJAX
            data: formData,
            cache: false,
            processData: false,
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 10000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando cancelar nota con pdf...");
    }

    function llegada(datos) {
        console.log(datos);
        switch (datos.estatus){
            case "Correcto":
                alert("Se pudo cancelar de menera correcta la nota de crédito con folio "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            case "Error":
                alert("Ocurrio un error al tratar de cancelar la nota de crédito con folio "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            case "Archivo invalido":
                alert("El archivo introducido es de tipo "+datos.tipo+" solamente se permiten introducir archivos pdf, verificalo por favor");
                break;
        }
        
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});