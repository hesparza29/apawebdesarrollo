$(document).ready(function(){

    $("#guardarNota").on('click', function(){
        let cantidad = [];
        let clave = [];
        let lista = [];
        let devolucion = [];
        let contador = $("#contador").val();
        let estatus = true;
        for (let i = 1; i <= contador; i++) {
            //Verificar que se haya capturado un lugar de nota de crédito
            if($("#lugar").val()==""){
                alert("Captura un lugar para la nota de crédito, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un motivo de nota de crédito
            else if($("#motivo").val()==""){
                alert("Captura un motivo para la nota de crédito, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un tipo de nota de crédito
            else if($("#tipo").val()==""){
                alert("Captura un tipo para la nota de crédito, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un folio de recepción de nota de crédito
            else if($("#folioRecepcion").val()=="" || $("#folioRecepcion").val()<1){
                alert("Captura un folio de recepción mayor a 0 para la nota de crédito, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado una factura de nota de crédito
            else if($("#factura").val()==""){
                alert("Captura una factura para la nota de crédito, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un cliente de nota de crédito
            else if($("#cliente").val()==""){
                alert("Captura un cliente para la nota de crédito, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado una observación de nota de crédito
            else if($("#descuento").val()==""){
                alert("Captura un descuento para la nota de crédito, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un descuento de nota de crédito
            else if($("#observaciones").val()==""){
                alert("Captura una observación para la nota de crédito, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que la clave del producto sea diferente de undefined y además sea diferente de vacio
            else if($("#clave-"+i+"").val()!=undefined && $("#clave-"+i+"").val()!=""){
                //Verificar que el producto no se encuentre capturado más de una vez
                if(clave.includes($("#clave-"+i+"").val())==true){
                    alert("El producto "+$("#clave-"+i+"").val()+" se capturo más de una vez, verificalo por favor");
                    estatus = false;
                    break;
                }
                //Verificar que la cantidad ingresada para cada producto sea diferente de vacio
                else if($("#cantidad-"+i+"").val()==""){
                    alert("Debe de ingresar una cantidad para el producto "+$("#clave-"+i+"").val()+" , verificalo por favor");
                    estatus = false;
                    break;
                }
                //Verificar que la devolución ingresada para cada producto sea diferente de vacio
                else if($("#devolucion-"+i+"").val()==""){
                    alert("Debe de ingresar una devolución para el producto "+$("#clave-"+i+"").val()+" , verificalo por favor");
                    estatus = false;
                    break;
                }
                //Guardar los datos validos en arreglos
                else{
                    cantidad.push($("#cantidad-"+i+"").val());
                    clave.push($("#clave-"+i+"").val());
                    lista.push($("#lista-"+i+"").val());
                    devolucion.push($("#devolucion-"+i+"").val());
                    
                }
            }
            
        }

        //Verificar que se haya capturado por lo menos un producto con su cantidad y su respectiva devolución
        if(cantidad.length==0 && clave.length==0 && lista.length==0 && devolucion.length==0){
            alert("Captura al menos un producto, por favor");
            estatus = false;
        }

        if(estatus){
            enviar(cantidad, clave, lista, devolucion);
            //alert("Bien");
        }
    });

    function enviar(cantidad, clave, lista, devolucion) {
        var parametros = {
          lugar: $("#lugar").val(),
          motivo: $("#motivo").val(),
          tipo: $("#tipo").val(),
          folioRecepcion: $("#folioRecepcion").val(),
          factura: $("#factura").val(),
          cliente: $("#cliente").val(),
          descuento: $("#descuento").val(),
          observaciones: $("#observaciones").val(),
          penalizacion: $("#valorDePenalizacion").val(),
          cantidad: cantidad,
          clave: clave,
          lista: lista,
          devolucion: devolucion,
        };
        
        $.ajax({
          async: true, //Activar la transferencia asincronica
          type: "POST", //El tipo de transaccion para los datos
          dataType: "json", //Especificaremos que datos vamos a enviar
          contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
          url: "ajax/guardarNota.php", //Sera el archivo que va a procesar la petición AJAX
          data: parametros, //Datos que le vamos a enviar
          // data: "total="+total+"&penalizacion="+penalizacion,
          beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
          success: llegada, //Función que se ejecuta en caso de tener exito
          timeout: 4000,
          error: problemas, //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando el guardado de la nota de crédito...");
    }
    
    function llegada(datos){
        console.log(datos);

        switch (datos.estatus){
            case "Correcto":
                alert("Se pudo capturar de manera exitosa la nota de crédito "+datos.folio);
                //Desarrollo
                var url = window.location.host+"/apawebdesarrollo/notas/impresion.php?folio="+datos.folio;
                //Produccion
                //var url = "apawebdesarrollo.com/notas/impresion.php?folio="+datos.folio;
                window.open('http://'+url+'', '_blank');
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            case "Error":
                alert("Ocurrio un error al capturar la nota de crédito "+datos.folio+" intentalo más tarde, por favor");
                break;
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});