$(document).ready(function(){

    $(".notasSinSae").on('click', '#notasSinSae', function(){
        enviar();
    });

    function enviar(){
     
        var parametros =
        {
          
        }
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/notasSinSae.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 8000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Cargando...");
    }
    function llegada(datos){
        console.log(datos);
        switch(datos.estatus){
          case 'Correcto':
            let contador = Object.keys(datos.folio).length;
            let contenido = "";
            for (let i = 0; i < contador; i++) {
                contenido += '<tr class="'+datos.clase[i]+'">'+
                              '<td>'+datos.folio[i]+'</td>'+
                              '<td>'+datos.folioRecepcion[i]+'</td>'+
                              '<td>'+datos.cliente[i]+'</td>'+
                              '<td>'+datos.notaSAE[i]+'</td>'+
                              '<td>'+formatNumber.new(datos.total[i], "$")+'</td>'+
                              '<td>'+datos.fecha[i]+'</td>'+
                              '<td>'+datos.estatuses[i]+'</td>'+
                              '<td>'+datos.usuario[i]+'</td>'+
                              '<td>'+
                                '<input type="button" class="btn btn-info btn-sm verImpresion" id="notas-'+datos.folio[i]+'" value="Ver" />'+
                              '</td>'+
                            '</tr>';
              
            }
            $("#paginador").hide();
            $("#table").empty();
            $("#table").append(contenido);
            break;
        case 'Sin resultados':
            alert("No se encontraron datos en la consulta");
            break;
        
        default:
            break;
        }
    }
    
      function problemas(textError, textStatus) {
            //var error = JSON.parse(textError);
            alert("Problemas en el Servlet: " + JSON.stringify(textError));
            alert("Problemas en el servlet: " + JSON.stringify(textStatus));
      }
});