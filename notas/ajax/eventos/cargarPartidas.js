$(document).ready(function(){

    //Obteniendo el folio de la cobranza
    var url = window.location.search;
    url = url.split("?");
    url = url[1];
    url = url.split("=");
    folio = url[1];
    $("body").ready(function(){
        enviar(folio)
    });

    function enviar(folio){
        
        var parametros =
        {
            folio: folio,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/cargarPartidas.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 6000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Cargando Partidas...");
    }
    
    function llegada(datos){
        console.log(datos);
        switch (datos.estatus){
            case "Correcto":
                let tabla = 0;
                let contador = Object.keys(datos.clave).length;
                if (contador<=10){
                     tabla = 11;
                }
                else{
                    tabla = contador+1;
                }
                let contenido = "";
                for (let i = 0; i < tabla; i++){

                    if(i==0){
                        contenido += '<tr>'+
                                        '<td>'+datos.cantidad[i]+'</td>'+
                                        '<td>'+datos.clave[i]+'</td>'+
                                        '<td>'+datos.devolucion[i]+'</td>'+
                                        '<td>'+datos.factura+'</td>'+
                                        '<td colspan="2" rowspan="'+tabla+'">'+
                                            datos.motivo+'<br />Observaciones: <br />'+datos.observaciones+
                                        '</td>'+
                                        '<td>'+formatNumber.new(datos.costos[i], "$")+'</td>'+
                                        '<td>'+formatNumber.new(datos.importes[i], "$")+'</td>'+
                                        '<td>'+datos.descuento+'%</td>'+
                                        '<td>'+formatNumber.new(datos.subtotales[i], "$")+'</td>'+
                                    '</tr>';
                    }
                    else if(i<contador){
                        contenido += '<tr>'+
                                        '<td>'+datos.cantidad[i]+'</td>'+
                                        '<td>'+datos.clave[i]+'</td>'+
                                        '<td>'+datos.devolucion[i]+'</td>'+
                                        '<td></td>'+
                                        '<td>'+formatNumber.new(datos.costos[i], "$")+'</td>'+
                                        '<td>'+formatNumber.new(datos.importes[i], "$")+'</td>'+
                                        '<td></td>'+
                                        '<td>'+formatNumber.new(datos.subtotales[i], "$")+'</td>'+
                                    '</tr>';
                    }
                    else{
                        contenido += '<tr>'+
                                        '<td>&nbsp;</td>'+
                                        '<td>&nbsp;</td>'+
                                        '<td>&nbsp;</td>'+
                                        '<td>&nbsp;</td>'+
                                        '<td>&nbsp;</td>'+
                                        '<td>&nbsp;</td>'+
                                        '<td>&nbsp;</td>'+
                                        '<td>&nbsp;</td>'+
                                    '</tr>';
                    }

                    
                    
                }
                $("#mes").text(datos.mes);
                $("#fecha").text(datos.fecha);
                $("#tipo").text(datos.tipo);
                $("#folio").text(datos.folio);
                $("#nombreCliente").text("NOMBRE: "+datos.nombreCliente);
                $("#vendedor").text("VENDEDOR: "+datos.vendedor);
                $("#folioRecepcion").text(datos.folioRecepcion);
                $("#cliente").text(datos.cliente);
                $("#table").append(contenido);
                $("#totalConLetra").text(datos.totalConLetra);
                $("#subtotal").text(formatNumber.new(datos.subtotal, "$"));
                $("#iva").text(formatNumber.new(datos.iva, "$"));
                $("#total").text(formatNumber.new(datos.total, "$"));
                $("#usuario").text(datos.usuario);
                $("#notaSAE").val(datos.notaSAE);
                $("#notaEstatus").val(datos.estatus);
                //Sí ya tiene un número de SAE asignado esconder el botón de 'SAE'
                if(datos.notaSAE!=""){
                    $("#sae").hide();
                }
                //Sí ya se encuentra cancelada esconder el botón de 'Cancelar'
                if(datos.estatusNota=="Cancelada"){
                    $("#cancelar").hide();
                }
                //Sí ya tiene un número de SAE asignado y además es administrador esconder el botón de 'Cancelar'
                if(datos.notaSAE!="" && datos.permiso!="administrador"){
                    $("#cancelar").hide();
                }
                break;
        
            case "Sin resultados":
                alert("No se econtraron resultados de la nota de crédito "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});