$(document).ready(function(){

    $("#contenidoModalDeListas").on('click', '.seleccionarListaDePrecio', function(){
        
        if($("#cliente").val()=="" && $("#descuento").val()==""){
            alert("Selecciona un cliente, por favor");
        }
        else{
            let identificador = $(this).attr('id');
                identificador = identificador.split("-");
            let lista = $("#idProductoListaDePrecio-"+identificador[1]+"").val();
            let id = identificador[2];
            let descuento = $("#descuento").val();
                descuento = descuento.split("%");
                descuento = descuento[0];
            let cantidad = $("#cantidad-"+id+"").val();
        
            enviar(lista, id, descuento, cantidad);   
        }
    });

    function enviar(lista, id, descuento, cantidad){
    
        var parametros = {
            lista: lista,
            id: id,
            descuento: descuento,
            cantidad: cantidad,
        };
    
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/seleccionarListaDePrecio.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }

    function inicioEnvio(){
        console.log("Cargando Precio de lista seleccionado...");
    }

    function llegada(datos){
        console.log(datos);

        $("#listas").modal('hide');

        switch (datos.estatus){
            case "Correcto":
                $("#costo-"+datos.id).val(formatNumber.new(datos.costo, "$"));
                $("#importe-"+datos.id).val(formatNumber.new(datos.importe, "$"));
                $("#subtotal-"+datos.id).val(formatNumber.new(datos.subtotal, "$"));
                $("#lista-"+datos.id).val(datos.lista);
                break;
            
            case "Sin información":
                $("#costo-"+datos.id).val("");
                $("#importe-"+datos.id).val("");
                $("#subtotal-"+datos.id).val("");
                $("#lista-"+datos.id).val("");
                alert("El producto "+datos.clave+" no existe en la base de datos, verificalo por favor");
                break;
            
        }

        //Realizar la suma del total de la nota
        totalNota();
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});