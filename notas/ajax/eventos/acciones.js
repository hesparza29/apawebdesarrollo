$(document).ready(function(){

    enviar("Nota Visualización");

    function enviar(modulo){
        
        var parametros =
        {
            modulo: modulo,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/acciones.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 6000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
      }
      function inicioEnvio(){
          console.log("Cargando Acciones...");
      }
    
      function llegada(datos){
        console.log(datos);
        let contador = Object.keys(datos.boton).length;

        for (let i = 0; i < contador; i++) {
            let elemento = "<input class='btn btn-"+ datos.clase[i] +"' type='button' id='"+ datos.identificador[i] +"'"+ 
                            " data-toggle='modal' data-target='#"+ datos.boton[i] +"' value='"+ datos.boton[i] +"' />";
            $("."+ datos.identificador[i] +"").append(elemento);
            
        }
      }
    
      function problemas(textError, textStatus) {
            //var error = JSON.parse(textError);
            alert("Problemas en el Servlet: " + JSON.stringify(textError));
            alert("Problemas en el servlet: " + JSON.stringify(textStatus));
      }
});