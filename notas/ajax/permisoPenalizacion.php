<?php

    require_once("../../funciones.php");

    $usuario = $_POST["usuario"];
    $password = $_POST["password"];
    $estatus = "Correcto";
    $modulo = "Nota Nueva";
    $identificador = "penalizacion";
    $datos = array();

    $base = conexion_local();

    //Consulta para verificar si el usuario y la password es correcta, 
    //además de qué el usuario tenga permiso para ver las listas de precio
    $consultaPermiso = "SELECT Clave FROM USUARIO 
                        INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario 
                        INNER JOIN MODULO ON MODULO.idModulo=USUARIO_MODULO.idModulo 
                        WHERE Usuario=? AND MODULO.Nombre=? AND Identificador=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo, $identificador));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);

    if($registroPermiso["Clave"]!="" && password_verify($password, $registroPermiso["Clave"])){
        $estatus = "Correcto";
    }
    else{
        $estatus = "Sin permiso";
    }

    $resultadoPermiso->closeCursor();
    

    $datos["estatus"] = $estatus;

    $base = null;
    echo json_encode($datos);
?>