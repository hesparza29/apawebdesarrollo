<?php
    session_start();
    $usuario = $_SESSION["user"];
    require_once("../../funciones.php");

    $lugar = $_POST["lugar"];
    $motivo = $_POST["motivo"];
    $tipo = $_POST["tipo"];
    $folioRecepcion = $_POST["folioRecepcion"];
    $factura = $_POST["factura"];
    $cliente = $_POST["cliente"];
    $descuento = explode("%", $_POST["descuento"]);
    $descuento = $descuento[0];
    $observaciones = $_POST["observaciones"];
    $penalizacion = $_POST["penalizacion"];
    $cantidad = $_POST["cantidad"];
    $clave = $_POST["clave"];
    $lista = $_POST["lista"];
    $devolucion = $_POST["devolucion"];
    $fecha = fecha();
    $estatus = "Correcto";
    $folio = "";
    $estatusNota = "Revisando";
    $idNota = "";
    $notaSAE = "";
    $subtotal = 0;
    $contador = count($clave);
    $listaDePrecio = "Precio público";
    $datos = array();

    $base = conexion_local();
    //Consulta para insertar el precio del ADP en las listas de precio
    $consultaInsertarADP = "INSERT INTO PRODUCTO_LISTA_DE_PRECIO VALUES (?, ?, 
                                (SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?), 
                                (SELECT idListaDePrecio FROM LISTA_DE_PRECIO WHERE Descripcion=?))";
    $resultadoInsertarADP = $base->prepare($consultaInsertarADP);
    //Consulta para saber el último folio capturado del tipo de nota que se quiere capturar
    $consultaNota = "SELECT Folio FROM NOTA WHERE Lugar=? AND Tipo=? ORDER BY idNota DESC LIMIT 1";
    $resultadoNota = $base->prepare($consultaNota);
    $resultadoNota->execute(array($lugar, $tipo));
    //Construir el folio consecutivo
    switch ($lugar." ".$tipo){
        case 'azcapotzalco Devolución Parcial':
            if($resultadoNota->rowCount()==0){
                $folio = "DPPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "DPPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Factura Completa':
            if($resultadoNota->rowCount()==0){
                $folio = "FCPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "FCPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Entrada Caja':
            if($resultadoNota->rowCount()==0){
                $folio = "ECPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "ECPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Factor 3':
            if($resultadoNota->rowCount()==0){
                $folio = "F3PB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "F3PB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Cambio Físico':
            if($resultadoNota->rowCount()==0){
                $folio = "CFPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "CFPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Entrada Caja Factor 3':
            if($resultadoNota->rowCount()==0){
                $folio = "ECF3PB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "ECF3PB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Muestra':
            if($resultadoNota->rowCount()==0){
                $folio = "MPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "MPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Condonación':
            if($resultadoNota->rowCount()==0){
                $folio = "CPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "CPB" . (intval($registroFolio[1])+1);
            }
            break;
          
        case 'tecamac Devolución Parcial':
            if($resultadoNota->rowCount()==0){
                $folio = "DPTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "DPTC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Factura Completa':
            if($resultadoNota->rowCount()==0){
                $folio = "FCTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "FCTC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Entrada Caja':
            if($resultadoNota->rowCount()==0){
                $folio = "ECTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "ECTC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Factor 3':
            if($resultadoNota->rowCount()==0){
                $folio = "F3TC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "F3TC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Cambio Físico':
            if($resultadoNota->rowCount()==0){
                $folio = "CFTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "CFTC" . (intval($registroFolio[2])+1);
            }
            break;
        case 'tecamac Entrada Caja Factor 3':
            if($resultadoNota->rowCount()==0){
                $folio = "ECF3TC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "ECF3TC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Muestra':
            if($resultadoNota->rowCount()==0){
                $folio = "MTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "MTC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Condonación':
            if($resultadoNota->rowCount()==0){
                $folio = "CTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "CTC" . (intval($registroFolio[2])+1);
            }
            break;
    }
    $resultadoNota->closeCursor();

    //Obtener los precios de los productos
    $consultaPrecio = "SELECT Precio FROM PRODUCTO_LISTA_DE_PRECIO WHERE idProductoListaDePrecio=?";
    $resultadoPrecio = $base->prepare($consultaPrecio);
    for ($i=0; $i<$contador ; $i++) {
        //Verificar si existe un ADP
        if($clave[$i]=="ADP"){
            $resultadoInsertarADP->execute(array(NULL, $lista[$i], $clave[$i], $listaDePrecio));
            if($resultadoInsertarADP->rowCount()==1){
                $lista[$i] = $base->lastInsertId();
            }
            $resultadoInsertarADP->closeCursor();
        } 
        $resultadoPrecio->execute(array($lista[$i]));
        $registroPrecio = $resultadoPrecio->fetch(PDO::FETCH_ASSOC);
        if($penalizacion>0){
            $registroPrecio["Precio"] = round(($registroPrecio["Precio"]-($registroPrecio["Precio"]*($penalizacion/100)))*100)/100;
        }
        $importe = imp($cantidad[$i], $registroPrecio["Precio"]);
        $subtotal += sub($descuento, $importe);
    }
    $resultadoPrecio->closeCursor();

    switch($tipo){
        case "Factor 3":
            $subtotal = round($subtotal*100)/100;
            $total = $subtotal;
            $notaSAE = "F3";
            $estatusNota = "Correcta";
          break;
        case "Entrada Caja Factor 3":
            $subtotal = round($subtotal*100)/100;
            $total = $subtotal;
            $notaSAE = "F3";
            $estatusNota = "Correcta";
          break;
        case "Muestra":
            $subtotal = 0.00;
            $total = $subtotal;
            $notaSAE = "MUESTRA";
            $estatusNota = "Correcta";
          break;
        case "Cambio Físico":
            $subtotal = round($subtotal*100)/100;
            $total = impuesto($subtotal);
            $notaSAE = "C. FISICO";
            $estatusNota = "Correcta";
          break;
        default:
            $subtotal = round($subtotal*100)/100;
            $total = impuesto($subtotal);
            $notaSAE = "";
          break;
    }
    

    //Insertar la nueva nota de crédito
    $consultaInsertarNota = "INSERT INTO NOTA VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
                                                    (SELECT idDescuento FROM DESCUENTO WHERE Porcentaje=?),
                                                    (SELECT idUsuario FROM USUARIO WHERE Usuario=?),
                                                    (SELECT idVendedor FROM CLIENTE WHERE idCliente=?))";
    $resultadoInsertarNota = $base->prepare($consultaInsertarNota);
    $resultadoInsertarNota->execute(array(NULL, $folio, $fecha, $tipo, $lugar, $total, $estatusNota, $folioRecepcion, 
                                            $factura, $notaSAE, $motivo, $observaciones, "", "", 
                                            $penalizacion, $cliente, $descuento, $usuario, $cliente));
    if ($resultadoInsertarNota->rowCount()==1){
        $idNota = $base->lastInsertId();
        //Insertar los productos con su respectiva cantidad y devolucion
        $consultaInsertarProductos = "INSERT INTO NOTA_PRODUCTO_LISTA_DE_PRECIO VALUES(?,?,?,?,?)";
        $resultadoInsertarProductos = $base->prepare($consultaInsertarProductos);
        for ($i=0; $i<$contador; $i++){
            $resultadoInsertarProductos->execute(array(NULL, $cantidad[$i], $devolucion[$i], $idNota, $lista[$i]));
        }
        $resultadoInsertarProductos->closeCursor();
    }
    else {
        $estatus = "Error";
    }
    $resultadoInsertarNota->closeCursor();
    
    $datos["estatus"] = $estatus;
    $datos["folio"] = $folio;
    $datos["idNota"] = $idNota;
    

    $base = null;
    echo json_encode($datos);
?>