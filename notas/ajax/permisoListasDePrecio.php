<?php
    require_once("../../funciones.php");

    $usuario = $_POST["usuario"];
    $password = $_POST["password"];
    $id = $_POST["id"];
    $clave = $_POST["clave"];
    $estatus = "Correcto";
    $modulo = "Nota Nueva";
    $identificador = "listasDePrecio";
    $datos = array();
    $precio = array();
    $descripcion = array();
    $listas = array();
    $contador = 0;

    $base = conexion_local();

    //Consulta para obtener las listas de precio del producto
    $consultaPrecios = "SELECT Precio, LISTA_DE_PRECIO.Descripcion, idProductoListaDePrecio FROM PRODUCTO 
                            INNER JOIN PRODUCTO_LISTA_DE_PRECIO ON PRODUCTO.idProducto=PRODUCTO_LISTA_DE_PRECIO.idProducto 
                            INNER JOIN LISTA_DE_PRECIO ON PRODUCTO_LISTA_DE_PRECIO.idListaDePrecio=LISTA_DE_PRECIO.idListaDePrecio 
                            WHERE NumeroAPA=?";
    $resultadoPrecios = $base->prepare($consultaPrecios);
    //Consulta para verificar si el usuario y la password es correcta, 
    //además de qué el usuario tenga permiso para ver las listas de precio
    $consultaPermiso = "SELECT Clave FROM USUARIO 
                        INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario 
                        INNER JOIN MODULO ON MODULO.idModulo=USUARIO_MODULO.idModulo 
                        WHERE Usuario=? AND MODULO.Nombre=? AND Identificador=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo, $identificador));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);

    if($registroPermiso["Clave"]!="" && password_verify($password, $registroPermiso["Clave"])){
        $resultadoPrecios->execute(array($clave));
        if($resultadoPrecios->rowCount()>0){
            while($registroPrecios = $resultadoPrecios->fetch(PDO::FETCH_ASSOC)){
                $precio[$contador] = $registroPrecios["Precio"];
                $descripcion[$contador] = $registroPrecios["Descripcion"];
                $listas[$contador] = $registroPrecios["idProductoListaDePrecio"];
                $contador++;
            }

            $datos["precio"] = $precio;
            $datos["descripcion"] = $descripcion;
            $datos["listas"] = $listas;
        }
        else{
            $estatus = "Sin información";
        }
        $resultadoPrecios->closeCursor();
    }
    else{
        $estatus = "Sin permiso";
    }

    $resultadoPermiso->closeCursor();
    

    $datos["estatus"] = $estatus;
    $datos["id"] = $id;
    $datos["clave"] = $clave;
    echo json_encode($datos);
?>