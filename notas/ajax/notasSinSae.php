<?php

    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $modulo = "Nota";
    $permiso = "";
    $estatus = "Correcto";
    $folios = array();
    $foliosRecepcion = array();
    $clientes = array();
    $notaSAE = array();
    $totales = array();
    $fechas = array();
    $estatuses = array();
    $clases = array();
    $usuarios = array();
    $pdf = array();
    $pdfCancelada = array();
    $datos = array();
    $contador = 0;

    $base = conexion_local();
    //Obtener las notas que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
    INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
    INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
    WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    switch ($permiso){
        case 'administrador':
            $consultaNota = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido  
                                FROM NOTA 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN DESCUENTO ON NOTA.idDescuento=DESCUENTO.idDescuento 
                                INNER JOIN USUARIO ON NOTA.idUsuario=USUARIO.idUsuario WHERE NotaSAE=?";
            $resultadoNota = $base->prepare($consultaNota);
            $resultadoNota->execute(array(""));
            break;
	
	    default:
            $consultaNota = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido  
                                FROM NOTA 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN DESCUENTO ON NOTA.idDescuento=DESCUENTO.idDescuento 
                                INNER JOIN USUARIO ON NOTA.idUsuario=USUARIO.idUsuario WHERE NotaSAE=? AND Lugar=?";
            $resultadoNota = $base->prepare($consultaNota);
            $resultadoNota->execute(array("", $permiso));
            break;
    }

    if($resultadoNota->rowCount()>0){
        while($registroNota = $resultadoNota->fetch(PDO::FETCH_ASSOC)){
            $folios[$contador] = $registroNota["Folio"];
            $foliosRecepcion[$contador] = $registroNota["FolioRecepcion"];
            $clientes[$contador] = $registroNota["idCliente"] . " " . $registroNota["Cliente"];
            $notaSAE[$contador] = $registroNota["NotaSAE"];
            $totales[$contador] = $registroNota["Total"];
            $fechas[$contador] = fechaStandar($registroNota["Fecha"]);
            $estatuses[$contador] = $registroNota["Estatus"];
            switch($registroNota["Estatus"]){
                case 'Revisando':
                    $clases[$contador] = "revisando";
                    break;
                case 'Cancelada':
                    $clases[$contador] = "cancelada";
                    break;
                case 'Correcta':
                    $clases[$contador] = "correcta";
                    break;
                
            }
            $usuarios[$contador] = $registroNota["Nombre"] . " " . $registroNota["Apellido"];
            $contador++;
        }
    }
    else{
        $estatus = "Sin resultados";
    }

    $datos["estatus"] = $estatus;
    $datos["folio"] = $folios;
	$datos["folioRecepcion"] = $foliosRecepcion;
	$datos["cliente"] = $clientes;
	$datos["notaSAE"] = $notaSAE;
	$datos["total"] = $totales;
	$datos["fecha"] = $fechas;
	$datos["estatuses"] = $estatuses;
    $datos["clase"] = $clases;
    $datos["usuario"] = $usuarios;
    
    $base = null;

    echo json_encode($datos);

?>