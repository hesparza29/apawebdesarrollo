<?php 
    require_once("../../funciones.php");
    $folio = $_POST["folio"];
    $estatus = "Correcto";
    $estatusNota = "Revisando";
    $datos = array();
    $base = conexion_local();

    //Consulta para verificar la nota en la base de datos
    $consultaNota = "SELECT Estatus FROM NOTA WHERE Folio=? AND Estatus=?";
    $resultadoNota = $base->prepare($consultaNota);
    $resultadoNota->execute(array($folio, $estatusNota));
    switch ($resultadoNota->rowCount()){
        case 1:
            //Consulta para cancelar una nota
            $consultaCancelarNota = "UPDATE NOTA SET Estatus=?, NotaSAE=? WHERE Folio=?";
            $resultadoCancelarNota = $base->prepare($consultaCancelarNota);
            $resultadoCancelarNota->execute(array("Cancelada", "Cancelada", $folio));
            if($resultadoCancelarNota->rowCount()!=1){
                $estatus = "Error";
            }
            $resultadoCancelarNota->closeCursor();
            break;
        case 0:
            $estatus = "Error";
            break;
        
    }

    $resultadoNota->closeCursor();
    $base = null;

    $datos["estatus"] = $estatus;
    $datos["folio"] = $folio;

    echo json_encode($datos);
?>