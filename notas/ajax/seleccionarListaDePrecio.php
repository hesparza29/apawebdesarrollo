<?php
    require_once("../../funciones.php");

    $lista = $_POST["lista"];
    $id = $_POST["id"];
    $descuento = $_POST["descuento"];
    $cantidad = $_POST["cantidad"];
    $estatus = "Correcto";
    $datos = array();

    //Si la cantidad es vacio setearla al valor por defecto que es '1'
    if ($cantidad==""){
        $cantidad = 1;
    }

    $base = conexion_local();

    //Consulta para obtener la información del producto
    $consultaInformacion = "SELECT Precio, idProductoListaDePrecio FROM PRODUCTO_LISTA_DE_PRECIO WHERE idProductoListaDePrecio=?";
    $resultadoInformacion = $base->prepare($consultaInformacion);
    $resultadoInformacion->execute(array($lista));

    if ($resultadoInformacion->rowCount()==1){
        $estatus = "Correcto";
        $registroInformacion = $resultadoInformacion->fetch(PDO::FETCH_ASSOC);
        $datos["costo"] = $registroInformacion["Precio"];
        $datos["importe"] = imp($cantidad, $registroInformacion["Precio"]);
        $datos["subtotal"] = sub($descuento, $datos["importe"]);
        $datos["lista"] = $registroInformacion["idProductoListaDePrecio"];
        
    }
    else {
        $estatus = "Sin información";
    }
    $resultadoInformacion->closeCursor();


    $datos["id"] = $id;
    $datos["estatus"] = $estatus;
    
    $base = null;
    echo json_encode($datos);
?>