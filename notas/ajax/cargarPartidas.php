<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $cantidad = array();
    $clave = array();
    $devolucion = array();
    $costos = array();
    $importes = array();
    $subtotales = array();
    $contador = 0;
    $subtotal = 0;
    $estatus = "Correcto";
    $modulo = "Nota";
    $permiso = "";
    $totalCargo = 0;
    
    
    $datos = array();

    $base = conexion_local();
    //Obtener las notas que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
    INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
    INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
    WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    //Obtener la información de la nota
    switch ($permiso){
        case 'administrador':
            $consultaNota = "SELECT idNota, Fecha, Tipo, Total, NOTA.Estatus, FolioRecepcion, Factura, NotaSAE, 
                                Motivo, Observaciones, CLIENTE.idCliente, CLIENTE.Nombre as Cliente,  
                                Porcentaje, USUARIO.Nombre, USUARIO.Apellido, 
                                Penalizacion, VENDEDOR.Clave as Vendedor FROM NOTA 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN DESCUENTO ON NOTA.idDescuento=DESCUENTO.idDescuento 
                                INNER JOIN USUARIO ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN VENDEDOR ON CLIENTE.idVendedor=VENDEDOR.idVendedor WHERE Folio=?";
            $resultadoNota = $base->prepare($consultaNota);
            $resultadoNota->execute(array($folio));
            break;
        
        default:
            $consultaNota = "SELECT idNota, Fecha, Tipo, Total, NOTA.Estatus, FolioRecepcion, Factura, NotaSAE,  
                                Motivo, Observaciones, CLIENTE.idCliente, CLIENTE.Nombre as Cliente, 
                                Porcentaje, USUARIO.Nombre, USUARIO.Apellido, 
                                Penalizacion, VENDEDOR.Clave as Vendedor FROM NOTA 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN DESCUENTO ON NOTA.idDescuento=DESCUENTO.idDescuento 
                                INNER JOIN USUARIO ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN VENDEDOR ON CLIENTE.idVendedor=VENDEDOR.idVendedor WHERE Folio=? AND Lugar=?";
            $resultadoNota = $base->prepare($consultaNota);
            $resultadoNota->execute(array($folio, $permiso));
            break;
    }
    //Verificar si existe la nota con el folio solicitado
    switch ($resultadoNota->rowCount()) {
        case 1:
            $registroNota = $resultadoNota->fetch(PDO::FETCH_ASSOC);
            $datos["mes"] = mes();
            $datos["fecha"] = fechaStandar($registroNota["Fecha"]);
            $datos["tipo"] = $registroNota["Tipo"];
            $datos["total"] = $registroNota["Total"];
            $datos["estatusNota"] = $registroNota["Estatus"];
            $datos["folioRecepcion"] = $registroNota["FolioRecepcion"];
            $datos["factura"] = $registroNota["Factura"];
            $datos["notaSAE"] = $registroNota["NotaSAE"];
            $datos["motivo"] = $registroNota["Motivo"];
            $datos["observaciones"] = $registroNota["Observaciones"];
            $datos["cliente"] = $registroNota["idCliente"];
            $datos["nombreCliente"] = $registroNota["Cliente"];
            $datos["descuento"] = $registroNota["Porcentaje"];
            $datos["usuario"] = mb_strtoupper($registroNota["Nombre"] . " " . $registroNota["Apellido"]);
            $datos["penalizacion"] = $registroNota["Penalizacion"];
            $datos["vendedor"] = $registroNota["Vendedor"];
            if($registroNota["Total"]==0.00){
                $datos["totalConLetra"] = "CERO";
            }
            else{
                $datos["totalConLetra"] = num2letras($registroNota["Total"], $fem = false, $dec = true);
            }
            
            //Obtener todos los productos que se capturaron en la nota
            $consultaPartidas = "SELECT Cantidad, NumeroAPA, Devolucion, Precio  
                                FROM NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO ON 
                                NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE idNota=?";
            $resultadoPartidas = $base->prepare($consultaPartidas);
            $resultadoPartidas->execute(array($registroNota["idNota"]));
            while ($registroPartidas = $resultadoPartidas->fetch(PDO::FETCH_ASSOC)){
                
                $cantidad[$contador] = $registroPartidas["Cantidad"];
                $clave[$contador] = $registroPartidas["NumeroAPA"];
                $devolucion[$contador] = $registroPartidas["Devolucion"];

                if($datos["tipo"]=="Muestra"){
                    $costos[$contador] = 0.00;
                    $importes[$contador] = 0.00;
                    $subtotales[$contador] = 0.00;
                }
                else{
                    if($datos["penalizacion"]>0){
                        $costos[$contador] = round(($registroPartidas["Precio"]-($registroPartidas["Precio"]*
                                                ($datos["penalizacion"]/100)))*100)/100;
                        $totalCargo += round(($registroPartidas["Precio"]*($datos["penalizacion"]/100))*100)/100;
                    }
                    else{
                        $costos[$contador] = $registroPartidas["Precio"];
                    }
                    $importes[$contador] = imp($cantidad[$contador], $costos[$contador]);
                    $subtotales[$contador] = sub($datos["descuento"], $importes[$contador]);
                }

                $subtotal += $subtotales[$contador];
                $contador++;
                
            }
            $resultadoPartidas->closeCursor();
            if($datos["penalizacion"]>0){
                array_push($cantidad, 1);
                array_push($clave, "CARG".$datos["penalizacion"]);
                array_push($devolucion, "");
                array_push($costos, ($totalCargo*-1));
                array_push($importes, ($totalCargo*-1));
                array_push($subtotales, ($totalCargo*-1));
            }
            
            $datos["cantidad"] = $cantidad;
            $datos["clave"] = $clave;
            $datos["devolucion"] = $devolucion;
            $datos["costos"] = $costos;
            $datos["importes"] = $importes;
            $datos["subtotales"] = $subtotales;

            if($datos["tipo"]=="Factor 3" || $datos["tipo"]=="Entrada Caja Factor 3"){
                $datos["subtotal"] = round($subtotal*100)/100;
                $datos["iva"] = 0.00;
            }
            else if($datos["tipo"]=="Muestra"){
                $datos["subtotal"] = 0.00;
                $datos["iva"] = 0.00;
            }
            else{
                $datos["subtotal"] = round($subtotal*100)/100;
                $datos["iva"] = iva($subtotal);
            }
            break;
        
        case 0:
            $estatus = "Sin resultados";
            break;
    }
    
    $resultadoNota->closeCursor();
    $base = null;

    $datos["folio"] = $folio;
    $datos["estatus"] = $estatus;
    $datos["permiso"] = $permiso;



    echo json_encode($datos);
?>