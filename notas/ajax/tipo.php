<?php
    require_once("../../funciones.php");

    $lugar = $_POST["lugar"];
    $tipo = $_POST["tipo"];
    $folio = "";
    $datos = array();

    $base = conexion_local();

    //Consulta para saber el último folio capturado del tipo de nota que se quiere capturar
    $consultaNota = "SELECT Folio FROM NOTA WHERE Lugar=? AND Tipo=? ORDER BY idNota DESC LIMIT 1";
    $resultadoNota = $base->prepare($consultaNota);
    $resultadoNota->execute(array($lugar, $tipo));
    //Construir el folio consecutivo
    switch ($lugar." ".$tipo){
        case 'azcapotzalco Devolución Parcial':
            if($resultadoNota->rowCount()==0){
                $folio = "DPPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "DPPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Factura Completa':
            if($resultadoNota->rowCount()==0){
                $folio = "FCPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "FCPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Entrada Caja':
            if($resultadoNota->rowCount()==0){
                $folio = "ECPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "ECPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Factor 3':
            if($resultadoNota->rowCount()==0){
                $folio = "F3PB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "F3PB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Cambio Físico':
            if($resultadoNota->rowCount()==0){
                $folio = "CFPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "CFPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Entrada Caja Factor 3':
            if($resultadoNota->rowCount()==0){
                $folio = "ECF3PB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "ECF3PB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Muestra':
            if($resultadoNota->rowCount()==0){
                $folio = "MPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "MPB" . (intval($registroFolio[1])+1);
            }
            break;
        case 'azcapotzalco Condonación':
            if($resultadoNota->rowCount()==0){
                $folio = "CPB1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("B", $registroFolio["Folio"]);
                $folio = "CPB" . (intval($registroFolio[1])+1);
            }
            break;
          
        case 'tecamac Devolución Parcial':
            if($resultadoNota->rowCount()==0){
                $folio = "DPTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "DPTC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Factura Completa':
            if($resultadoNota->rowCount()==0){
                $folio = "FCTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "FCTC" . (intval($registroFolio[2])+1);
            }
            break;
        case 'tecamac Entrada Caja':
            if($resultadoNota->rowCount()==0){
                $folio = "ECTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "ECTC" . (intval($registroFolio[2])+1);
            }
            break;
        case 'tecamac Factor 3':
            if($resultadoNota->rowCount()==0){
                $folio = "F3TC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "F3TC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Cambio Físico':
            if($resultadoNota->rowCount()==0){
                $folio = "CFTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "CFTC" . (intval($registroFolio[2])+1);
            }
            break;
        case 'tecamac Entrada Caja Factor 3':
            if($resultadoNota->rowCount()==0){
                $folio = "ECF3TC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "ECF3TC" . (intval($registroFolio[2])+1);
            }
            break;
        case 'tecamac Muestra':
            if($resultadoNota->rowCount()==0){
                $folio = "MTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "MTC" . (intval($registroFolio[1])+1);
            }
            break;
        case 'tecamac Condonación':
            if($resultadoNota->rowCount()==0){
                $folio = "CTC1";
            }
            else{
                $registroFolio = $resultadoNota->fetch(PDO::FETCH_ASSOC);
                $registroFolio = explode("C", $registroFolio["Folio"]);
                $folio = "CTC" . (intval($registroFolio[2])+1);
            }
            break;
    }
    

    $datos["lugar"] = $lugar;
    $datos["tipo"] = $tipo;
    $datos["folio"] = $folio;

    $base = null;
    echo json_encode($datos);
?>