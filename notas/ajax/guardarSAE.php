<?php

    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $notaSAE = $_POST["notaSAEForm"];
    $folio = $_POST["folioNota"];
    $archivo = $_FILES['pdf']['name'];
    $ruta = $_FILES['pdf']['tmp_name'];
    $destino = "..\\cargas\\".$archivo;
    $info = new SplFileInfo($archivo);
    $tipo =  $info->getExtension();
    $estatus = "Correcto";
    $estatusNota = "Correcta";
    $datos = array();
    $base = conexion_local();

    if($tipo=="pdf"){
        //Consulta para guardar la nota sae
        $consultaGuardarSAE = "UPDATE NOTA SET NotaSAE=?, Pdf=?, Estatus=? WHERE Folio=?";
        $resultadoGuardarSAE = $base->prepare($consultaGuardarSAE);
        $resultadoGuardarSAE->execute(array($notaSAE, $archivo, $estatusNota, $folio));

        switch ($resultadoGuardarSAE->rowCount()){
            case 1:
                move_uploaded_file($ruta, $destino);
                break;
            
            case 0:
                $estatus = "Error";
                break;
        }
        $resultadoGuardarSAE->closeCursor();
    }
    else{
        $estatus = "Archivo invalido";
        $datos["tipo"] = $tipo;
    }
    $base = null;

    $datos["estatus"] = $estatus;
    $datos["notaSAE"] = $notaSAE;
    $datos["folio"] = $folio;

    echo json_encode($datos);
?>