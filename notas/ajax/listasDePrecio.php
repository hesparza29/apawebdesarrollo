<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $id = $_POST["id"];
    $clave = $_POST["clave"];
    $estatus = "Correcto";
    $modulo = "Nota Nueva";
    $datos = array();
    $precio = array();
    $descripcion = array();
    $listas = array();
    $contador = 0;

    $base = conexion_local();
    //Consulta para obtener las listas de precio del producto
    $consultaPrecios = "SELECT Precio, LISTA_DE_PRECIO.Descripcion, idProductoListaDePrecio FROM PRODUCTO 
                            INNER JOIN PRODUCTO_LISTA_DE_PRECIO ON PRODUCTO.idProducto=PRODUCTO_LISTA_DE_PRECIO.idProducto 
                            INNER JOIN LISTA_DE_PRECIO ON PRODUCTO_LISTA_DE_PRECIO.idListaDePrecio=LISTA_DE_PRECIO.idListaDePrecio 
                            WHERE NumeroAPA=?";
    $resultadoPrecios = $base->prepare($consultaPrecios);
    //Consulta para obtener el permiso del usuario para ver listas de precios
    $consultaPermiso = "SELECT Identificador FROM MODULO 
                            INNER JOIN USUARIO_MODULO ON MODULO.idModulo=USUARIO_MODULO.idModulo 
                            INNER JOIN USUARIO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario 
                            WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);

    switch ($registroPermiso["Identificador"]){
        case "listasDePrecio":
            $resultadoPrecios->execute(array($clave));
            if($resultadoPrecios->rowCount()>0){
                while($registroPrecios = $resultadoPrecios->fetch(PDO::FETCH_ASSOC)){
                    $precio[$contador] = $registroPrecios["Precio"];
                    $descripcion[$contador] = $registroPrecios["Descripcion"];
                    $listas[$contador] = $registroPrecios["idProductoListaDePrecio"];
                    $contador++;
                }

                $datos["precio"] = $precio;
                $datos["descripcion"] = $descripcion;
                $datos["listas"] = $listas;
            }
            else {
                $estatus = "Sin información";
            }
            $resultadoPrecios->closeCursor();
            break;
        
        default:
            $estatus = "Sin permiso";
            break;
    }

    $resultadoPermiso->closeCursor();

    $base = null;
    
    $datos["estatus"] = $estatus;
    $datos["id"] = $id;
    $datos["clave"] = $clave;

    echo json_encode($datos);

?>