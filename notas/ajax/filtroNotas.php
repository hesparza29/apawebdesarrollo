<?php

    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $numeroCliente = $_POST["numeroCliente"];
    $nombreCliente = $_POST["nombreCliente"];
    $fecha = $_POST["fecha"];
    $folio = $_POST["folio"];
    $folioRecepcion = $_POST["folioRecepcion"];
    $clave = $_POST["clave"];
    $modulo = "Nota";
    $estatus = "Correcto";
    $folios = array();
    $foliosRecepcion = array();
    $clientes = array();
    $notaSAE = array();
    $totales = array();
    $fechas = array();
    $estatuses = array();
    $clases = array();
    $usuarios = array();
    $pdf = array();
    $pdfCancelada = array();
    $datos = array();
    $contador = 0;
    $base = conexion_local();

    //Obtener las cobranzas que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
    INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
    INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
    WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    switch ($permiso){
        case 'administrador':
            if($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $folio));
            }
            //Grupos de 5
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folioRecepcion, $clave));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folioRecepcion, $folio));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $clave, $folio));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folioRecepcion, $clave, $folio));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $folio));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $folio));
            }
            //Grupos 4
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folioRecepcion));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $clave));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente  
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folio));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=?  
                                AND FolioRecepcion=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folioRecepcion, $clave));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente  
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=?  
                                AND FolioRecepcion=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folioRecepcion, $folio));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? 
                                AND NumeroAPA=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $clave, $folio));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folioRecepcion, $clave));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND FolioRecepcion=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folioRecepcion, $folio));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $clave, $folio));
            }
            elseif($numeroCliente!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folioRecepcion, $clave, $folio));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=?  
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folioRecepcion, $clave));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente  
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folioRecepcion, $folio));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $clave, $folio));
            }
            elseif($nombreCliente!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=?  
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folioRecepcion, $clave, $folio));
            }
            elseif($fecha!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folioRecepcion, $clave, $folio));
            }
            //Grupos de 3
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha)));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND FolioRecepcion=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folioRecepcion));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $clave));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folio));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Fecha=? AND FolioRecepcion=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folioRecepcion));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND Fecha=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $clave));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Fecha=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folio));
            }
            elseif($numeroCliente!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND FolioRecepcion=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folioRecepcion, $clave));
            }
            elseif($numeroCliente!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND FolioRecepcion=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folioRecepcion, $folio));
            }
            elseif($numeroCliente!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $clave, $folio));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Fecha=? AND FolioRecepcion=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folioRecepcion));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND Fecha=? AND NumeroAPA=?  
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $clave));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Fecha=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folio));
            }
            elseif($nombreCliente!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND FolioRecepcion=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folioRecepcion, $clave));
            }
            elseif($nombreCliente!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND FolioRecepcion=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folioRecepcion, $folio));
            }
            elseif($nombreCliente!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND NumeroAPA=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $clave, $folio));
            }
            elseif($fecha!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE Fecha=? AND FolioRecepcion=? AND NumeroAPA=?  
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folioRecepcion, $clave));
            }
            elseif($fecha!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? AND FolioRecepcion=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folioRecepcion, $folio));
            }
            elseif($fecha!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE Fecha=? AND NumeroAPA=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $clave, $folio));
            }
            elseif($folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE FolioRecepcion=? AND NumeroAPA=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folioRecepcion, $clave, $folio));
            }
            //Grupos 2
            elseif($numeroCliente!=""&&$nombreCliente!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=?  
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente));
            }
            elseif($numeroCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha)));
            }
            elseif($numeroCliente!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND FolioRecepcion=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folioRecepcion));
            }
            elseif($numeroCliente!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $clave));
            }
            elseif($numeroCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folio));
            }
            elseif($nombreCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha)));
            }
            elseif($nombreCliente!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND FolioRecepcion=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folioRecepcion));
            }
            elseif($nombreCliente!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $clave));
            }
            elseif($nombreCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folio));
            }
            elseif($fecha!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? AND FolioRecepcion=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folioRecepcion));
            }
            elseif($fecha!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE Fecha=? AND NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $clave));
            }
            elseif($fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folio));
            }
            elseif($folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE FolioRecepcion=? AND NumeroAPA=?  
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folioRecepcion, $clave));
            }
            elseif($folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE FolioRecepcion=? AND Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folioRecepcion, $folio));
            }
            elseif($clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NumeroAPA=? AND Folio=?
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($clave, $folio));
            }
            //Grupos de 1
            elseif ($numeroCliente!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente));
            }
            elseif ($nombreCliente!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente));
            }
            elseif ($fecha!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha)));
            }
            elseif ($folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE FolioRecepcion=?  
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folioRecepcion));
            }
            elseif ($clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NumeroAPA=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($clave));
            }
            elseif ($folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Folio=? 
                                ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folio));
            }
          break;
        //Para los usuarios que no son administradores
        default:
            if($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $folio, $permiso));
            }
            //Grupos de 5
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folioRecepcion, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $clave, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folioRecepcion, $clave, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $folio, $permiso));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $folio, $permiso));
            }
            //Grupos 4
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folioRecepcion, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $clave, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente  
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=?  
                                AND FolioRecepcion=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folioRecepcion, $clave, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente  
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=?  
                                AND FolioRecepcion=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folioRecepcion, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? 
                                AND NumeroAPA=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $clave, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $permiso));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND FolioRecepcion=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folioRecepcion, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $clave, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folioRecepcion, $clave, $folio, $permiso));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=?  
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folioRecepcion, $clave, $permiso));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente  
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND FolioRecepcion=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folioRecepcion, $folio, $permiso));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $clave, $folio, $permiso));
            }
            elseif($nombreCliente!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=?  
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folioRecepcion, $clave, $folio, $permiso));
            }
            elseif($fecha!=""&&$folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE Fecha=? 
                                AND FolioRecepcion=? AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folioRecepcion, $clave, $folio, $permiso));
            }
            //Grupos de 3
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Fecha=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, fechaConsulta($fecha), $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND FolioRecepcion=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folioRecepcion, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $clave, $permiso));
            }
            elseif($numeroCliente!=""&&$nombreCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Fecha=? AND FolioRecepcion=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folioRecepcion, $permiso));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND Fecha=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $clave, $permiso));
            }
            elseif($numeroCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Fecha=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND FolioRecepcion=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folioRecepcion, $clave, $permiso));
            }
            elseif($numeroCliente!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND FolioRecepcion=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folioRecepcion, $folio, $permiso));
            }
            elseif($numeroCliente!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $clave, $folio, $permiso));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Fecha=? AND FolioRecepcion=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folioRecepcion, $permiso));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND Fecha=? AND NumeroAPA=?  
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $clave, $permiso));
            }
            elseif($nombreCliente!=""&&$fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Fecha=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $folio, $permiso));
            }
            elseif($nombreCliente!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND FolioRecepcion=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folioRecepcion, $clave, $permiso));
            }
            elseif($nombreCliente!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND FolioRecepcion=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folioRecepcion, $folio, $permiso));
            }
            elseif($nombreCliente!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND NumeroAPA=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $clave, $folio, $permiso));
            }
            elseif($fecha!=""&&$folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE Fecha=? AND FolioRecepcion=? AND NumeroAPA=?  
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folioRecepcion, $clave, $permiso));
            }
            elseif($fecha!=""&&$folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? AND FolioRecepcion=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folioRecepcion, $folio, $permiso));
            }
            elseif($fecha!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE Fecha=? AND NumeroAPA=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $clave, $folio, $permiso));
            }
            elseif($folioRecepcion!=""&&$clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE FolioRecepcion=? AND NumeroAPA=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folioRecepcion, $clave, $folio, $permiso));
            }
            //Grupos 2
            elseif($numeroCliente!=""&&$nombreCliente!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND CLIENTE.Nombre=?  
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $nombreCliente, $permiso));
            }
            elseif($numeroCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Fecha=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, fechaConsulta($fecha), $permiso));
            }
            elseif($numeroCliente!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND FolioRecepcion=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folioRecepcion, $permiso));
            }
            elseif($numeroCliente!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NOTA.idCliente=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $clave, $permiso));
            }
            elseif($numeroCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $folio, $permiso));
            }
            elseif($nombreCliente!=""&&$fecha!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Fecha=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, fechaConsulta($fecha), $permiso));
            }
            elseif($nombreCliente!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND FolioRecepcion=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folioRecepcion, $permiso));
            }
            elseif($nombreCliente!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE CLIENTE.Nombre=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $clave, $permiso));
            }
            elseif($nombreCliente!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $folio, $permiso));
            }
            elseif($fecha!=""&&$folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? AND FolioRecepcion=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folioRecepcion, $permiso));
            }
            elseif($fecha!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE Fecha=? AND NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $clave, $permiso));
            }
            elseif($fecha!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $folio, $permiso));
            }
            elseif($folioRecepcion!=""&&$clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE FolioRecepcion=? AND NumeroAPA=?  
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folioRecepcion, $clave, $permiso));
            }
            elseif($folioRecepcion!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE FolioRecepcion=? AND Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folioRecepcion, $folio, $permiso));
            }
            elseif($clave!=""&&$folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NumeroAPA=? AND Folio=?
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($clave, $folio, $permiso));
            }
            //Grupos de 1
            elseif ($numeroCliente!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE NOTA.idCliente=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($numeroCliente, $permiso));
            }
            elseif ($nombreCliente!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE CLIENTE.Nombre=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($nombreCliente, $permiso));
            }
            elseif ($fecha!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Fecha=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array(fechaConsulta($fecha), $permiso));
            }
            elseif ($folioRecepcion!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada  
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE FolioRecepcion=?  
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folioRecepcion, $permiso));
            }
            elseif ($clave!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada, NumeroAPA 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                INNER JOIN NOTA_PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA.idNota=NOTA_PRODUCTO_LISTA_DE_PRECIO.idNota 
                                INNER JOIN PRODUCTO_LISTA_DE_PRECIO 
                                ON NOTA_PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio=PRODUCTO_LISTA_DE_PRECIO.idProductoListaDePrecio 
                                INNER JOIN PRODUCTO ON PRODUCTO_LISTA_DE_PRECIO.idProducto=PRODUCTO.idProducto 
                                WHERE NumeroAPA=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($clave, $permiso));
            }
            elseif ($folio!=""){
                $consulta = "SELECT Folio, FolioRecepcion, NOTA.idCliente, CLIENTE.Nombre AS Cliente, 
                                NotaSAE, Total, Fecha, NOTA.Estatus, USUARIO.Nombre, USUARIO.Apellido, 
                                Pdf, PdfCancelada 
                                FROM NOTA INNER JOIN USUARIO
                                ON NOTA.idUsuario=USUARIO.idUsuario 
                                INNER JOIN CLIENTE ON NOTA.idCliente=CLIENTE.idCliente 
                                WHERE Folio=? 
                                AND Lugar=? ORDER BY NOTA.idNota DESC";
                $resultado = $base->prepare($consulta);
                $resultado->execute(array($folio, $permiso));
            }
            break;
    }

    if($resultado->rowCount()>0){
        while($registro = $resultado->fetch(PDO::FETCH_ASSOC)){
            $folios[$contador] = $registro["Folio"];
            $foliosRecepcion[$contador] = $registro["FolioRecepcion"];
            $clientes[$contador] = $registro["idCliente"] . " " . $registro["Cliente"];
            $notaSAE[$contador] = $registro["NotaSAE"];
            $totales[$contador] = $registro["Total"];
            $fechas[$contador] = fechaStandar($registro["Fecha"]);
            $estatuses[$contador] = $registro["Estatus"];
            switch($registro["Estatus"]){
                case 'Revisando':
                    $clases[$contador] = "revisando";
                    break;
                case 'Cancelada':
                    $clases[$contador] = "cancelada";
                    break;
                case 'Correcta':
                    $clases[$contador] = "correcta";
                    break;
                
            }
            $usuarios[$contador] = $registro["Nombre"] . " " . $registro["Apellido"];
            $pdf[$contador] = $registro["Pdf"];
            $pdfCancelada[$contador] = $registro["PdfCancelada"];
            $contador++;
        }
    }
    else{
        $estatus = "Sin resultados";
    }

    $datos["estatus"] = $estatus;
    $datos["folio"] = $folios;
	$datos["folioRecepcion"] = $foliosRecepcion;
	$datos["cliente"] = $clientes;
	$datos["notaSAE"] = $notaSAE;
	$datos["total"] = $totales;
	$datos["fecha"] = $fechas;
	$datos["estatuses"] = $estatuses;
    $datos["clase"] = $clases;
    $datos["usuario"] = $usuarios;
    $datos["pdf"] = $pdf;
    $datos["pdfCancelada"] = $pdfCancelada;
    

    $base = null;

    echo json_encode($datos);

?>