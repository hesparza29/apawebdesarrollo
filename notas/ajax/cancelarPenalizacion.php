<?php
    require_once("../../funciones.php");
    $cantidad = $_POST["cantidad"];
    $lista = $_POST["lista"];
    $descuento = $_POST["descuento"];
    $productos = count($cantidad);
    $contador = 0;
    $datos = array();
    $costo = array();
    $importe = array();
    $subtotal = array();

    $base = conexion_local();
    //Obtener los precios de los productos
    $consultaPrecio = "SELECT Precio FROM PRODUCTO_LISTA_DE_PRECIO WHERE idProductoListaDePrecio=?";
    $resultadoPrecio = $base->prepare($consultaPrecio);
    for ($i=0; $i<$productos ; $i++) {
        $resultadoPrecio->execute(array($lista[$i]));
        $registroPrecio = $resultadoPrecio->fetch(PDO::FETCH_ASSOC);
        if($cantidad[$i]<1){
            $cantidad[$i] = 1;
        }
        $costo[$contador] = $registroPrecio["Precio"];
        $importe[$contador] = imp($cantidad[$i], $registroPrecio["Precio"]);
        $subtotal[$contador] = sub($descuento, $importe[$contador]);
        $contador++;
    }
    $resultadoPrecio->closeCursor();

    $datos["costo"] = $costo;
    $datos["importe"] = $importe;
    $datos["subtotal"] = $subtotal;

    $base = null;

    echo json_encode($datos);

?>