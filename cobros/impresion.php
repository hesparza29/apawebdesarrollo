<!DOCTYPE html>
<html>
  <head>
  	<title>Cobranza en Efectivo</title>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estiloCobros.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/verificarSesion.js"></script>
    <script type="text/javascript" src="../js/cierreSesion.js"></script>
    <script type="text/javascript" src="../js/cierreInactividad.js"></script>
    <script type="text/javascript" src="../js/formatoNumero.js"></script>
    <script type="text/javascript" src="../js/visualizacion.js"></script>
    <script type="text/javascript" src="../js/imprimir.js"></script>
    <script type="text/javascript" src="ajax/eventos/cargarFacturas.js"></script>
  </head>
  <body>
  <header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-6">
				</div>
				<div class="col-sm-12 col-md-2">
          <input class="btn btn-info" type="button" id="imprimir" value="Imprimir" />
        </div>
        <div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-2">
          <input type="button" class="btn btn-success" id="visualizacion" value="Regresar" />
				</div>
				<div class="col-sm-12 col-md-10">
						<h4 class="text-left" id="cobranzaDelDia"></h4>
				</div>
			</div>
		</div>
  </header>
  <br /><br />
  <section>
		<div class="container">		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-table-striped">
								<thead class="thead-dark">
                  <tr>
                    <th class="text-center" colspan="6">ABASTECEDORA DE PRODUCTOS AUTOMOTRICES</th>
                  </tr>
                  <tr>
                    <th>FACTURA</th>
                    <th>CLIENTE</th>
                    <th>MONTO</th>
                    <th>PAGO</th>
                    <th>OBSERVACIONES</th>
                    <th>NOTA DE CREDITO</th>
                  </tr>
								</thead>
								<tbody id="table">

                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2"><strong>IMPORTE TOTAL FACTURAS</strong></td>
                    <td colspan="4"><strong id="total"></strong></td>
                  </tr>
                </tfoot>
							</table>
						</div>
					</div>
				</div>			
		</div>
	</section>	
  </body>
</html>
