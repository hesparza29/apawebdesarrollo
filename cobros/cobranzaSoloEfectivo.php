<?php
require_once("../funciones.php");
session_start();
$usuario = $_SESSION["user"];
$modulo = "Cobranza";
$permiso = "";
// obtiene los valores para realizar la paginacion
$limit = isset($_POST["limit"]) && intval($_POST["limit"]) > 0 ? intval($_POST["limit"])	: 20;
$offset = isset($_POST["offset"]) && intval($_POST["offset"])>=0	? intval($_POST["offset"])	: 0;

$base = conexion_local();
// realiza la conexion
//$con = new mysqli("50.62.209.84","hesparza","b29194303","aplicacion");
$con = new mysqli("localhost","root","","aplicacion");
$con->set_charset("utf8");
//$base = new PDO('mysql:host=localhost; dbname=aplicacion', 'root', '');
//$base = new PDO("mysql:host=50.62.209.117;dbname=aplicacion","hesparza","b29194303");
//$base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
//$base->exec("SET CHARACTER SET utf8");

// array para devolver la informacion
$json = array();
$data = array();
//consulta que deseamos realizar a la db
//$query = $con->prepare("select id_usuario,nombres,apellidos from  usuarios limit ? offset ?");

//Consulta para obtener el usuario que recibio el importe
$consultaUsuarioRecibe = "SELECT Nombre, Apellido FROM USUARIO WHERE idUsuario=?";
$resultadoUsuarioRecibe = $base->prepare($consultaUsuarioRecibe);
//Obtener las cobranzas que puede ver el usuario
$consultaPermiso = "SELECT Identificador FROM USUARIO 
					INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
					INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
					WHERE Usuario=? AND MODULO.Nombre=?";
$resultadoPermiso = $base->prepare($consultaPermiso);
$resultadoPermiso->execute(array($usuario, $modulo));
$registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
$resultadoPermiso->closeCursor();
$permiso = $registroPermiso["Identificador"];

switch ($permiso){
	case 'administrador':
		$query = $con->prepare("SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario ORDER BY A.idCobro DESC LIMIT ? OFFSET ?");
        $query->bind_param("ii",$limit,$offset);
        $query->execute();
		break;
	
	default:
        $query = $con->prepare("SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante  
                                FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                WHERE B.Lugar=? ORDER BY A.idCobro DESC LIMIT ? OFFSET ?");
        $query->bind_param("sii",$permiso,$limit,$offset);
        $query->execute();
		break;
}

//El limite empieza con 10 y el Offset con 0


// vincular variables a la sentencia preparada
//$query->bind_result($id_usuario, $nombres,$apellidos);
$query->bind_result($folio, $fecha, $total, $nombreElaboro, $apellidoElaboro, $estatus,
                        $importeRecibido, $idUsuarioRecibe, $observacion, $comprobante);

// obtener valores
while ($query->fetch()) {
	$data_json = array();

	$data_json["folio"] = $folio;
    $data_json["fecha"] = fechaStandar($fecha);
    $data_json["total"] = $total;
    $data_json["usuarioElaboro"] = $nombreElaboro . " " . $apellidoElaboro;
    $data_json["estatus"] = $estatus;
    $data_json["importeRecibido"] = $importeRecibido;
    $data_json["observacion"] = $observacion;
    $data_json["comprobante"] = $comprobante;
    //Obtener el usuario que recibio el importe
    $resultadoUsuarioRecibe->execute(array($idUsuarioRecibe));
    $registroUsuarioRecibe = $resultadoUsuarioRecibe->fetch(PDO::FETCH_ASSOC);
    $data_json["usuarioRecibe"] = $registroUsuarioRecibe["Nombre"] . " " . $registroUsuarioRecibe["Apellido"];
    //Obtener la clase en función a el estatus
    switch($estatus){
        case 'Pendiente':
            $data_json["clase"] = "pendiente";
            break;

        case 'Revisando':
            $data_json["clase"] = "revisando";
            break;

        case 'Discrepancia':
            $data_json["clase"] = "discrepancia";
            break;

        case 'Correcto':
            $data_json["clase"] = "correcto";
            break;
        
        default:
            # code...
            break;
    }

	$data[]=$data_json;
}
$resultadoUsuarioRecibe->closeCursor();

$base = null;

switch ($permiso) {
	case 'administrador':
		// obtiene la cantidad de registros
		$cantidad_consulta = $con->query("SELECT count(*) AS total FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                            INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario ORDER BY A.idCobro DESC");
        $row = $cantidad_consulta->fetch_assoc();
        $cantidad['cantidad']=$row['total'];
		break;
	
	default:
		// obtiene la cantidad de registros
		$cantidad_consulta = $con->query("SELECT count(*) AS total FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                            INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                            WHERE B.Lugar='$permiso' ORDER BY A.idCobro DESC");
        $row = $cantidad_consulta->fetch_assoc();
        $cantidad['cantidad']=$row['total'];
		break;
}

// obtiene la cantidad de registros
$json["lista"] = array_values($data);
$json["cantidad"] = array_values($cantidad);
//$json["cantidad"] = array_values([242]);

// envia la respuesta en formato json
header("Content-type:application/json; charset = utf-8");
echo json_encode($json);
exit();
?>
