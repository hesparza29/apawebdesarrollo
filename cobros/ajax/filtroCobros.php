<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $estatusCobro = $_POST["estatus"];
    $fechaInicio = $_POST["fechaInicio"];
    $fechaFin = $_POST["fechaFin"];
    $modulo = "Cobranza";
    $estatus = "";
    $contador = 0;
    $folios = array();
    $fecha = array();
    $usuarioElaboro = array();
    $total = array();
    $estatuses = array();
    $importeRecibido = array();
    $usuarioRecibe = array();
    $observaciones = array();
    $comprobante = array();
    $clase = array();
    $datos = array();

    //Si las fechas vienen vacias setearlas a un valor por defecto
    ($fechaInicio=="") ? $fechaInicio = '19/10/2020' : false;

    ($fechaFin=="") ? $fechaFin = fechaStandar(fecha()) : false;

    $base = conexion_local();

    //Consulta para obtener el usuario que recibio el importe
    $consultaUsuarioRecibe = "SELECT Nombre, Apellido FROM USUARIO WHERE idUsuario=?";
    $resultadoUsuarioRecibe = $base->prepare($consultaUsuarioRecibe);
    //Obtener las cobranzas que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
                        INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
                        INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
                        WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    switch ($permiso){
        case 'administrador':
            if($folio!="" && $estatusCobro!=""){
                $consultaCobros = "SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                    A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                    WHERE B.Folio=? AND A.Estatus=? AND B.Fecha BETWEEN ? AND ? ORDER BY A.idCobro DESC";
                $resultadoCobros = $base->prepare($consultaCobros);
                $resultadoCobros->execute(array($folio, $estatusCobro, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
            }
            else if($folio!=""){
                $consultaCobros = "SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                    A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                    WHERE B.Folio=? AND B.Fecha BETWEEN ? AND ? ORDER BY A.idCobro DESC";
                $resultadoCobros = $base->prepare($consultaCobros);
                $resultadoCobros->execute(array($folio, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
            }
            else if($estatusCobro!=""){
                $consultaCobros = "SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                    A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                    WHERE A.Estatus=? AND B.Fecha BETWEEN ? AND ? ORDER BY A.idCobro DESC";
                $resultadoCobros = $base->prepare($consultaCobros);
                $resultadoCobros->execute(array($estatusCobro, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
            }
            else{
                $consultaCobros = "SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                    A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                    WHERE B.Fecha BETWEEN ? AND ? ORDER BY A.idCobro DESC";
                $resultadoCobros = $base->prepare($consultaCobros);
                $resultadoCobros->execute(array(fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
            }
            break;
        
        default:
            if($folio!="" && $estatusCobro!=""){
                $consultaCobros = "SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                    A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                    WHERE B.Folio=? AND A.Estatus=? AND B.Fecha BETWEEN ? AND ? AND B.Lugar=? ORDER BY A.idCobro DESC";
                $resultadoCobros = $base->prepare($consultaCobros);
                $resultadoCobros->execute(array($folio, $estatusCobro, fechaConsulta($fechaInicio), fechaConsulta($fechaFin), $permiso));
            }
            else if($folio!=""){
                $consultaCobros = "SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                    A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                    WHERE B.Folio=? AND B.Fecha BETWEEN ? AND ? AND B.Lugar=? ORDER BY A.idCobro DESC";
                $resultadoCobros = $base->prepare($consultaCobros);
                $resultadoCobros->execute(array($folio, fechaConsulta($fechaInicio), fechaConsulta($fechaFin), $permiso));
            }
            else if($estatusCobro!=""){
                $consultaCobros = "SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                    A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                    WHERE A.Estatus=? AND B.Fecha BETWEEN ? AND ? AND B.Lugar=? ORDER BY A.idCobro DESC";
                $resultadoCobros = $base->prepare($consultaCobros);
                $resultadoCobros->execute(array($estatusCobro, fechaConsulta($fechaInicio), fechaConsulta($fechaFin), $permiso));
            }
            else{
                $consultaCobros = "SELECT B.Folio, B.Fecha, A.Total, C.Nombre, C.Apellido, A.Estatus, 
                                    A.importeRecibido, A.idUsuarioRecibe, A.Observacion, A.Comprobante 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                    WHERE B.Fecha BETWEEN ? AND ? AND B.Lugar=? ORDER BY A.idCobro DESC";
                $resultadoCobros = $base->prepare($consultaCobros);
                $resultadoCobros->execute(array(fechaConsulta($fechaInicio), fechaConsulta($fechaFin), $permiso));
            }
            break;
    }
    
    if($resultadoCobros->rowCount()>0){
        while ($registroCobro = $resultadoCobros->fetch(PDO::FETCH_ASSOC)){
            $folios[$contador] = $registroCobro["Folio"];
            $fecha[$contador] = fechaStandar($registroCobro["Fecha"]);
            $total[$contador] = $registroCobro["Total"];
            $usuarioElaboro[$contador] = $registroCobro["Nombre"] . " " . $registroCobro["Apellido"];
            $estatuses[$contador] = $registroCobro["Estatus"];
            $importeRecibido[$contador] = $registroCobro["importeRecibido"];
            $observaciones[$contador] = $registroCobro["Observacion"];
            $comprobante[$contador] = $registroCobro["Comprobante"];
            //Obtener el usuario que recibio el importe
            $resultadoUsuarioRecibe->execute(array($registroCobro["idUsuarioRecibe"]));
            $registroUsuarioRecibe = $resultadoUsuarioRecibe->fetch(PDO::FETCH_ASSOC);
            $usuarioRecibe[$contador] = $registroUsuarioRecibe["Nombre"] . " " . $registroUsuarioRecibe["Apellido"];
            //Obtener la clase en función a el estatus
            switch($estatuses[$contador]){
                case 'Pendiente':
                    $clase[$contador] = "pendiente";
                    break;

                case 'Revisando':
                    $clase[$contador] = "revisando";
                    break;

                case 'Discrepancia':
                    $clase[$contador] = "discrepancia";
                    break;

                case 'Correcto':
                    $clase[$contador] = "correcto";
                    break;
                
                default:
                    # code...
                    break;
            }
            $contador++;
        }
        $resultadoUsuarioRecibe->closeCursor();
        $resultadoCobros->closeCursor();   

        $datos["folio"] = $folios;
        $datos["fecha"] = $fecha;
        $datos["total"] = $total;
        $datos["usuarioElaboro"] = $usuarioElaboro;
        $datos["estatuses"] = $estatuses;
        $datos["importeRecibido"] = $importeRecibido;
        $datos["usuarioRecibe"] = $usuarioRecibe;
        $datos["observaciones"] = $observaciones;
        $datos["comprobante"] = $comprobante;
        $datos["clase"] = $clase;
        $estatus = "Correcto";
    }
    else{
        $estatus = "Sin resultados";
    }

    $base = null;

    $datos["estatus"] = $estatus;
    echo json_encode($datos);

?>