$(document).ready(function(){
    $("#buscar").click(function(){
        if($("#folioCobranza").val()!="" || $("#estatus").val()!="" || 
            $("#fechaInicio").val()!="" || $("#fechaFin").val()!=""){

                let fechaInicio = $("#fechaInicio").val();
                    fechaInicio = fechaInicio.split("/");
                    fechaInicio = new Date(fechaInicio[2], fechaInicio[1] - 1, fechaInicio[0]);
                let fechaFin = $("#fechaFin").val();
                    fechaFin = fechaFin.split("/");
                    fechaFin = new Date(fechaFin[2], fechaFin[1] - 1, fechaFin[0]);
                let fecha = new Date('2020', '9', '19');

                if($("#fechaInicio").val()!="" && $("#fechaFin").val()!=""){
                    if(fecha>fechaInicio){
                        alert("La fecha inicio debe de ser del 19/10/2020 en adelante, verificala por favor");
                    }
                    else if(fechaInicio>fechaFin){
                        alert("La fecha inicio debe de ser menor a la fecha fin, verificalas por favor");
                    }
                    else{
                        enviar();
                    }
                }
                else if($("#fechaInicio").val()!="" && $("#fechaFin").val()==""){
                    if(fecha>fechaInicio){
                        alert("La fecha inicio debe de ser del 19/10/2020 en adelante, verificala por favor");
                    }
                    else{
                        enviar();
                    }
                }
                else if($("#fechaInicio").val()=="" && $("#fechaFin").val()!=""){
                    if(fecha>fechaFin){
                        alert("La fecha fin debe de ser del 19/10/2020 en adelante, verificala por favor");
                    }
                    else{
                        enviar();
                    }
                }
                else{
                    enviar();
                }
        }
        else{
            alert("Captura al menos un campo, por favor");
        }
    });

    function enviar(){
      
        const parametros =
        {
          folio: $("#folioCobranza").val(),
          estatus: $("#estatus").val(),
          fechaInicio: $("#fechaInicio").val(), 
          fechaFin: $("#fechaFin").val(),
        }
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/filtroCobros.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
        function inicioEnvio(){
            console.log("Cargando Filtro Cobros...");
        }
    
        function llegada(datos){
            console.log(datos);
            switch(datos.estatus){
                case 'Correcto':
                    let contador = Object.keys(datos.folio).length;
                    let contenido = "";
                    for (let i = 0; i < contador; i++){
                        contenido += `<tr class="${datos.clase[i]}">
                                        <td id="folio-${i}">${datos.folio[i]}</td>
                                        <td>${datos.fecha[i]}</td>
                                        <td>${formatNumber.new(datos.total[i], "$")}</td>
                                        <td>${datos.usuarioElaboro[i]}</td>
                                        <td id="estatus-${i}">${datos.estatuses[i]}</td>
                                        <td id="importe-${i}">${formatNumber.new(datos.importeRecibido[i], "$")}</td>
                                        <td>${datos.usuarioRecibe[i]}</td>
                                        <td>${datos.observaciones[i]}</td>
                                        <td>
                                            <input type="button" class="btn btn-info btn-sm verImpresion" 
                                                id="cobros-${datos.folio[i]}" value="Ver" />
                                            &nbsp;
                                            <input type="button" class="btn btn-success btn-sm revisar" 
                                                id="revisar-${i}" value="+"/>
                                            &nbsp;
                                            <input type="button" class="btn btn-warning btn-sm comprobante"
                                                id="comprobante-${i}" value="Comprobante"/>
                                            <input type="hidden" id="archivo-${i}" value="${datos.comprobante[i]}"/>
                                        </td>
                                    </tr>`;
                    }
                    $("#paginador").hide();
                    $("#table").empty();
                    $("#table").append(contenido);
                    break;
                case 'Sin resultados':
                    alert("No se encontraron datos en la consulta");
                    break;
            
                default:
                break;
            }
        }
        
        function problemas(textError, textStatus) {
                //var error = JSON.parse(textError);
                alert("Problemas en el Servlet: " + JSON.stringify(textError));
                alert("Problemas en el servlet: " + JSON.stringify(textStatus));
        }
});