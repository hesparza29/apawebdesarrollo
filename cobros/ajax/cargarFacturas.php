<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $contador = 0;
    $clave = array();
    $cliente = array();
    $importe = array();
    $metodo = array();
    $observaciones = array();
    $notaDeCredito = array();
    $total = 0;
    $estatus = "Correcto";
    $modulo = "Cobranza";
    $metodoCobranza = "Efectivo"; 
    $permiso = "";
    $fecha = "";

    $base = conexion_local();
    //Obtener las cobranzas que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
    INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
    INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
    WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    //Obtener la información de la cobranza
    switch ($permiso){
        case 'administrador':
            $consultaCobranza = "SELECT Fecha FROM COBRANZA WHERE Folio=?";
            $resultadoCobranza = $base->prepare($consultaCobranza);
            $resultadoCobranza->execute(array($folio));
            break;
        
        default:
            $consultaCobranza = "SELECT Fecha FROM COBRANZA WHERE Folio=? AND Lugar=?";
            $resultadoCobranza = $base->prepare($consultaCobranza);
            $resultadoCobranza->execute(array($folio, $permiso));
            break;
    }
    //Verificar si existe la cobranza con el folio solicitado
    switch ($resultadoCobranza->rowCount()) {
        case 1:
            $registroCobranza = $resultadoCobranza->fetch(PDO::FETCH_ASSOC);
            $datos["fecha"] = fechaStandar($registroCobranza["Fecha"]);
        
            //Obtener todas las facturas que se capturaron en efectivo dentro de la cobranza
            $consultaPartidas = "SELECT CLAVE, CLIENTE, NOMBRE, IMPORTE, OBSERVACIONES, 
                                importeNotaDeCredito FROM CARGAS 
                                WHERE ENTRADA=? AND METODO=? ORDER BY NUMERO_ENTRADA ASC";
            $resultadoPartidas = $base->prepare($consultaPartidas);
            $resultadoPartidas->execute(array($folio, $metodoCobranza));
            while ($registroPartidas = $resultadoPartidas->fetch(PDO::FETCH_ASSOC)){
                $clave[$contador] = $registroPartidas["CLAVE"];
                $cliente[$contador] = $registroPartidas["CLIENTE"] . " " . $registroPartidas["NOMBRE"];
                $importe[$contador] = $registroPartidas["IMPORTE"];
                $metodo[$contador] = "Efectivo";
                $observaciones[$contador] = $registroPartidas["OBSERVACIONES"];
                if($registroPartidas["importeNotaDeCredito"]>0.00){
                    $notaDeCredito[$contador] = $registroPartidas["importeNotaDeCredito"];
                }
                else{
                    $notaDeCredito[$contador] = "";
                }
                //Calculando el total de las facturas pagadas en efectivo
                $total += $registroPartidas["IMPORTE"];
                //Descontando el importe que exista de notas de crédito
                $total -= $registroPartidas["importeNotaDeCredito"];
                $contador++;
            }
            $resultadoPartidas->closeCursor();
            $datos["clave"] = $clave;
            $datos["cliente"] = $cliente;
            $datos["importe"] = $importe;
            $datos["metodo"] = $metodo;
            $datos["observaciones"] = $observaciones;
            $datos["notaDeCredito"] = $notaDeCredito;
            $datos["total"] = round($total*100)/100;
            $datos["cobranzaDelDia"] = saber_dia_cobro($datos["fecha"]) . " " . $folio;
            break;
        
        case 0:
            $estatus = "Sin resultados";
            $datos["folio"] = $folio;
            break;
    }
    
    $resultadoCobranza->closeCursor();
    
    $base = null;

    $datos["estatus"] = $estatus;

    echo json_encode($datos);
?>