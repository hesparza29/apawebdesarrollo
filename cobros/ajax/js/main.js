//URL para llamar la peticion por ajax
let url_listar_usuario = "cobranzaSoloEfectivo.php";

$( document ).ready(function() {
   // se genera el paginador
   paginador = $(".pagination");
	// cantidad de items por pagina
	let items = 10, numeros =4;
	// inicia el paginador
	init_paginator(paginador,items,numeros);
	// se envia la peticion ajax que se realizara como callback
	set_callback(get_data_callback);
	cargaPagina(0);
});

function get_data_callback(){
	$.ajax({
		data:{
		limit: itemsPorPagina,
		offset: desde,
		},
		type:"POST",
		url:url_listar_usuario
	}).done(function(data,textStatus,jqXHR){
		// obtiene la clave lista del json data
		let lista = data.lista;
		$("#table").html("");

		// si es necesario actualiza la cantidad de paginas del paginador
		if(pagina==0){
			creaPaginador(data.cantidad);
		}
		// genera el cuerpo de la tabla
		$.each(lista, function(ind, elem){
			$(
				`<tr class="${elem.clase}">
					<td id="folio-${ind}">${elem.folio}</td>
					<td>${elem.fecha}</td>
					<td>${formatNumber.new(elem.total, "$")}</td>
					<td>${elem.usuarioElaboro}</td>
					<td id="estatus-${ind}">${elem.estatus}</td>
					<td>${formatNumber.new(elem.importeRecibido, "$")}</td>
					<td>${elem.usuarioRecibe}</td>
					<td>${elem.observacion}</td>
					<td>
						<input type="button" class="btn btn-info btn-sm verImpresion" id="cobros-${elem.folio}" value="Ver" />
						&nbsp;
						<input type="button" class="btn btn-success btn-sm revisar" id="revisar-${ind}" value="+"/>
						&nbsp;
						<input type="button" class="btn btn-warning btn-sm comprobante" id="comprobante-${ind}" value="Comprobante"/>
						<input type="hidden" id="archivo-${ind}" value="${elem.comprobante}"/>
					</td>
				</tr>`
			).appendTo($("#table"));
		});
		
	}).fail(function(jqXHR,textStatus,textError){
		alert(textError);
	});

}
