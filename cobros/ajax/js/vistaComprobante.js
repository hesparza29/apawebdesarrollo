$(document).ready(function(){
    $("#table").on('click', '.comprobante', function(){
        //Obteniendo el id de la solicitud
        var id = $(this).attr("id");
        id = id.split("-");
        id = id[1];
        //Obteniendo el folio de la solicitud
        var folio = $("#folio-"+id+"").text();
        //Obteniendo el nombre del archivo
        var archivo = $("#archivo-"+id+"").attr('value');
        
        switch (archivo) {
            case "":
                alert("La cobranza con folio "+folio+" aún no cuenta con un comprobante asignado");
                break;
        
            default:
                //Desarrollo
                var url = window.location.host+"/apawebdesarrollo/cobros/cargas/"+archivo;
                //Produccion
                //var url = "apawebdesarrollo.com/cobros/cargas/"+archivo;
                window.open('http://'+url+'', '_blank');
                break;
        }
    });
});