$(document).ready(function(){
    $('#table').on('click', '.revisar', function(){

        //Obteniendo el id de la solicitud
        let id = $(this).attr("id");
            id = id.split("-");
            id = id[1];
        //Obteniendo el folio de la solicitud
        let folio = $("#folio-"+id+"").text();
        //Asignando el folio a el formulario
        $("#folio").val(folio);
        //Obteniendo el estatus
        let estatus = $("#estatus-"+id+"").text();
        //Asignando el estatus a el formulario
        $("#estatusForm").val(estatus);
        //Obteniendo el nombre del archivo
        let archivo = $("#archivo-"+id+"").attr('value');

        //Mostrando las partes del formulario en función del estatus
        switch (estatus) {
            case "Pendiente":
                $("#vistaImporteRecibido").show();
                $("#vistaComprobante").hide();
                $("#vistaMensaje").hide();
                
                break;

            case "Revisando":
                $("#vistaImporteRecibido").hide();
                $("#vistaComprobante").show();
                $("#vistaMensaje").hide();

                break;

            case "Discrepancia":
                if(archivo==""){
                    $("#vistaImporteRecibido").hide();
                    $("#vistaComprobante").show();
                    $("#vistaMensaje").hide();
                }
                else{
                    $("#vistaImporteRecibido").hide();
                    $("#vistaComprobante").hide();
                    $("#vistaMensaje").show();
                }
    
                break;
        
            default:
                $("#vistaImporteRecibido").hide();
                $("#vistaComprobante").hide();
                $("#vistaMensaje").show();

                break;
        }

        $( "#revisionVista" ).dialog({
            height: 350,
            width: 410,
            dialogClass: "no-close",
            buttons: [
              {
                class: "btn btn-danger",
                text: "Cancelar",
                click: function() {
                  $( this ).dialog( "close" );
                }
              },
              {
                class: "btn btn-success",
                text: "Enviar",
                click: function() {
                    switch (estatus) {
                        case "Pendiente":
                            if($("#importeRecibido").val()>0){
                                enviar();
                            }
                            else{
                                alert("Captura el importe recibido, dicho importe debe ser mayor a 0.00, por favor");
                            }
                            break;
                        
                        case "Revisando":
                            if($("#comprobante").val()!=""){
                                enviar();
                            }
                            else{
                                alert("Introduce el comprobante del depósito, por favor");
                            }
                            break;

                        case "Discrepancia":
                            if(archivo==""){
                                if($("#comprobante").val()!=""){
                                    enviar();
                                }
                                else{
                                    alert("Introduce el comprobante del depósito, por favor");
                                }
                            }
                            else{
                                alert("El proceso finalizó");
                            }
                            
                            break;
                    
                        default:
                            alert("El proceso finalizó");
                            break;
                    }
                }
              }
            ]
        });
    });

    function enviar() {

        var formData = new FormData(document.getElementById("formularioCobros"));

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: false, //Especificaremos el tipo de contenido
            url: "ajax/guardarCobro.php", //Sera el archivo que va a procesar la petición AJAX
            data: formData,
            cache: false,
            processData: false,
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 10000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando guardado del cobro...");
    }

    function llegada(datos) {
        console.log(datos);
        switch (datos.estatus) {
            case "Importe recibido":
                alert("Se registro un importe por la cantidad de "+datos.importeRecibido+" para la cobranza con folio "+datos.folio);
                //Desarrollo
                let url = window.location.host+"/apawebdesarrollo/cobros/impresionDeposito.php?folio="+datos.folio+"&total="+datos.total;
                //Produccion
                //let url = "apawebdesarrollo.com/cobros/impresionDeposito.php?folio="+datos.folio+"&total="+datos.total;
                window.open('http://'+url+'', '_blank');
                location.reload();
                break;

            case "Usuarios diferentes":
                alert("El usuario "+datos.usuario+" es el único que puede realizar la carga del archivo, verificalo por favor");
                location.reload();
                break;
            
            case "Carga de archivo":
                alert("Se cargo de manera correcta el comprobante del depósito para la cobranza con folio "+datos.folio);
                location.reload();
                break;
        
            default:
                break;
        }
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});
