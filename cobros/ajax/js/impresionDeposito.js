$(document).ready(function(){
    var url = window.location.search;
    url = url.split("?");
    url = url[1];
    url = url.split("&");
    var folio = url[0];
    folio = folio.split("=");
    folio = folio[1];
    var importe = url[1];
    importe = importe.split("=");
    importe = importe[1];

    $("#folio").text(folio);
    $("#importe").text(formatNumber.new(importe, "$"));

});