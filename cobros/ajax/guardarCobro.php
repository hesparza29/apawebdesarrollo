<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $estatus = $_POST["estatusForm"];
    $metodo = "Efectivo";
    $discrepancia = "";
    $estatusCobro = "";
    $datos = array();
    

    $base = conexion_local();
    switch ($estatus) {
        case "Pendiente":
            $importeRecibido = $_POST["importeRecibido"];
            $observacion = $_POST["observacion"];
            //Obteniendo el total del cobro del día
            $consultaTotalCobro = "SELECT Total FROM COBRO WHERE idCobranza=(SELECT idCobranza FROM COBRANZA WHERE Folio=?)";
            $resultadoTotalCobro = $base->prepare($consultaTotalCobro);
            $resultadoTotalCobro->execute(array($folio));
            $registroTotalCobro = $resultadoTotalCobro->fetch(PDO::FETCH_ASSOC);
            $resultadoTotalCobro->closeCursor();
            //Verificar si existe una discrepancia entre el total y el importe recibido
            (($registroTotalCobro["Total"]-$importeRecibido<=-5) || ($registroTotalCobro["Total"]-$importeRecibido>=5)) 
            ? $discrepancia = true
            : $discrepancia = false;
            //Sí existe discrepancia mandamos el correo y cambiamos el estatus del cobro del día
            if($discrepancia){
                $estatusCobro = "Discrepancia";
                //Obtener el nombre y apellido del usuario que recibio el importe
                $consultaUsuario = "SELECT Nombre, Apellido FROM USUARIO WHERE Usuario=?";
                $resultadoUsuario = $base->prepare($consultaUsuario);
                $resultadoUsuario->execute(array($usuario));
                $registroUsuario = $resultadoUsuario->fetch(PDO::FETCH_ASSOC);
                $resultadoUsuario->closeCursor();
                //Enviamos correo para avisar de la discrepancia
                $to = "jagarciar@hulesapa.mx, hesparza@hulesapa.mx, bgarcia@hulesapa.mx";
                $subject = "Discrepancia en cobranza " . $folio;
                $message = "El usuario " . $registroUsuario["Nombre"] . " " . $registroUsuario["Apellido"] . "\n";
                $message .= "recibio un importe por la cantidad de " . $importeRecibido . " pesos \n";
                $message .= "el importe total del cobro del día es por la cantidad de " . $registroTotalCobro["Total"] . "\n";
                $message .= "por lo tanto existe una discrepancia de " . ($importeRecibido-$registroTotalCobro["Total"]) . " pesos";
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                mail($to, $subject, $message, $headers);
            }
            else{
                $estatusCobro = "Revisando";
            }
            //Actualizando el registro en la tabla COBRO
            $consultaActualizarCobro = "UPDATE COBRO SET Estatus=?, importeRecibido=?, 
                                        idUsuarioRecibe=(SELECT idUsuario FROM USUARIO WHERE Usuario=?),  
                                        Observacion=? WHERE idCobranza=(SELECT idCobranza FROM COBRANZA WHERE Folio=?)";
            $resultadoActualizarCobro = $base->prepare($consultaActualizarCobro);
            $resultadoActualizarCobro->execute(array($estatusCobro, $importeRecibido, $usuario, $observacion, $folio));
            if($resultadoActualizarCobro->rowCount()==1){
                $estatus = "Importe recibido";
                $datos["importeRecibido"] = $importeRecibido;
                $datos["total"] = $registroTotalCobro["Total"];
            }
            $resultadoActualizarCobro->closeCursor();

            break;

        case ("Revisando" || "Discrepancia"):
            //Obtener el usuario que capturo el importe recibido
            $consultaIdUsuario = "SELECT idCobro, idUsuarioRecibe, Nombre, Apellido, C.Usuario 
                                    FROM COBRO AS A INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                    INNER JOIN USUARIO AS C ON A.idUsuarioRecibe=C.idUsuario 
                                    WHERE B.folio=?";
            $resultadoIdUsuario = $base->prepare($consultaIdUsuario);
            $resultadoIdUsuario->execute(array($folio));
            $registroIdUsuario = $resultadoIdUsuario->fetch(PDO::FETCH_ASSOC);
            $resultadoIdUsuario->closeCursor();
            if($registroIdUsuario["Usuario"]==$usuario){
                ($estatus=="Revisando") ? $estatusCobro = "Correcto" : $estatusCobro = "Discrepancia";
                //Si el usuario es el mismo proseguimos a guardar el archivo
                $archivo = $_FILES['comprobante']['name'];
                $ruta = $_FILES['comprobante']['tmp_name'];
                $destino = "..\\cargas\\".$archivo;
                move_uploaded_file($ruta, $destino);
                //Despues de guardar el archivo se debe de actualizar el registro del cobro
                $consultaActualizaCobro = "UPDATE COBRO SET Estatus=?, Comprobante=? WHERE idCobro=?";
                $resultadoActualizaCobro = $base->prepare($consultaActualizaCobro);
                $resultadoActualizaCobro->execute(array($estatusCobro, $archivo, $registroIdUsuario["idCobro"]));
                if($resultadoActualizaCobro->rowCount()==1){
                    $estatus = "Carga de archivo";
                }
                $resultadoActualizaCobro->closeCursor();
            }
            else{
                $estatus = "Usuarios diferentes";
                $datos["usuario"] = $registroIdUsuario["Nombre"] . " " . $registroIdUsuario["Apellido"];
            }
            
            break;

        default:
            # code...
            break;
    }
    $base = null;

    $datos["folio"] = $folio;
    $datos["estatus"] = $estatus;

    echo json_encode($datos);
