<!DOCTYPE html>
<html>
<head>
	<title>Visualización Cobros</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloCobros.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/paginator.min.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/recargarPagina.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/verImpresion.js"></script>
	<script type="text/javascript" src="ajax/js/main.js"></script>
	<script type="text/javascript" src="ajax/js/vistaRevisar.js"></script>
	<script type="text/javascript" src="ajax/js/vistaComprobante.js"></script>
	<script type="text/javascript" src="ajax/eventos/filtroCobros.js"></script>
	
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Visualización Cobros del día</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="home" class="btn btn-primary" style='background:url("../imagenes/home3.jpg"); width: 50px; height: 50px;' />
				</div>
				<div class="col-sm-12 col-md-8">
						<h3 class="text-left">Filtro de Búsqueda</h3>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="container">
				<div class="row form-group">
					<div class="col-sm-12 col-md-6">
						<input type="text" id="folioCobranza" class="folioCobranza form-control" placeholder="Folio"/>
					</div>
					<div class="col-sm-12 col-md-6">
						<select name="estatus" id="estatus" class="form-control">
							<option value="">Selecciona un Estatus...</option>
							<option value="Pendiente">Pendiente</option>
							<option value="Revisando">Revisando</option>
							<option value="Discrepancia">Discrepancia</option>
							<option value="Correcto">Correcto</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-12 col-md-6">
						<input type="text" id="fechaInicio" class="fecha form-control" placeholder="Fecha Inicio"/>
					</div>
					<div class="col-sm-12 col-md-6">
						<input type="text" id="fechaFin" class="fecha form-control" placeholder="Fecha Fin"/>
					</div>
				</div>	
				<br />
				<div class="row">
					<div class="col-sm-12 col-md-4">
						
					</div>
					<div class="col-sm-12 col-md-4" id="botonesBusqueda">
						<input type="button" class="btn btn-primary" id="buscar" value="Buscar" />
						<input type="button" class="btn btn-primary" id="recargar" value='Tabla Completa' />
					</div>
					<div class="col-sm-12 col-md-4">
						
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-12 col-md-12 text-center">
						<ul class="pagination" id="paginador"></ul>
					</div>
				</div>		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead class="thead-dark">
									<th>Folio</th>
									<th>Fecha</th>
									<th>Total</th>
									<th>Elaboro</th>
									<th>Estatus</th>
									<th>Importe Recibido</th>
									<th>Recibio</th>
									<th>Observación</th>
									<th colspan="2">Info</th>
								</thead>
								<tbody id="table">

								</tbody>
							</table>
						</div>
					</div>
				</div>			
		</div>
	</section>
	<!--Ventanas de jquery UI para cargar el importe recibido y el comprobante-->
	<div id="revisionVista" title="Revisión del cobro del día" hidden>
		<form enctype="multipart/form-data" id="formularioCobros" method="post">
			<div class="form-row" id="vistaImporteRecibido">
				<div class="form-group col-sm-12 col-md-12">
					<label for="importeRecibido">Importe Recibido</label>
					<input type="number" class="form-control" id="importeRecibido" name="importeRecibido" value="" />
				</div>
				<div class="form-group col-sm-12 col-md-12">
					<label for="observacion">Observación</label>
					<textarea class="form-control" id="observacion" name="observacion" rows="3"></textarea>
				</div>
			</div>
			<div class="form-row" id="vistaComprobante">
				<div class="form-group col-sm-12 col-md-12">
					<label for="comprobante">Comprobante del depósito</label>
					<input type="file" class="form-control" id="comprobante" name="comprobante" value="" />
				</div>
			</div>
			<div class="form-row" id="vistaMensaje">
				<div class="form-group col-sm-12 col-md-12">
					<label for="mensaje" id="mensaje">Proceso completo</label>
				</div>
			</div>
			<input type="hidden" id="folio" name="folio" />
			<input type="hidden" id="estatusForm" name="estatusForm" />
		</form>
	</div>	
	<div id="scriptParaCargas"></div>
</body>
</html>
