<?php
    require_once("../funciones.php");

    $base = conexion_local();

    //Actualizar folio de la tabla pedidos
    $consultaObtenerFolios = "SELECT DISTINCT(FOLIOINTERNO) FROM PEDIDOS";
    $resultadoObtenerFolios = $base->prepare($consultaObtenerFolios);
    $resultadoObtenerFolios->execute(array());

    while ($registroObtenerFolios = $resultadoObtenerFolios->fetch(PDO::FETCH_ASSOC)){
        $folio = explode("-", $registroObtenerFolios["FOLIOINTERNO"]);
        $folio = $folio[0] . $folio[1];
        echo $folio . " " . $registroObtenerFolios["FOLIOINTERNO"] . "<br />";
        $consultaActualizarFolios = "UPDATE PEDIDOS SET FOLIOINTERNO=? WHERE FOLIOINTERNO=?";
        $resultadoActualizarFolios = $base->prepare($consultaActualizarFolios);
        $resultadoActualizarFolios->execute(array($folio, $registroObtenerFolios["FOLIOINTERNO"]));
        $resultadoActualizarFolios->closeCursor();
    }

    $resultadoObtenerFolios->closeCursor();

    //Actualizar folio de la tabla pedidos_vis
    $consultaObtenerFolios = "SELECT DISTINCT(FOLIOINTERNO) FROM PEDIDOS_VIS";
    $resultadoObtenerFolios = $base->prepare($consultaObtenerFolios);
    $resultadoObtenerFolios->execute(array());

    while ($registroObtenerFolios = $resultadoObtenerFolios->fetch(PDO::FETCH_ASSOC)){
        $folio = explode("-", $registroObtenerFolios["FOLIOINTERNO"]);
        $folio = $folio[0] . $folio[1];
        echo $folio . " " . $registroObtenerFolios["FOLIOINTERNO"] . "<br />";
        $consultaActualizarFolios = "UPDATE PEDIDOS_VIS SET FOLIOINTERNO=? WHERE FOLIOINTERNO=?";
        $resultadoActualizarFolios = $base->prepare($consultaActualizarFolios);
        $resultadoActualizarFolios->execute(array($folio, $registroObtenerFolios["FOLIOINTERNO"]));
        $resultadoActualizarFolios->closeCursor();
    }

    $resultadoObtenerFolios->closeCursor();
    $base = null;
    
    echo "La actualización de folios se realizo de manera correcta :)";
?>