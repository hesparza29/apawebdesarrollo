$(document).ready(function () {
    $("body").on('click', '#nota', ()=>{
        setTimeout("location.href='../notas/visualizacion.php'", 500);    
    });

    $("body").on('click', '#carta', ()=>{
        setTimeout("location.href='../cartaFactura/visualizacion.php'", 500);    
    });

    $("body").on('click', '#sae', ()=>{
        setTimeout("location.href='../notaCredito/sae.php'", 500);    
    });

    $("body").on('click', '#pedido', ()=>{
        setTimeout("location.href='../pedido/visualizacion.php'", 500);    
    });

    $("body").on('click', '#altas', ()=>{
        setTimeout("location.href='../altaTrabajador/visualizacion.php'", 500);    
    });

    $("body").on('click', '#analisis', ()=>{
        setTimeout("location.href='../analisis/analisis.php'", 500);    
    });

    $("body").on('click', '#caja', ()=>{
        setTimeout("location.href='../caja/visualizacion.php'", 500);    
    });

    $("body").on('click', '#contado', ()=>{
        setTimeout("location.href='../contados/visualizacion.php'", 500);    
    });

    $("body").on('click', '#cliente', ()=>{
        setTimeout("location.href='../clientes/visualizacion.php'", 500);    
    });

    $("body").on('click', '#factor3', ()=>{
        setTimeout("location.href='../factor3/visualizacion.php'", 500);    
    });

    $("body").on('click', '#cobros', ()=>{
        setTimeout("location.href='../cobros/visualizacion.php'", 500);    
    });

    $("body").on('click', '#consultaF3', ()=>{
        setTimeout("location.href='../consultaf3/visualizacion.php'", 500);    
    });

    $("body").on('click', '#politicas', ()=>{
        setTimeout("location.href='../politicas/visualizacion.php'", 500);    
    });

    $("body").on('click', '#compras', ()=>{
        setTimeout("location.href='../compras/visualizacion.php'", 500);   
    });

    $("body").on('click', '#recepcionDineroRemision', ()=>{
        setTimeout("location.href='../recepcionDineroRemision/visualizacion.php'", 500);   
    });

    $("body").on("click", "#catalogoEmpleado", () => location.href="../catalogoEmpleado/visualizacion.php");
});