$(document).ready(function(){
    //Ocultar los módulos principales de los departamentos
    $("#head-ventas").hide();
    $("#head-creditoycobranza").hide();
    $("#head-recursoshumanos").hide();
    $("#head-administracion").hide();
    enviar();

    function enviar(){
        const parametros =
        {
          
        }
        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "../php/cargarModulos.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
      }
      function inicioEnvio(){
          console.log("Enviando petición para cargar Módulos...");
      }
    
      function llegada(datos){
        console.log(datos);
        let contador = Object.keys(datos.modulos).length;
        let contenido = "";

        for(let i = 0; i < contador; i++){
            //Mostrar el modulo principal de cada departamento
            $(`#head-${datos.departamento[i]}`).show();
            //Agrupar los sub-modulos a los modulos principales
            contenido = `<button class="dropdown-item" type="button" id="${datos.identificador[i]}">${datos.modulos[i]}</button>`;
            //Agregando contenido dinámico
            $(`#${datos.departamento[i]}`).append(contenido);
        }
      }
    
      function problemas(textError, textStatus) {
            //var error = JSON.parse(textError);
            alert("Problemas en el Servlet: " + JSON.stringify(textError));
            alert("Problemas en el servlet: " + JSON.stringify(textStatus));
      }
});