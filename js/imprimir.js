$(document).ready(function(){
    $("#imprimir").click(function(){

        $("#imprimir").hide();
        $("#visualizacion").hide();
        $("#cierreSesion").hide();
        $("#nuevoPedido").hide();
        $("#pdf").hide();
        $("#cancela").hide();
        $("#cancelar").hide();
        $("#sae").hide();

        window.print();

        $("#imprimir").show();
        $("#visualizacion").show();
        $("#cierreSesion").show();
        $("#nuevoPedido").show();
        if($("#notaSAE").val()==""){
            $("#sae").show();
        }
        else{
            $("#sae").hide();
        }
        if($("#notaEstatus").val()!="Cancelada"){
            $("#cancelar").show();
        }
        else{
            $("#cancelar").hide();
        }
    });
});