export const construirGrafica = (textosDeLaGrafica, puntosDeLaGraficaA, puntosDeLaGraficaB) => {
    Highcharts.chart('container', {
        chart: {
          type: 'spline'
        },
        title: {
          text: textosDeLaGrafica.titulo
        },
        subtitle: {
          text: textosDeLaGrafica.subtitulo
        },
        xAxis: {
          type: 'datetime',
          dateTimeLabelFormats: { // don't display the dummy year
            day: '%d',
            month: '%m',
            year: '%Y'
          },
          title: {
            text: textosDeLaGrafica.tituloEjeX
          }
        },
        yAxis: {
          title: {
            text: textosDeLaGrafica.tituloEjeY
          },
          min: 0
        },
        tooltip: {
          headerFormat: `<b>${textosDeLaGrafica.tituloInformativoA}:</b> {series.name}<br>`,
          pointFormat: `<b>${textosDeLaGrafica.tituloInformativoB}:</b> {point.x:%d/%m/%Y}
                         <br />
                         <b>${textosDeLaGrafica.tituloInformativoC}:</b> \${point.y:.2f}`
        },
      
        plotOptions: {
          series: {
            marker: {
              enabled: true
            }
          }
        },
      
        colors: ['#6CF', '#39F', '#06C', '#036', '#000'],
      
        // Define the data points. All series have a dummy year
        // of 1970/71 in order to be compared on the same x axis. Note
        // that in JavaScript, months start at 0 for January, 1 for February etc.
        series: [{
          name: textosDeLaGrafica.nombreDeLaGraficaA,
          data: puntosDeLaGraficaA
        }],
      
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              plotOptions: {
                series: {
                  marker: {
                    radius: 2.5
                  }
                }
              }
            }
          }]
        }
    });
}