<?php
    require_once("../../funciones.php");
    $folio = $_POST["folio"];
    $fechaInicio = $_POST["fechaInicio"];
    $fechaFin = $_POST["fechaFin"];
    $estatus = "";
    $estatus1 = "Emitida";
    $estatus2 = "Original";
    $entrada = "";
    $historial = 1;
    $prefijoAzcapo = "FD%";
    $prefijoTecamac = "FTC%";
    $contador = 0;
    $folios = array();
    $cliente = array();
    $nombreCliente = array();
    $fecha = array();
    $importe = array();
    $vendedor = array();
    $descuento = array();
    $observacion = array();
    $fechaEntrada = array();
    $usuario = array();
    $clase = array();
    $datos = array();

    //Si las fechas vienen vacias setearlas a un valor por defecto
    ($fechaInicio=="") ? $fechaInicio = '19/10/2020' : false;

    ($fechaFin=="") ? $fechaFin = fechaStandar(fecha()) : false;

    $base = conexion_local();

    switch(true){
        case ($folio!=""):
            $consultaHistorialFacturas = "SELECT A.CLAVE, A.CLIENTE, LEFT(A.NOMBRE, 15) AS NOMBRE, 
                                            A.FECHA, A.IMPORTE, A.VENDEDOR, A.DESCUENTO, 
                                            B.Observacion, B.Fecha AS FechaEntrada, C.Nombre, C.Apellido 
                                            FROM CARGAS AS A LEFT JOIN HISTORIAL_FACTURA_REMISION 
                                            AS B ON A.idFacturaRemision=B.idFacturaRemision 
                                            LEFT JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                            WHERE (A.ESTATUS=? or A.ESTATUS=?) AND (A.ENTRADA=? OR A.Historial=?) 
                                            AND A.CLAVE=? AND A.FECHA BETWEEN ? AND ? ORDER BY A.idFacturaRemision DESC";
            $resultadoHistorialFacturas = $base->prepare($consultaHistorialFacturas);
            $resultadoHistorialFacturas->execute(array($estatus1, $estatus2, $entrada, $historial, 
                                                         $folio, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
            break;

        case ($folio==""):
            $consultaHistorialFacturas = "SELECT A.CLAVE, A.CLIENTE, LEFT(A.NOMBRE, 15) AS NOMBRE, 
                                            A.FECHA, A.IMPORTE, A.VENDEDOR, A.DESCUENTO, 
                                            B.Observacion, B.Fecha AS FechaEntrada, C.Nombre, C.Apellido 
                                            FROM CARGAS AS A LEFT JOIN HISTORIAL_FACTURA_REMISION 
                                            AS B ON A.idFacturaRemision=B.idFacturaRemision 
                                            LEFT JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                            WHERE (A.ESTATUS=? or A.ESTATUS=?) AND (A.ENTRADA=? OR A.Historial=?) 
                                            AND (A.CLAVE LIKE ? OR A.CLAVE LIKE ?) 
                                            AND A.FECHA BETWEEN ? AND ? ORDER BY A.idFacturaRemision DESC";
            $resultadoHistorialFacturas = $base->prepare($consultaHistorialFacturas);
            $resultadoHistorialFacturas->execute(array($estatus1, $estatus2, $entrada, $historial, $prefijoAzcapo, 
                                                         $prefijoTecamac, fechaConsulta($fechaInicio), fechaConsulta($fechaFin)));
            break;
        
        default:
            break;
    }

    if($resultadoHistorialFacturas->rowCount()>0){
        while($registroHistorialFacturas = $resultadoHistorialFacturas->fetch(PDO::FETCH_ASSOC)){
            $folios[$contador] = $registroHistorialFacturas["CLAVE"];
            $cliente[$contador] = $registroHistorialFacturas["CLIENTE"];
            $nombreCliente[$contador] = $registroHistorialFacturas["NOMBRE"];
            $fecha[$contador] = fechaStandar($registroHistorialFacturas["FECHA"]);
            $importe[$contador] = $registroHistorialFacturas["IMPORTE"];
            $vendedor[$contador] = $registroHistorialFacturas["VENDEDOR"];
            $descuento[$contador] = $registroHistorialFacturas["DESCUENTO"];
            $observacion[$contador] = $registroHistorialFacturas["Observacion"];
            ($registroHistorialFacturas["FechaEntrada"]!=null) 
                ? $fechaEntrada[$contador] = fechaStandar($registroHistorialFacturas["FechaEntrada"])
                : $fechaEntrada[$contador] = $registroHistorialFacturas["FechaEntrada"];
            ($registroHistorialFacturas["Nombre"]!=null) 
                ? $usuario[$contador] = $registroHistorialFacturas["Nombre"] . " " . $registroHistorialFacturas["Apellido"]
                : $usuario[$contador] = null;
            ($registroHistorialFacturas["Observacion"]!="" && 
                $registroHistorialFacturas["FechaEntrada"]!="" && 
                $registroHistorialFacturas["Nombre"]!="" && 
                $registroHistorialFacturas["Apellido"]) 
                ? $clase[$contador] = "correcto"
                : $clase[$contador] = "revisando";

            $contador++;
        }
        $resultadoHistorialFacturas->closeCursor();
        $estatus = "Correcto";
        $datos["folios"] = $folios;
        $datos["cliente"] = $cliente;
        $datos["nombreCliente"] = $nombreCliente;
        $datos["fecha"] = $fecha;
        $datos["importe"] = $importe;
        $datos["vendedor"] = $vendedor;
        $datos["descuento"] = $descuento;
        $datos["observacion"] = $observacion;
        $datos["fechaEntrada"] = $fechaEntrada;
        $datos["usuario"] = $usuario;
        $datos["clase"] = $clase;

    }
    else{
        $estatus = "Sin resultados";
    }

    $base = null;

    $datos["estatus"] = $estatus;

    echo json_encode($datos);

?>