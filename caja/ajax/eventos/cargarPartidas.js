$(document).ready(function(){

    //Obteniendo el folio de la cobranza
    var url = window.location.search;
    url = url.split("?");
    url = url[1];
    url = url.split("=");
    folio = url[1];
    enviar(folio);

    function enviar(folio){
        
        var parametros =
        {
            folio: folio,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/cargarPartidas.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 6000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
      }
      function inicioEnvio(){
          console.log("Cargando Partidas...");
      }
    
      function llegada(datos){
        console.log(datos);
        switch (datos.estatus){
            case "Correcto":
                $("#fechaDeCobranza").text(datos.fechaDeCobranza+" "+datos.folio);
                $("#valorDelTipo").text(datos.valorDelTipo);
                let contador = Object.keys(datos.clave).length;
                let contenido = "";
                for (let i = 0; i < contador; i++){

                    contenido += '<tr>'+
                                    '<td>'+datos.clave[i]+'</td>'+
                                    '<td>'+datos.cliente[i]+'</td>'+
                                    '<td>'+formatNumber.new(datos.importe[i], "$")+'</td>'+
                                    '<td>'+datos.metodo[i]+'</td>'+
                                    '<td>'+datos.observaciones[i]+'</td>'+
                                '</tr>';
                    
                }
                $("#table").append(contenido);
                $("#total").text(formatNumber.new(datos.total, "$"));
                break;
        
            case "Sin resultados":
                alert("No se econtraron resultados de la cobranza "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
        }
        
      }
    
      function problemas(textError, textStatus) {
            //var error = JSON.parse(textError);
            alert("Problemas en el Servlet: " + JSON.stringify(textError));
            alert("Problemas en el servlet: " + JSON.stringify(textStatus));
      }
});