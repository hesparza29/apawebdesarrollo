$(document).ready(function(){

    $("#guardarCobranza").on('click', function(){
        let clave = [];
        let metodo = [];
        let observaciones = [];
        let notaDeCredito = [];
        let contador = $("#contador").val();
        let estatus = true;
        for (let i = 1; i <= contador; i++) {
            //Verificar que se haya capturado la fecha de cobranza
            if($("#fecha").val()==""){
                alert("Captura una fecha para la cobranza, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un lugar de cobranza
            else if($("#lugar").val()==""){
                alert("Captura un lugar para la cobranza, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que se haya capturado un tipo de cobranza
            else if($("#tipo").val()==""){
                alert("Captura un tipo para la cobranza, verificalo por favor");
                estatus = false;
                break;
            }
            //Verificar que la clave de la factura/remision sea diferente de undefined y además sea diferente de vacio
            else if($("#clave-"+i+"").val()!=undefined && $("#clave-"+i+"").val()!=""){
                //Verificar que la factura/remision no se encuentre capturada más de una vez
                if(clave.includes($("#clave-"+i+"").val())==true){
                    alert("La factura/remisión "+$("#clave-"+i+"").val()+" se capturo más de una vez, verificalo por favor");
                    estatus = false;
                    break;
                }
                //Verificar que el metodo ingresado para cada factura/remisión sea diferente de vacio
                else if($("#metodo-"+i+"").val()==""){
                    alert("Debe de ingresar un método para la factura/remisión "+$("#clave-"+i+"").val()+" , verificalo por favor");
                    estatus = false;
                    break;
                }
                //Guardar los datos validos en arreglos
                else{
                    clave.push($("#clave-"+i+"").val());
                    metodo.push($("#metodo-"+i+"").val());
                    observaciones.push($("#observaciones-"+i+"").val());
                    notaDeCredito.push($("#notaDeCredito-"+i+"").val());
                }
            }
            
        }

        //Verificar que se haya capturado por lo menos una factura/remision y su respectivo metodo
        if(clave.length==0 && metodo.length==0){
            alert("Captura al menos una factura/remisión, por favor");
            estatus = false;
        }

        if(estatus){
            enviar(clave, metodo, observaciones, notaDeCredito);
        }
    });

    function enviar(clave, metodo, observaciones, notaDeCredito) {
        var parametros = {
          fecha: $("#fecha").val(),
          lugar: $("#lugar").val(),
          tipo: $("#tipo").val(),
          clave: clave,
          metodo: metodo,
          observaciones: observaciones,
          notaDeCredito: notaDeCredito,
        };
        
        $.ajax({
          async: true, //Activar la transferencia asincronica
          type: "POST", //El tipo de transaccion para los datos
          dataType: "json", //Especificaremos que datos vamos a enviar
          contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
          url: "ajax/guardarCobranza.php", //Sera el archivo que va a procesar la petición AJAX
          data: parametros, //Datos que le vamos a enviar
          // data: "total="+total+"&penalizacion="+penalizacion,
          beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
          success: llegada, //Función que se ejecuta en caso de tener exito
          timeout: 4000,
          error: problemas, //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando el guardado de la cobranza...");
    }
    
    function llegada(datos){
        console.log(datos);

        switch (datos.estatus){
            case "Correcto":
                alert("Se pudo capturar de manera exitosa la cobranza "+datos.nuevoFolio);
                //Desarrollo
                var url = window.location.host+"/apawebdesarrollo/caja/impresion.php?folio="+datos.nuevoFolio;
                //Produccion
                //var url = "apawebdesarrollo.com/caja/impresion.php?folio="+datos.nuevoFolio;
                window.open('http://'+url+'', '_blank');
                setTimeout("location.href='visualizacion.php'", 500);
                break;
            case "Fecha no disponible":
                alert("La fecha ya fue seleccionada en el reporte de cobranza "+ datos.folio +", selecciona otra fecha por favor");
                break;

            case "No corresponde":
                alert("La factura/remisión "+ datos.clave +" no pertenece a "+datos.lugar+", verificalo por favor");
                break;
        }
        
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});