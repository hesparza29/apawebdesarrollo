$(document).ready(function(){
    $("#buscar").click(function(){
        if($("#clave").val()!="" || $("#fechaInicio").val()!="" || $("#fechaFin").val()!=""){

                let fechaInicio = $("#fechaInicio").val();
                    fechaInicio = fechaInicio.split("/");
                    fechaInicio = new Date(fechaInicio[2], fechaInicio[1] - 1, fechaInicio[0]);
                let fechaFin = $("#fechaFin").val();
                    fechaFin = fechaFin.split("/");
                    fechaFin = new Date(fechaFin[2], fechaFin[1] - 1, fechaFin[0]);
                let fecha = new Date('2020', '0', '01');

                switch(true){
                    case ($("#fechaInicio").val()!=="" && $("#fechaFin").val()!==""):
                        if(fecha>fechaInicio){
                            alert("La fecha inicio debe de ser del 01/01/2020 en adelante, verificala por favor");
                        }
                        else if(fechaInicio>fechaFin){
                            alert("La fecha inicio debe de ser menor a la fecha fin, verificalas por favor");
                        }
                        else{
                            enviar();
                        }
                        break;

                    case ($("#fechaInicio").val()!=="" && $("#fechaFin").val()===""):
                        if(fecha>fechaInicio){
                            alert("La fecha inicio debe de ser del 01/01/2020 en adelante, verificala por favor");
                        }
                        else{
                            enviar();
                        }
                        break;

                    case ($("#fechaInicio").val()==="" && $("#fechaFin").val()!==""):
                        if(fecha>fechaFin){
                            alert("La fecha fin debe de ser del 01/01/2020 en adelante, verificala por favor");
                        }
                        else{
                            enviar();
                        }   
                        break;
                
                    default:
                        enviar();
                        break;
                }
        }
        else{
            alert("Captura al menos un campo, por favor");
        }
    });

    function enviar(){

        let parametros = {
            folio: $("#clave").val(),
            fechaInicio: $("#fechaInicio").val(), 
            fechaFin: $("#fechaFin").val(),
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/filtroHistorialFacturas.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 4000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Cargando Filtro Historial Facturas...");
    }
    
    function llegada(datos){
        console.log(datos);
        switch(datos.estatus){
            case 'Correcto':
                let contador = Object.keys(datos.folios).length;
                let contenido = "";
                
                for (let i = 0; i < contador; i++){
                    if(datos.observacion[i]!==null && datos.fechaEntrada[i]!==null && datos.usuario[i]!==null){
                        contenido += `<tr class="${datos.clase[i]}">
                                        <td id="folio-${i}">${datos.folios[i]}</td>
                                        <td>${datos.cliente[i]}</td>
                                        <td>${datos.nombreCliente[i]}</td>
                                        <td>${datos.fecha[i]}</td>
                                        <td>${formatNumber.new(datos.importe[i], "$")}</td>
                                        <td>${datos.vendedor[i]}</td>
                                        <td>${datos.descuento[i]}%</td>
                                        <input type="hidden" id="observacion-${i}" value="${datos.observacion[i]}"> 
                                        <input type="hidden" id="fechaEntrada-${i}" value="${datos.fechaEntrada[i]}">
                                        <input type="hidden" id="usuario-${i}" value="${datos.usuario[i]}">
                                        <td>
                                            <input type="button" class="btn btn-info btn-sm visualizar" id="visualizar-${i}" value="Ver"/>
                                        </td>
                                    </tr>`;
                    }
                    else{
                        contenido += `<tr class="${datos.clase[i]}">
                                        <td id="folio-${i}">${datos.folios[i]}</td>
                                        <td>${datos.cliente[i]}</td>
                                        <td>${datos.nombreCliente[i]}</td>
                                        <td>${datos.fecha[i]}</td>
                                        <td>${formatNumber.new(datos.importe[i], "$")}</td>
                                        <td>${datos.vendedor[i]}</td>
                                        <td>${datos.descuento[i]}%</td>
                                        <td>
                                            <input type="button" class="btn btn-success btn-sm revisar" id="revisar-${i}" value="+"/>
                                        </td>
                                    </tr>`;
                    }
                }
                $("#paginador").hide();
                $("#table").empty();
                $("#table").append(contenido);
                break;
            case 'Sin resultados':
              alert("No se encontraron datos en la consulta");
              break;
          
            default:
              break;
        }
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});