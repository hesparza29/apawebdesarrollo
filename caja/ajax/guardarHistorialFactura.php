<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $observacion = $_POST["observacion"];
    $estatus = "";
    $datos = array();

    $base = conexion_local();
    //Consulta actualizar entrada a caja manual
    $consultaActualizarEntradaCaja = "UPDATE CARGAS SET Historial=?, ENTRADA=? WHERE CLAVE=?";
    $resultadoActualizarEntradaCaja = $base->prepare($consultaActualizarEntradaCaja);
    //Consulta para darle entrada manual a la factura
    $consultaInsertarEntradaCaja = "INSERT INTO HISTORIAL_FACTURA_REMISION 
                                        VALUES(?,?,CURDATE(), 
                                        (SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=?), 
                                        (SELECT idUsuario FROM USUARIO WHERE Usuario=?))";
    $resultadoInsertarEntradaCaja = $base->prepare($consultaInsertarEntradaCaja);
    //Consulta para verificar que la factura no está cancelada
    $consultaEstatus = "SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=? AND ESTATUS!=?";
    $resultadoEstatus = $base->prepare($consultaEstatus);
    //Consulta para verificar que la factura no tenga entrada a caja, tanto en cobranza como entrada manual
    $consulaEntradaCaja = "SELECT idFacturaRemision FROM CARGAS WHERE ENTRADA!=? AND CLAVE=?";
    $resultadoEntradaCaja = $base->prepare($consulaEntradaCaja);
    $resultadoEntradaCaja->execute(array("", $folio));

    switch ($resultadoEntradaCaja->rowCount()){
        case 0:
            $resultadoEstatus->execute(array($folio, "Cancelada"));

            if($resultadoEstatus->rowCount()==1){
                $resultadoInsertarEntradaCaja->execute(array(NULL, $observacion, $folio, $usuario));

                if($resultadoInsertarEntradaCaja->rowCount()==1){
                    $resultadoActualizarEntradaCaja->execute(array(1, "MANUAL", $folio));
                    ($resultadoActualizarEntradaCaja->rowCount()==1) ? $estatus = "Correcto" : $estatus = "Error";
                    $resultadoActualizarEntradaCaja->closeCursor();
                }
                else{
                    $estatus = "Error";
                }

                $resultadoInsertarEntradaCaja->closeCursor();
            }
            else{
                $estatus = "Ya cancelada";
            }
            $resultadoEstatus->closeCursor();
            break;

        case 1:
            $estatus = "Ya capturada";
            break;
        
        default:
            # code...
            break;
    }
    $resultadoEntradaCaja->closeCursor();

    $base = null;

    $datos["usuario"] = $usuario;
    $datos["folio"] = $folio;
    $datos["observacion"] = $observacion;
    $datos["estatus"] = $estatus;

    echo json_encode($datos);
?>