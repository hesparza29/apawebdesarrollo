<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $modulo = $_POST["modulo"];
    $accion = 1;
    $boton = array();
    $identificador = array();
    $clase = array();
    $contador = 0;

    $datos = array();

    $base = conexion_local();
    //Obtener las acciones a las que tendra acceso el usuario
    $consultaAcciones = "SELECT Boton, Identificador, Clase FROM MODULO
                        INNER JOIN USUARIO_MODULO ON MODULO.idModulo=USUARIO_MODULO.idModulo
                        INNER JOIN USUARIO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
                        WHERE Usuario=? AND MODULO.Nombre=? AND Accion=?";
    $resultadoAcciones = $base->prepare($consultaAcciones);
    $resultadoAcciones->execute(array($usuario, $modulo, $accion));
    if($resultadoAcciones->rowCount()>0){
        while ($registroAcciones = $resultadoAcciones->fetch(PDO::FETCH_ASSOC)){
            $boton[$contador] = $registroAcciones["Boton"];
            $identificador[$contador] = $registroAcciones["Identificador"];
            $clase[$contador] = $registroAcciones["Clase"];
            $contador++;
        }
    }
    $resultadoAcciones->closeCursor();
    $base = null;

    $datos["boton"] = $boton;
    $datos["identificador"] = $identificador;
    $datos["clase"] = $clase;

    echo json_encode($datos);
?>