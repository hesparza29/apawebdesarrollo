//URL para llamar la peticion por ajax
let url_listar_usuario = "facturasConHistorial.php";

$( document ).ready(function(){
   // se genera el paginador
   paginador = $(".pagination");
	// cantidad de items por pagina
	let items = 10, numeros = 4;
	// inicia el paginador
	init_paginator(paginador,items,numeros);
	// se envia la peticion ajax que se realizara como callback
	set_callback(get_data_callback);
	cargaPagina(0);
});

function get_data_callback(){
	$.ajax({
		data:{
		limit: itemsPorPagina,
		offset: desde,
		},
		type:"POST",
		url:url_listar_usuario
	}).done(function(data,textStatus,jqXHR){
		// obtiene la clave lista del json data
		let lista = data.lista;
		$("#table").html("");

		// si es necesario actualiza la cantidad de paginas del paginador
		if(pagina==0){
			creaPaginador(data.cantidad);
		}
		// genera el cuerpo de la tabla
		$.each(lista, function(ind, elem){

			if(elem.observacion!==null && elem.fechaEntrada!==null && elem.usuario!==null){
				$(
					`<tr class="${elem.clase}">
						<td id="folio-${ind}">${elem.folio}</td>
						<td>${elem.cliente}</td>
						<td>${elem.nombreCliente}</td>
						<td>${elem.fecha}</td>
						<td>${formatNumber.new(elem.importe, "$")}</td>
						<td>${elem.vendedor}</td>
						<td>${elem.descuento}%</td>
						<input type="hidden" id="observacion-${ind}" value="${elem.observacion}"> 
						<input type="hidden" id="fechaEntrada-${ind}" value="${elem.fechaEntrada}">
						<input type="hidden" id="usuario-${ind}" value="${elem.usuario}">
						<td><input type="button" class="btn btn-info btn-sm visualizar" id="visualizar-${ind}" value="Ver"/></td>
					</tr>`
				).appendTo($("#table"));
			}
			else{
				$(
					`<tr class="${elem.clase}">
						<td id="folio-${ind}">${elem.folio}</td>
						<td>${elem.cliente}</td>
						<td>${elem.nombreCliente}</td>
						<td>${elem.fecha}</td>
						<td>${formatNumber.new(elem.importe, "$")}</td>
						<td>${elem.vendedor}</td>
						<td>${elem.descuento}%</td>
						<td><input type="button" class="btn btn-success btn-sm revisar" id="revisar-${ind}" value="+"/></td>
					</tr>`
				).appendTo($("#table"));
			}
		});

	}).fail(function(jqXHR,textStatus,textError){
		alert(textError);
	});

}