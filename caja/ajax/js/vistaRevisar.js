$(document).ready(function(){
    $('#table').on('click', '.revisar', function(){

        //Obteniendo el id de la solicitud
        let id = $(this).attr("id");
            id = id.split("-");
            id = id[1];
        //Obteniendo el folio de la solicitud
        let folio = $("#folio-"+id+"").text();
        //Asignando el folio a el formulario
        $("#folio").val(folio);


        $( "#revisionVista" ).dialog({
            height: 350,
            width: 410,
            dialogClass: "no-close",
            buttons: [
              {
                class: "btn btn-danger",
                text: "Cancelar",
                click: function() {
                  $( this ).dialog( "close" );
                }
              },
              {
                class: "btn btn-success",
                text: "Enviar",
                click: function(){
                    ($("#observacion").val()!=="" && $("#entradaCaja").prop('checked')) 
                    ? enviar() 
                    : alert("Selecciona el checkbox y agrega una observación, por favor");
                }
              }
            ]
        });
    });

    function enviar() {

        let formData = new FormData(document.getElementById("formularioHistorialFacturas"));

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: false, //Especificaremos el tipo de contenido
            url: "ajax/guardarHistorialFactura.php", //Sera el archivo que va a procesar la petición AJAX
            data: formData,
            cache: false,
            processData: false,
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 10000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio() {
        console.log("Cargando entrada a caja manual...");
    }

    function llegada(datos) {
        console.log(datos);
        switch (datos.estatus) {
            case "Correcto":
                alert(`Se pudo dar entrada a caja de forma manual a la factura ${datos.folio}`);
                location.reload();
                break;

            case "Ya cancelada":
                alert(`La factura ${datos.folio} se encuentra cancelada por lo tanto no se le puede dar entrada a caja de forma manual`);
                break;
            
            case "Ya capturada":
                alert(`La factura ${datos.folio} ya se encuentra con entrada a caja`);
                break;

            case "Error":
                alert(`Error, no se pudo dar entrada a caja de forma manual a la factura ${datos.folio}`);
                break;
        
            default:
                break;
        }
    }

    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});
