$(document).ready(function(){
    
    $("#factura_remision").on('click', '.eliminarFacturaRemision', function(){
        //Obtener el id del producto que se va a eliminar
        let id = $(this).attr("id");
            id = id.split("-");
            id = id[1];
        //Eliminar el producto
        $("#fila-" + id + "").remove();
        //Realizar la suma del total de la cobranza
        totalCobranza();
    });
});