$(document).ready(function(){

    $("#fecha").change(function(){
        if($("#fecha").val()!=""){
            let fecha = ($("#fecha").val()).split("/");
            let mounths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", 
                            "October", "November", "December"];
            let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", 
                            "Octubre", "Noviembre", "Diciembre"];
            let mes = mounths[parseInt(fecha[1])-1];
            let dia = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
            let dt = new Date(mes+' '+fecha[0]+', '+fecha[2]+' 12:00:00');
            $("#fechaDeCobranza").text("Reporte de Cobranza "+dia[dt.getUTCDay()]+" "+fecha[0]+" de "+
                                        meses[parseInt(fecha[1])-1]+" "+fecha[2]);
        }
        else{
            $("#fechaDeCobranza").text("");
        }
    });

});