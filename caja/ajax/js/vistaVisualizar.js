$(document).ready(function(){
    $('#table').on('click', '.visualizar', function(){

        //Obteniendo el id de la solicitud
        let id = $(this).attr("id");
            id = id.split("-");
            id = id[1];
        //Asignando el folio a el formulario
        $("#folioVisualizacion").val($("#folio-"+id+"").text());
        //Asignando fecha de entrada a el formulario
        $("#fechaEntradaVisualizacion").val($("#fechaEntrada-"+id+"").val());
        //Asignando observacion a el formulario
        $("#observacionVisualizacion").val($("#observacion-"+id+"").val());
        //Asignando usuario a el formulario
        $("#usuarioVisualizacion").val($("#usuario-"+id+"").val());


        $( "#revisionVisualizacion" ).dialog({
            height: 350,
            width: 410,
            dialogClass: "no-close",
            buttons: [
              {
                class: "btn btn-danger",
                text: "Cerrar",
                click: function() {
                  $( this ).dialog( "close" );
                }
              },
            ]
        });
    });
});
