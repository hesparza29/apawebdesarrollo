<?php
    session_start();
    $usuario = $_SESSION["user"];
    require_once("../../funciones.php");

    $fecha = fechaConsulta($_POST["fecha"]);
    $lugar = $_POST["lugar"];
    $tipo = $_POST["tipo"];
    $clave = $_POST["clave"];
    $metodo = $_POST["metodo"];
    $observaciones = $_POST["observaciones"];
    $notaDeCredito = $_POST["notaDeCredito"];
    $marcadores = str_repeat('?,', count($clave) - 1) . '?';
    $modulo = "Cobranza";
    $estatus = "Correcto";
    $permiso = "";
    $folio = "";
    $contador = count($clave);
    $flagEfectivo = 0;
    $datos = array();

    $base = conexion_local();

    //Consulta para saber si la fecha se encuentra disponible
    $consultaCobranza = "SELECT Folio FROM COBRANZA WHERE Fecha=? AND Tipo=? AND Lugar=?";
    $resultadoCobranza = $base->prepare($consultaCobranza);
    $resultadoCobranza->execute(array($fecha, $tipo, $lugar));

    switch ($resultadoCobranza->rowCount()) {
        case 0:
            //Consulta para obtener el permiso del usuario en la cobranza
            $consultaPermiso = "SELECT Identificador FROM USUARIO 
                                INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
                                INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
                                WHERE Usuario=? AND MODULO.Nombre=?";
            $resultadoPermiso = $base->prepare($consultaPermiso);
            $resultadoPermiso->execute(array($usuario, $modulo));
            $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
            $resultadoPermiso->closeCursor();
            $permiso = $registroPermiso["Identificador"];
            //Sí el usuario es un administrador verificar que las facturas/remisiones pertenezcan al lugar
            if($permiso=="administrador"){
                for ($i=0; $i < $contador; $i++){ 
                    //Verificar que se capture dicha Factura este capturada en el lugar que le corresponde
                    if(($tipo=="facturas") && (($lugar=="azcapotzalco" && ($clave[$i][0] . $clave[$i][1]!="FD")) || 
                            ($lugar=="tecamac" && ($clave[$i][0] . $clave[$i][1] . $clave[$i][2]!="FTC")))){
                        $estatus = "No corresponde";
                        $datos["clave"] = $clave[$i];  
                        break;   
                    }//Verificar que se capture dicha Factura este capturada en el lugar que le corresponde
                    else if(($tipo=="remisiones") && (($lugar=="azcapotzalco" && ($clave[$i][0] . $clave[$i][1]!="RR")) || 
                            ($lugar=="tecamac" && ($clave[$i][0] . $clave[$i][1] . $clave[$i][2]!="RTC")))){
                        $estatus = "No corresponde";  
                        $datos["clave"] = $clave[$i];
                        break;    
                    }
                } 
            }
            //Si el estatus es correcto comenzar a capturar la nueva cobranza
            if($estatus=="Correcto"){
                //Obtener el folio de la última cobranza
                $consultaFolio = "SELECT Folio FROM COBRANZA WHERE Lugar=? AND Tipo=? ORDER BY idCobranza DESC LIMIT 1";
                $resultadoFolio = $base->prepare($consultaFolio);
                $resultadoFolio->execute(array($lugar, $tipo));
                //Construir el folio consecutivo
                switch ($lugar." ".$tipo){
                    case 'azcapotzalco facturas':
                        if($resultadoFolio->rowCount()==0){
                            $folio = "FPB1";
                        }
                        else{
                            $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
                            $registroFolio = explode("B", $registroFolio["Folio"]);
                            $folio = "FPB" . (intval($registroFolio[1])+1);
                        }
                        break;
                    case 'azcapotzalco remisiones':
                        if($resultadoFolio->rowCount()==0){
                            $folio = "RPB1";
                        }
                        else{
                            $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
                            $registroFolio = explode("B", $registroFolio["Folio"]);
                            $folio = "RPB" . (intval($registroFolio[1])+1);
                        }
                        break;
                    case 'tecamac facturas':
                        if($resultadoFolio->rowCount()==0){
                            $folio = "FTC1";
                        }
                        else{
                            $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
                            $registroFolio = explode("C", $registroFolio["Folio"]);
                            $folio = "FTC" . (intval($registroFolio[1])+1);
                        }
                        break;
                    case 'tecamac remisiones':
                        if($resultadoFolio->rowCount()==0){
                            $folio = "RTC1";
                        }
                        else{
                            $registroFolio = $resultadoFolio->fetch(PDO::FETCH_ASSOC);
                            $registroFolio = explode("C", $registroFolio["Folio"]);
                            $folio = "RTC" . (intval($registroFolio[1])+1);
                        }
                        break;
                }
                $resultadoFolio->closeCursor();
                $datos["nuevoFolio"] = $folio;
                //Consulta para actualizar la entrada de las facturas/remisiones
                $consultaActualizarEntrada = "UPDATE CARGAS SET METODO=?, OBSERVACIONES=?, importeNotaDeCredito=?, ENTRADA=?, 
                                FECHA_ENTRADA=?, NUMERO_ENTRADA=? WHERE CLAVE=?";
                $resultadoActualizarEntrada = $base->prepare($consultaActualizarEntrada);
                //Obtener el importe de la cobranza
                $consultaTotal = "SELECT SUM(IMPORTE) AS Total FROM CARGAS WHERE CLAVE IN ($marcadores)";
                $resultadoTotal = $base->prepare($consultaTotal);
                $resultadoTotal->execute($clave);
                $registroTotal = $resultadoTotal->fetch(PDO::FETCH_ASSOC);
                $total = $registroTotal["Total"];
                $resultadoTotal->closeCursor();
                //Capturar la nueva cobranza
                $consultaInsertarCobranza = "INSERT INTO COBRANZA VALUES(?,?,?,?,?,?,
                                            (SELECT idUsuario FROM USUARIO WHERE Usuario=?))";
                $resultadoInsertarCobranza = $base->prepare($consultaInsertarCobranza);
                $resultadoInsertarCobranza->execute(array(NULL, $folio, $fecha, $tipo, $lugar, $total, $usuario));
                $datos["consulta"] = $resultadoInsertarCobranza->rowCount();
                $resultadoInsertarCobranza->closeCursor();
                //Si se inserto la cobranza de manera correcta se 
                //prosigue a actualizar la entrada de las facturas 
                //y a insertar el cobro del día correspondiente
                if($datos["consulta"]==1){
                    //Actualizando la entrada a caja de las facturas/remisiones
                    for($i=0; $i < $contador ; $i++){
                        $resultadoActualizarEntrada->execute(array($metodo[$i], $observaciones[$i], $notaDeCredito[$i], $folio, 
                                                    $fecha, (intval($i)+1), $clave[$i]));
                        ($metodo[$i]=="Efectivo") ? $flagEfectivo++ : false;
                    }
                    //Consulta para insertar el cobro del día
                    //Se deben cumplir 2 condiciones para poder insertarlo
                    //La primera es que la cobranza debe de ser del tipo facturas
                    //La segunda que haya al menos 1 factura que se pago en efectivo($flagEfectivo>0)
                    switch (true){
                        case ($tipo=="facturas" && $flagEfectivo>0):
                            $consultaInsertarCobro = "INSERT INTO COBRO VALUES(?,?,
                                                (SELECT (SELECT SUM(IMPORTE) FROM CARGAS WHERE METODO=? AND ENTRADA=?)-
                                                    (SELECT SUM(importeNotaDeCredito) FROM CARGAS WHERE METODO=? AND ENTRADA=?) 
                                                    AS Total),?,?,
                                                (SELECT idCobranza FROM COBRANZA WHERE Folio=?),
                                                (SELECT idUsuario FROM USUARIO WHERE Usuario=?),?)";
                            $resultadoInsertarCobro = $base->prepare($consultaInsertarCobro);
                            $resultadoInsertarCobro->execute(array(NULL, "Pendiente", "Efectivo", $folio, 
                                                                    "Efectivo", $folio, 0.00, "", $folio, $usuario, ""));
                            $resultadoInsertarCobro->closeCursor();
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                    
                }

                $resultadoActualizarEntrada->closeCursor();
            }
            break;

        case 1:
            $registroCobranza = $resultadoCobranza->fetch(PDO::FETCH_ASSOC);
            $datos["folio"] = $registroCobranza["Folio"];
            $estatus = "Fecha no disponible";
            break;
        
        default:
            # code...
            break;
    }

    $resultadoCobranza->closeCursor();

    
    $datos["fecha"] = $fecha;
    if ($lugar=="azcapotzalco") {
        $datos["lugar"] = "Pozo Brasil";
    }
    else if($lugar=="tecamac"){
        $datos["lugar"] = "Tecámac";
    }
    $datos["estatus"] = $estatus;
    

    $base = null;
    echo json_encode($datos);
?>