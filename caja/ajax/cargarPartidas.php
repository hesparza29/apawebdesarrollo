<?php
    require_once("../../funciones.php");
    session_start();
    $usuario = $_SESSION["user"];
    $folio = $_POST["folio"];
    $clave = array();
    $cliente = array();
    $importe = array();
    $metodo = array();
    $observaciones = array();
    $contador = 0;
    $estatus = "Correcto";
    $modulo = "Cobranza";
    $permiso = "";
    
    
    $datos = array();

    $base = conexion_local();
    //Obtener las cobranzas que puede ver el usuario
    $consultaPermiso = "SELECT Identificador FROM USUARIO 
    INNER JOIN USUARIO_MODULO ON USUARIO.idUsuario=USUARIO_MODULO.idUsuario
    INNER JOIN MODULO ON USUARIO_MODULO.idModulo=MODULO.idModulo
    WHERE Usuario=? AND MODULO.Nombre=?";
    $resultadoPermiso = $base->prepare($consultaPermiso);
    $resultadoPermiso->execute(array($usuario, $modulo));
    $registroPermiso = $resultadoPermiso->fetch(PDO::FETCH_ASSOC);
    $resultadoPermiso->closeCursor();
    $permiso = $registroPermiso["Identificador"];

    //Obtener la información de la cobranza
    switch ($permiso){
        case 'administrador':
            $consultaCobranza = "SELECT Fecha, Tipo, Total FROM COBRANZA WHERE Folio=?";
            $resultadoCobranza = $base->prepare($consultaCobranza);
            $resultadoCobranza->execute(array($folio));
            break;
        
        default:
            $consultaCobranza = "SELECT Fecha, Tipo, Total FROM COBRANZA WHERE Folio=? AND Lugar=?";
            $resultadoCobranza = $base->prepare($consultaCobranza);
            $resultadoCobranza->execute(array($folio, $permiso));
            break;
    }
    //Verificar si existe la cobranza con el folio solicitado
    switch ($resultadoCobranza->rowCount()) {
        case 1:
            $registroCobranza = $resultadoCobranza->fetch(PDO::FETCH_ASSOC);
            $datos["fecha"] = fechaStandar($registroCobranza["Fecha"]);
            $datos["tipo"] = strtoupper($registroCobranza["Tipo"]);
            $datos["total"] = $registroCobranza["Total"];
            $datos["fechaDeCobranza"] = saber_dia($datos["fecha"]);
            switch ($datos["tipo"]){
                case 'FACTURAS':
                    $datos["valorDelTipo"] = "FACTURA";
                    break;
                    
                case 'REMISIONES':
                    $datos["valorDelTipo"] = "REMISIÓN";
                    break;
            }
            //Obtener todas las facturas/remisiones que se capturaron en la cobranza
            $consultaPartidas = "SELECT CLAVE, CLIENTE, NOMBRE, IMPORTE, METODO, OBSERVACIONES 
                                FROM CARGAS WHERE ENTRADA=? ORDER BY NUMERO_ENTRADA ASC";
            $resultadoPartidas = $base->prepare($consultaPartidas);
            $resultadoPartidas->execute(array($folio));
            while ($registroPartidas = $resultadoPartidas->fetch(PDO::FETCH_ASSOC)){
                $clave[$contador] = $registroPartidas["CLAVE"];
                $cliente[$contador] = $registroPartidas["CLIENTE"] . " " . $registroPartidas["NOMBRE"];
                $importe[$contador] = $registroPartidas["IMPORTE"];
                $metodo[$contador] = $registroPartidas["METODO"];
                $observaciones[$contador] = $registroPartidas["OBSERVACIONES"];
                $contador++;
            }
            $resultadoPartidas->closeCursor();
            $datos["clave"] = $clave;
            $datos["cliente"] = $cliente;
            $datos["importe"] = $importe;
            $datos["metodo"] = $metodo;
            $datos["observaciones"] = $observaciones;
            break;
        
        case 0:
            $estatus = "Sin resultados";
            break;
    }
    
    $resultadoCobranza->closeCursor();
    $base = null;

    $datos["folio"] = $folio;
    $datos["estatus"] = $estatus;
    $datos["permiso"] = $permiso;



    echo json_encode($datos);
?>