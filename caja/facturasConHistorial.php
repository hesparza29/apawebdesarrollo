<?php
    require_once("../funciones.php");
    // obtiene los valores para realizar la paginacion
    $limit = isset($_POST["limit"]) && intval($_POST["limit"]) > 0 ? intval($_POST["limit"])	: 20;
    $offset = isset($_POST["offset"]) && intval($_POST["offset"])>=0	? intval($_POST["offset"])	: 0;
    $estatus1 = "Emitida";
    $estatus2 = "Original";
    $entrada = "";
    $historial = 1;
    $prefijoAzcapo = "FD%";
    $prefijoTecamac = "FTC%";
    $fecha = "2020-01-01";


    // realiza la conexion
    $base = conexion_local();
    $con = new mysqli("50.62.209.84","hesparza","b29194303","aplicacion");
    //$con = new mysqli("localhost","root","","aplicacion");
    $con->set_charset("utf8");
    //$base = new PDO('mysql:host=localhost; dbname=aplicacion', 'root', '');
    //$base = new PDO("mysql:host=50.62.209.117;dbname=aplicacion","hesparza","b29194303");
    //$base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    //$base->exec("SET CHARACTER SET utf8");

    // array para devolver la informacion
    $json = array();
    $data = array();
    //consulta que deseamos realizar a la db
    $query = $con->prepare("SELECT A.CLAVE, A.CLIENTE, LEFT(A.NOMBRE, 15), A.FECHA, A.IMPORTE, A.VENDEDOR, A.DESCUENTO, 
                                B.Observacion, B.Fecha, C.Nombre, C.Apellido 
                                FROM CARGAS AS A LEFT JOIN HISTORIAL_FACTURA_REMISION AS B ON A.idFacturaRemision=B.idFacturaRemision 
                                LEFT JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                                WHERE (A.ESTATUS=? or A.ESTATUS=?) AND (A.ENTRADA=? OR A.Historial=?) 
                                AND (A.CLAVE LIKE ? OR A.CLAVE LIKE ?)
                                AND A.FECHA BETWEEN ? AND CURDATE() ORDER BY A.idFacturaRemision DESC LIMIT ? OFFSET ?");
    $query->bind_param("sssssssii", $estatus1, $estatus2, $entrada, $historial, $prefijoAzcapo, 
                        $prefijoTecamac, $fecha, $limit,$offset);
    $query->execute();


    // vincular variables a la sentencia preparada
    $query->bind_result($folio, $cliente, $nombreCliente, $fecha, $importe, $vendedor, 
                        $descuento, $observacion, $fechaEntrada, $nombre, $apellido);

    // obtener valores
    while ($query->fetch()) {
        $data_json = array();

        $data_json["folio"] = $folio;
        $data_json["cliente"] = $cliente;
        $data_json["nombreCliente"] = $nombreCliente;
        $data_json["fecha"] = fechaStandar($fecha);
        $data_json["importe"] = $importe;
        $data_json["vendedor"] = $vendedor;
        $data_json["descuento"] = $descuento;
        $data_json["observacion"] = $observacion;
        ($fechaEntrada!=null) ? $data_json["fechaEntrada"] = fechaStandar($fechaEntrada): $data_json["fechaEntrada"] = $fechaEntrada;
        $data_json["usuario"] = $nombre . " " . $apellido;
        ($observacion!="" && $fechaEntrada!="" && $nombre!="" && $apellido!="") 
        ? $data_json["clase"] = "correcto"
        : $data_json["clase"] = "revisando" ;
        
        $data[] = $data_json;
    }

   

    // obtiene la cantidad de registros
    $consultaRegistros = "SELECT COUNT(*) as total from CARGAS AS A LEFT JOIN HISTORIAL_FACTURA_REMISION AS B 
                            ON A.idFacturaRemision=B.idFacturaRemision LEFT JOIN USUARIO AS C ON B.idUsuario=C.idUsuario 
                            WHERE (A.ESTATUS=? OR A.ESTATUS=?) 
                            AND (A.ENTRADA=? OR A.Historial=?) 
                            AND (A.CLAVE LIKE ? OR A.CLAVE LIKE ?)
                            AND A.FECHA BETWEEN ? AND CURDATE()";
    $resultadoRegistros = $base->prepare($consultaRegistros);
    $resultadoRegistros->execute(array('Emitida', 'Original', '', '1', 'FD%', 'FTC%', '2020-01-01'));
    $registros = $resultadoRegistros->fetch(PDO::FETCH_ASSOC);
    $cantidad['cantidad'] = $registros["total"];
    $resultadoRegistros->closeCursor();

    $json["lista"] = array_values($data);
    $json["cantidad"] = array_values($cantidad);

    $base = null;
    // envia la respuesta en formato json
    header("Content-type:application/json; charset = utf-8");
    echo json_encode($json);
    exit();
?>