<!DOCTYPE html>
<html>
<head>
	<title>Visualización Historial de Facturas</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/estiloCobranza.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
	<script type="text/javascript" src="../js/paginator.min.js"></script>
	<script type="text/javascript" src="../js/home.js"></script>
	<script type="text/javascript" src="../js/recargarPagina.js"></script>
	<script type="text/javascript" src="../js/formatoNumero.js"></script>
	<script type="text/javascript" src="../js/fechaDatepicker.js"></script>
	<script type="text/javascript" src="../js/verImpresion.js"></script>
	<script type="text/javascript" src="../js/visualizacion.js"></script>
	<script type="text/javascript" src="ajax/js/mainFacturasConHistorial.js"></script>
	<script type="text/javascript" src="ajax/js/vistaRevisar.js"></script>
	<script type="text/javascript" src="ajax/js/vistaVisualizar.js"></script>
	<script type="text/javascript" src="ajax/eventos/filtroHistorialFacturas.js"></script>
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Visualización Historial de Facturas</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
                    <input type="button" class="btn btn-success" id="visualizacion" value="Regresar" />
				</div>
				<div class="col-sm-12 col-md-8">
						<h3 class="text-left">Filtro de Búsqueda</h3>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="container">
				<div class="row form-group">
					<div class="col-sm-12 col-md-4">
						<input type="text" id="clave" class="clave form-control" placeholder="Folio"/>
					</div>
					<div class="col-sm-12 col-md-4">
						<input type="text" id="fechaInicio" class="fecha form-control" placeholder="Fecha Inicio"/>
					</div>
					<div class="col-sm-12 col-md-4">
						<input type="text" id="fechaFin" class="fecha form-control" placeholder="Fecha Fin"/>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-12 col-md-4">
						
					</div>
					<div class="col-sm-12 col-md-4" id="botonesBusqueda">
						<input type="button" class="btn btn-primary" id="buscar" value="Buscar" />
						<input type="button" class="btn btn-primary" id="recargar" value='Tabla Completa' />
					</div>
					<div class="col-sm-12 col-md-4">
						
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-12 col-md-12 text-center">
						<ul class="pagination" id="paginador"></ul>
					</div>
				</div>		
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead class="thead-dark">
									<th>Clave</th>
									<th>Cliente</th>
									<th>Nombre</th>
									<th>Fecha</th>
									<th>Importe</th>
									<th>Vendedor</th>
									<th>Descuento</th>
									<th colspan="2">Info</th>
								</thead>
								<tbody id="table">

								</tbody>
							</table>
						</div>
					</div>
				</div>			
		</div>
	</section>
	<!--Ventanas de jquery UI para cargar la entrada de forma manual a la factura-->
	<div id="revisionVista" title="Historial Factura" hidden>
		<form enctype="multipart/form-data" id="formularioHistorialFacturas" method="post">
			<div class="form-row" id="vistaAsignarFactura">
				<div class="form-group col-sm-12 col-md-12">
					<label for="entradaCaja">Darle Entrada a Caja</label>
					<input type="checkbox" class="form-control" id="entradaCaja" name="entradaCaja" />
				</div>
				<div class="form-group col-sm-12 col-md-12">
					<label for="observacion">Observación</label>
					<textarea class="form-control" id="observacion" name="observacion" rows="3"></textarea>
				</div>
			</div>
			<input type="hidden" id="folio" name="folio" />
		</form>
	</div>	
	<!--Ventanas de jquery UI para mostrar la información de la factura entrada de forma manual-->
	<div id="revisionVisualizacion" title="Historial Factura" hidden>
		<form>
			<div class="form-row" id="vistaVisualizarFactura">
				<div class="form-group col-sm-12 col-md-12">
					<label for="folioVisualizacion">Factura</label>
					<input type="text" class="form-control" id="folioVisualizacion" 
						name="folioVisualizacion" value="" readonly/>
				</div>
				<div class="form-group col-sm-12 col-md-12">
					<label for="fechaEntradaVisualizacion">Fecha de Entrada</label>
					<input type="text" class="form-control" id="fechaEntradaVisualizacion" 
						name="fechaEntradaVisualizacion" value="" readonly/>
				</div>
				<div class="form-group col-sm-12 col-md-12">
					<label for="observacionVisualizacion">Observación</label>
					<textarea class="form-control" id="observacionVisualizacion" name="observacionVisualizacion" rows="3" readonly>

					</textarea>
				</div>
				<div class="form-group col-sm-12 col-md-12">
					<label for="usuarioVisualizacion">Usuario</label>
					<input type="text" class="form-control" id="usuarioVisualizacion" 
						name="usuarioVisualizacion" value="" readonly/>
				</div>
			</div>
		</form>
	</div>
</body>
</html>
