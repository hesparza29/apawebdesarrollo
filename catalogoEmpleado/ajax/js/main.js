//URL para llamar la peticion por ajax
let url_listar_usuario = "catalogoEmpleados.php";

$( document ).ready(function() {
   // se genera el paginador
   paginador = $(".pagination");
	// cantidad de items por pagina
	let items = 10, numeros =4;
	// inicia el paginador
	init_paginator(paginador,items,numeros);
	// se envia la peticion ajax que se realizara como callback
	set_callback(get_data_callback);
	cargaPagina(0);
});

function get_data_callback(){
	$.ajax({
		data:{
		limit: itemsPorPagina,
		offset: desde,
		},
		type:"POST",
		url:url_listar_usuario
	}).done(function(data,textStatus,jqXHR){
		// obtiene la clave lista del json data
		let lista = data.lista;
		$("#table").html("");

		// si es necesario actualiza la cantidad de paginas del paginador
		if(pagina==0){
			creaPaginador(data.cantidad);
		}
		// genera el cuerpo de la tabla
		$.each(lista, function(ind, elem){
			$(
				`<tr class="${elem.clase}">
					<td>${elem.numeroEmpleado}</td>
					<td>${elem.estatus}</td>
                    <td>${elem.rfc}</td>
                    <td>${elem.curp}</td>
                    <td>${elem.nombreCompleto}</td>
					<td>${elem.nss}</td>
					<td>${elem.puesto}</td>
					<td>${elem.departamento}</td>
					<td>${elem.fechaAlta}</td>
					<td>
						<input type="button" class="btn btn-info btn-sm verImpresion" id="catalogoEmpleado-${elem.idEmpleado}" value="Ver" />
					</td>
				</tr>`
			).appendTo($("#table"));
		});
		
	}).fail(function(jqXHR,textStatus,textError){
		alert(textError);
	});

}