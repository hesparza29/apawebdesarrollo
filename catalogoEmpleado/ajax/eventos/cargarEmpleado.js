$(document).ready(function(){

    //Obteniendo el id del empleado
    let url = window.location.search;
        url = url.split("?");
        url = url[1];
        url = url.split("=");
    let idEmpleado = url[1];
    enviar(idEmpleado);

    function enviar(idEmpleado){
        
        const parametros ={
            idEmpleado: idEmpleado,
        }

        $.ajax({
            async: true, //Activar la transferencia asincronica
            type: "POST", //El tipo de transaccion para los datos
            dataType: "json", //Especificaremos que datos vamos a enviar
            contentType: "application/x-www-form-urlencoded", //Especificaremos el tipo de contenido
            url: "ajax/cargarEmpleado.php", //Sera el archivo que va a procesar la petición AJAX
            data: parametros, //Datos que le vamos a enviar
            // data: "total="+total+"&penalizacion="+penalizacion,
            beforeSend: inicioEnvio, //Es la función que se ejecuta antes de empezar la transacción
            success: llegada, //Función que se ejecuta en caso de tener exito
            timeout: 6000,
            error: problemas //Función que se ejecuta si se tiene problemas al superar el timeout
        });
        return false;
    }
    function inicioEnvio(){
        console.log("Cargando Empleado...");
    }
    
    function llegada(datos){
        console.log(datos);
        switch (datos.estatus){
            case "Correcto":
                $("#numeroEmpleado").val(datos.numeroEmpleado);
                $("#estatus").val(datos.estatusEmpleado);
                $("#rfc").val(datos.rfc);
                $("#curp").val(datos.curp);
                $("#apellidoPaterno").val(datos.apellidoPaterno);
                $("#apellidoMaterno").val(datos.apellidoMaterno);
                $("#nombres").val(datos.nombres);
                $("#nss").val(datos.nss);
                $("#salarioDiario").val(datos.salarioDiario);
                $("#salarioSemanal").val(datos.salarioSemanal);
                $("#despensa").val(datos.despensa);
                $("#premioPuntualidad").val(datos.premioPuntualidad);
                $("#premioAsistencia").val(datos.premioAsistencia);
                $("#salarioTotal").val(datos.salarioTotal);
                $("#puesto").val(datos.puesto);
                $("#departamento").val(datos.departamento);
                $("#fechaIngreso").val(datos.fechaIngreso);
                $("#fechaModificacionSalario").val(datos.fechaModificacionSalario);
                break;
        
            case "Sin resultados":
                alert("No se econtraron resultados de la cobranza "+datos.folio);
                setTimeout("location.href='visualizacion.php'", 500);
                break;
        }
    }
    
    function problemas(textError, textStatus) {
        //var error = JSON.parse(textError);
        alert("Problemas en el Servlet: " + JSON.stringify(textError));
        alert("Problemas en el servlet: " + JSON.stringify(textStatus));
    }
});