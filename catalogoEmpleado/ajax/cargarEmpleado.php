<?php
    require_once("../../funciones.php");
    $idEmpleado = $_POST["idEmpleado"];
    $estatus = "";
    $datos = array();

    $base = conexion_local();
    
    //Consulta para obtener la información del empleado
    $consultaEmpleado = "SELECT A.NumeroEmpleado, A.Estatus, A.Rfc, A.Curp, A.ApellidoPaterno, 
                            A.ApellidoMaterno, A.Nombres, A.Nss, A.SalarioDiario, A.SalarioSemanal, 
                            A.Despensa, A.PremioPuntualidad, A.PremioAsistencia, A.SalarioTotal, 
                            B.Nombre AS Puesto, C.Nombre AS Departamento, A.FechaAlta, A.FechaModificacionSalario 
                            FROM EMPLEADO AS A  INNER JOIN PUESTO AS B ON A.idPuesto=B.idPuesto 
                            INNER JOIN DEPARTAMENTO AS C ON B.idDepartamento=C.idDepartamento 
                            WHERE A.idEmpleado=?";
    $resultadoEmpleado = $base->prepare($consultaEmpleado);
    $resultadoEmpleado->execute(array($idEmpleado));
    //Verificar si existe el empleado con id solicitado
    switch($resultadoEmpleado->rowCount()){
        case 1:
            $registroEmpleado = $resultadoEmpleado->fetch(PDO::FETCH_ASSOC);
            $estatus = "Correcto";
            $datos["numeroEmpleado"] = $registroEmpleado["NumeroEmpleado"];
            $datos["estatusEmpleado"] = $registroEmpleado["Estatus"];
            $datos["rfc"] = $registroEmpleado["Rfc"];
            $datos["curp"] = $registroEmpleado["Curp"];
            $datos["apellidoPaterno"] = $registroEmpleado["ApellidoPaterno"];
            $datos["apellidoMaterno"] = $registroEmpleado["ApellidoMaterno"];
            $datos["nombres"] = $registroEmpleado["Nombres"];
            $datos["nss"] = $registroEmpleado["Nss"];
            $datos["salarioDiario"] = $registroEmpleado["SalarioDiario"];
            $datos["salarioSemanal"] = $registroEmpleado["SalarioSemanal"];
            $datos["despensa"] = $registroEmpleado["Despensa"];
            $datos["premioPuntualidad"] = $registroEmpleado["PremioPuntualidad"];
            $datos["premioAsistencia"] = $registroEmpleado["PremioAsistencia"];
            $datos["salarioTotal"] = $registroEmpleado["SalarioTotal"];
            $datos["puesto"] = $registroEmpleado["Puesto"];
            $datos["departamento"] = $registroEmpleado["Departamento"];
            $datos["fechaIngreso"] = fechaStandar($registroEmpleado["FechaAlta"]);
            $datos["fechaModificacionSalario"] = fechaStandar($registroEmpleado["FechaModificacionSalario"]);
            break;
        
        case 0:
            $estatus = "Sin resultados";
            $datos["folio"] = $folio;
            break;
    }
    
    $resultadoEmpleado->closeCursor();
    
    $base = null;

    $datos["estatus"] = $estatus;

    echo json_encode($datos);
?>