<!DOCTYPE html>
<html>
    <head>
        <title>Información Empleado</title>
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
        <link href="../css/bootstrap_v3-3-5.min.css" rel="stylesheet">
        <link href="../css/estiloVisualizacion.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/verificarSesion.js"></script>
        <script type="text/javascript" src="../js/cierreSesion.js"></script>
        <script type="text/javascript" src="../js/cierreInactividad.js"></script>
        <script type="text/javascript" src="../js/formatoNumero.js"></script>
        <script type="text/javascript" src="../js/visualizacion.js"></script>
        <script type="text/javascript" src="../js/imprimir.js"></script>
        <script type="text/javascript" src="ajax/eventos/cargarEmpleado.js"></script>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-2">
                        <img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
                    </div>
                    <div class="col-sm-12 col-md-6">
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <input class="btn btn-info" type="button" id="imprimir" value="Imprimir" />
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-12 col-md-2">
                        <input type="button" class="btn btn-success" id="visualizacion" value="Regresar" />
                    </div>
                    <div class="col-sm-12 col-md-10">
                        <h4 class="text-left" id="cobranzaDelDia"></h4>
                    </div>
                </div>
            </div>
        </header>
        <br /><br />
        <section>
            <div class="container">		
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-table-striped">
                                <thead class="thead-dark">
                                    <tr>
                                        <th class="text-center" colspan="12">INFORMACIÓN EMPLEADO</th>
                                    </tr>
                                </thead>
                                <tbody id="table">
                                    <tr>
                                        <td colspan="2">Número Empleado</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="numeroEmpleado" value="" readonly>
                                        </td>
                                        <td colspan="2">Estatus</td>
                                        <td colspan="4">
                                            <select name="estatus" id="estatus" class="form-control opcionesEstatus" disabled>
                                                <option value="ALTA">Alta</option>
                                                <option value="BAJA">Baja</option>
                                                <option value="VOLANTE">Volante</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">RFC</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="rfc" value="" readonly>
                                        </td>
                                        <td colspan="2">CURP</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="curp" value="" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Apellido Paterno</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="apellidoPaterno" value="" readonly>
                                        </td>
                                        <td colspan="2">Apellido Materno</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="apellidoMaterno" value="" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Nombres</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="nombres" value="" readonly>
                                        </td>
                                        <td colspan="2">NSS</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="nss" value="" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Salario Diario</td>
                                        <td colspan="2">
                                            <input type="text" class="form-control" id="salarioDiario" value="" readonly>
                                        </td>
                                        <td colspan="2">Salario Semanal</td>
                                        <td colspan="2">
                                            <input type="text" class="form-control" id="salarioSemanal" value="" readonly>
                                        </td>
                                        <td colspan="2">Despensa</td>
                                        <td colspan="2">
                                            <input type="text" class="form-control" id="despensa" value="" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Premio Puntualidad</td>
                                        <td colspan="2">
                                            <input type="text" class="form-control" id="premioPuntualidad" value="" readonly>
                                        </td>
                                        <td colspan="2">Premio Asistencia</td>
                                        <td colspan="2">
                                            <input type="text" class="form-control" id="premioAsistencia" value="" readonly>
                                        </td>
                                        <td colspan="2">Salario Total</td>
                                        <td colspan="2">
                                            <input type="text" class="form-control" id="salarioTotal" value="" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Puesto</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="puesto" value="" readonly>
                                        </td>
                                        <td colspan="2">Departamento</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="departamento" value="" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Fecha de Ingreso</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="fechaIngreso" value="" readonly>
                                        </td>
                                        <td colspan="2">Fecha de Modificación Salario</td>
                                        <td colspan="4">
                                            <input type="text" class="form-control" id="fechaModificacionSalario" value="" readonly>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="text-center" colspan="12">
                                            <input type="button" class="btn btn-warning" id="guardar" value="Guardar" disabled>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>			
            </div>
        </section>	
    </body>
</html>