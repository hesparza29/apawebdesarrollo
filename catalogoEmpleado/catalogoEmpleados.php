<?php
    require_once("../funciones.php");
    // obtiene los valores para realizar la paginacion
    $limit = isset($_POST["limit"]) && intval($_POST["limit"]) > 0 ? intval($_POST["limit"])	: 20;
    $offset = isset($_POST["offset"]) && intval($_POST["offset"])>=0	? intval($_POST["offset"])	: 0;
    
    $base = conexion_local();
    // realiza la conexion
    //$con = new mysqli("50.62.209.84","hesparza","b29194303","aplicacion");
    $con = new mysqli("localhost","root","","aplicacion");
    $con->set_charset("utf8");
    //$base = new PDO('mysql:host=localhost; dbname=aplicacion', 'root', '');
    //$base = new PDO("mysql:host=50.62.209.117;dbname=aplicacion","hesparza","b29194303");
    //$base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    //$base->exec("SET CHARACTER SET utf8");
    
    // array para devolver la informacion
    $json = array();
    $data = array();
    //consulta que deseamos realizar a la db
    //$query = $con->prepare("select id_usuario,nombres,apellidos from  usuarios limit ? offset ?");
    
    //El limite empieza con 10 y el Offset con 0
    
    $query = $con->prepare("SELECT A.idEmpleado, A.NumeroEmpleado, A.Estatus, A.Rfc, A.Curp, A.NombreCompleto, 
                                A.Nss, B.Nombre, C.Nombre, A.FechaAlta FROM EMPLEADO AS A  
                                INNER JOIN PUESTO AS B ON A.idPuesto=B.idPuesto 
                                INNER JOIN DEPARTAMENTO AS C ON B.idDepartamento=C.idDepartamento 
                                ORDER BY FechaAlta DESC LIMIT ? OFFSET ?");
    $query->bind_param("ii",$limit,$offset);
    $query->execute();
    
    // vincular variables a la sentencia preparada
    //$query->bind_result($id_usuario, $nombres,$apellidos);
    $query->bind_result($idEmpleado, $numeroEmpleado, $estatus, $rfc, $curp, 
                            $nombreCompleto, $nss, $puesto, $departamento, $fechaAlta);
    
    // obtener valores
    while ($query->fetch()) {
        $data_json = array();
    
        $data_json["idEmpleado"] = $idEmpleado;
        $data_json["numeroEmpleado"] = $numeroEmpleado;
        $data_json["estatus"] = $estatus;
        $data_json["rfc"] = $rfc;
        $data_json["curp"] = $curp;
        $data_json["nombreCompleto"] = $nombreCompleto;
        $data_json["nss"] = $nss;
        $data_json["puesto"] = $puesto;
        $data_json["departamento"] = $departamento;
        $data_json["fechaAlta"] = fechaStandar($fechaAlta);
        switch($estatus){
            case 'ALTA':
                $data_json["clase"] = "correcta";
                break;
            case 'BAJA':
                    $data_json["clase"] = "cancelada";
                    break;
            case 'VOLANTE':
                    $data_json["clase"] = "pendiente";
                    break;
        }
        
        $data[]=$data_json;
    }
    
    $base = null;
    
    // obtiene la cantidad de registros
    $cantidad_consulta = $con->query("select count(*) as total FROM EMPLEADO");
    $row = $cantidad_consulta->fetch_assoc();
    $cantidad['cantidad']=$row['total'];
    
    
    $json["lista"] = array_values($data);
    $json["cantidad"] = array_values($cantidad);
    
    // envia la respuesta en formato json
    header("Content-type:application/json; charset = utf-8");
    echo json_encode($json);
    exit();
?>