<!DOCTYPE html>
<html>
<head>
	<title>Visualización Políticas</title>
	<link rel="shortcut icon" href="../imagenes/favicon.ico" type="image/x-icon" />
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="css/estiloPoliticas.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../js/verificarSesion.js"></script>
	<script type="text/javascript" src="../js/cierreSesion.js"></script>
	<script type="text/javascript" src="../js/cierreInactividad.js"></script>
    <script type="text/javascript" src="../js/home.js"></script>
    <script type="text/javascript" src="../js/popper.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.bundle.min.js"></script>
    
</head>
<body>
	<header>
		<div class="container" id="cabecera">
			<div class="row">
				<div class="col-sm-12 col-md-2">
					<img src="../imagenes/apa.jpg" class="rounded mx-auto d-block" alt="APA">
				</div>
				<div class="col-sm-12 col-md-8">
					<h1 class="text-center">Visualización de Políticas</h1>
				</div>
				<div class="col-sm-12 col-md-2">
					<input class="btn btn-danger" type='button' id="cierreSesion" value='Cierra Sesión' />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<input type='button' id="home" class="btn btn-primary" style='background:url("../imagenes/home3.jpg"); width: 50px; height: 50px;' />
				</div>
				<div class="col-sm-12 col-md-8">

				</div>
			</div>
		</div>
    </header>
    <br />
	<section>
		<div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="dropdown">
                        <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ventas
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button"><a href="https://docs.google.com/document/d/1A9u5EcMTuufxjVNG8viUEL5qiCq9WCk61clSCBdI8hc/edit?usp=sharing" target="_blank">Políticas Generales</a></button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
					<div class="dropdown">
                        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Crédito y Cobranza
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">En proceso...</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
					<div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Recursos Humanos
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">En proceso...</button>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</section>
</body>
</html>